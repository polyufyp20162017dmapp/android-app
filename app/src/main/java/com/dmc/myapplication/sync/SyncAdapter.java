package com.dmc.myapplication.sync;

import android.accounts.Account;
import android.content.*;
import android.net.http.RequestQueue;
import android.os.Bundle;
import android.util.Log;
import android.util.Xml;
import com.dmc.myapplication.Models.*;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.net.ssl.*;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.GeneralSecurityException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 11/12/2016.
 */
public class SyncAdapter extends AbstractThreadedSyncAdapter {
    ContentResolver contentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);

        contentResolver = context.getContentResolver();
    }

    public SyncAdapter(Context context, boolean autoInitialize, boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);

        contentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(Account account, Bundle extras, String authority, ContentProviderClient provider, SyncResult syncResult) {

        // this method is on background thread
        // perform network tasks here
        // I am just inserting data in database manually

        //ContentValues contentValues = new ContentValues();
        //contentValues.put(bodyRecord.StudentTable.STUDENT_NAME , "Data Synced");

        //contentResolver.insert(bodyRecord.bodyRecord_CONTENT_URI , contentValues);
        //contentResolver.notifyChange(bodyRecord.bodyRecord_CONTENT_URI , null);


        try {
            System.out.println("System Update Sync Working!");
            checkLatestVersion();


            System.out.println("Sync Working!");

            List<String> serverRequestList = getServerRequestDataTOSendForBodyrecord();
            if (serverRequestList!= null && serverRequestList.size() >0){
                sendRequestedDataBodyRecord(serverRequestList);
            }

            serverRequestList =getServerRequestDataTOSendForExercise();
            if (serverRequestList!= null && serverRequestList.size() >0) {
                sendRequestedDataExercise(serverRequestList);
            }

            serverRequestList = getServerRequestDataTOSendForFood();
            if (serverRequestList!= null && serverRequestList.size() >0) {
                sendRequestedDataFood(serverRequestList);
            }

            serverRequestList = getServerRequestDataTOSendForStep();
            if (serverRequestList!= null && serverRequestList.size() >0) {
                sendRequestedDataStepRecord(serverRequestList);
            }

            serverRequestList = getServerRequestDataTOSendFordrug();
            if (serverRequestList!= null && serverRequestList.size() >0) {
                sendRequestedDataDrugRecord(serverRequestList);
            }

            serverRequestList = getServerRequestDataTOSendForinjection();
            if (serverRequestList!= null && serverRequestList.size() >0) {
                sendRequestedDataInjectionRecord(serverRequestList);
            }

            uploadImages();
        }catch (Exception e){
            System.out.println(e);
        }




    }

    private void uploadImages(){
        imageSyncStack imageSyncStackDB = new imageSyncStack(getContext());
        List<imageSyncStack.imageSyncStack_class> imagesStack = imageSyncStackDB.getAll();
        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        Integer userid = userLocalStore.getLoggedInUser().userid;
        Integer x = 0;

        Log.d("imagesStack.size", String.valueOf(imagesStack.size()));
        while (x <= imagesStack.size() - 1){
            Log.d("imagesStack.x", String.valueOf(x));
            //List<HashMap<String, String>> BRList = new ArrayList<>();
            //HashMap<String, String> dataObj = new HashMap<>();
            //dataObj.put("filename", imagesStack.get(x).getFilename());
            //dataObj.put("image", URLEncoder.encode(imagesStack.get(x).getImageString(getContext().getFilesDir())));
            //BRList.add(dataObj);
            //String BRListString = new Gson().toJson(BRList);
            String data = "userId="+userid+"&event=uploadImage&method=&data=&filename="+URLEncoder.encode(imagesStack.get(x).getFilename())+"&image="+URLEncoder.encode(imagesStack.get(x).getImageContent());
            String responce = webHelper.sendRequest("POST", data);
            Gson gson = new Gson();
            List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>(){}.getType());
            if (newData.get(0).get("result").equals("ok")){
                imageSyncStackDB.delete(imagesStack.get(x).get_ID());
                x++;
            }else{
                System.out.println("Failed : "+ newData.get(0).get("msg"));
            }
        }

    }

    /**
     *
     *
     *
     * @return Server Request BodyRecords List of record create dateTime
     */
    private List<String> getServerRequestDataTOSendForBodyrecord(){

            //Prepare data
            bodyRecord bodyRecordDB = new bodyRecord(getContext());
            List<HashMap<String, String>> datetimeList = bodyRecordDB.getAllCreatedatetimeEditdatetime();
            if (datetimeList.size() <= 0){
                bodyRecordDB.close();
                return null;
            }
            String datetimeListJson = new Gson().toJson(datetimeList);
            datetimeListJson = URLEncoder.encode(datetimeListJson);
            //System.out.println(datetimeListJson);

            UserLocalStore userLocalStore  = new UserLocalStore(getContext());
            User user = userLocalStore.getLoggedInUser();

            // post请求的参数
            String data = "userId="+user.userid+"&event=bodyrecord&method=compareData&data="+datetimeListJson;

            String result = webHelper.sendRequest("POST", data);
            Gson gson = new Gson();

            return gson.fromJson(result, new TypeToken<List<String>>(){}.getType());
    }

    private List<String> getServerRequestDataTOSendForStep(){
            //Prepare data
            stepRecord bodyRecordDB = new stepRecord(getContext());
            List<HashMap<String, String>> datetimeList = bodyRecordDB.getAllCreatedatetimeEditdatetime();
            if (datetimeList.size() <= 0){
                bodyRecordDB.close();
                return null;
            }
            String datetimeListJson = new Gson().toJson(datetimeList);
            datetimeListJson = URLEncoder.encode(datetimeListJson);
            //System.out.println(datetimeListJson);

            UserLocalStore userLocalStore  = new UserLocalStore(getContext());
            User user = userLocalStore.getLoggedInUser();

            // post请求的参数
            String data = "userId="+user.userid+"&event=step&method=compareData&data="+datetimeListJson;

            String result = webHelper.sendRequest("POST", data);
            Gson gson = new Gson();
            return gson.fromJson(result, new TypeToken<List<String>>(){}.getType());
    }

    private List<String> getServerRequestDataTOSendFordrug(){
        //Prepare data
        MEDICATION_RECORD bodyRecordDB = new MEDICATION_RECORD(getContext());
        List<HashMap<String, String>> datetimeList = bodyRecordDB.getAllCreatedatetimeEditdatetime();
        if (datetimeList.size() <= 0){
            bodyRecordDB.close();
            return null;
        }
        String datetimeListJson = new Gson().toJson(datetimeList);
        datetimeListJson = URLEncoder.encode(datetimeListJson);
        //System.out.println(datetimeListJson);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        // post请求的参数
        String data = "userId="+user.userid+"&event=drug&method=compareData&data="+datetimeListJson;

        String result = webHelper.sendRequest("POST", data);
        Gson gson = new Gson();
        return gson.fromJson(result, new TypeToken<List<String>>(){}.getType());
    }

    private List<String> getServerRequestDataTOSendForinjection(){
        //Prepare data
        INJECTION_RECORD bodyRecordDB = new INJECTION_RECORD(getContext());
        List<HashMap<String, String>> datetimeList = bodyRecordDB.getAllCreatedatetimeEditdatetime();
        if (datetimeList.size() <= 0){
            bodyRecordDB.close();
            return null;
        }
        String datetimeListJson = new Gson().toJson(datetimeList);
        datetimeListJson = URLEncoder.encode(datetimeListJson);
        //System.out.println(datetimeListJson);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        // post请求的参数
        String data = "userId="+user.userid+"&event=injection&method=compareData&data="+datetimeListJson;

        String result = webHelper.sendRequest("POST", data);
        Gson gson = new Gson();
        return gson.fromJson(result, new TypeToken<List<String>>(){}.getType());
    }

    /**
     *
     * @return
     */
    private List<String> getServerRequestDataTOSendForExercise(){

            //Prepare data
            exercise_type_record exerciseRecordDB = new exercise_type_record(getContext());
            List<HashMap<String, String>> datetimeList = exerciseRecordDB.getAllCreatedatetimeEditdatetime();
            if (datetimeList.size() <= 0){
                return null;
            }
            String datetimeListJson = new Gson().toJson(datetimeList);
            datetimeListJson = URLEncoder.encode(datetimeListJson);
            //System.out.println(datetimeListJson);

            UserLocalStore userLocalStore  = new UserLocalStore(getContext());
            User user = userLocalStore.getLoggedInUser();

            // post请求的参数
            String data = "userId="+user.userid+"&event=exercise&method=compareData&data="+datetimeListJson;
            String result = webHelper.sendRequest("POST", data);
            Gson gson = new Gson();
            return gson.fromJson(result, new TypeToken<List<String>>(){}.getType());

    }

    private List<String> getServerRequestDataTOSendForFood(){

            //Prepare data
            foodRecord foodRecordDB = new foodRecord(getContext());
            List<HashMap<String, String>> datetimeList = foodRecordDB.getAllCreatedatetimeEditdatetime();
            if (datetimeList.size() <= 0){
                return null;
            }
            String datetimeListJson = new Gson().toJson(datetimeList);
            datetimeListJson = URLEncoder.encode(datetimeListJson);
            //System.out.println(datetimeListJson);

            UserLocalStore userLocalStore  = new UserLocalStore(getContext());
            User user = userLocalStore.getLoggedInUser();

            // post请求的参数
            String data = "userId="+user.userid+"&event=food&method=compareData&data="+datetimeListJson;
            String result = webHelper.sendRequest("POST", data);
            Gson gson = new Gson();
            return gson.fromJson(result, new TypeToken<List<String>>(){}.getType());
    }

    private void sendRequestedDataBodyRecord(List<String> requestList){
        bodyRecord bodyRecordDB = new bodyRecord(getContext());
        List<HashMap<String, String>> BRList = bodyRecordDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId="+user.userid+"&event=bodyrecord&method=updateRecords&data="+BRListString;
        String result = webHelper.sendRequest("POST", data);

    }

    private void sendRequestedDataStepRecord(List<String> requestList){
        stepRecord bodyRecordDB = new stepRecord(getContext());
        List<HashMap<String, String>> BRList = bodyRecordDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId="+user.userid+"&event=step&method=updateRecords&data="+BRListString;
        String result = webHelper.sendRequest("POST", data);

        bodyRecordDB.close();
    }

    private void sendRequestedDataDrugRecord(List<String> requestList){
        MEDICATION_RECORD bodyRecordDB = new MEDICATION_RECORD(getContext());
        List<HashMap<String, String>> BRList = bodyRecordDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId="+user.userid+"&event=drug&method=updateRecords&data="+BRListString;
        String result = webHelper.sendRequest("POST", data);

        bodyRecordDB.close();
    }

    private void sendRequestedDataInjectionRecord(List<String> requestList){
        INJECTION_RECORD bodyRecordDB = new INJECTION_RECORD(getContext());
        List<HashMap<String, String>> BRList = bodyRecordDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId="+user.userid+"&event=injection&method=updateRecords&data="+BRListString;
        String result = webHelper.sendRequest("POST", data);

        bodyRecordDB.close();
    }

    private void sendRequestedDataExercise(List<String> requestList){
        exercise_type_record exercDB = new exercise_type_record(getContext());
        List<HashMap<String, String>> BRList = exercDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId="+user.userid+"&event=exercise&method=updateRecords&data="+BRListString;
        String result = webHelper.sendRequest("POST", data);

    }

    private void sendRequestedDataFood(List<String> requestList){
        foodRecord exercDB = new foodRecord(getContext());
        List<HashMap<String, String>> BRList = exercDB.getRecordHashMapByCreateDatetime(requestList);
        String BRListString = new Gson().toJson(BRList);
        BRListString = URLEncoder.encode(BRListString);

        UserLocalStore userLocalStore  = new UserLocalStore(getContext());
        User user = userLocalStore.getLoggedInUser();

        //Send
        String data = "userId="+user.userid+"&event=food&method=updateRecords&data="+BRListString;
        String result = webHelper.sendRequest("POST", data);

    }


    private void checkLatestVersion(){
        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser().userid+"&event=syncdataversion&method=getLatestVersion&data=");
        if (responce != null){
            Gson gson = new Gson();
            HashMap<String, Integer> latestVersion = gson.fromJson(responce, new TypeToken<HashMap<String, Integer>>(){}.getType());
            sync_data_version sdvDB = new sync_data_version(getContext());
            System.out.println("Debug: latestVersion.size = "+latestVersion.size());
            for (int x = 0; x < latestVersion.size(); x ++){
                System.out.println("Debug: latestVersion.keySet().toArray()[x].toString() = "+latestVersion.keySet().toArray()[x].toString());
                System.out.println("Debug: sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION() = "+sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION());
                System.out.println("Debug: latestVersion.get(latestVersion.keySet().toArray()[x].toString()) = "+latestVersion.get(latestVersion.keySet().toArray()[x].toString()));

                if (sdvDB.get(latestVersion.keySet().toArray()[x].toString()).getVERSION() != latestVersion.get(latestVersion.keySet().toArray()[x].toString())){
                    System.out.println("Not match!!");

                    switch (latestVersion.keySet().toArray()[x].toString()){
                        case "EXERCISE_TYPE":
                            System.out.println("Run EXERCISE_TYPE!");
                            updateExerciseTypeTable("EXERCISE_TYPE", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        case "FOOD":
                            System.out.println("Run FOOD!");
                            updateFoodTable("FOOD", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        case "FOOD_CATEGORIES":
                            System.out.println("Run FOOD_CATEGORIES!");
                            updateFoodCateTypeTable("FOOD_CATEGORIES", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        case "FOOD_SUB_CATEGORIES":
                            System.out.println("Run FOOD_SUB_CATEGORIES!");
                            updateFoodSubCateTypeTable("FOOD_SUB_CATEGORIES", latestVersion.get(latestVersion.keySet().toArray()[x].toString()));
                            break;

                        default:
                            break;
                    }
                }
            }
        }
    }

    private void updateExerciseTypeTable(String TABLE, Integer VERSION){
        System.out.println("Start update ExerciseTypeTable to version "+VERSION);
        exercise_type etDB = new exercise_type(getContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser().userid+"&event=syncdataversion&method=getLatestExerciseType&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>(){}.getType());
        for (int x = 0; x < newData.size(); x++){
            etDB.insert(etDB.new exercise_type_class(Integer.parseInt(newData.get(x).get("EXERCISE_TYPE_ID")), newData.get(x).get("EXERCISE_TYPE_NAME")));
        }

        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateFoodCateTypeTable(String TABLE, Integer VERSION){
        System.out.println("Start update FoodCateTypeTable to version "+VERSION);
        foodCategories etDB = new foodCategories(getContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser()+"&event=syncdataversion&method=getLatestFoodCategories&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>(){}.getType());
        for (int x = 0; x < newData.size(); x++){
            etDB.insert(etDB.new foodCategories_class(Integer.parseInt(newData.get(x).get("FOOD_CATE_ID")), newData.get(x).get("FOOD_CATE_NAME")));
        }

        etDB.close();
        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateFoodSubCateTypeTable(String TABLE, Integer VERSION){
        System.out.println("Start update FoodSubCateTypeTable to version "+VERSION);
        foodSubCategories etDB = new foodSubCategories(getContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser()+"&event=syncdataversion&method=getLatestFoodSubCategories&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>(){}.getType());
        for (int x = 0; x < newData.size(); x++){
            etDB.insert(etDB.new foodSubCategories_class(Integer.parseInt(newData.get(x).get("FOOD_SUB_CATE_ID")), newData.get(x).get("FOOD_SUB_CATE_NAME"), Integer.parseInt(newData.get(x).get("FOOD_CATE_ID"))));
        }

        etDB.close();
        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateFoodTable(String TABLE, Integer VERSION){
        System.out.println("Start update FoodTable to version "+VERSION);
        food etDB = new food(getContext());
        etDB.deleteAll();

        UserLocalStore userLocalStore = new UserLocalStore(getContext());
        String responce = webHelper.sendRequest("POST", "userId="+userLocalStore.getLoggedInUser().userid+"&event=syncdataversion&method=getLatestFood&data=");
        Gson gson = new Gson();
        List<HashMap<String, String>> newData = gson.fromJson(responce, new TypeToken<List<HashMap<String, String>>>(){}.getType());
        for (int x = 0; x < newData.size(); x++){
            etDB.insert(etDB.new food_class(Integer.parseInt(newData.get(x).get("FOOD_ID")), newData.get(x).get("FOOD_NAME"), (newData.get(x).get("FOOD_UNIT")), Double.parseDouble(newData.get(x).get("FOOD_GRAM")), Double.parseDouble(newData.get(x).get("FOOD_CALORIE")), Double.parseDouble(newData.get(x).get("FOOD_CARBOHYDRATE")), Double.parseDouble(newData.get(x).get("FOOD_PROTEIN")), Double.parseDouble(newData.get(x).get("FOOD_FAT")), Double.parseDouble(newData.get(x).get("FOOD_SUGAR")), Double.parseDouble(newData.get(x).get("FOOD_FIBER")), Double.parseDouble(newData.get(x).get("FOOD_CHOLESTEROL")), Double.parseDouble(newData.get(x).get("FOOD_SODIUM")), Integer.parseInt(newData.get(x).get("FOOD_SUB_CATE_ID")), (newData.get(x).get("PHOTO_PATH"))));
        }

        etDB.close();
        //UpdateVersion
        updateLocalVersion(TABLE, VERSION);
    }

    private void updateLocalVersion(String TABLE, Integer VERSION){
        sync_data_version sdvDB = new sync_data_version(getContext());
        sdvDB.update(sdvDB.new sync_data_version_class(TABLE, VERSION));
        //sdvDB.close();
    }
}
