package com.dmc.myapplication.navFood;

import com.dmc.myapplication.systemConstant;

/**
 * Created by KwokSinMan on 16/1/2016.
 */
public class navFoodConstant {
    public final static String GET_FOOD_CATE_DDL = "navFood/getFoodCate.php";
    public final static String GET_FOOD_SUB_CATE_DDL = "navFood/getFoodSubCate.php";

    public final static String GET_FOOD_RECORD ="navFood/getFoodRecord.php";
    public final static String GET_ADD_FOOD_DETAIL = "navFood/getAddFoodDetail.php";
    public final static String GET_ADD_FREE_TEXT_FOOD_DETAIL = "navFood/getAddFreeTextFoodDetail.php";

    public final static String SAVE_FOOD_RECORDFROMDB = "navFood/saveFoodRecordFromDB.php";
    public final static String SAVE_FREE_TEXT_FOOD_RECORD = "navFood/saveFreeTextFoodRecord.php";

    public final static String GET_FOOD_COMMON_LIST = "navFood/getFoodCommonList.php";
    public final static String GET_FOOD_All_LIST = "navFood/getFoodAllList.php";
    public final static String GET_FOOD_SEARCH_RESULT = "navFood/getFoodSearchResult.php";

    public final static String GET_FOOD_DETAIL = "navFood/getFoodDetail.php";
    public final static String GET_FREE_TEXT_FOOD_DETAIL = "navFood/getFreeTextFoodDetail.php";

    public final static String SAVE_EDIT_FOOD_DETAIL = "navFood/saveEditFoodDetail.php";
    public final static String SAVE_EDIT_FREE_TEXT_FOOD_DETAIL = "navFood/saveEditFreeTextFoodDetail.php";

    public final static String DELETE_FOOD_RECORD = "navFood/deleteFoodRecord.php";

    public final static String GET_FOOD_PIE_CHART_DATA = "navFood/getFoodPieChartData.php";

    public final static String GET_ADD_FOOD_IMAGE = systemConstant.SERVER_ADDRESS+"navFood/addfoodImage.php";


    public final static int GET_FREE_TEXT_ADD_FOOD_CODE = -1;
    public final static int GET_SPEECH_RESULT_CODE = 689;
    public final static int GET_SPEECH_RESULT_OK = -1;
    public final static int GET_NOT_FOUND_CODE= 0;

    public final static int GET_ADD_FOOD_IMAGE_CODE = 666;
    public final static int GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE = 667;

    public final static String GET_IS_FREE_TEXT_CODE = "Y";
    public final static String GET_IS_NOT_FREE_TEXT_CODE = "N";

    public final static String GET_HAS_IMAGE = "Y";
    public final static String GET_NOT_HAS_IMAGE = "N";

    public final static String DISPLAY_PIE_CHART = "Y";
    public final static String NOT_DISPLAY_PIE_CHART = "N";

    public final static String GET_BREAKFAST_CODE = "B";
    public final static String GET_LUNCH_CODE = "L";
    public final static String GET_DINNER_CODE = "D";
    public final static String GET_SNACK_CODE = "S";

    public final static int NOT_FOUND_IN_EXPANDABLE_LIST = -1;

    public final static String GET_FOODPLACE_HOME_CODE = "H";
    public final static String GET_FOODPLACE_OUTSIDE_CODE = "O";
    public final static String GET_FOODPLACE_TAKEOUT_CODE = "T";

    public final static String GET_REPEAT_FOOD_PLACE = "N/A";

    public final static String GET_NOT_IMAGE_CODE = "NA";

    // how many Carbohohydrate (grams ) per each (份) = 10
    public final static String threeDecimalFormat = "#.0";
    public final static int CA_GRAMS_PER_EACH = 10;
    public final static int KCAL_CARBOHYDRATE = 4;
    public final static int KCAL_PROTEIN = 4;
    public final static int KCAL_FAT = 9;


    // intent name
    public final static String GET_FOOD_DATE = "redirect_food_date"; //DB formal
    public final static Boolean IS_DELETE_ACTION_Y = true;
    public final static Boolean IS_DELETE_ACTION_N = false;


    public final static int nutrientSize = 8;
    public final static String[] nutrientName = {"熱量","碳水化合物","脂肪","蛋白質","糖","膽固酵","鈉","膳食纖維"};
    public final static String[] nutrientUnit = {"卡路里","克","克","克","克","克","毫克","克"};

}
