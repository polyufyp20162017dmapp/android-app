package com.dmc.myapplication.navFood;

import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;

public class navFoodAddActivity extends AppCompatActivity {
    String mCurrentPhotoPath;
    String imageFileName;
    private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navfood_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //set Table View
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        setupViewPager(viewPager);

        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        System.gc();
        //bitmap.recycle()
        // Drawable.setCallback(null);
    }

    private void setupViewPager(ViewPager viewPager) {
        navFoodTableViewAdapter adapter = new navFoodTableViewAdapter(getSupportFragmentManager());
        //Janus
        //adapter.addFragment(new navFoodAddCommonFrag(), "常用");
        adapter.addFragment(new navFoodAddSearchFrag(), "搜尋");
        adapter.addFragment(new navFoodAddAllFrag(), "所有");
        viewPager.setAdapter(adapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {

            case R.id.home:
                handleBackAction();
                break;

            case R.id.foodimageclass:
                selectFromGallery();
                break;
        }
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }


    private File createImageFile() throws IOException {
        // Create an image file name
        imageFileName = imageTool.getImageName();
        new exportTool().createDirIfNotExists(systemConstant.FILE_PATH_IMAGE);

        File image = new File(systemConstant.FILE_PATH_IMAGE + "/" + imageFileName + systemConstant.IMAGE_TYPE);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    private void selectFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"),  navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE) { // 照相機
                Thread a = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        System.out.println("Posting image to predict server...");

                        Uri selectedImageUri = data.getData();
                        String[] projection = { MediaStore.MediaColumns.DATA };
                        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                                null);
                        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                        cursor.moveToFirst();

                        mCurrentPhotoPath = cursor.getString(column_index);
                        imageFileName =imageTool.getImageName();


                        Bitmap image = BitmapFactory.decodeFile(mCurrentPhotoPath);
                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        image.compress(Bitmap.CompressFormat.JPEG, systemConstant.IMAGE_COMPRESS, stream);
                        String reducedImage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);
                        String edcoded = "";
                        try {
                            edcoded = URLEncoder.encode(reducedImage, "UTF-8");
                        }catch (UnsupportedEncodingException e){
                            e.printStackTrace();
                        }

                        String result = webHelper.sendRequestForPredict("POST", "img="+ edcoded);
                        Gson gson = new Gson();
                        HashMap<String, Integer> resultObjs = gson.fromJson(result, new TypeToken<HashMap<String, Integer>>(){}.getType());
                        Integer foodId = resultObjs.get("result");

                        startAddFreeFoodPage(foodId);
                    }
                });

                a.start();
            }
        }
    }

    private void startAddFreeFoodPage(Integer foodid) {
        Intent intent = new Intent();
        intent.setClass(this, navFoodAddDetailActivity.class);
        intent.putExtra("food_id", foodid);
        intent.putExtra("is_free_text",navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE, getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
        startActivity(intent);
        finish();
    }

    private void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            if (photoFile != null) {
                //Janus
                //UserLocalStore uls = new UserLocalStore(getBaseContext());
                //SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                //String currentTimestamp = s.format(new Date());

                //String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+uls.getLoggedInUser().userid+"_"+currentTimestamp+".jpg";
                //File myImage = new File(strImage);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(myImage));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, navFoodConstant.GET_ADD_FOOD_IMAGE_CODE);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.food_menu2, menu);
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String foodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        //overridePendingTransition(R.anim.sli, R.anim.slide_out_right);
        finish();
    }

}
