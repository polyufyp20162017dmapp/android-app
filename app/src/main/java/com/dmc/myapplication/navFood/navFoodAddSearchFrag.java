package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.ArrayList;

import static android.content.Context.INPUT_METHOD_SERVICE;

/**
 * Created by KwokSinMan on 4/1/2016.
 */
public class navFoodAddSearchFrag extends Fragment {

    SearchView search;
    View v;

    public navFoodAddSearchFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v =  inflater.inflate(R.layout.navfood_add_search,container,false);

        // input box for seachinf food
        search= (SearchView) v.findViewById(R.id.searchFood);
        search.setOnQueryTextListener(new foodSearchOnQueryTextListener(this.getActivity()));
        //search.setSubmitButtonEnabled(true);
        search.setQueryHint("輸入食物名稱");
        //search.setFocusable(true);
        //search.setIconified(false);
        //search.requestFocusFromTouch();

        // display searched food list
        //Janus
       // ListView listView = (ListView) v.findViewById(R.id.searchedFoodList);
       // listView.setOnItemClickListener(new foodSearchOnItemClickListener(this.getActivity()));
       // listView.setAdapter(null);

        // free input food item
        Button navFoodFreeAdd = (Button) v.findViewById(R.id.navFoodFreeAdd);
        navFoodFreeAdd.setOnClickListener(new foodFreeAddOnClickListener(this.getActivity()));
        navFoodFreeAdd.setVisibility(View.GONE);
        Button navFoodOtherAdd = (Button) v.findViewById(R.id.navFoodOtherAdd);
        navFoodOtherAdd.setOnClickListener(new foodFreeOtherAddOnClickListener(this.getActivity()));
        navFoodOtherAdd.setVisibility(View.GONE);


        // textView foodNotFound
        TextView foodNotFoundTextView = (TextView) v.findViewById(R.id.foodNotFound);
        foodNotFoundTextView.setVisibility(View.GONE);

        // speakButton
        ImageButton btnSpeak = (ImageButton) v.findViewById(R.id.speakButton);
        btnSpeak.setOnClickListener(new speakToTextOnClickListener(this.getActivity()));

        search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SearchView sv = ((SearchView) view.findViewById(R.id.searchFood));
                sv.onActionViewExpanded();
            }
        });


        //Janus
        search.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    hideKeyboard(v);
                }
            }
        });

        enableCommonList(v);

        return v;
    }

    //Janus
    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager)getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    private class foodSearchOnQueryTextListener implements SearchView.OnQueryTextListener{
        Activity activity;
        public foodSearchOnQueryTextListener(Activity activity){ this.activity = activity;}

        @Override
        public boolean onQueryTextSubmit(String query) {
            //System.out.println("Candy onQueryTextSubmit = " + query);
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            //System.out.println("Candy onQueryTextChange =" + newText);

            // remove all space
            newText = newText.replaceAll(" ", "");

            if (newText.isEmpty()) {
                /*ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);

                listView.setAdapter(null);

                TextView labAddAllFood = (TextView) activity.findViewById(R.id.labAddAllFood);
                labAddAllFood.setText(R.string.nav_food_common);
                // set food Common list  OnItemClickListener
                listView.setOnItemClickListener(new foodCommonListOnItemClickListener(getActivity()));

                UserLocalStore userLocalStore = new UserLocalStore(getActivity());
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                navfoodAsyncTask connect = new navfoodAsyncTask(getActivity(), navFoodConstant.GET_FOOD_COMMON_LIST);
                connect.execute(userId);

                // free input food item
                Button navFoodFreeAdd = (Button) activity.findViewById(R.id.navFoodFreeAdd);
                navFoodFreeAdd.setVisibility(View.GONE);
                Button navFoodOtherAdd = (Button) activity.findViewById(R.id.navFoodOtherAdd);
                navFoodOtherAdd.setVisibility(View.GONE);

                // textView foodNotFound
                TextView foodNotFoundTextView = (TextView) activity.findViewById(R.id.foodNotFound);
                foodNotFoundTextView.setVisibility(View.GONE);*/

                enableCommonList(v);



            }
            else{
                //navfoodAsyncTask connect = new navfoodAsyncTask(activity, navFoodConstant.GET_FOOD_SEARCH_RESULT);
                //connect.execute(newText);
                TextView labAddAllFood = (TextView) activity.findViewById(R.id.labAddAllFood);
                labAddAllFood.setText(R.string.nav_food_search);

                ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);
                listView.setOnItemClickListener(new foodSearchOnItemClickListener(getActivity()));
                navFoodBiz biz = new navFoodBiz();
                biz.setFoodSearchResultNew(activity, "{}", newText);
            }
            return false;
        }
    }

    public void enableCommonList(View activity){
        ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);

        listView.setAdapter(null);

        TextView labAddAllFood = (TextView) activity.findViewById(R.id.labAddAllFood);
        labAddAllFood.setText(R.string.nav_food_common);
        // set food Common list  OnItemClickListener
        listView.setOnItemClickListener(new foodCommonListOnItemClickListener(getActivity()));

        UserLocalStore userLocalStore = new UserLocalStore(getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        navfoodAsyncTask connect = new navfoodAsyncTask(getActivity(), navFoodConstant.GET_FOOD_COMMON_LIST);
        connect.execute(userId);

        // free input food item
        Button navFoodFreeAdd = (Button) activity.findViewById(R.id.navFoodFreeAdd);
        navFoodFreeAdd.setVisibility(View.GONE);
        Button navFoodOtherAdd = (Button) activity.findViewById(R.id.navFoodOtherAdd);
        navFoodOtherAdd.setVisibility(View.GONE);

        // textView foodNotFound
        TextView foodNotFoundTextView = (TextView) activity.findViewById(R.id.foodNotFound);
        foodNotFoundTextView.setVisibility(View.GONE);
    }

    private class foodCommonListOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public foodCommonListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int food_id= ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId();
            //System.out.println("Candy food_id=" + food_id);
            Intent intent = new Intent();
            intent.setClass(activity, navFoodAddDetailActivity.class);
            intent.putExtra("food_id", food_id);
            intent.putExtra("is_free_text",navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(navFoodConstant.GET_FOOD_DATE, activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

    private class foodSearchOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public foodSearchOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int food_id= ((navFoodBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId();
            //System.out.println("Candy food_id=" + food_id);
            Intent intent = new Intent();
            intent.setClass(activity, navFoodAddDetailActivity.class);
            intent.putExtra("food_id", food_id);
            intent.putExtra("is_free_text",navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(navFoodConstant.GET_FOOD_DATE, activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

    private class foodFreeOtherAddOnClickListener implements View.OnClickListener{
        Activity activity;
        public foodFreeOtherAddOnClickListener(Activity activity){ this.activity=activity; }
        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent();
            intent.setClass(activity, navFoodAddDetailActivity.class);
            intent.putExtra("food_name", search.getQuery().toString());
            intent.putExtra("is_free_text",navFoodConstant.GET_IS_FREE_TEXT_CODE);
            intent.putExtra("has_image",navFoodConstant.GET_HAS_IMAGE);
            intent.putExtra(navFoodConstant.GET_FOOD_DATE, activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }


    private class foodFreeAddOnClickListener implements View.OnClickListener{
        Activity activity;
        public foodFreeAddOnClickListener(Activity activity){ this.activity=activity; }
        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent();
            intent.setClass(activity, navFoodAddDetailActivity.class);
            intent.putExtra("food_name", search.getQuery().toString());
            intent.putExtra("is_free_text",navFoodConstant.GET_IS_FREE_TEXT_CODE);
            intent.putExtra("has_image",navFoodConstant.GET_NOT_HAS_IMAGE);
            intent.putExtra(navFoodConstant.GET_FOOD_DATE, activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

    private class speakToTextOnClickListener implements View.OnClickListener{
        Activity activity;
        public speakToTextOnClickListener(Activity activity){ this.activity=activity; }
        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "zh-TW"); //繁體中文
            try {
                // start 2 way interface ( A pass data to B, B return data to A)
                startActivityForResult(intent, navFoodConstant.GET_SPEECH_RESULT_CODE);
            } catch (ActivityNotFoundException aException) {
                Toast t = Toast.makeText(activity,"你的手機不支援語音辦別", Toast.LENGTH_LONG);
                t.show();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    // get the result from the google speech to text (Call Back)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case navFoodConstant.GET_SPEECH_RESULT_CODE: {
                if (resultCode == navFoodConstant.GET_SPEECH_RESULT_OK && null != data) {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    search.setQuery(text.get(0),true);
                    search.setFocusable(true);
                    search.setIconified(false);
                    search.requestFocusFromTouch();
            }
                break;
            }

        }
    }

}


//String[] mobileArray = {"叉燒包","菜芯","白飯","上素蒸粉果","咖喱蒸魷魚","大包","山竹牛肉","棉花雞","灌湯餃","春卷",
// "叉燒腸粉","燒賣","牛奶朱古力","節瓜","苦瓜","西生菜","全脂奶粉","香梨(連皮)","香梨(不連皮)","雪糕"};

//ArrayAdapter adapter = new ArrayAdapter<String>(container.getContext(), R.layout.navfood_add_search_list, R.id.itemName, mobileArray);
//ListView listView = (ListView) v.findViewById(R.id.searchedFoodList);
//listView.setAdapter(adapter);



    /*
    search.setOnQueryTextFocusChangeListener(new foodSearchOnQueryTextFocusChangeListener(this.getActivity()));

    private class foodSearchOnQueryTextFocusChangeListener implements View.OnFocusChangeListener {
        Activity activity;
        public foodSearchOnQueryTextFocusChangeListener(Activity activity){ this.activity = activity;}

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            System.out.println("Candy onFocusChange!!!");
        }
    }
*/
