package com.dmc.myapplication.navFood;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dmc.myapplication.R;

/**
 * Created by KwokSinMan on 31/1/2016.
 */
public class navFoodRecordTimePickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayTime = (String)((TextView) getActivity().findViewById(R.id.navFoodRecordTime)).getText();
        navFoodTime displayDateTimeObject = new navFoodTime(displayTime);
        int hour = displayDateTimeObject.getHour();
        int min = displayDateTimeObject.getMin();
        return new TimePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,hour,min,true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        navFoodTime foodTime = new navFoodTime();
        String displayTime = foodTime.getTimeString(hourOfDay)+":"+foodTime.getTimeString(minute);
        ((TextView) getActivity().findViewById(R.id.navFoodRecordTime)).setText(displayTime);
    }
}