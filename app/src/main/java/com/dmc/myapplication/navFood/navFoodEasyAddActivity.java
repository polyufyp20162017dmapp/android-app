package com.dmc.myapplication.navFood;

import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.Models.imageStorage;
import com.dmc.myapplication.Models.imageSyncStack;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class navFoodEasyAddActivity extends AppCompatActivity {
    ImageView foodImage;
    String imageFileName;
    String mCurrentPhotoPath;
    Button finishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_food_easy_add);

        String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        TextView navFoodDate = (TextView)findViewById(R.id.navFoodRecordDate);
        navFoodDate.setText(new navFoodTime().getUIDateFormal(getFoodDate));

        finishButton = (Button) findViewById(R.id.navFoodAdd);
        finishButton.setEnabled(false);

        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                navFoodTime timeTool = new navFoodTime();

                String foodDate = timeTool.getDBDateFormal((String) ((TextView) findViewById(R.id.navFoodRecordDate)).getText());
                String foodTime = (String) ((TextView) findViewById(R.id.navFoodRecordTime)).getText();
                String foodType = ((navFoodBiz.DbRecordStringString) (((Spinner) findViewById(R.id.foodType)).getSelectedItem())).getId();
                String foodPlace = ((navFoodBiz.DbRecordStringString) (((Spinner) findViewById(R.id.foodPlace)).getSelectedItem())).getId();

                UserLocalStore userLocalStore = new UserLocalStore(arg0.getContext());
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

                String photoPath="NA";

                    if(imageFileName!=null && !imageFileName.isEmpty()){
                        Bitmap reducedImage = imageTool.decreaseImageSie(mCurrentPhotoPath);
                        //System.out.println("Candy photoPath="+photoPath);
                        //System.out.println("Candy reducedImage="+reducedImage);

                        //Janus Save to internal
                        //new navFoodUploadImageAsyncTask(reducedImage,photoPath).execute();

                        UserLocalStore uls = new UserLocalStore(getBaseContext());
                        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                        String currentTimestamp = s.format(new Date());
                        //photoPath = imageFileName + userId + systemConstant.IMAGE_TYPE;
                        photoPath = uls.getLoggedInUser().userid+"_"+currentTimestamp;

                        String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+uls.getLoggedInUser().userid+"_"+currentTimestamp+".jpg";

                        /*try{
                            copy(new File(mCurrentPhotoPath),new File(strImage));
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        reducedImage.compress(Bitmap.CompressFormat.JPEG, systemConstant.IMAGE_COMPRESS, stream);
                        String reducedImageString = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

                        imageSyncStack imageSyncStackdb = new imageSyncStack(getBaseContext());
                        imageSyncStackdb.insert(imageSyncStackdb.new imageSyncStack_class(0, photoPath+".jpg", reducedImageString));
                        imageSyncStackdb.close();

                        imageStorage imageStorage = new imageStorage(getBaseContext());
                        imageStorage.insert(imageStorage.new imageStorage_class(0, photoPath+".jpg", reducedImageString));
                        imageStorage.close();
                    }


                    //System.out.println("Candy mCurrentPhotoPath="+mCurrentPhotoPath);
                    //System.out.println("Candy foodName=" + foodName);
                    //System.out.println("Candy foodUnit=" + inputFoodUnit);
                    //System.out.println("Candy photoPath=" + photoPath);

                    //Janus
                    //navfoodAsyncTask connect = new navfoodAsyncTask(getActivityContentView(arg0), navFoodConstant.SAVE_FREE_TEXT_FOOD_RECORD);
                    //connect.execute(userId, foodDate, foodTime, foodValue, foodType, foodPlace, foodName, inputFoodUnit, photoPath);
                    foodRecord frdb = new foodRecord(getApplicationContext());
                    frdb.insert(frdb.new foodRecord_class(0, Integer.parseInt(userId), foodDate, foodTime, foodType, foodPlace, 0, -1, null, null, null, null, "其他（快速）", "", photoPath));
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);

                    Bundle bundle = new Bundle();
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    ContentResolver.requestSync(null, "com.dmc.myapplication.provider", bundle);

                    startActivity(intent);
                    finish();
            }
        });
        //set Food Time
        TextView navFoodTimeTextView = (TextView)findViewById(R.id.navFoodRecordTime);
        navFoodTimeTextView.setText(new navFoodTime().getCurrentTimeFormal());

        // set data picker
        ImageView setFoodRecordDate = (ImageView) findViewById(R.id.setNavFoodRecordDate);
        setFoodRecordDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodRecordDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setFoodRecordTime = (ImageView) findViewById(R.id.setNavFoodRecordTime);
        setFoodRecordTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodRecordTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        navFoodBiz foodBiz = new navFoodBiz();
        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();

        // set Food Stype
        List<navFoodBiz.DbRecordStringString> foodTypeList = foodBiz.getFoodTypeList();
        ArrayAdapter<navFoodBiz.DbRecordStringString> foodTypeAdapter = new ArrayAdapter<navFoodBiz.DbRecordStringString>(this, R.layout.navfood_spinner_center_item, foodTypeList);
        foodTypeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner foodTypeSpinner = (Spinner) findViewById(R.id.foodType);
        foodTypeSpinner.setAdapter(foodTypeAdapter);

        try{
            Date nowtime = formatter.parse(formatter.format(cal.getTime()));
            System.out.println("formatter.parse(userLocalStore.getLoggedInUser().dinner_start)="+formatter.parse(userLocalStore.getLoggedInUser().dinner_start).toString());
            System.out.println("formatter.parse(userLocalStore.getLoggedInUser().dinner_end)="+formatter.parse(userLocalStore.getLoggedInUser().dinner_end).toString());
            System.out.println("cal.getTime()="+nowtime.toString());

            if (formatter.parse(userLocalStore.getLoggedInUser().breakfast_start).before(nowtime) && formatter.parse(userLocalStore.getLoggedInUser().breakfast_end).after(nowtime)){
                foodTypeSpinner.setSelection(0);
            }else if (formatter.parse(userLocalStore.getLoggedInUser().lunch_start).before(nowtime) && formatter.parse(userLocalStore.getLoggedInUser().lunch_end).after(nowtime)){
                foodTypeSpinner.setSelection(1);
            }else if (formatter.parse(userLocalStore.getLoggedInUser().dinner_start).before(nowtime) && formatter.parse(userLocalStore.getLoggedInUser().dinner_end).after(nowtime)){
                foodTypeSpinner.setSelection(2);
            }else{
                foodTypeSpinner.setSelection(3);
            }

        }catch (Exception e){
            e.printStackTrace();
        }

        // set Food Place
        List<navFoodBiz.DbRecordStringString> foodPlaceList = foodBiz.getFoodPlaceList();
        ArrayAdapter<navFoodBiz.DbRecordStringString> foodPlaceAdapter = new ArrayAdapter<navFoodBiz.DbRecordStringString>(this, R.layout.navfood_spinner_center_item, foodPlaceList);
        foodPlaceAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner foodPlaceSpinner = (Spinner) findViewById(R.id.foodPlace);
        foodPlaceSpinner.setAdapter(foodPlaceAdapter);


        foodImage = (ImageView) findViewById(R.id.addPhoto);
        foodImage.setOnClickListener(new imageOnClickListener());

        Button navFoodDelete = (Button) findViewById(R.id.navFoodDelete);
        navFoodDelete.setVisibility(View.GONE);

        Button navFoodEdit = (Button) findViewById(R.id.navFoodEdit);
        navFoodEdit.setVisibility(View.GONE);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

    }

    public void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    private class imageOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            CharSequence[] items = { "相機", "相簿" };
            AlertDialog.Builder builder = new AlertDialog.Builder(navFoodEasyAddActivity.this);
            builder.setItems(items, new getImageMethodDialogOnClickListener());
            builder.show();
        }
    }

    private class getImageMethodDialogOnClickListener implements DialogInterface.OnClickListener{
        public void onClick(DialogInterface dialog, int item) {
            //System.out.println("Candy testing get image=" + item);
            if (item == 0) { // 照相機
                takePhoto();
            }else if (item == 1) { // 相簿
                selectFromGallery();
            }
        }
    }

    private void selectFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"),  navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE);

    }

    private void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            if (photoFile != null) {
                //Janus
                //UserLocalStore uls = new UserLocalStore(getBaseContext());
                //SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                //String currentTimestamp = s.format(new Date());

                //String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+uls.getLoggedInUser().userid+"_"+currentTimestamp+".jpg";
                //File myImage = new File(strImage);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(myImage));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, navFoodConstant.GET_ADD_FOOD_IMAGE_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        imageFileName = imageTool.getImageName();
        new exportTool().createDirIfNotExists(systemConstant.FILE_PATH_IMAGE);

        File image = new File(systemConstant.FILE_PATH_IMAGE + "/" + imageFileName + systemConstant.IMAGE_TYPE);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackAction();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_CODE) { // 照相機
                setPic();
                finishButton.setEnabled(true);
            }else if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE){ //
                setGalleryPic(data);
                finishButton.setEnabled(true);
            }
        }
    }

    private void setGalleryPic(Intent data){
        Uri selectedImageUri = data.getData();

        if (selectedImageUri.getScheme().equals("file")){

                mCurrentPhotoPath = selectedImageUri.getEncodedPath();
                imageFileName =imageTool.getImageName();
                //System.out.println("mCurrentPhotoPath="+selectedImagePath);

        }else{
            String[] projection = { MediaStore.MediaColumns.DATA };
            Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                    null);
            if (null != cursor){
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
                cursor.moveToFirst();

                mCurrentPhotoPath = cursor.getString(column_index);
                imageFileName =imageTool.getImageName();
                //System.out.println("mCurrentPhotoPath="+selectedImagePath);

            }
        }

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, options));
        foodImage.setImageBitmap(bm);



    }


    private  void setPic(){
        int targetW = foodImage.getWidth();
        int targetH = foodImage.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap rotatedBMP = imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)) ;

        foodImage.setImageBitmap(rotatedBMP);
        //foodImage.setImageBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
        intent.putExtra(systemConstant.REDIRECT_PAGE ,systemConstant.DISPLAY_NAVFOOD);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }
}
