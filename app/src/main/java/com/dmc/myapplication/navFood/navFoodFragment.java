package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.DialogInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.menu.ActionMenuItemView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmc.myapplication.Models.food;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;


/**
 * Created by KwokSinMan on 30/11/2015.
 */
public class navFoodFragment extends Fragment {

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v =  inflater.inflate(R.layout.navfood_main, container, false);

        String getFoodDate = this.getArguments().getString(navFoodConstant.GET_FOOD_DATE);

        // set food  Date
        TextView navFoodDate = (TextView) v.findViewById(R.id.navFoodViewDate);
        navFoodDate.setText(new navFoodTime().getUIDateFormal(getFoodDate));

        // set data picker
        ImageView setfooddate = (ImageView) v.findViewById(R.id.setNavFoodViewDate);
        setfooddate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodViewDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //set Pie Chart onClickListener
        Button navFoodPieChart = (Button) v.findViewById(R.id.navFoodChart);
        navFoodPieChart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), navFoodPieChartActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });

        //set Add record event
/*
        Button navfoodAdd = (Button) v.findViewById(R.id.navFoodAdd);
        navfoodAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), navFoodAddActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                startActivity(intent);
            }
        });
*/
        //set edit food record event
        ExpandableListView expListView = (ExpandableListView) v.findViewById(R.id.lvExp);
        expListView.setOnChildClickListener(new editFoodOnChildClickListener(this.getActivity()));

        // get Data from DB to display total CARBOHYDRATE and food result
        navfoodAsyncTask connect = new navfoodAsyncTask(this.getActivity(), navFoodConstant.GET_FOOD_RECORD);
        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());

        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

        //navFoodBiz biz = new navFoodBiz();
        //biz.setFoodRecord(this.getActivity(), "{}", getFoodDate);

        connect.execute(userId,getFoodDate);

        return v;
    }

    private class editFoodOnChildClickListener implements ExpandableListView.OnChildClickListener {
        private Activity activity;
        public editFoodOnChildClickListener(Activity activity){this.activity = activity;}

        @Override
        public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            foodRecord foodDB = new foodRecord(getContext());
            int foodRecordId = ((navFoodBiz.foodRecord) parent.getExpandableListAdapter().getChild(groupPosition, childPosition)).getFoodRecordId();
            String isFreeText = ((navFoodBiz.foodRecord) parent.getExpandableListAdapter().getChild(groupPosition, childPosition)).getIsFreeTextInput();
            Boolean isSpeedMode = foodDB.get(foodRecordId).isSpeedRecordMode();
            foodDB.close();
            if (isSpeedMode){
                Intent intent = new Intent();
                intent.setClass(activity, navFoodEasyEditActivity.class);
                intent.putExtra("food_record_id", foodRecordId);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide1,R.anim.slide2);
                activity.finish();

            }else{
                //System.out.println("Candy testing foodRecordId=" + foodRecordId);
                //System.out.println("Candy testing isFreeText=" + isFreeText);
                Intent intent = new Intent();
                intent.setClass(activity, navFoodEditDetailActivity.class);
                intent.putExtra("food_record_id", foodRecordId);
                intent.putExtra("is_free_text",isFreeText);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                activity.startActivity(intent);
                activity.overridePendingTransition(R.anim.slide1,R.anim.slide2);
                activity.finish();
            }
            return false;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }


    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.food_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent = new Intent();
        switch (item.getItemId()){
            case R.id.food_menuItem_add:
                intent.setClass(getActivity(), navFoodAddActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().finish();
                break;

            case R.id.food_menuItem_speed_add:
                intent.setClass(getActivity(), navFoodEasyAddActivity.class);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE,  new navFoodTime().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().overridePendingTransition(R.anim.slide1,R.anim.slide2);
                getActivity().finish();
                break;

        }
        return true;
    }




}

