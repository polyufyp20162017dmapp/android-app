package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.food;
import com.dmc.myapplication.Models.foodCategories;
import com.dmc.myapplication.Models.foodSubCategories;
import com.dmc.myapplication.Models.foodUserCommon;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.imageTool;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import org.json.JSONArray;
import org.json.JSONException;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.lang.Thread.sleep;

/**
 * Created by KwokSinMan on 18/1/2016.
 */
public class navFoodBiz {
    public static void startNavFood(Activity activity,String foodDateUI, FragmentTransaction ft){
        activity.setTitle(R.string.nav_food);
        Bundle bundle = new Bundle();
        bundle.putString(navFoodConstant.GET_FOOD_DATE, foodDateUI);
        navFoodFragment navFoodFrag = new navFoodFragment();
        navFoodFrag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, navFoodFrag).addToBackStack(null).commit();
    }

    public void afterSaveFoodRecord (Activity activity, Boolean isDeleteAction){
        String foodDate = activity.getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        if (!isDeleteAction){
            foodDate = new navFoodTime().getDBDateFormal((String) ((TextView) activity.findViewById(R.id.navFoodRecordDate)).getText());
        }
        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);
        activity.startActivity(intent);
        activity.finish();
    }

    //set DDL of foodCategories
    public void setFoodCateDDL(Activity activity,String result){
        try {
            //peogress data from server side

            //Janus
            //JSONArray JA = new JSONArray(result);

            foodCategories foodCategoriesDB = new foodCategories(activity);
            List<HashMap<String, String>> JA = foodCategoriesDB.getAll();

            List<DbRecordStringId> foodCateDDL = new ArrayList<DbRecordStringId>();
            foodCateDDL.add(new DbRecordStringId("請選擇", 0));
            for (int i = 0; i < JA.size(); i++) {
                foodCateDDL.add(new DbRecordStringId(JA.get(i).get("FOOD_CATE_NAME"), Integer.parseInt(JA.get(i).get("FOOD_CATE_ID"))));
            }

            // set food Cate DDL
            Spinner foodCateSpinner = (Spinner) activity.findViewById(R.id.foodCategories);
            ArrayAdapter<DbRecordStringId> foodCateAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.navfood_spinner_center_item, foodCateDDL);
            foodCateAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            foodCateSpinner.setAdapter(foodCateAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //set DDL of foodDetailCategories
    public  void setFoodSubCateDDL (Activity activity,String result){
        try {
            //Janus
            //JSONArray JA = new JSONArray(result);
            foodSubCategories foodSCdb = new foodSubCategories(MainActivity.getContext);
            List<HashMap<String, String>> JA = foodSCdb.getAll();
            List<DbRecordStringId> foodSubCateDDL = new ArrayList<DbRecordStringId>();
            foodSubCateDDL.add(new DbRecordStringId("請選擇", 0));
            for (int i = 0; i < JA.size(); i++) {
                foodSubCateDDL.add(new DbRecordStringId(JA.get(i).get("FOOD_SUB_CATE_NAME"), Integer.parseInt(JA.get(i).get("FOOD_SUB_CATE_ID"))));
            }
            Spinner foodSubCateSpinner = (Spinner) activity.findViewById(R.id.foodDetailCategories);
            ArrayAdapter<DbRecordStringId> foodSubCateAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.navfood_spinner_center_item, foodSubCateDDL);
            foodSubCateAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            foodSubCateSpinner.setAdapter(foodSubCateAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void setFoodSubCateDDLNew (Activity activity,String result, String foodCate){
        try {
            //Janus
            //JSONArray JA = new JSONArray(result);
            foodSubCategories foodSCdb = new foodSubCategories(activity);
            List<HashMap<String, String>> JA = foodSCdb.getAllByFoodCateKey(Integer.parseInt(foodCate));
            List<DbRecordStringId> foodSubCateDDL = new ArrayList<DbRecordStringId>();
            foodSubCateDDL.add(new DbRecordStringId("請選擇", 0));
            for (int i = 0; i < JA.size(); i++) {
                foodSubCateDDL.add(new DbRecordStringId(JA.get(i).get("FOOD_SUB_CATE_NAME"), Integer.parseInt(JA.get(i).get("FOOD_SUB_CATE_ID"))));
            }
            Spinner foodSubCateSpinner = (Spinner) activity.findViewById(R.id.foodDetailCategories);
            ArrayAdapter<DbRecordStringId> foodSubCateAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.navfood_spinner_center_item, foodSubCateDDL);
            foodSubCateAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
            foodSubCateSpinner.setAdapter(foodSubCateAdapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //set  food List
    public  void setFoodAllList (Activity activity,String result){
        //System.out.println("Candy setFoodAllList=" + result);
        try {
            JSONArray JA = new JSONArray(result);
            List<Map<String, DbRecordStringId>> items = new ArrayList<Map<String,DbRecordStringId>>();
            for (int i = 0; i < JA.length(); i++) {
                Map<String, DbRecordStringId> item = new HashMap<String, DbRecordStringId>();
                item.put("food", new DbRecordStringId(JA.getJSONObject(i).getString("FOOD_NAME"), JA.getJSONObject(i).getInt("FOOD_ID")));
                items.add(item);
            }

            SimpleAdapter adapter = new SimpleAdapter(activity, items, R.layout.navfood_add_all_list, new String[]{"food"}, new int[]{R.id.itemName});
            GridView gridView = (GridView) activity.findViewById(R.id.allFoodListView);
            gridView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public  void setFoodAllListNew (Activity activity,String result, String foodSubCateID){
        //System.out.println("Candy setFoodAllList=" + result);
        try {
            //JSONArray JA = new JSONArray(result);
            food fooddb = new food(activity);
            List<HashMap<String, String>> JA = fooddb.getAllByFoodSubCateID(Integer.parseInt(foodSubCateID));
            List<Map<String, DbRecordStringId>> items = new ArrayList<Map<String,DbRecordStringId>>();
            for (int i = 0; i < JA.size(); i++) {
                Map<String, DbRecordStringId> item = new HashMap<String, DbRecordStringId>();
                item.put("food", new DbRecordStringId(JA.get(i).get("FOOD_NAME"), Integer.parseInt(JA.get(i).get("FOOD_ID")
                )));
                items.add(item);
            }

            SimpleAdapter adapter = new SimpleAdapter(activity, items, R.layout.navfood_add_all_list, new String[]{"food"}, new int[]{R.id.itemName});
            GridView gridView = (GridView) activity.findViewById(R.id.allFoodListView);
            gridView.setAdapter(adapter);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void setFoodRecord(Activity activity,String result){
        if (!result.isEmpty()) {
            //set expandable list
            ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.lvExp);
            HashMap<String, Object> hashmap = prepareListData(result);
            navFoodExpandableListAdapter listAdapter = new navFoodExpandableListAdapter(activity, (List<String>) hashmap.get("HEADER"), (HashMap<String, List<foodRecord>>) hashmap.get("CHILD"));
            expListView.setAdapter(listAdapter);

            //expListView.expandGroup(1);
            openExpandableList(activity, (List<String>) hashmap.get("GROUP_POSITION"), expListView);

            //System.out.println("Candy is display chart=" + hashmap.get("DISPLAY"));

            //display Pie Chart && set totalCaloriesUnit
            if(hashmap.get("DISPLAY").equals(navFoodConstant.NOT_DISPLAY_PIE_CHART)){
                Button displayPieChart = (Button) activity.findViewById(R.id.navFoodChart);
                displayPieChart.setEnabled(false);
                TextView total = (TextView) activity.findViewById(R.id.total);
                total.setText("總碳水化合物：未能提供");
            }else if(hashmap.get("DISPLAY").equals(navFoodConstant.DISPLAY_PIE_CHART)){
                Button displayPieChart = (Button) activity.findViewById(R.id.navFoodChart);
                displayPieChart.setEnabled(true);
                TextView total = (TextView) activity.findViewById(R.id.total);
                total.setText("總碳水化合物：" + hashmap.get("TOTAL") + "份");
            }

        }else{
            //clear total session
            TextView total = (TextView) activity.findViewById(R.id.total);
            total.setText("沒有任何記錄");

            //clear expandable listview
            ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.lvExp);
            expListView.setAdapter((navFoodExpandableListAdapter) null);

            //display Pie Chart
            Button displayPieChart = (Button) activity.findViewById(R.id.navFoodChart);
            displayPieChart.setEnabled(false);
        }
    }

    public void setFoodRecord(Activity activity,String result, String foodDate){
        com.dmc.myapplication.Models.foodRecord frdb = new com.dmc.myapplication.Models.foodRecord(activity);
        List<HashMap<String, String>> JA = frdb.getAllByFoodDate(foodDate);
        System.out.println("setFoodRecord JA.size = "+JA.size()+ " foodDate = "+foodDate);
        if (JA.size() > 0) {
            //set expandable list
            ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.lvExp);
            HashMap<String, Object> hashmap = prepareListData(activity, JA);
            navFoodExpandableListAdapter listAdapter = new navFoodExpandableListAdapter(activity, (List<String>) hashmap.get("HEADER"), (HashMap<String, List<foodRecord>>) hashmap.get("CHILD"));
            expListView.setAdapter(listAdapter);

            //expListView.expandGroup(1);
            openExpandableList(activity, (List<String>) hashmap.get("GROUP_POSITION"), expListView);

            //System.out.println("Candy is display chart=" + hashmap.get("DISPLAY"));

            //display Pie Chart && set totalCaloriesUnit
            if(hashmap.get("DISPLAY").equals(navFoodConstant.NOT_DISPLAY_PIE_CHART)){
                Button displayPieChart = (Button) activity.findViewById(R.id.navFoodChart);
                displayPieChart.setEnabled(false);
                TextView total = (TextView) activity.findViewById(R.id.total);
                total.setText("總碳水化合物：未能提供");
            }else if(hashmap.get("DISPLAY").equals(navFoodConstant.DISPLAY_PIE_CHART)){
                Button displayPieChart = (Button) activity.findViewById(R.id.navFoodChart);
                displayPieChart.setEnabled(true);
                TextView total = (TextView) activity.findViewById(R.id.total);
                total.setText("總碳水化合物：" + hashmap.get("TOTAL") + "份");
            }

        }else{
            //clear total session
            TextView total = (TextView) activity.findViewById(R.id.total);
            total.setText("沒有任何記錄");

            //clear expandable listview
            ExpandableListView expListView = (ExpandableListView) activity.findViewById(R.id.lvExp);
            expListView.setAdapter((navFoodExpandableListAdapter) null);

            //display Pie Chart
            Button displayPieChart = (Button) activity.findViewById(R.id.navFoodChart);
            displayPieChart.setEnabled(false);
        }
    }

    private HashMap<String, Object> prepareListData(String result) {
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {
            List<String> header =  new ArrayList<String>();
            List<String> position =  new ArrayList<String>();
            HashMap<String, List<foodRecord>> child = new HashMap<String, List<foodRecord>>();
            String  displayPieChart = navFoodConstant.DISPLAY_PIE_CHART;
            double total =0;
            double breakfastTotal = 0;
            double lunchTotal = 0;
            double dinnerTotal = 0;
            double snackTotal = 0;

            // Child data
            List<foodRecord> breakfast = new ArrayList<foodRecord>();
            List<foodRecord> lunch = new ArrayList<foodRecord>();
            List<foodRecord> dinner = new ArrayList<foodRecord>();
            List<foodRecord> other = new ArrayList<foodRecord>();


            // Adding child data
            JSONArray JA = new JSONArray(result);
            for (int i = 0; i < JA.length(); i++) {
                foodRecord record;
                try {
                    double carboValue = carboChangetoPart(JA.getJSONObject(i).getDouble("FOOD_CARBOHYDRATE"));
                    total += carboValue;
                    if (navFoodConstant.GET_BREAKFAST_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                        breakfastTotal += carboValue;
                    }
                    if (navFoodConstant.GET_LUNCH_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                        lunchTotal += carboValue;
                    }
                    if (navFoodConstant.GET_DINNER_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                        dinnerTotal += carboValue;
                    }
                    if (navFoodConstant.GET_SNACK_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                        snackTotal += carboValue;
                    }
                    record = new foodRecord(JA.getJSONObject(i).getString("FOOD_ITEM"), Double.toString(formatDouble(carboValue)), JA.getJSONObject(i).getInt("FOOD_RECORD_ID"), JA.getJSONObject(i).getString("IS_FREE_TEXT"));

                }catch (JSONException e){
                    record = new foodRecord(JA.getJSONObject(i).getString("FOOD_ITEM"), "-", JA.getJSONObject(i).getInt("FOOD_RECORD_ID"), JA.getJSONObject(i).getString("IS_FREE_TEXT"));
                    displayPieChart = navFoodConstant.NOT_DISPLAY_PIE_CHART;
                }

                if (navFoodConstant.GET_BREAKFAST_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                    breakfast.add(record);
                }
                if (navFoodConstant.GET_LUNCH_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                    lunch.add(record);
                }
                if (navFoodConstant.GET_DINNER_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                    dinner.add(record);
                }
                if (navFoodConstant.GET_SNACK_CODE.equals(JA.getJSONObject(i).getString("FOOD_SESSION"))){
                    other.add(record);
                }
            }

            int count =0;
            if (!breakfast.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART) {
                    header.add("早餐," + Integer.toString(formatDouble(breakfastTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("早餐,未能提供");
                }
                position.add(navFoodConstant.GET_BREAKFAST_CODE);
                child.put(header.get(0), breakfast); // Header, Child data
                count= count+1;
            }
            if (!lunch.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART) {
                    header.add("午餐," + Integer.toString(formatDouble(lunchTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("午餐,未能提供");
                }
                position.add(navFoodConstant.GET_LUNCH_CODE);
                child.put(header.get(count), lunch); // Header, Child data
                count= count+1;
            }
            if (!dinner.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART){
                    header.add("晚餐,"+Integer.toString(formatDouble(dinnerTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("晚餐,未能提供");
                }
                position.add(navFoodConstant.GET_DINNER_CODE);
                child.put(header.get(count), dinner); // Header, Child data
                count= count+1;
            }
            if (!other.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART) {
                    header.add("小食," + Integer.toString(formatDouble(snackTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("小食,未能提供");
                }
                position.add(navFoodConstant.GET_SNACK_CODE);
                child.put(header.get(count), other); // Header, Child data
                count= count+1;
            }

            temp.put("HEADER",header);
            temp.put("CHILD", child);
            temp.put("GROUP_POSITION", position);
            temp.put("TOTAL",formatDouble(total));
            temp.put("DISPLAY",displayPieChart);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    private HashMap<String, Object> prepareListData(Activity activity, List<HashMap<String, String>> result) {
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {
            List<String> header =  new ArrayList<String>();
            List<String> position =  new ArrayList<String>();
            HashMap<String, List<foodRecord>> child = new HashMap<String, List<foodRecord>>();
            String  displayPieChart = navFoodConstant.DISPLAY_PIE_CHART;
            double total =0;
            double breakfastTotal = 0;
            double lunchTotal = 0;
            double dinnerTotal = 0;
            double snackTotal = 0;

            // Child data
            List<foodRecord> breakfast = new ArrayList<foodRecord>();
            List<foodRecord> lunch = new ArrayList<foodRecord>();
            List<foodRecord> dinner = new ArrayList<foodRecord>();
            List<foodRecord> other = new ArrayList<foodRecord>();

            food fooddb = new food(activity);

            // Adding child data
            //JSONArray JA = new JSONArray(result);
            for (int i = 0; i < result.size(); i++) {
                foodRecord record;
                food.food_class foodNut = null;

                System.out.println("prepareListData FOOD_ID=" + result.get(i).get("FOOD_ID"));
                System.out.println("prepareListData FOOD_ID!=NULL????? =" + !result.get(i).get("FOOD_ID").equals("null"));

                if (!(result.get(i).get("FOOD_ID").equals("null") || result.get(i).get("FOOD_ID").equals("0"))){
                    foodNut = fooddb.get(Integer.parseInt(result.get(i).get("FOOD_ID")));

                    try {
                        double carboValue = carboChangetoPart(foodNut.getFOOD_CARBOHYDRATE());
                        total += carboValue;
                        if (navFoodConstant.GET_BREAKFAST_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            breakfastTotal += carboValue;
                        }
                        if (navFoodConstant.GET_LUNCH_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            lunchTotal += carboValue;
                        }
                        if (navFoodConstant.GET_DINNER_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            dinnerTotal += carboValue;
                        }
                        if (navFoodConstant.GET_SNACK_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            snackTotal += carboValue;
                        }
                        record = new foodRecord(foodNut.getFOOD_NAME(), Double.toString(formatDouble(carboValue)), Integer.parseInt(result.get(i).get("FOOD_RECORD_ID")), "N");

                    }catch (Exception e){
                        record = new foodRecord(foodNut.getFOOD_NAME(), "-", Integer.parseInt(result.get(i).get("FOOD_RECORD_ID")), "N");
                        displayPieChart = navFoodConstant.NOT_DISPLAY_PIE_CHART;
                    }

                    if (navFoodConstant.GET_BREAKFAST_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        breakfast.add(record);
                    }
                    if (navFoodConstant.GET_LUNCH_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        lunch.add(record);
                    }
                    if (navFoodConstant.GET_DINNER_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        dinner.add(record);
                    }
                    if (navFoodConstant.GET_SNACK_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        other.add(record);
                    }
                }else{
                    displayPieChart = navFoodConstant.NOT_DISPLAY_PIE_CHART;
                    try {
                        double carboValue = carboChangetoPart(Double.parseDouble(result.get(i).get("FOOD_CARBOHYDRATE")));
                        total += carboValue;
                        if (navFoodConstant.GET_BREAKFAST_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            breakfastTotal += carboValue;
                        }
                        if (navFoodConstant.GET_LUNCH_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            lunchTotal += carboValue;
                        }
                        if (navFoodConstant.GET_DINNER_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            dinnerTotal += carboValue;
                        }
                        if (navFoodConstant.GET_SNACK_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                            snackTotal += carboValue;
                        }
                        record = new foodRecord(result.get(i).get("FOOD_NAME"), Double.toString(formatDouble(carboValue)), Integer.parseInt(result.get(i).get("FOOD_RECORD_ID")), "Y");

                    }catch (Exception e){
                        record = new foodRecord(result.get(i).get("FOOD_NAME"), "-", Integer.parseInt(result.get(i).get("FOOD_RECORD_ID")), "Y");
                        displayPieChart = navFoodConstant.NOT_DISPLAY_PIE_CHART;
                    }

                    if (navFoodConstant.GET_BREAKFAST_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        breakfast.add(record);
                    }
                    if (navFoodConstant.GET_LUNCH_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        lunch.add(record);
                    }
                    if (navFoodConstant.GET_DINNER_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        dinner.add(record);
                    }
                    if (navFoodConstant.GET_SNACK_CODE.equals(result.get(i).get("FOOD_SESSION"))){
                        other.add(record);
                    }
                }



            }

            int count =0;
            if (!breakfast.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART) {
                    header.add("早餐," + Integer.toString(formatDouble(breakfastTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("早餐,未能提供");
                }
                position.add(navFoodConstant.GET_BREAKFAST_CODE);
                child.put(header.get(0), breakfast); // Header, Child data
                count= count+1;
            }
            if (!lunch.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART) {
                    header.add("午餐," + Integer.toString(formatDouble(lunchTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("午餐,未能提供");
                }
                position.add(navFoodConstant.GET_LUNCH_CODE);
                child.put(header.get(count), lunch); // Header, Child data
                count= count+1;
            }
            if (!dinner.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART){
                    header.add("晚餐,"+Integer.toString(formatDouble(dinnerTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("晚餐,未能提供");
                }
                position.add(navFoodConstant.GET_DINNER_CODE);
                child.put(header.get(count), dinner); // Header, Child data
                count= count+1;
            }
            if (!other.isEmpty()){
                if(displayPieChart == navFoodConstant.DISPLAY_PIE_CHART) {
                    header.add("小食," + Integer.toString(formatDouble(snackTotal))+"份碳水化合物");
                }
                if (displayPieChart == navFoodConstant.NOT_DISPLAY_PIE_CHART){
                    header.add("小食,未能提供");
                }
                position.add(navFoodConstant.GET_SNACK_CODE);
                child.put(header.get(count), other); // Header, Child data
                count= count+1;
            }

            temp.put("HEADER",header);
            temp.put("CHILD", child);
            temp.put("GROUP_POSITION", position);
            temp.put("TOTAL",formatDouble(total));
            temp.put("DISPLAY",displayPieChart);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }


    public class DbRecordStringId {
        String value;
        int id;

        public DbRecordStringId(String value, int id) {
            this.value = value;
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public String toString() {
            return this.value;
        }
    }

    public class DbRecordStringString {
        String value;
        String id;

        public DbRecordStringString(String value, String id) {
            this.value = value;
            this.id = id;
        }

        public String getId() {
            return id;
        }

        public String toString() {
            return this.value;
        }
    }

    public class foodRecord {
        private String value; // for display 碳水化合物
        private String foodName;
        private int foodRecordId;
        private String isFreeTextInput;

        public foodRecord(String foodName, String value, int foodRecordId, String isFreeTextInput) {
            this.value = value;
            this.foodName = foodName;
            this.foodRecordId = foodRecordId;
            this.isFreeTextInput = isFreeTextInput;
        }

        public int getFoodRecordId() {
            return foodRecordId;
        }
        public String getValue() {
            return value;
        }
        public String getFoodName() {
            return foodName;
        }
        public String getIsFreeTextInput(){return  isFreeTextInput;}

    }

    private double carboChangetoPart(double gram){
        return  gram / navFoodConstant.CA_GRAMS_PER_EACH;
    }

    public List<DbRecordStringString> getFoodPlaceList(){
        List<DbRecordStringString> foodPlaceList = new ArrayList<DbRecordStringString>();
        foodPlaceList.add(new DbRecordStringString("在家準備",navFoodConstant.GET_FOODPLACE_HOME_CODE));
        foodPlaceList.add(new DbRecordStringString("出外用膳",navFoodConstant.GET_FOODPLACE_OUTSIDE_CODE));
        foodPlaceList.add(new DbRecordStringString("外賣",navFoodConstant.GET_FOODPLACE_TAKEOUT_CODE));
        return foodPlaceList;
    }

    public List<DbRecordStringString> getFoodTypeList(){
        List<navFoodBiz.DbRecordStringString> foodTypeList = new ArrayList<navFoodBiz.DbRecordStringString>();
        foodTypeList.add(new DbRecordStringString("早餐",navFoodConstant.GET_BREAKFAST_CODE));
        foodTypeList.add(new DbRecordStringString("午餐",navFoodConstant.GET_LUNCH_CODE));
        foodTypeList.add(new DbRecordStringString("晚餐",navFoodConstant.GET_DINNER_CODE));
        foodTypeList.add(new DbRecordStringString("小食",navFoodConstant.GET_SNACK_CODE));
        return foodTypeList;
    }

    public List<String> getFoodUnitList(){
        List<String> foodUnitList = new ArrayList<String>();
        foodUnitList.add("個");
        foodUnitList.add("件");
        foodUnitList.add("碗");
        foodUnitList.add("碟");
        foodUnitList.add("條");
        foodUnitList.add("粒");
        return foodUnitList;
    }

    public void setAddFoodDetail(Activity activity, String result){
        System.out.println("setAddFoodDetail run!");
        try{
            //select Spinner to select which option
            setSelectionOnSpinner(activity,result);

            JSONArray JA = new JSONArray(result);
            //set data
            double[] nutrientValue = new double[navFoodConstant.nutrientSize];
            String foodName="";
            String foodUnit="";

            if (JA.length()!=0) {
                foodName = JA.getJSONObject(0).getString("FOOD_NAME");
                foodUnit = JA.getJSONObject(0).getString("FOOD_UNIT");
                nutrientValue[0] = JA.getJSONObject(0).getDouble("FOOD_CALORIE");
                nutrientValue[1] = JA.getJSONObject(0).getDouble("FOOD_CARBOHYDRATE");
                nutrientValue[2] = JA.getJSONObject(0).getDouble("FOOD_FAT");
                nutrientValue[3] = JA.getJSONObject(0).getDouble("FOOD_PROTEIN");
                nutrientValue[4] = JA.getJSONObject(0).getDouble("FOOD_SUGAR");
                nutrientValue[5] = JA.getJSONObject(0).getDouble("FOOD_CHOLESTEROL");
                nutrientValue[6] = JA.getJSONObject(0).getDouble("FOOD_SODIUM");
                nutrientValue[7] = JA.getJSONObject(0).getDouble("FOOD_FIBER");
            }

            //Set Food Name
            TextView foodNameTextView = (TextView) activity.findViewById(R.id.foodName);
            foodNameTextView.setText(foodName);

            //Set Food Unit
            TextView foodUnitTextView = (TextView) activity.findViewById(R.id.FoodUnit);
            foodUnitTextView.setText(foodUnit);

            //set nutrientList
            ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
            for (int i=0; i<navFoodConstant.nutrientSize; i++){
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("nutrientName",navFoodConstant.nutrientName[i]);
                map.put("nutrientValue",Integer.toString( formatDouble(nutrientValue[i]) ) );
                map.put("nutrientUnit",navFoodConstant.nutrientUnit[i]);
                mylist.add(map);
            }
            SimpleAdapter nutrinentAdapter = new SimpleAdapter(activity, mylist, R.layout.navfood_nutrient_list, new String[] {"nutrientName", "nutrientValue","nutrientUnit"}, new int[] {R.id.nutrientName,R.id.nutrientValue,R.id.nutrientUnit});
            ListView nutrientList = (ListView) activity.findViewById(R.id.nutrientList);
            nutrientList.setAdapter(nutrinentAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setAddFoodDetail(Activity activity, String result, String food_id, String food_date){
        System.out.println("setAddFoodDetail run!");
        try{

            //JSONArray JA = new JSONArray(result);
            food fooddb = new food(activity);
            List<HashMap<String, String>> JA = new ArrayList<>();
            HashMap<String, String> temp = fooddb.get(Integer.parseInt(food_id)).toHashMap();

            UserLocalStore ulocal = new UserLocalStore(activity);

            temp.put("BREAKFAST_START_TIME", ulocal.getLoggedInUser().breakfast_start);
            temp.put("BREAKFAST_END_TIME", ulocal.getLoggedInUser().breakfast_end);
            temp.put("LUNCH_START_TIME", ulocal.getLoggedInUser().lunch_start);
            temp.put("LUNCH_END_TIME", ulocal.getLoggedInUser().lunch_end);
            temp.put("DINNER_START_TIME", ulocal.getLoggedInUser().dinner_start);
            temp.put("DINNER_END_TIME", ulocal.getLoggedInUser().dinner_end);

            JA.add(temp);

            com.dmc.myapplication.Models.foodRecord foodRecordDB = new com.dmc.myapplication.Models.foodRecord(activity);
            List<HashMap<String, String>> frl = foodRecordDB.getAllByFoodDate(food_date);

            for (int z = 0; z < frl.size(); z++){
                JA.add(frl.get(z));
            }

            //select Spinner to select which option
            setSelectionOnSpinner(activity,result, JA);

            //set data
            double[] nutrientValue = new double[navFoodConstant.nutrientSize];
            String foodName="";
            String foodUnit="";

            if (JA.size()!=0) {
                foodName = JA.get(0).get("FOOD_NAME");
                foodUnit = JA.get(0).get("FOOD_UNIT");
                nutrientValue[0] = Double.parseDouble(JA.get(0).get("FOOD_CALORIE"));
                nutrientValue[1] = Double.parseDouble(JA.get(0).get("FOOD_CARBOHYDRATE"));
                nutrientValue[2] = Double.parseDouble(JA.get(0).get("FOOD_FAT"));
                nutrientValue[3] = Double.parseDouble(JA.get(0).get("FOOD_PROTEIN"));
                nutrientValue[4] = Double.parseDouble(JA.get(0).get("FOOD_SUGAR"));
                nutrientValue[5] = Double.parseDouble(JA.get(0).get("FOOD_CHOLESTEROL"));
                nutrientValue[6] = Double.parseDouble(JA.get(0).get("FOOD_SODIUM"));
                nutrientValue[7] = Double.parseDouble(JA.get(0).get("FOOD_SODIUM"));
            }

            //Set Food Name
            TextView foodNameTextView = (TextView) activity.findViewById(R.id.foodName);
            foodNameTextView.setText(foodName);

            //Set Food Unit
            TextView foodUnitTextView = (TextView) activity.findViewById(R.id.FoodUnit);
            foodUnitTextView.setText(foodUnit);

            //set nutrientList
            ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
            for (int i=0; i<navFoodConstant.nutrientSize; i++){
                HashMap<String, String> map = new HashMap<String, String>();
                map.put("nutrientName",navFoodConstant.nutrientName[i]);
                map.put("nutrientValue",Integer.toString( formatDouble(nutrientValue[i]) ) );
                map.put("nutrientUnit",navFoodConstant.nutrientUnit[i]);
                mylist.add(map);
            }
            SimpleAdapter nutrinentAdapter = new SimpleAdapter(activity, mylist, R.layout.navfood_nutrient_list, new String[] {"nutrientName", "nutrientValue","nutrientUnit"}, new int[] {R.id.nutrientName,R.id.nutrientValue,R.id.nutrientUnit});
            ListView nutrientList = (ListView) activity.findViewById(R.id.nutrientList);
            nutrientList.setAdapter(nutrinentAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public void setAddFreeTextFoodDetail(Activity activity, String result){
        setSelectionOnSpinner(activity, result);
    }

    public void setAddFreeTextFoodDetail(Activity activity, String result, String foodDate){
        List<HashMap<String, String>> JA = new ArrayList<>();
        HashMap<String, String> obj1 = new HashMap<>();
        UserLocalStore ulocal = new UserLocalStore(activity);

        obj1.put("BREAKFAST_START_TIME", ulocal.getLoggedInUser().breakfast_start);
        obj1.put("BREAKFAST_END_TIME", ulocal.getLoggedInUser().breakfast_end);
        obj1.put("LUNCH_START_TIME", ulocal.getLoggedInUser().lunch_start);
        obj1.put("LUNCH_END_TIME", ulocal.getLoggedInUser().lunch_end);
        obj1.put("DINNER_START_TIME", ulocal.getLoggedInUser().dinner_start);
        obj1.put("DINNER_END_TIME", ulocal.getLoggedInUser().dinner_end);

        JA.add(obj1);

        com.dmc.myapplication.Models.foodRecord foodRecordDB = new com.dmc.myapplication.Models.foodRecord(activity);
        List<HashMap<String, String>> frl = foodRecordDB.getAllByFoodDate(foodDate);

        for (int z = 0; z < frl.size(); z++){
            JA.add(frl.get(z));
        }


        setSelectionOnSpinner(activity, result, JA);
    }

    private void setSelectionOnSpinner(Activity activity, String result){
        try{
            //System.out.println("Candy Result="+result);
            JSONArray JA = new JSONArray(result);
            //set data
            TimeSettingObject timeObject=null;
            HashMap<String, String> foodPlaceList = new HashMap<String, String>();

            for (int i = 0; i < JA.length(); i++) {
                if (i==0) {
                    timeObject = new TimeSettingObject(
                            JA.getJSONObject(i).getString("BREAKFAST_START_TIME"), JA.getJSONObject(i).getString("BREAKFAST_END_TIME"),
                            JA.getJSONObject(i).getString("LUNCH_START_TIME"), JA.getJSONObject(i).getString("LUNCH_END_TIME"),
                            JA.getJSONObject(i).getString("DINNER_START_TIME"), JA.getJSONObject(i).getString("DINNER_END_TIME"));
                }
                else{
                    if (!foodPlaceList.containsKey(JA.getJSONObject(i).getString("FOOD_SESSION"))) {
                        foodPlaceList.put(JA.getJSONObject(i).getString("FOOD_SESSION"), JA.getJSONObject(i).getString("FOOD_PLACE"));
                    }else{
                        foodPlaceList.put(JA.getJSONObject(i).getString("FOOD_SESSION"),navFoodConstant.GET_REPEAT_FOOD_PLACE);
                    }
                }
            }

            // set open list (foodType and foodPlace Spinner)
            Spinner foodTypeSpinner = (Spinner) activity.findViewById(R.id.foodType);
            Spinner foodPlaceSpinner = (Spinner) activity.findViewById(R.id.foodPlace);
            String displayTime = (String)((TextView) activity.findViewById(R.id.navFoodRecordTime)).getText();
            if (timeObject.isBreakfast(new navFoodTime(displayTime))) {
                foodTypeSpinner.setSelection(0);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_BREAKFAST_CODE));
            } else if (timeObject.isLunch(new navFoodTime(displayTime))) {
                foodTypeSpinner.setSelection(1);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_LUNCH_CODE));
            } else if (timeObject.isDinner(new navFoodTime(displayTime))) {
                foodTypeSpinner.setSelection(2);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_DINNER_CODE));
            } else {
                foodTypeSpinner.setSelection(3);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_SNACK_CODE));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void setSelectionOnSpinner(Activity activity, String result, List<HashMap<String, String>> JA){
        try{
            //System.out.println("Candy Result="+result);
            //JSONArray JA = new JSONArray(result);
            //set data
            TimeSettingObject timeObject=null;
            HashMap<String, String> foodPlaceList = new HashMap<String, String>();

            UserLocalStore ulocal = new UserLocalStore(activity);

            for (int i = 0; i < JA.size(); i++) {
                if (i==0) {
                    timeObject = new TimeSettingObject(
                            ulocal.getLoggedInUser().breakfast_start, ulocal.getLoggedInUser().breakfast_end,
                            ulocal.getLoggedInUser().lunch_start, ulocal.getLoggedInUser().lunch_end,
                            ulocal.getLoggedInUser().dinner_start, ulocal.getLoggedInUser().dinner_end);
                }
                else{
                    if (!foodPlaceList.containsKey(JA.get(i).get("FOOD_SESSION"))) {
                        foodPlaceList.put(JA.get(i).get("FOOD_SESSION"), JA.get(i).get("FOOD_PLACE"));
                    }else{
                        foodPlaceList.put(JA.get(i).get("FOOD_SESSION"),navFoodConstant.GET_REPEAT_FOOD_PLACE);
                    }
                }
            }

            // set open list (foodType and foodPlace Spinner)
            Spinner foodTypeSpinner = (Spinner) activity.findViewById(R.id.foodType);
            Spinner foodPlaceSpinner = (Spinner) activity.findViewById(R.id.foodPlace);
            String displayTime = (String)((TextView) activity.findViewById(R.id.navFoodRecordTime)).getText();
            if (timeObject.isBreakfast(new navFoodTime(displayTime))) {
                foodTypeSpinner.setSelection(0);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_BREAKFAST_CODE));
            } else if (timeObject.isLunch(new navFoodTime(displayTime))) {
                foodTypeSpinner.setSelection(1);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_LUNCH_CODE));
            } else if (timeObject.isDinner(new navFoodTime(displayTime))) {
                foodTypeSpinner.setSelection(2);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_DINNER_CODE));
            } else {
                foodTypeSpinner.setSelection(3);
                foodPlaceSpinner.setSelection(openFoodPlacePosition(foodPlaceList, navFoodConstant.GET_SNACK_CODE));
            }

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    private void openExpandableList(Activity activity, List<String> position ,ExpandableListView expListView ){
        User user = new UserLocalStore(activity).getLoggedInUser();
        TimeSettingObject timeObject = new TimeSettingObject(user.breakfast_start,user.breakfast_end,user.lunch_start,user.lunch_end,user.dinner_start,user.dinner_end);
        String currentTime = new navFoodTime().getCurrentTimeFormal();
        if (timeObject.isBreakfast(new navFoodTime(currentTime))) {
            openAction(position, expListView, navFoodConstant.GET_BREAKFAST_CODE);
        } else if (timeObject.isLunch(new navFoodTime(currentTime))) {
            openAction(position, expListView, navFoodConstant.GET_LUNCH_CODE);
        } else if (timeObject.isDinner(new navFoodTime(currentTime))) {
            openAction(position, expListView, navFoodConstant.GET_DINNER_CODE);
        } else{
            openAction(position, expListView, navFoodConstant.GET_SNACK_CODE);
        }
    }

    private void openAction(List<String> position ,ExpandableListView expListView, String code){
        int getPosition = position.indexOf(code);
        if (getPosition != navFoodConstant.NOT_FOUND_IN_EXPANDABLE_LIST){
            expListView.expandGroup(getPosition);
        }
    }

    public int formatDouble(double value){
        //DecimalFormat df = new DecimalFormat(navFoodConstant.threeDecimalFormat);
        //value = Double.parseDouble(df.format(value));

        return (int) Math.round(value);
    }

    public class TimeSettingObject{
        private navFoodTime startB;
        private navFoodTime endB;
        private navFoodTime startL;
        private navFoodTime endL;
        private navFoodTime startD;
        private navFoodTime endD;
        public TimeSettingObject(String startB, String endB, String startL, String endL, String startD, String endD){
            this.startB = new navFoodTime(startB);
            this.endB = new navFoodTime(endB);
            this.startL = new navFoodTime(startL);
            this.endL = new navFoodTime(endL);
            this.startD = new navFoodTime(startD);
            this.endD = new navFoodTime(endD);
        }

        public boolean isBreakfast(navFoodTime current){
            return compare(this.startB,this.endB,current);
        }

        public boolean isLunch(navFoodTime current){
            return compare(this.startL,this.endL,current);
        }

        public boolean isDinner(navFoodTime current){
            return compare(this.startD,this.endD,current);
        }

        private boolean compare(navFoodTime start, navFoodTime end, navFoodTime current){
            //hour in the range (not include =)
            if (start.getHour() < current.getHour() && current.getHour() < end.getHour()){
                return true;
            }

            // start hour = end hour and start min < = now end min
            if (start.getHour() == current.getHour() && current.getHour() == end.getHour()){
                if (start.getMin()<= current.getMin() && current.getMin() <= end.getMin()){
                    return true;
                }else{
                    return false;
                }
            }

            // start hour = current hour but end hour!= current hour than start min< = current min
            if (start.getHour() == current.getHour() && start.getMin() <= current.getMin()){
                return true;
            }

            // end hour = current hour but start hour!= current hour than current min< = end min
            if (end.getHour() == current.getHour() && current.getMin() <= end.getMin()){
                return true;
            }

            return false;
        }
    }

    private int openFoodPlacePosition( HashMap<String,String> hashMap, String code) {
        int position = 0;
        if (hashMap.containsKey(code) && !hashMap.get(code).equals(navFoodConstant.GET_REPEAT_FOOD_PLACE)) {
            if (hashMap.get(code).equals(navFoodConstant.GET_FOODPLACE_HOME_CODE))
                position = 0;
            if (hashMap.get(code).equals(navFoodConstant.GET_FOODPLACE_OUTSIDE_CODE))
                position = 1;
            if (hashMap.get(code).equals(navFoodConstant.GET_FOODPLACE_TAKEOUT_CODE))
                position = 2;
        }
        return  position;
    }

    public void setFoodCommonList(Activity activity, String result) {
        try {
            if (result.isEmpty()) {
                String[] mobileArray = {"沒有任何常用的食物"};
                ArrayAdapter adapter = new ArrayAdapter<String>(activity, R.layout.navfood_add_search_list, R.id.itemName, mobileArray);
                ListView listView = (ListView) activity.findViewById(R.id.commonListView);
                listView.setAdapter(adapter);
                listView.setSelector(android.R.color.transparent);
                listView.setOnItemClickListener(null);

            }else{
                //System.out.println("Candy test=" + result);
                JSONArray JA = new JSONArray(result);
                result = null; //clear object
                List<DbRecordStringId> mobileArray = new ArrayList<DbRecordStringId>();
                List<String> foodImage = new ArrayList<String>();
                for (int i = 0; i < JA.length(); i++) {
                    mobileArray.add(new DbRecordStringId(JA.getJSONObject(i).getString("FOOD_NAME"), JA.getJSONObject(i).getInt("FOOD_ID")));
                    foodImage.add(JA.getJSONObject(i).getString("PHOTO_IMAGE"));

                    //TODO: Photo
                    //imageResId[i] = findImageIdFromRes(JA.getJSONObject(i).getString("PHOTO_IMAGE"), activity);
                   // if (JA.getJSONObject(i).getString("PHOTO_IMAGE").equals(navFoodConstant.GET_NOT_IMAGE_CODE)){
                    //}else{
                        //byte[] image = Base64.decode(JA.getJSONObject(i).getString("PHOTO_IMAGE"), Base64.DEFAULT);
                        //foodImage[i] = BitmapFactory.decodeByteArray(image, 0, image.length);
                        //BitmapFactory.decodeStream()
                        //image = null; //clear object
                    //}
                }
                JA = null; //clear object
                navFoodAddCommonListViewAdapter adapter = new navFoodAddCommonListViewAdapter(activity, mobileArray, foodImage);
                mobileArray = null;  //clear object
                foodImage=null; //clear object
                ListView listView = (ListView) activity.findViewById(R.id.commonListView);
                listView.setAdapter(adapter);
                adapter = null; //clear object
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setFoodCommonListNew(final Activity activity, String result) {
        /*Thread a = new Thread(new Runnable() {
            @Override
            public void run() {*/
                foodUserCommon foodUserCommondb = new foodUserCommon(activity);
                List<foodUserCommon.foodUserCommon_class> objs = foodUserCommondb.getAll();
                try {
                    if (objs.size() <= 0) {
                        String[] mobileArray = {"沒有任何常用的食物"};
                        ArrayAdapter adapter = new ArrayAdapter<String>(activity, R.layout.navfood_add_search_list, R.id.itemName, mobileArray);
                        //ListView listView = (ListView) activity.findViewById(R.id.commonListView);
                        ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);
                        listView.setAdapter(adapter);
                        listView.setSelector(android.R.color.transparent);
                        listView.setOnItemClickListener(null);

                    }else{
                        //System.out.println("Candy test=" + result);
                        //JSONArray JA = new JSONArray(result);
                        //result = null; //clear object
                        List<DbRecordStringId> mobileArray = new ArrayList<DbRecordStringId>();
                        List<String> foodImage = new ArrayList<String>();
                        for (int i = 0; i < objs.size(); i++) {
                            food.food_class foodobj = objs.get(i).getFoodClass(activity);
                            mobileArray.add(new DbRecordStringId(foodobj.getFOOD_NAME(), foodobj.getFOOD_ID()));
                            //foodImage.add(navFoodConstant.GET_NOT_IMAGE_CODE);
                            if (foodobj.getPHOTO_PATH() != null){
                                foodImage.add(i, foodobj.getPHOTO_PATH());
                            }else{
                                foodImage.add(i, navFoodConstant.GET_NOT_IMAGE_CODE);
                            }

                            //TODO: Photo
                            //imageResId[i] = findImageIdFromRes(JA.getJSONObject(i).getString("PHOTO_IMAGE"), activity);
                            //if (JA.getJSONObject(i).getString("PHOTO_IMAGE").equals(navFoodConstant.GET_NOT_IMAGE_CODE)){
                            //}else{
                            //byte[] image = Base64.decode(JA.getJSONObject(i).getString("PHOTO_IMAGE"), Base64.DEFAULT);
                            //foodImage[i] = BitmapFactory.decodeByteArray(image, 0, image.length);
                            //BitmapFactory.decodeStream()
                            //image = null; //clear object
                            //}
                        }
                        objs = null; //clear object
                        navFoodAddCommonListViewAdapter adapter = new navFoodAddCommonListViewAdapter(activity, mobileArray, foodImage);
                        mobileArray = null;  //clear object
                        foodImage=null; //clear object
                        ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);
                        //sleep(900);
                        listView.setAdapter(adapter);
                        adapter = null; //clear object
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    //System.out.println(e.getMessage());
                }
            //}
        //});


       /* try {
            a.start();
           a.wait();
        }catch (Exception e){
            e.printStackTrace();
        }*/
    }

    private int findImageIdFromRes(String imagePath,Activity activity){
        int imageId=activity.getResources().getIdentifier("ic_photo_black_48dp", "drawable", activity.getPackageName());
        try{
                int resID = activity.getResources().getIdentifier(imagePath, "drawable", activity.getPackageName());
        //System.out.println("Candy " + imagePathArray[i] + " redid=" + resID);
        if (resID != 0) {
            imageId = resID;
        }
    }catch (Exception e){
        e.printStackTrace();
    }
    return imageId;
}

    public void setFoodSearchResult(Activity activity, String result){
        //System.out.println("Candy result =" + result);
        if (result.isEmpty()){
            //clear item in listView
            ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);
            listView.setAdapter(null);

            // textView foodNotFound
            TextView foodNotFoundTextView = (TextView) activity.findViewById(R.id.foodNotFound);
            foodNotFoundTextView.setVisibility(View.VISIBLE);

            // free text button
            Button navFoodFreeAdd = (Button)activity.findViewById(R.id.navFoodFreeAdd);
            navFoodFreeAdd.setVisibility(View.VISIBLE);
            Button navFoodOtherAdd = (Button) activity.findViewById(R.id.navFoodOtherAdd);
            navFoodOtherAdd.setVisibility(View.VISIBLE);
        }
        else{
            try{
                JSONArray JA = new JSONArray(result);
                List<DbRecordStringId> foodSearchList = new ArrayList<DbRecordStringId>();
                for (int i = 0; i < JA.length(); i++) {
                    foodSearchList.add(new DbRecordStringId(JA.getJSONObject(i).getString("FOOD_NAME"), JA.getJSONObject(i).getInt("FOOD_ID")));
                }
                ArrayAdapter foodSearchAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.navfood_add_search_list, R.id.itemName, foodSearchList);
                ListView searchResultListView = (ListView) activity.findViewById(R.id.searchedFoodList);
                searchResultListView.setAdapter(foodSearchAdapter);

                // textView foodNotFound
                TextView foodNotFoundTextView = (TextView) activity.findViewById(R.id.foodNotFound);
                foodNotFoundTextView.setVisibility(View.GONE);

                // free text button
                Button navFoodFreeAdd = (Button)activity.findViewById(R.id.navFoodFreeAdd);
                navFoodFreeAdd.setVisibility(View.GONE);
                Button navFoodOtherAdd = (Button) activity.findViewById(R.id.navFoodOtherAdd);
                navFoodOtherAdd.setVisibility(View.GONE);


            }catch (Exception e ){
                e.printStackTrace();
            }
        }
    }

    public void setFoodSearchResultNew(Activity activity, String result, String keywords){
        //System.out.println("Candy result =" + result);
        food fdDb = new food(activity);
        List<HashMap<String, String>> JA = fdDb.search(keywords);
        if (JA.size() <= 0){
            //clear item in listView
            ListView listView = (ListView) activity.findViewById(R.id.searchedFoodList);
            listView.setAdapter(null);

            // textView foodNotFound
            TextView foodNotFoundTextView = (TextView) activity.findViewById(R.id.foodNotFound);
            foodNotFoundTextView.setVisibility(View.VISIBLE);

            // free text button
            Button navFoodFreeAdd = (Button)activity.findViewById(R.id.navFoodFreeAdd);
            navFoodFreeAdd.setVisibility(View.VISIBLE);
            Button navFoodOtherAdd = (Button) activity.findViewById(R.id.navFoodOtherAdd);
            navFoodOtherAdd.setVisibility(View.VISIBLE);
        }
        else{
            try{
                //JSONArray JA = new JSONArray(result);

                List<DbRecordStringId> foodSearchList = new ArrayList<DbRecordStringId>();
                for (int i = 0; i < JA.size(); i++) {
                    foodSearchList.add(new DbRecordStringId(JA.get(i).get("FOOD_NAME"), Integer.parseInt(JA.get(i).get("FOOD_ID"))));
                }
                ArrayAdapter foodSearchAdapter = new ArrayAdapter<DbRecordStringId>(activity, R.layout.navfood_add_search_list, R.id.itemName, foodSearchList);
                ListView searchResultListView = (ListView) activity.findViewById(R.id.searchedFoodList);
                searchResultListView.setAdapter(foodSearchAdapter);

                // textView foodNotFound
                TextView foodNotFoundTextView = (TextView) activity.findViewById(R.id.foodNotFound);
                foodNotFoundTextView.setVisibility(View.GONE);

                // free text button
                Button navFoodFreeAdd = (Button)activity.findViewById(R.id.navFoodFreeAdd);
                navFoodFreeAdd.setVisibility(View.GONE);
                Button navFoodOtherAdd = (Button) activity.findViewById(R.id.navFoodOtherAdd);
                navFoodOtherAdd.setVisibility(View.GONE);


            }catch (Exception e ){
                e.printStackTrace();
            }
        }
    }

    public void setEditFreeTextFoodDetail(Activity activity, String result){
        System.out.println("Candy result edit free text food detail="+result);
        try{
            JSONArray JA = new JSONArray(result);
            if(0<JA.length()){
                // set common session (free text input and food record from DB
                setEditCommonSession(activity, JA);

                // food unit
                Spinner inputFoodUnit = (Spinner) activity.findViewById(R.id.inputFoodUnit);
                List<String> foodUnitList = getFoodUnitList();
                inputFoodUnit.setSelection(foodUnitList.indexOf(JA.getJSONObject(0).getString("FOOD_UNIT")));

                // image path
                LinearLayout addPhotoSession = (LinearLayout) activity.findViewById(R.id.addPhotoSession);
                if (!JA.getJSONObject(0).getString("PHOTO_PATH").equals("null")){
                    addPhotoSession.setVisibility(View.VISIBLE);
                    addPhotoSession.setContentDescription(JA.getJSONObject(0).getString("PHOTO_PATH"));
                    if(!JA.getJSONObject(0).getString("PHOTO_IMAGE").equals("null")){
                        ImageView  foodImageView = (ImageView) activity.findViewById(R.id.addPhoto);
                        byte[] image = Base64.decode(JA.getJSONObject(0).getString("PHOTO_IMAGE"), Base64.DEFAULT);
                        Bitmap foodImageBitmap = imageTool.rotateImage90(BitmapFactory.decodeByteArray(image, 0, image.length));
                        foodImageView.setImageBitmap(foodImageBitmap);
                    }
                }else{
                    addPhotoSession.setVisibility(View.GONE);
                }
                //image
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
    }

    public void setEditFreeTextFoodDetail(Activity activity, String result, String foodRecordId){
        System.out.println("Candy result edit free text food detail="+result);
        try{
            //JSONArray JA = new JSONArray(result);
            com.dmc.myapplication.Models.foodRecord foodRecordDB = new com.dmc.myapplication.Models.foodRecord(activity);
            com.dmc.myapplication.Models.foodRecord.foodRecord_class foodRecordObj = foodRecordDB.get(Integer.parseInt(foodRecordId));

            if(foodRecordObj != null){
                // set common session (free text input and food record from DB
                setEditCommonSession(activity, foodRecordObj);

                // food unit
                Spinner inputFoodUnit = (Spinner) activity.findViewById(R.id.inputFoodUnit);
                List<String> foodUnitList = getFoodUnitList();
                inputFoodUnit.setSelection(foodUnitList.indexOf(foodRecordObj.getFOOD_UNIT()));

                // image path
                LinearLayout addPhotoSession = (LinearLayout) activity.findViewById(R.id.addPhotoSession);

                //System.out.println(">>>>--->>>> this food-->"+foodRecordObj.getFOOD_NAME());
                //System.out.println(">>>>--->>>> this-->"+foodRecordObj.getPHOTO_PATH());
                if (!foodRecordObj.getPHOTO_PATH().equals("null") && !foodRecordObj.getPHOTO_PATH().equals("")&& !foodRecordObj.getPHOTO_PATH().equals("NA")){
                    addPhotoSession.setVisibility(View.VISIBLE);
                    addPhotoSession.setContentDescription(foodRecordObj.getPHOTO_PATH());


                    //TODO: Get Photo from Server
                    /*if(!JA.getJSONObject(0).getString("PHOTO_IMAGE").equals("null")){
                        ImageView  foodImageView = (ImageView) activity.findViewById(R.id.addPhoto);
                        byte[] image = Base64.decode(JA.getJSONObject(0).getString("PHOTO_IMAGE"), Base64.DEFAULT);
                        Bitmap foodImageBitmap = imageTool.rotateImage90(BitmapFactory.decodeByteArray(image, 0, image.length));
                        foodImageView.setImageBitmap(foodImageBitmap);
                    }*/
                }else{
                    addPhotoSession.setVisibility(View.GONE);
                }
                //image
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
    }

    public void setEditFoodDetail(Activity activity, String result){
        //System.out.println("Candy setEditFoodDetail=" + result);
        try{
            JSONArray JA = new JSONArray(result);
            if(0<JA.length()){
                // set common session (free text input and food record from DB
                setEditCommonSession(activity, JA);

                // food unit
                TextView FoodUnit = (TextView) activity.findViewById(R.id.FoodUnit);
                FoodUnit.setText(JA.getJSONObject(0).getString("FOOD_UNIT"));

                // set nutrientList
                int foodValue = JA.getJSONObject(0).getInt("FOOD_QUANTITY");
                double[] nutrientValue = new double[navFoodConstant.nutrientSize];
                nutrientValue[0] = JA.getJSONObject(0).getDouble("FOOD_CALORIE");
                nutrientValue[1] = JA.getJSONObject(0).getDouble("FOOD_CARBOHYDRATE");
                nutrientValue[2] = JA.getJSONObject(0).getDouble("FOOD_FAT");
                nutrientValue[3] = JA.getJSONObject(0).getDouble("FOOD_PROTEIN");
                nutrientValue[4] = JA.getJSONObject(0).getDouble("FOOD_SUGAR");
                nutrientValue[5] = JA.getJSONObject(0).getDouble("FOOD_CHOLESTEROL");
                nutrientValue[6] = JA.getJSONObject(0).getDouble("FOOD_SODIUM");
                nutrientValue[7] = JA.getJSONObject(0).getDouble("FOOD_FIBER");
                ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
                for (int i=0; i<navFoodConstant.nutrientSize; i++){
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("nutrientName",navFoodConstant.nutrientName[i]);
                    map.put("nutrientValue",Integer.toString( formatDouble(nutrientValue[i] * foodValue) ) );
                    map.put("nutrientUnit",navFoodConstant.nutrientUnit[i]);
                    mylist.add(map);
                }
                SimpleAdapter nutrinentAdapter = new SimpleAdapter(activity, mylist, R.layout.navfood_nutrient_list, new String[] {"nutrientName", "nutrientValue","nutrientUnit"}, new int[] {R.id.nutrientName,R.id.nutrientValue,R.id.nutrientUnit});
                ListView nutrientList = (ListView) activity.findViewById(R.id.nutrientList);
                nutrientList.setAdapter(nutrinentAdapter);
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
    }

    public void setEditFoodDetail(Activity activity, String result, String RecordID){
        //System.out.println("Candy setEditFoodDetail=" + result);
        try{
            //JSONArray JA = new JSONArray(result);
            com.dmc.myapplication.Models.foodRecord foodRecordDB = new com.dmc.myapplication.Models.foodRecord(activity);
            com.dmc.myapplication.Models.foodRecord.foodRecord_class RecordObj = foodRecordDB.get(Integer.parseInt(RecordID));
            if(RecordObj != null){
                // set common session (free text input and food record from DB
                setEditCommonSession(activity, RecordObj);

                // food unit
                TextView FoodUnit = (TextView) activity.findViewById(R.id.FoodUnit);
                FoodUnit.setText(RecordObj.getFOOD_UNIT());

                    food foodDB = new food(activity);
                    food.food_class foodObj = foodDB.get(RecordObj.getFOOD_ID());
                    // set nutrientList
                    int foodValue = RecordObj.getFOOD_QUANTITY();
                    double[] nutrientValue = new double[navFoodConstant.nutrientSize];
                    nutrientValue[0] = RecordObj.getFOOD_CALORIE();
                    nutrientValue[1] = RecordObj.getFOOD_CARBOHYDRATE();
                    nutrientValue[2] = RecordObj.getFOOD_FAT();
                    nutrientValue[3] = RecordObj.getFOOD_PROTEIN();
                    nutrientValue[4] = foodObj.getFOOD_SUGAR() * foodValue;
                    nutrientValue[5] = foodObj.getFOOD_CHOLESTEROL() * foodValue;
                    nutrientValue[6] = foodObj.getFOOD_SODIUM() * foodValue;
                    nutrientValue[7] = foodObj.getFOOD_FIBER() * foodValue;


                ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
                for (int i=0; i<navFoodConstant.nutrientSize; i++){
                    HashMap<String, String> map = new HashMap<String, String>();
                    map.put("nutrientName",navFoodConstant.nutrientName[i]);
                    map.put("nutrientValue",Integer.toString( formatDouble(nutrientValue[i] * foodValue) ) );
                    map.put("nutrientUnit",navFoodConstant.nutrientUnit[i]);
                    mylist.add(map);
                }
                SimpleAdapter nutrinentAdapter = new SimpleAdapter(activity, mylist, R.layout.navfood_nutrient_list, new String[] {"nutrientName", "nutrientValue","nutrientUnit"}, new int[] {R.id.nutrientName,R.id.nutrientValue,R.id.nutrientUnit});
                ListView nutrientList = (ListView) activity.findViewById(R.id.nutrientList);
                nutrientList.setAdapter(nutrinentAdapter);
            }
        }catch (Exception e ){
            e.printStackTrace();
        }
    }

    private void setEditCommonSession(Activity activity, JSONArray JA) throws JSONException {

        // set Food Date
        navFoodTime navDateTool = new navFoodTime();
        TextView navFoodDate = (TextView)activity.findViewById(R.id.navFoodRecordDate);
        navFoodDate.setText(navDateTool.getUIDateFormal(JA.getJSONObject(0).getString("FOOD_DATE")));

        //set Time
        navFoodTime navTimeTool =  new navFoodTime(JA.getJSONObject(0).getString("FOOD_TIME"));
        TextView navFoodTimeTextView = (TextView)activity.findViewById(R.id.navFoodRecordTime);
        navFoodTimeTextView.setText(navTimeTool.getTimeString(navTimeTool.getHour()) + ":" + navTimeTool.getTimeString(navTimeTool.getMin()));

        //set Food Name
        TextView foodName = (TextView) activity.findViewById(R.id.foodName);
        foodName.setText(JA.getJSONObject(0).getString("FOOD_NAME"));

        //set food value
        TextView foodValue = (TextView) activity.findViewById(R.id.foodValue);
        foodValue.setText( JA.getJSONObject(0).getString("FOOD_QUANTITY"));

        // set Food Stype
        String foodType = JA.getJSONObject(0).getString("FOOD_SESSION");
        Spinner foodTypeSpinner = (Spinner) activity.findViewById(R.id.foodType);
        if (foodType.equals(navFoodConstant.GET_BREAKFAST_CODE)) {
            foodTypeSpinner.setSelection(0);
        }
        if (foodType.equals(navFoodConstant.GET_LUNCH_CODE)) {
            foodTypeSpinner.setSelection(1);
        }
        if (foodType.equals(navFoodConstant.GET_DINNER_CODE)) {
            foodTypeSpinner.setSelection(2);
        }
        if (foodType.equals(navFoodConstant.GET_SNACK_CODE)) {
            foodTypeSpinner.setSelection(3);
        }

        // set Food Place
        String foodPlace = JA.getJSONObject(0).getString("FOOD_PLACE");
        Spinner foodPlaceSpinner = (Spinner) activity.findViewById(R.id.foodPlace);
        if(foodPlace.equals(navFoodConstant.GET_FOODPLACE_HOME_CODE)){
            foodPlaceSpinner.setSelection(0);
        }
        if(foodPlace.equals(navFoodConstant.GET_FOODPLACE_OUTSIDE_CODE)){
            foodPlaceSpinner.setSelection(1);
        }
        if(foodPlace.equals(navFoodConstant.GET_FOODPLACE_TAKEOUT_CODE)){
            foodPlaceSpinner.setSelection(2);
        }

    }

    private void setEditCommonSession(Activity activity, com.dmc.myapplication.Models.foodRecord.foodRecord_class JA) {

        // set Food Date
        navFoodTime navDateTool = new navFoodTime();
        TextView navFoodDate = (TextView)activity.findViewById(R.id.navFoodRecordDate);
        navFoodDate.setText(navDateTool.getUIDateFormal(JA.getFOOD_DATE()));

        //set Time
        navFoodTime navTimeTool =  new navFoodTime(JA.getFOOD_TIME());
        TextView navFoodTimeTextView = (TextView)activity.findViewById(R.id.navFoodRecordTime);
        navFoodTimeTextView.setText(navTimeTool.getTimeString(navTimeTool.getHour()) + ":" + navTimeTool.getTimeString(navTimeTool.getMin()));

        //set Food Name
        TextView foodName = (TextView) activity.findViewById(R.id.foodName);
        foodName.setText(JA.getFOOD_NAME());

        //set food value
        TextView foodValue = (TextView) activity.findViewById(R.id.foodValue);
        foodValue.setText( String.valueOf(JA.getFOOD_QUANTITY()));

        // set Food Stype
        String foodType = JA.getFOOD_SESSION();
        Spinner foodTypeSpinner = (Spinner) activity.findViewById(R.id.foodType);
        if (foodType.equals(navFoodConstant.GET_BREAKFAST_CODE)) {
            foodTypeSpinner.setSelection(0);
        }
        if (foodType.equals(navFoodConstant.GET_LUNCH_CODE)) {
            foodTypeSpinner.setSelection(1);
        }
        if (foodType.equals(navFoodConstant.GET_DINNER_CODE)) {
            foodTypeSpinner.setSelection(2);
        }
        if (foodType.equals(navFoodConstant.GET_SNACK_CODE)) {
            foodTypeSpinner.setSelection(3);
        }

        // set Food Place
        String foodPlace = JA.getFOOD_PLACE();
        Spinner foodPlaceSpinner = (Spinner) activity.findViewById(R.id.foodPlace);
        if(foodPlace.equals(navFoodConstant.GET_FOODPLACE_HOME_CODE)){
            foodPlaceSpinner.setSelection(0);
        }
        if(foodPlace.equals(navFoodConstant.GET_FOODPLACE_OUTSIDE_CODE)){
            foodPlaceSpinner.setSelection(1);
        }
        if(foodPlace.equals(navFoodConstant.GET_FOODPLACE_TAKEOUT_CODE)){
            foodPlaceSpinner.setSelection(2);
        }

    }

    public void setDrawPieChart(Activity activity, String result){
        //System.out.println("Candy testing result="+result);
        double calorieSum=0;
        double carbohydrateSum =0;
        double proteinSum =0;
        double fatSum =0;

        try{
            JSONArray JA = new JSONArray(result);
            if(0<JA.length()) {
                calorieSum = JA.getJSONObject(0).getDouble("SUM_FOOD_CALORIE");
                carbohydrateSum = JA.getJSONObject(0).getDouble("SUM_FOOD_CARBOHYDRATE");
                proteinSum = JA.getJSONObject(0).getDouble("SUM_FOOD_PROTEIN");
                fatSum = JA.getJSONObject(0).getDouble("SUM_FOOD_FAT");

            }
        }catch(JSONException e){
            e.printStackTrace();
        }

        ArrayList<Entry> percentagePerEach = generateDietaryIntakes(calorieSum,carbohydrateSum,proteinSum,fatSum);
        ArrayList<String> itemInPieChart = new ArrayList<String>();
        itemInPieChart.add("碳水化合物");
        itemInPieChart.add("蛋白質");
        itemInPieChart.add("脂肪");
        //itemInPieChart.add("酒精");

        setPieChartDetail(activity,percentagePerEach, itemInPieChart);

    }

    public void setDrawPieChart(Activity activity, String result, String foodDate){
        //System.out.println("Candy testing result="+result);
        double calorieSum=0;
        double carbohydrateSum =0;
        double proteinSum =0;
        double fatSum =0;

        try{
            //JSONArray JA = new JSONArray(result);
            com.dmc.myapplication.Models.foodRecord frDB = new com.dmc.myapplication.Models.foodRecord(activity);
            List<HashMap<String, String>> JA = frDB.getAllByFoodDate(foodDate);

                System.out.println("setDrawPieChart JA.size="+JA.size());
                for (int x = 0; x < JA.size(); x ++){
                    if (JA.get(x).get("FOOD_ID") != null || JA.get(x).get("FOOD_ID") != ""){
                        System.out.println("setDrawPieChart JA.x="+x);
                        System.out.println("setDrawPieChart JA.x.FOOD_CALORIE="+JA.get(x).get("FOOD_CALORIE"));
                        calorieSum = calorieSum + Double.parseDouble(JA.get(x).get("FOOD_CALORIE"));
                        carbohydrateSum = carbohydrateSum + Double.parseDouble(JA.get(x).get("FOOD_CARBOHYDRATE"));
                        proteinSum = proteinSum + Double.parseDouble(JA.get(x).get("FOOD_PROTEIN"));
                        fatSum = fatSum + Double.parseDouble(JA.get(x).get("FOOD_FAT"));
                    }

                }


        }catch(Exception e){
            e.printStackTrace();
        }

        System.out.println("calorieSum = "+calorieSum);
        ArrayList<Entry> percentagePerEach = generateDietaryIntakes(calorieSum,carbohydrateSum,proteinSum,fatSum);
        ArrayList<String> itemInPieChart = new ArrayList<String>();
        itemInPieChart.add("碳水化合物");
        itemInPieChart.add("蛋白質");
        itemInPieChart.add("脂肪");
        //itemInPieChart.add("酒精");

        setPieChartDetail(activity,percentagePerEach, itemInPieChart);

    }
    private ArrayList<Entry>  generateDietaryIntakes(double calorieSum,double carbohydrateSum, double proteinSum, double fatSum){
        // IMPORTANT: In a PieChart, no values (Entry) should have the same
        // xIndex (even if from different DataSets), since no values can be
        // drawn above each other.
        ArrayList<Entry> percentagePerEach = new ArrayList<Entry>();
        float carbohydratePercentage =0;
        float proteinPercentage =0;
        float fatPercentage =0;

        try {
            carbohydratePercentage = (float) (carbohydrateSum * navFoodConstant.KCAL_CARBOHYDRATE / calorieSum) * 100;
            proteinPercentage =  (float) (proteinSum * navFoodConstant.KCAL_PROTEIN / calorieSum) * 100;
            fatPercentage =  (float) (fatSum * navFoodConstant.KCAL_FAT / calorieSum) * 100;
        }catch (ArithmeticException Ae){
            carbohydratePercentage =0;
            proteinPercentage =0;
            fatPercentage =0;
        }

        //System.out.println("calorieSum="+calorieSum);
        //System.out.println("carbohydrateSum="+carbohydrateSum);
        //System.out.println("proteinSum="+proteinSum);
        //System.out.println("fatSum="+fatSum);
        //System.out.println("carbohydratePercentage="+carbohydratePercentage);
        //System.out.println("proteinPercentage=" +proteinPercentage);
        //System.out.println("fatPercentage="+fatPercentage);

        percentagePerEach.add(new Entry(carbohydratePercentage,0)); //碳水化合物
        percentagePerEach.add(new Entry(proteinPercentage,1));//蛋白質
        percentagePerEach.add(new Entry(fatPercentage, 2)); //脂肪
        //percentagePerEach.add(new Entry(3, 3)); //酒精
        return percentagePerEach;
    }

    private void setPieChartDetail(Activity activity,ArrayList<Entry> percentagePerEach, ArrayList<String> itemInPieChart) {
        PieChart pieChart = (PieChart)activity.findViewById(R.id.pieChart);

        //mChart.setUsePercentValues(true);
        // 設定圓形圖的位置 (left, top, right,  bottom) e.g. (0, 0, 100, 0) = 留空RIGHT 的位置100
        //mChart.setExtraOffsets(10, 10, 10, 10);
        //mChart.setRotationAngle(0);

        // 設定 Description (在RIGHT 下角)
        pieChart.setDescription("這分析圖表只供參考");
        //pieChart.setDescriptionTextSize(24);

        // 設定 需要圓形圖中間的位置 中空 (即是 一個大圓內有一個小圓) TRUE = 要留小圓
        pieChart.setDrawHoleEnabled(false);
        // enable rotation of the chart by touch
        pieChart.setRotationEnabled(false);

        // 設定各顏色代表的是什麼  ( 口 碳水化合物, 口 蛋白質, 口 脂肪)
        Legend legend = pieChart.getLegend();
        legend.setPosition(Legend.LegendPosition.ABOVE_CHART_LEFT);
        legend.setTextSize(15);


        // 設定圓形圖內   每個扇形的 百分比 (e.g. 碳水化合物 60%, 脂肪 30%, 蛋白質 10%)    // value , Unit
        PieDataSet dataSet = new PieDataSet(percentagePerEach, "");

        //設定 扇形之間的空位，0為最少(即是沒有)
        dataSet.setSliceSpace(2f);

        // 設定 圓形圖的大小 0f為最大
        dataSet.setSelectionShift(0f);

        // 設定扇形的顏色
        ArrayList<Integer> colors = new ArrayList<Integer>();
        //colors.add(ColorTemplate.getHoloBlue());
        for (int c : ColorTemplate.VORDIPLOM_COLORS) {
            colors.add(c);
        }
        dataSet.setColors(colors);

        // 設定扇形的名稱 (碳水化合物, 脂肪, 蛋白質)
        PieData data = new PieData(itemInPieChart, dataSet);

        // 設定資料 顯示的方法
        data.setValueFormatter(new PercentFormatter());
        // 圖表上的字型大小
        data.setValueTextSize(11f);
        // 圖表上的顏色
        data.setValueTextColor(Color.BLACK);
        // set Data
        pieChart.setData(data);
        //強制重繪View
        pieChart.invalidate();

    }

}
