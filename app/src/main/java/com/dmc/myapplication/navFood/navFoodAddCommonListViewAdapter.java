package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.v4.util.LruCache;
import android.support.v7.widget.RecyclerView;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.google.gson.Gson;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.*;
import java.net.URLEncoder;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.*;

import static com.dmc.myapplication.systemConstant.tempbitmap;

public class navFoodAddCommonListViewAdapter extends ArrayAdapter<navFoodBiz.DbRecordStringId> {

    private LruCache<String, Bitmap> mMemoryCache;
    private DiskLruCache mDiskLruCache;
    private Set<BitmapWorkerTask> taskCollection;


    private Activity activity;
    private List<navFoodBiz.DbRecordStringId> itemname;
    private List<String> imgid;

    public navFoodAddCommonListViewAdapter(Activity  activity, List<navFoodBiz.DbRecordStringId> itemname, List<String> imgid) {
        super(activity, R.layout.navfood_add_common_list, itemname);
        this.activity=activity;
        this.itemname=itemname;
        this.imgid=imgid;

        taskCollection = new HashSet<BitmapWorkerTask>();
        int maxMemory = (int) Runtime.getRuntime().maxMemory();
        int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                return bitmap.getByteCount();
            }
        };


        try {
            // 获取图片缓存路径
            File cacheDir = getDiskCacheDir(activity, "thumb");
            if (!cacheDir.exists()) {
                cacheDir.mkdirs();
            }
            // 创建DiskLruCache实例，初始化缓存数据
            mDiskLruCache = DiskLruCache
                    .open(cacheDir, getAppVersion(getContext()), 1, 10 * 1024 * 1024);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public int getAppVersion(Context context) {
        try {
            PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(),
                    0);
            return info.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return 1;
    }

    public void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemoryCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    public Bitmap getBitmapFromMemoryCache(String key) {
        return mMemoryCache.get(key);
    }

    public void loadBitmaps(ImageView imageView, String imageUrl) {
        try {
            Bitmap bitmap = getBitmapFromMemoryCache(imageUrl+".jpg");
            if (bitmap == null) {
                BitmapWorkerTask task = new BitmapWorkerTask();
                taskCollection.add(task);
                task.execute(imageUrl);
            } else {
                if (imageView != null && bitmap != null) {
                    imageView.setImageBitmap(bitmap);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Bitmap loadBitmaps(final String imageUrl) {
        try {
            Bitmap bitmap = navFoodImageFactory.getImage(getContext(), imageUrl);

            if (bitmap ==null){

                bitmap = getBitmapFromMemoryCache(imageUrl+".jpg");

                if (bitmap == null) {
                    //BitmapWorkerTask task = new BitmapWorkerTask();
                    //taskCollection.add(task);

                    Thread t = new Thread(new Runnable() {
                        @Override
                        public void run() {
                            DiskLruCache.Snapshot snapShot = null;
                            FileDescriptor fileDescriptor = null;
                            FileInputStream fileInputStream = null;

                            try {
                                final String key = hashKeyForDisk(imageUrl+".jpg");
                                //mDiskLruCache.remove(key);
                                snapShot = mDiskLruCache.get(key);


                                if (snapShot == null) {
                                    // 如果没有找到对应的缓存，则准备从网络上请求数据，并写入缓存
                                    DiskLruCache.Editor editor = mDiskLruCache.edit(key);
                                    if (editor != null) {
                                        UserLocalStore uld = new UserLocalStore(getContext());
                                        List<HashMap<String, String>> BRList = new ArrayList<>();
                                        HashMap<String, String> temp = new HashMap<>();
                                        temp.put("filename", imageUrl);
                                        BRList.add(temp);
                                        String BRListString = new Gson().toJson(BRList);
                                        BRListString = URLEncoder.encode(BRListString);
                                        String data = "userId="+uld.getLoggedInUser().userid+"&event=food&method=getFoodImage&data="+BRListString;
                                        String bitmapString = navFoodImageFactory.sendRequest("POST", data);
                                        editor.set(0, bitmapString);
                                        System.out.println("bitmapString="+bitmapString);
                                        if (bitmapString != null) {
                                            editor.commit();
                                        } else {
                                            editor.abort();
                                        }
                                    }
                                    snapShot = mDiskLruCache.get(key);
                                }

                                Bitmap bitmap2 = null;
                                if (snapShot != null) {
                                    byte[] image = Base64.decode(snapShot.getString(0), Base64.DEFAULT);
                                    bitmap2 = BitmapFactory.decodeByteArray(image, 0, image.length);
                                }
                                if (bitmap2 != null) {
                                    // 将Bitmap对象添加到内存缓存当中
                                    addBitmapToMemoryCache(imageUrl+".jpg", bitmap2);
                                }
                                tempbitmap =  bitmap2;

                            }catch (Exception e){
                                e.printStackTrace();
                            } finally {
                                if (fileDescriptor == null && fileInputStream != null) {
                                    try {
                                        fileInputStream.close();
                                    } catch (IOException e) {
                                    }
                                }
                            }

                        }
                    });

                    t.start();

                    try {
                        t.join();
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                    return systemConstant.tempbitmap;

                }else{

                    if (bitmap != null) {
                        return (bitmap);
                    }
                }
            }else {
                return bitmap;
            }

                    //getBitmapFromMemoryCache(imageUrl+".jpg");
           /* if (bitmap == null) {
                BitmapWorkerTask task = new BitmapWorkerTask();
                taskCollection.add(task);
                task.execute(imageUrl);
            } else {
                if (bitmap != null) {
                    return (bitmap);
                }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }


    public void cancelAllTasks() {
        if (taskCollection != null) {
            for (BitmapWorkerTask task : taskCollection) {
                task.cancel(false);
            }
        }
    }



    public File getDiskCacheDir(Context context, String uniqueName) {
        String cachePath;
        if (Environment.MEDIA_MOUNTED.equals(Environment.getExternalStorageState())
                || !Environment.isExternalStorageRemovable()) {
            cachePath = context.getExternalCacheDir().getPath();
        } else {
            cachePath = context.getCacheDir().getPath();
        }
        return new File(cachePath + File.separator + uniqueName);
    }

    public String hashKeyForDisk(String key) {
        String cacheKey;
        try {
            final MessageDigest mDigest = MessageDigest.getInstance("MD5");
            mDigest.update(key.getBytes());
            cacheKey = bytesToHexString(mDigest.digest());
        } catch (NoSuchAlgorithmException e) {
            cacheKey = String.valueOf(key.hashCode());
        }
        return cacheKey;
    }

    private String bytesToHexString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            String hex = Integer.toHexString(0xFF & bytes[i]);
            if (hex.length() == 1) {
                sb.append('0');
            }
            sb.append(hex);
        }
        return sb.toString();
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.navfood_add_common_list, null,true);

        TextView txtTitle = (TextView) rowView.findViewById(R.id.itemname);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.icon);

        //System.out.println("Candy imgid.length="+imgid.length);
        //System.out.println("Candy position="+position);
        //System.out.println("Candy imgid[position]="+imgid[position]);
        txtTitle.setText(itemname.get(position).toString());
        //imageView.setImageResource(imgid[position]);
        //imageView.setBackgroundResource(imgid[position]);
        if (imgid.get(position).equals(navFoodConstant.GET_NOT_IMAGE_CODE)){
            imageView.setImageResource(R.drawable.ic_photo_black_24dp);
        }else {
            //byte[] image = Base64.decode(imgid.get(position), Base64.DEFAULT);

                Bitmap image = loadBitmaps(imgid.get(position));

                if (image!= null){
                    imageView.setImageBitmap(image);
                }else {
                    imageView.setImageResource(R.drawable.ic_photo_black_24dp);
                }

            //imageView.setImageBitmap(BitmapFactory.decodeByteArray(Base64.decode(imgid.get(position), Base64.DEFAULT), 0, image.length));
            image = null;
        }
        return rowView;
    };

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private String imageUrl;

        public BitmapWorkerTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            imageUrl = strings[0];
            DiskLruCache.Snapshot snapShot = null;
            FileDescriptor fileDescriptor = null;
            FileInputStream fileInputStream = null;

            try {
                final String key = hashKeyForDisk(imageUrl+".jpg");
                snapShot = mDiskLruCache.get(key);


                if (snapShot == null) {
                    // 如果没有找到对应的缓存，则准备从网络上请求数据，并写入缓存
                    DiskLruCache.Editor editor = mDiskLruCache.edit(key);
                    if (editor != null) {
                        OutputStream outputStream = editor.newOutputStream(0);


                        List<HashMap<String, String>> BRList = new ArrayList<>();
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put("filename", imageUrl);
                        BRList.add(temp);
                        String BRListString = new Gson().toJson(BRList);
                        BRListString = URLEncoder.encode(BRListString);
                        UserLocalStore uld = new UserLocalStore(getContext());

                        String data = "userId="+uld.getLoggedInUser().userid+"&event=food&method=getFoodImage&data="+BRListString;
                        outputStream = navFoodImageFactory.sendRequest("POST", data, outputStream);

                        if (outputStream != null) {
                            editor.commit();
                        } else {
                            editor.abort();
                        }
                    }
                    snapShot = mDiskLruCache.get(key);
                }

                if (snapShot != null) {
                    fileInputStream = (FileInputStream) snapShot.getInputStream(0);
                    fileDescriptor = fileInputStream.getFD();
                }
                Bitmap bitmap = null;
                if (fileDescriptor != null) {
                    bitmap = BitmapFactory.decodeFileDescriptor(fileDescriptor);
                }
                if (bitmap != null) {
                    // 将Bitmap对象添加到内存缓存当中
                    addBitmapToMemoryCache(strings[0]+".jpg", bitmap);
                }
                return bitmap;

            }catch (Exception e){
                e.printStackTrace();
            } finally {
            if (fileDescriptor == null && fileInputStream != null) {
                try {
                    fileInputStream.close();
                } catch (IOException e) {
                }
            }
            }

            return null;
        }
    }
}
