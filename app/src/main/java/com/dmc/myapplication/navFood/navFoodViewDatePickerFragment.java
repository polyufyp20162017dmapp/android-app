package com.dmc.myapplication.navFood;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.os.Bundle;

import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by KwokSinMan on 9/12/2015.
 */

public class navFoodViewDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayDate = (String)((TextView) getActivity().findViewById(R.id.navFoodViewDate)).getText();
        String [] displayDateArray = displayDate.split("/");
        int yy = Integer.parseInt(displayDateArray[2]);
        int mm = Integer.parseInt(displayDateArray[1]) - 1; // because Jan =0
        int dd = Integer.parseInt(displayDateArray[0]);
        DatePickerDialog dialog =  new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, yy,mm,dd);
        dialog.getDatePicker().setMaxDate( Calendar.getInstance().getTimeInMillis());
        return dialog;
    }

    // On the date picker , to confirm the following action
    public void onDateSet(DatePicker view, int yy, int mm, int dd) {
        // set display date
        mm = mm+1; // because Jan =0
        String displayAsDate = dd + "/" + mm + "/" + yy;
        ((TextView) getActivity().findViewById(R.id.navFoodViewDate)).setText(displayAsDate);

        //get food record from DB
        navfoodAsyncTask connect = new navfoodAsyncTask(this.getActivity(), navFoodConstant.GET_FOOD_RECORD);
        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        String foodDate = yy + "-" + mm + "-" +dd;
        connect.execute(userId, foodDate);

    }


}
