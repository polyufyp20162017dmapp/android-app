package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dmc.myapplication.R;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class navFoodNutrientListListener implements View.OnClickListener {

    Activity activity;

    public navFoodNutrientListListener(Activity activity){
        this.activity = activity;
    }

    @Override
    public void onClick(View arg0) {
        LinearLayout nutrientListSession = (LinearLayout) activity.findViewById(R.id.nutrientListSession);
        ImageView icon = (ImageView) activity.findViewById(R.id.nutrientIcon);
        if(nutrientListSession.getVisibility() == View.GONE){
            nutrientListSession.setVisibility(View.VISIBLE);
            icon.setImageResource(R.drawable.ic_keyboard_arrow_up_black_24dp);
        }else if(nutrientListSession.getVisibility() == View.VISIBLE){
            nutrientListSession.setVisibility(View.GONE);
            icon.setImageResource(R.drawable.ic_keyboard_arrow_down_black_24dp);
        }
    }
}
