package com.dmc.myapplication.navFood;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.NumberPicker;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by KwokSinMan on 31/1/2016.
 */
public class navFoodRecordNumberPickerFragment extends DialogFragment implements DialogInterface.OnClickListener {
    String isFreeText;
    public navFoodRecordNumberPickerFragment(String isFreeText) {
        this.isFreeText = isFreeText;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final NumberPicker foodValuePick = new NumberPicker(getActivity());
        foodValuePick.setMaxValue(99);
        foodValuePick.setMinValue(1);
        TextView foodValue = (TextView) getActivity().findViewById(R.id.foodValue);
        foodValuePick.setValue(Integer.parseInt((String) foodValue.getText()));
        foodValuePick.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
        foodValuePick.setWrapSelectorWheel(false);


        return new AlertDialog.Builder(getActivity())
                .setTitle("進食數量")
                .setView(foodValuePick)
                .setPositiveButton("確定",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                navFoodBiz foodBiz = new navFoodBiz();
                                TextView foodValue = (TextView) getActivity().findViewById(R.id.foodValue);
                                int oldFoodValue = Integer.parseInt((String) foodValue.getText());
                                if (foodValuePick.getValue() != oldFoodValue) {

                                    foodValue.setText(String.valueOf(foodValuePick.getValue()));

                                    // input food data from DB record
                                    if(isFreeText.equals(navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE)) {
                                        //System.out.println("Candy count=" + ((ListView) getActivity().findViewById(R.id.nutrientList)).getCount());
                                        // update nutrientList
                                        ListView nutrientList = (ListView) getActivity().findViewById(R.id.nutrientList);
                                        ArrayList<HashMap<String, String>> mylist = new ArrayList<HashMap<String, String>>();
                                        for (int i = 0; i < nutrientList.getCount(); i++) {
                                            double oldValue = Double.parseDouble((String) (((HashMap<String, String>) (nutrientList.getItemAtPosition(i))).get("nutrientValue")));
                                            int newValue = foodBiz.formatDouble(oldValue / oldFoodValue * foodValuePick.getValue());

                                            HashMap<String, String> map = new HashMap<String, String>();
                                            map.put("nutrientName",navFoodConstant.nutrientName[i]);
                                            map.put("nutrientValue", Integer.toString(newValue));
                                            map.put("nutrientUnit",navFoodConstant.nutrientUnit[i]);
                                            mylist.add(map);
                                        }
                                        SimpleAdapter nutrientAdapter = new SimpleAdapter(getActivity(), mylist, R.layout.navfood_nutrient_list, new String[]{"nutrientName", "nutrientValue", "nutrientUnit"}, new int[]{R.id.nutrientName, R.id.nutrientValue, R.id.nutrientUnit});
                                        nutrientList.setAdapter(nutrientAdapter);
                                    }

                                }
                            }
                        })
                .setNegativeButton("取消",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                            }
                        })
                .create();
    }

    @Override
    public void onClick(DialogInterface dialog, int whichButton){}
}