package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.food;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.Models.foodUserCommon;
import com.dmc.myapplication.Models.imageStorage;
import com.dmc.myapplication.Models.imageSyncStack;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;

import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;

import java.io.*;
import java.nio.channels.FileChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class navFoodAddDetailActivity extends AppCompatActivity {
    static String isFreeText ="";
    String hasImage="";
    ImageView foodImage;
    String mCurrentPhotoPath;
    String imageFileName;
    Button finishButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.navfood_add_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        navFoodBiz foodBiz = new navFoodBiz();

        Intent intent = getIntent();
        final int food_id = intent.getIntExtra("food_id", navFoodConstant.GET_FREE_TEXT_ADD_FOOD_CODE);
        isFreeText = intent.getExtras().getString("is_free_text");
        hasImage = intent.getExtras().getString("has_image");

        //hidden edit record session
        LinearLayout editRecordButtonSession = (LinearLayout) findViewById(R.id.editRecordButtonSession);
        editRecordButtonSession.setVisibility(View.GONE);

        // hidden nutrientListSession, after click the nutrientLabelSession to open
        LinearLayout nutrientListSession = (LinearLayout) findViewById(R.id.nutrientListSession);
        nutrientListSession.setVisibility(View.GONE);
        LinearLayout nutrientLabelSession = (LinearLayout) findViewById(R.id.nutrientLabelSession);
        nutrientLabelSession.setOnClickListener(new navFoodNutrientListListener(this));


        // set food  Date
        String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        TextView navFoodDate = (TextView)findViewById(R.id.navFoodRecordDate);
        navFoodDate.setText(new navFoodTime().getUIDateFormal(getFoodDate));

        //set Food Time
        TextView navFoodTimeTextView = (TextView)findViewById(R.id.navFoodRecordTime);
        navFoodTimeTextView.setText(new navFoodTime().getCurrentTimeFormal());

        // set data picker
        ImageView setFoodRecordDate = (ImageView) findViewById(R.id.setNavFoodRecordDate);
        setFoodRecordDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodRecordDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setFoodRecordTime = (ImageView) findViewById(R.id.setNavFoodRecordTime);
        setFoodRecordTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodRecordTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        //set food value (default)
        TextView foodValue = (TextView) findViewById(R.id.foodValue);
        foodValue.setText("1");

        // set value picker
        ImageView setFoodRecordValue = (ImageView) findViewById(R.id.changeFoodValue);
        setFoodRecordValue.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new navFoodRecordNumberPickerFragment(isFreeText);
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        // set Food Stype
        List<navFoodBiz.DbRecordStringString> foodTypeList = foodBiz.getFoodTypeList();
        ArrayAdapter<navFoodBiz.DbRecordStringString> foodTypeAdapter = new ArrayAdapter<navFoodBiz.DbRecordStringString>(this, R.layout.navfood_spinner_center_item, foodTypeList);
        foodTypeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner foodTypeSpinner = (Spinner) findViewById(R.id.foodType);
        foodTypeSpinner.setAdapter(foodTypeAdapter);

        // set Food Place
        List<navFoodBiz.DbRecordStringString> foodPlaceList = foodBiz.getFoodPlaceList();
        ArrayAdapter<navFoodBiz.DbRecordStringString> foodPlaceAdapter = new ArrayAdapter<navFoodBiz.DbRecordStringString>(this, R.layout.navfood_spinner_center_item, foodPlaceList);
        foodPlaceAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner foodPlaceSpinner = (Spinner) findViewById(R.id.foodPlace);
        foodPlaceSpinner.setAdapter(foodPlaceAdapter);

        //set "Finish" Button Action
        finishButton = (Button) findViewById(R.id.navFoodAdd);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                navFoodTime timeTool = new navFoodTime();

                String foodDate = timeTool.getDBDateFormal((String) ((TextView) findViewById(R.id.navFoodRecordDate)).getText());
                String foodTime = (String) ((TextView) findViewById(R.id.navFoodRecordTime)).getText();
                String foodValue = (String) ((TextView) findViewById(R.id.foodValue)).getText();
                String foodType = ((navFoodBiz.DbRecordStringString) (((Spinner) findViewById(R.id.foodType)).getSelectedItem())).getId();
                String foodPlace = ((navFoodBiz.DbRecordStringString) (((Spinner) findViewById(R.id.foodPlace)).getSelectedItem())).getId();

                UserLocalStore userLocalStore = new UserLocalStore(arg0.getContext());
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

                String photoPath="NA";

                if (isFreeText.equals(navFoodConstant.GET_IS_FREE_TEXT_CODE)) {
                    if(imageFileName!=null && !imageFileName.isEmpty()){
                        Bitmap reducedImage = imageTool.decreaseImageSie(mCurrentPhotoPath);
                        //System.out.println("Candy photoPath="+photoPath);
                        //System.out.println("Candy reducedImage="+reducedImage);

                        //Janus Save to internal
                        //new navFoodUploadImageAsyncTask(reducedImage,photoPath).execute();

                        UserLocalStore uls = new UserLocalStore(getBaseContext());
                        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                        String currentTimestamp = s.format(new Date());
                        //photoPath = imageFileName + userId + systemConstant.IMAGE_TYPE;
                        photoPath = uls.getLoggedInUser().userid+"_"+currentTimestamp;

                        String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+uls.getLoggedInUser().userid+"_"+currentTimestamp+".jpg";

                        /*try{
                            copy(new File(mCurrentPhotoPath),new File(strImage));
                        }catch (Exception e){
                            e.printStackTrace();
                        }*/

                        ByteArrayOutputStream stream = new ByteArrayOutputStream();
                        reducedImage.compress(Bitmap.CompressFormat.JPEG, systemConstant.IMAGE_COMPRESS, stream);
                        String reducedImageString = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

                        imageSyncStack imageSyncStackdb = new imageSyncStack(getBaseContext());
                        imageSyncStackdb.insert(imageSyncStackdb.new imageSyncStack_class(0, photoPath+".jpg", reducedImageString));
                        imageSyncStackdb.close();

                        imageStorage imageStorage = new imageStorage(getBaseContext());
                        imageStorage.insert(imageStorage.new imageStorage_class(0, photoPath+".jpg", reducedImageString));
                        imageStorage.close();
                    }

                    String foodName = (String) ((TextView) findViewById(R.id.foodName)).getText();
                    String inputFoodUnit = ((Spinner) findViewById(R.id.inputFoodUnit)).getSelectedItem().toString();

                    //System.out.println("Candy mCurrentPhotoPath="+mCurrentPhotoPath);
                    //System.out.println("Candy foodName=" + foodName);
                    //System.out.println("Candy foodUnit=" + inputFoodUnit);
                    //System.out.println("Candy photoPath=" + photoPath);

                    //Janus
                    //navfoodAsyncTask connect = new navfoodAsyncTask(getActivityContentView(arg0), navFoodConstant.SAVE_FREE_TEXT_FOOD_RECORD);
                    //connect.execute(userId, foodDate, foodTime, foodValue, foodType, foodPlace, foodName, inputFoodUnit, photoPath);
                    foodRecord frdb = new foodRecord(getApplicationContext());
                    frdb.insert(frdb.new foodRecord_class(0, Integer.parseInt(userId), foodDate, foodTime, foodType, foodPlace, null, Integer.parseInt(foodValue), null, null, null, null, foodName, inputFoodUnit, photoPath));
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);

                    Bundle bundle = new Bundle();
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
                    bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    ContentResolver.requestSync(null, "com.dmc.myapplication.provider", bundle);

                    startActivity(intent);
                    finish();

                } else if (isFreeText.equals(navFoodConstant.GET_IS_NOT_FREE_TEXT_CODE)) {
                    /// input food data from DB record
                    //navfoodAsyncTask connect = new navfoodAsyncTask(getActivityContentView(arg0), navFoodConstant.SAVE_FOOD_RECORDFROMDB);
                    //connect.execute(userId, Integer.toString(food_id), foodDate, foodTime, foodValue, foodType, foodPlace);
                    //// Save Free Record

                    foodRecord frdb = new foodRecord(getApplicationContext());
                    food foodDB = new food(getApplicationContext());
                    food.food_class foodObj = foodDB.get(food_id);
                    frdb.insert(frdb.new foodRecord_class(0, Integer.parseInt(userId), foodDate, foodTime, foodType, foodPlace, food_id, Integer.parseInt(foodValue), foodObj.getFOOD_CALORIE() * Integer.parseInt(foodValue), foodObj.getFOOD_CARBOHYDRATE() * Integer.parseInt(foodValue), foodObj.getFOOD_PROTEIN() * Integer.parseInt(foodValue), foodObj.getFOOD_FAT() * Integer.parseInt(foodValue), "", "", ""));

                    foodUserCommon foodUserCommondb = new foodUserCommon(getApplicationContext());
                    foodUserCommon.foodUserCommon_class foodUserCommon_class = foodUserCommondb.getByFoodId(food_id);
                    if (foodUserCommon_class != null){
                        foodUserCommondb.update(foodUserCommondb.new foodUserCommon_class(foodUserCommon_class.getUSER_ID(), foodUserCommon_class.getFOOD_ID(), foodUserCommon_class.getFOOD_COMMON_COUNT() + 1));
                    }else{
                        foodUserCommondb.insert(foodUserCommondb.new foodUserCommon_class(Integer.parseInt(userId), food_id,  1));
                    }
                    frdb.close();
                    foodUserCommondb.close();
                    Intent intent = new Intent();
                    intent.setClass(getApplicationContext(), MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);
                    startActivity(intent);
                    finish();
                }
            }
        });

        UserLocalStore userLocalStore = new UserLocalStore(this);
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

        // UI of free text input and input food data from DB
        if (food_id == navFoodConstant.GET_FREE_TEXT_ADD_FOOD_CODE){
            //free text input
            // food unit
            Spinner inputFoodUnit = (Spinner) findViewById(R.id.inputFoodUnit);
            List<String> foodUnitList = foodBiz.getFoodUnitList();

            ArrayAdapter<String> inputFoodUnitAdapter = new ArrayAdapter<String>(this,R.layout.navfood_spinner_center_item,foodUnitList);
            inputFoodUnitAdapter.setDropDownViewResource(R.layout.navfood_spinner_center_item);
            inputFoodUnit.setAdapter(inputFoodUnitAdapter);

            // hidden nutrientLabel
            nutrientLabelSession.setVisibility(View.GONE);

            LinearLayout addPhotoSession = (LinearLayout) findViewById(R.id.addPhotoSession);
            if (hasImage.equals(navFoodConstant.GET_HAS_IMAGE)){
                addPhotoSession.setVisibility(View.VISIBLE);
            }else if (hasImage.equals(navFoodConstant.GET_NOT_HAS_IMAGE)){
                addPhotoSession.setVisibility(View.GONE);
            }

            foodImage = (ImageView) findViewById(R.id.addPhoto);
            foodImage.setOnClickListener(new imageOnClickListener());

            TextView foodName = (TextView) findViewById(R.id.foodName);
            if(hasImage.equals(navFoodConstant.GET_HAS_IMAGE)) {
                foodName.setText("其他");
                finishButton.setEnabled(false);
            }else if (hasImage.equals(navFoodConstant.GET_NOT_HAS_IMAGE)){
                foodName.setText(intent.getExtras().getString("food_name"));
            }

            //get Data from DB
            navfoodAsyncTask connect = new navfoodAsyncTask(this, navFoodConstant.GET_ADD_FREE_TEXT_FOOD_DETAIL);
            connect.execute(userId, getFoodDate);
        }
        else{
            //input food data from DB
            LinearLayout addPhotoSession = (LinearLayout) findViewById(R.id.addPhotoSession);
            addPhotoSession.setVisibility(View.GONE);
            LinearLayout foodUnitSession = (LinearLayout) findViewById(R.id.foodUnitSession);
            foodUnitSession.setVisibility(View.GONE);

            //get Data from DB
            //navfoodAsyncTask connect = new navfoodAsyncTask(this, navFoodConstant.GET_ADD_FOOD_DETAIL);
            //connect.execute(userId, Integer.toString(food_id), getFoodDate);

            navFoodBiz biz = new navFoodBiz();
            biz.setAddFoodDetail(this, "{}", String.valueOf(food_id), getFoodDate);
        }

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        Intent intent = new Intent();
        intent.setClass(this, navFoodAddActivity.class);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE,getFoodDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    private class imageOnClickListener implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            CharSequence[] items = { "相機", "相簿" };
            AlertDialog.Builder builder = new AlertDialog.Builder(navFoodAddDetailActivity.this);
            builder.setItems(items, new getImageMethodDialogOnClickListener());
            builder.show();
        }
    }

    private class getImageMethodDialogOnClickListener implements DialogInterface.OnClickListener{
        public void onClick(DialogInterface dialog, int item) {
            //System.out.println("Candy testing get image=" + item);
            if (item == 0) { // 照相機
                takePhoto();
            }else if (item == 1) { // 相簿
                selectFromGallery();
            }
        }
    }

    private void selectFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"),  navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE);

    }

        private void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            if (photoFile != null) {
                //Janus
                //UserLocalStore uls = new UserLocalStore(getBaseContext());
                //SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                //String currentTimestamp = s.format(new Date());

                //String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+uls.getLoggedInUser().userid+"_"+currentTimestamp+".jpg";
                //File myImage = new File(strImage);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(myImage));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, navFoodConstant.GET_ADD_FOOD_IMAGE_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        imageFileName =imageTool.getImageName();
        new exportTool().createDirIfNotExists(systemConstant.FILE_PATH_IMAGE);

        File image = new File(systemConstant.FILE_PATH_IMAGE + "/" + imageFileName + systemConstant.IMAGE_TYPE);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_CODE) { // 照相機
                setPic();
                finishButton.setEnabled(true);
            }else if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE){ //
                setGalleryPic(data);
                finishButton.setEnabled(true);
            }
        }
    }

    private void setGalleryPic(Intent data){
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();

        mCurrentPhotoPath = cursor.getString(column_index);
        imageFileName =imageTool.getImageName();
        //System.out.println("mCurrentPhotoPath="+selectedImagePath);


        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, options));
        foodImage.setImageBitmap(bm);
    }

    public void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    private  void setPic(){
        int targetW = foodImage.getWidth();
        int targetH = foodImage.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

       Bitmap rotatedBMP = imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)) ;

        foodImage.setImageBitmap(rotatedBMP);
        //foodImage.setImageBitmap(bitmap);
    }



}



//                String calorie = ((HashMap<String, String>)(((ListView) findViewById(R.id.nutrientList)).getItemAtPosition(0))).get("nutrientValue");
//                String carbohydrate = ((HashMap<String, String>)(((ListView) findViewById(R.id.nutrientList)).getItemAtPosition(1))).get("nutrientValue");
//                String fat = ((HashMap<String, String>)(((ListView) findViewById(R.id.nutrientList)).getItemAtPosition(2))).get("nutrientValue");
//                String protein = ((HashMap<String, String>)(((ListView) findViewById(R.id.nutrientList)).getItemAtPosition(3))).get("nutrientValue");

