package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;



/**
 * Created by KwokSinMan on 16/1/2016.
 */
public class navfoodAsyncTask extends AsyncTask<String,Void,String> {

    private ProgressDialog progressBar;
    private String model;
    private Activity activity;

    public navfoodAsyncTask(Activity activity, String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        //執行中 在背景做事情
        String data="";
        //System.out.println("Candy arg0.length="+arg0.length);
        System.out.println("model ======"+model);
        try {
            if (model.equals(navFoodConstant.GET_FOOD_CATE_DDL)) {

            }
            if (model.equals(navFoodConstant.GET_FOOD_SUB_CATE_DDL)){
                String foodCateId = (String)arg0[0];
                data  = URLEncoder.encode("food_cate_id", "UTF-8") + "=" + URLEncoder.encode(foodCateId, "UTF-8");
            }
            if (model.equals(navFoodConstant.GET_FOOD_All_LIST)){
                String foodSubCateId = (String)arg0[0];
                data  = URLEncoder.encode("food_sub_cate_id", "UTF-8") + "=" + URLEncoder.encode(foodSubCateId, "UTF-8");
            }
            if (model.equals(navFoodConstant.GET_FOOD_RECORD)){
                String userId = (String)arg0[0];
                String foodDate = (String)arg0[1];
                //data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                //data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");
                return foodDate;
            }
            if (model.equals(navFoodConstant.GET_ADD_FOOD_DETAIL)){
                String userId = (String)arg0[0];
                String foodId = (String)arg0[1];
                String foodDate = (String)arg0[2];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_id", "UTF-8") + "=" + URLEncoder.encode(foodId, "UTF-8");
                data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");
            }
            if(model.equals(navFoodConstant.SAVE_FOOD_RECORDFROMDB)){
                String userId = (String)arg0[0];
                String foodId = (String)arg0[1];
                String foodDate = (String)arg0[2];
                String foodTime = (String)arg0[3];
                String foodValue = (String)arg0[4];
                String foodType = (String)arg0[5];
                String foodPlace = (String)arg0[6];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_id", "UTF-8") + "=" + URLEncoder.encode(foodId, "UTF-8");
                data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");
                data += "&" + URLEncoder.encode("food_time", "UTF-8") + "=" + URLEncoder.encode(foodTime, "UTF-8");
                data += "&" + URLEncoder.encode("food_value", "UTF-8") + "=" + URLEncoder.encode(foodValue, "UTF-8");
                data += "&" + URLEncoder.encode("food_type", "UTF-8") + "=" + URLEncoder.encode(foodType, "UTF-8");
                data += "&" + URLEncoder.encode("food_place", "UTF-8") + "=" + URLEncoder.encode(foodPlace, "UTF-8");
            }
            if(model.equals(navFoodConstant.SAVE_FREE_TEXT_FOOD_RECORD)){
                String userId = (String)arg0[0];
                String foodDate = (String)arg0[1];
                String foodTime = (String)arg0[2];
                String foodValue = (String)arg0[3];
                String foodType = (String)arg0[4];
                String foodPlace = (String)arg0[5];
                String foodName = (String)arg0[6];
                String inputFoodUnit = (String)arg0[7];
                String photoPath = (String)arg0[8];

                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");
                data += "&" + URLEncoder.encode("food_time", "UTF-8") + "=" + URLEncoder.encode(foodTime, "UTF-8");
                data += "&" + URLEncoder.encode("food_value", "UTF-8") + "=" + URLEncoder.encode(foodValue, "UTF-8");
                data += "&" + URLEncoder.encode("food_type", "UTF-8") + "=" + URLEncoder.encode(foodType, "UTF-8");
                data += "&" + URLEncoder.encode("food_place", "UTF-8") + "=" + URLEncoder.encode(foodPlace, "UTF-8");
                data += "&" + URLEncoder.encode("food_name", "UTF-8") + "=" + URLEncoder.encode(foodName, "UTF-8");
                data += "&" + URLEncoder.encode("food_unit", "UTF-8") + "=" + URLEncoder.encode(inputFoodUnit, "UTF-8");
                data += "&" + URLEncoder.encode("photo_path", "UTF-8") + "=" + URLEncoder.encode(photoPath, "UTF-8");

            }
            if(model.equals(navFoodConstant.GET_FOOD_COMMON_LIST)){
                String userId=(String)arg0[0];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                return "[]";
            }
            if(model.equals(navFoodConstant.GET_FOOD_SEARCH_RESULT)){
                String searchKeyword=(String)arg0[0];
                data = URLEncoder.encode("search_keyword", "UTF-8") + "=" + URLEncoder.encode(searchKeyword, "UTF-8");
            }
            if(model.equals(navFoodConstant.GET_ADD_FREE_TEXT_FOOD_DETAIL)){
                String userId = (String)arg0[0];
                String foodDate = (String)arg0[1];
                //data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                //data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");

                //Janus
                return foodDate;
            }
            if(model.equals(navFoodConstant.GET_FREE_TEXT_FOOD_DETAIL)){
                String userId = (String)arg0[0];
                String foodRecordId = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_record_id", "UTF-8") + "=" + URLEncoder.encode(foodRecordId, "UTF-8");

                //Janus
                return foodRecordId;
            }
            if(model.equals(navFoodConstant.GET_FOOD_DETAIL)){
                //Janus
                //String userId = (String)arg0[0];
                String foodRecordId = (String)arg0[1];
                //data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                //data += "&" + URLEncoder.encode("food_record_id", "UTF-8") + "=" + URLEncoder.encode(foodRecordId, "UTF-8");
                return foodRecordId;
            }
            if(model.equals(navFoodConstant.SAVE_EDIT_FREE_TEXT_FOOD_DETAIL)){
                String userId = (String)arg0[0];
                String foodRecordId = (String)arg0[1];
                String foodDate = (String)arg0[2];
                String foodTime = (String)arg0[3];
                String foodValue = (String)arg0[4];
                String foodType = (String)arg0[5];
                String foodPlace = (String)arg0[6];
                String inputFoodUnit = (String)arg0[7];
                String photoPath = (String)arg0[8];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_record_id", "UTF-8") + "=" + URLEncoder.encode(foodRecordId, "UTF-8");
                data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");
                data += "&" + URLEncoder.encode("food_time", "UTF-8") + "=" + URLEncoder.encode(foodTime, "UTF-8");
                data += "&" + URLEncoder.encode("food_value", "UTF-8") + "=" + URLEncoder.encode(foodValue, "UTF-8");
                data += "&" + URLEncoder.encode("food_type", "UTF-8") + "=" + URLEncoder.encode(foodType, "UTF-8");
                data += "&" + URLEncoder.encode("food_place", "UTF-8") + "=" + URLEncoder.encode(foodPlace, "UTF-8");
                data += "&" + URLEncoder.encode("food_unit", "UTF-8") + "=" + URLEncoder.encode(inputFoodUnit, "UTF-8");
                data += "&" + URLEncoder.encode("photo_path", "UTF-8") + "=" + URLEncoder.encode(photoPath, "UTF-8");
            }
            if(model.equals(navFoodConstant.SAVE_EDIT_FOOD_DETAIL)){
                String userId = (String)arg0[0];
                String foodRecordId = (String)arg0[1];
                String foodDate = (String)arg0[2];
                String foodTime = (String)arg0[3];
                String foodValue = (String)arg0[4];
                String foodType = (String)arg0[5];
                String foodPlace = (String)arg0[6];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_record_id", "UTF-8") + "=" + URLEncoder.encode(foodRecordId, "UTF-8");
                data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");
                data += "&" + URLEncoder.encode("food_time", "UTF-8") + "=" + URLEncoder.encode(foodTime, "UTF-8");
                data += "&" + URLEncoder.encode("food_value", "UTF-8") + "=" + URLEncoder.encode(foodValue, "UTF-8");
                data += "&" + URLEncoder.encode("food_type", "UTF-8") + "=" + URLEncoder.encode(foodType, "UTF-8");
                data += "&" + URLEncoder.encode("food_place", "UTF-8") + "=" + URLEncoder.encode(foodPlace, "UTF-8");
            }
            if(model.equals(navFoodConstant.DELETE_FOOD_RECORD)){
                String userId = (String)arg0[0];
                String foodRecordId = (String)arg0[1];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("food_record_id", "UTF-8") + "=" + URLEncoder.encode(foodRecordId, "UTF-8");
            }
            if(model.equals(navFoodConstant.GET_FOOD_PIE_CHART_DATA)){
                String userId = (String)arg0[0];
                String foodDate = (String)arg0[1];
                //Janus
                //data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                //data += "&" + URLEncoder.encode("food_date", "UTF-8") + "=" + URLEncoder.encode(foodDate, "UTF-8");

                return foodDate;
            }
            //get result  from server
            if (model.equals(navFoodConstant.GET_FOOD_CATE_DDL) || model.equals(navFoodConstant.GET_FOOD_CATE_DDL) || model.equals(navFoodConstant.GET_FOOD_SEARCH_RESULT) || model.equals(navFoodConstant.GET_ADD_FOOD_DETAIL) || model.equals(navFoodConstant.SAVE_FOOD_RECORDFROMDB) || model.equals(navFoodConstant.GET_FOOD_RECORD)){
                return "{}";
            }else{
                dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
                return dmasyncTask.getDataFromServer(data);

            }

        }  catch (SocketTimeoutException e){
           return dmAppAsyncTask.SERVER_SLEEP;
        } catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
    }

    //@Override
    //protected void onProgressUpdate(Integer... values) {
        //執行中 可以在這邊告知使用者進度
        //super.onProgressUpdate(values);
    //}

    @Override
    protected void onPostExecute(String result){
        if (result!=null){
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                    dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else{
                navFoodBiz foodBiz = new navFoodBiz();
                if (model.equals(navFoodConstant.GET_FOOD_CATE_DDL)) {
                    foodBiz.setFoodCateDDL(this.activity, result);
                }
                if (model.equals(navFoodConstant.GET_FOOD_SUB_CATE_DDL)) {
                    foodBiz.setFoodSubCateDDL(this.activity, result);
                }
                if (model.equals(navFoodConstant.GET_FOOD_All_LIST)) {
                    foodBiz.setFoodAllList(this.activity, result);
                }
                if (model.equals(navFoodConstant.GET_FOOD_RECORD)){
                    //Janus
                    //foodBiz.setFoodRecord(this.activity, result);
                    foodBiz.setFoodRecord(this.activity, "{}", result);
                }
                if (model.equals(navFoodConstant.GET_ADD_FOOD_DETAIL)){
                    foodBiz.setAddFoodDetail(this.activity, result);
                }
                if(model.equals(navFoodConstant.SAVE_FOOD_RECORDFROMDB)){
                    foodBiz.afterSaveFoodRecord(this.activity, navFoodConstant.IS_DELETE_ACTION_N);
                }
                if (model.equals(navFoodConstant.SAVE_FREE_TEXT_FOOD_RECORD)){
                    //System.out.println("Candy result="+result);
                    foodBiz.afterSaveFoodRecord(this.activity, navFoodConstant.IS_DELETE_ACTION_N);
                }
                if(model.equals(navFoodConstant.GET_FOOD_COMMON_LIST)) {
                    //foodBiz.setFoodCommonList(this.activity, result);
                    //navFoodBiz foodBiz = new navFoodBiz();
                    foodBiz.setFoodCommonListNew(activity, "{}");
                }
                if(model.equals(navFoodConstant.GET_FOOD_SEARCH_RESULT)) {
                    foodBiz.setFoodSearchResult(this.activity, result);
                }
                if(model.equals(navFoodConstant.GET_ADD_FREE_TEXT_FOOD_DETAIL)){
                    foodBiz.setAddFreeTextFoodDetail(this.activity, "{}", result);
                }
                if(model.equals(navFoodConstant.GET_FREE_TEXT_FOOD_DETAIL)){
                    foodBiz.setEditFreeTextFoodDetail(this.activity, "{}", result);
                }
                if(model.equals(navFoodConstant.GET_FOOD_DETAIL)){
                    foodBiz.setEditFoodDetail(this.activity, "{}", result);
                }
                if(model.equals(navFoodConstant.SAVE_EDIT_FREE_TEXT_FOOD_DETAIL)){
                    foodBiz.afterSaveFoodRecord(this.activity, navFoodConstant.IS_DELETE_ACTION_N);
                }
                if(model.equals(navFoodConstant.SAVE_EDIT_FOOD_DETAIL)){
                    foodBiz.afterSaveFoodRecord(this.activity, navFoodConstant.IS_DELETE_ACTION_N);
                }
                if(model.equals(navFoodConstant.DELETE_FOOD_RECORD)){
                    foodBiz.afterSaveFoodRecord(this.activity, navFoodConstant.IS_DELETE_ACTION_Y);
                }
                if(model.equals(navFoodConstant.GET_FOOD_PIE_CHART_DATA)){
                    foodBiz.setDrawPieChart(this.activity, "{}",result);
                }
                progressBar.dismiss();
            }
        }

    }

}
