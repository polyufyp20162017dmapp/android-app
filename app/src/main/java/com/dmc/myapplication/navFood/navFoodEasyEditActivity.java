package com.dmc.myapplication.navFood;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.util.LruCache;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.*;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.foodRecord;
import com.dmc.myapplication.Models.imageStorage;
import com.dmc.myapplication.Models.imageSyncStack;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.exportTool;
import com.dmc.myapplication.tool.imageTool;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.jakewharton.disklrucache.DiskLruCache;

import java.io.*;
import java.net.URLEncoder;
import java.nio.channels.FileChannel;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class navFoodEasyEditActivity extends AppCompatActivity {
    ImageView foodImage;
    String mCurrentPhotoPath;
    Boolean isChangeImage = false;
    String newFileName;
    Bitmap foodbitmap;
    foodRecord.foodRecord_class foodRecord;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_food_easy_add);

        String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        Integer getFoodID = getIntent().getExtras().getInt("food_record_id");

        foodRecord foodRecorddb = new foodRecord(getBaseContext());
        foodRecord = foodRecorddb.get(getFoodID);
        foodRecorddb.close();

        TextView navFoodDate = (TextView)findViewById(R.id.navFoodRecordDate);
        navFoodDate.setText(foodRecord.getFOOD_DATE());

        //set Food Time
        TextView navFoodTimeTextView = (TextView)findViewById(R.id.navFoodRecordTime);
        navFoodTimeTextView.setText(foodRecord.getFOOD_TIME());

        // set data picker
        ImageView setFoodRecordDate = (ImageView) findViewById(R.id.setNavFoodRecordDate);
        setFoodRecordDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodRecordDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setFoodRecordTime = (ImageView) findViewById(R.id.setNavFoodRecordTime);
        setFoodRecordTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new navFoodRecordTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        navFoodBiz foodBiz = new navFoodBiz();
        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        Calendar cal = Calendar.getInstance();

        // set Food Stype
        List<navFoodBiz.DbRecordStringString> foodTypeList = foodBiz.getFoodTypeList();
        ArrayAdapter<navFoodBiz.DbRecordStringString> foodTypeAdapter = new ArrayAdapter<navFoodBiz.DbRecordStringString>(this, R.layout.navfood_spinner_center_item, foodTypeList);
        foodTypeAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner foodTypeSpinner = (Spinner) findViewById(R.id.foodType);
        foodTypeSpinner.setAdapter(foodTypeAdapter);


        for (int x =0; x < foodTypeList.size(); x++){
            if (foodTypeList.get(x).getId().equals(foodRecord.getFOOD_SESSION())){
                foodTypeSpinner.setSelection(x);
            }
        }

        // set Food Place
        List<navFoodBiz.DbRecordStringString> foodPlaceList = foodBiz.getFoodPlaceList();
        ArrayAdapter<navFoodBiz.DbRecordStringString> foodPlaceAdapter = new ArrayAdapter<navFoodBiz.DbRecordStringString>(this, R.layout.navfood_spinner_center_item, foodPlaceList);
        foodPlaceAdapter.setDropDownViewResource(android.R.layout.simple_dropdown_item_1line);
        Spinner foodPlaceSpinner = (Spinner) findViewById(R.id.foodPlace);
        foodPlaceSpinner.setAdapter(foodPlaceAdapter);
        //foodPlaceSpinner.setSelection(Integer.parseInt(foodRecord.getFOOD_PLACE()));

        for (int x =0; x < foodPlaceList.size(); x++){
            if (foodPlaceList.get(x).getId().equals(foodRecord.getFOOD_PLACE())){
                foodPlaceSpinner.setSelection(x);
            }
        }




        Button navFoodAdd = (Button) findViewById(R.id.navFoodAdd);
        navFoodAdd.setVisibility(View.GONE);

        Button navFoodDelete = (Button) findViewById(R.id.navFoodDelete);
        navFoodDelete.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new navFoodRecordDeleteDialogFragment(foodRecord.getFOOD_RECORD_ID());
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        Button navFoodEdit = (Button) findViewById(R.id.navFoodEdit);
        navFoodEdit.setOnClickListener(new navFoodEditButtonOnClickListener(foodRecord.getFOOD_RECORD_ID(), foodbitmap, this)) ;

        foodImage = (ImageView) findViewById(R.id.addPhoto);

        foodImage.setImageBitmap(BitmapFactory.decodeResource(getBaseContext().getResources(),
                R.drawable.imagebroken));

        final Activity getThis = this;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if (foodRecord.getPHOTO_PATH() != null){
                    BitmapWorkerTask task = new BitmapWorkerTask();
                    task.execute(foodRecord.getPHOTO_PATH());
                    try{
                        if (task.get() != null){
                            foodbitmap = task.get();
                            //foodImage.postInvalidate();
                            foodImage.setImageBitmap(task.get());
                            foodImage.setOnClickListener(new imageOnClickListener(getThis, task.get()));
                        }
                    }catch (Exception e){
                        e.printStackTrace();
                    }

                }
            }
        });

        foodImage.setOnClickListener(new imageOnClickListener(this, this.foodbitmap));

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

    }


    private class navFoodRecordDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private int foodRecordId;
        public navFoodRecordDeleteDialogFragment (int foodRecordId){
            this.foodRecordId = foodRecordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    UserLocalStore userLocalStore = new UserLocalStore(getActivity());
                                    String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                                    //navfoodAsyncTask connect = new navfoodAsyncTask(getActivity(), navFoodConstant.DELETE_FOOD_RECORD);
                                    //connect.execute(userId, Integer.toString(foodRecordId) );

                                    //Janus
                                    foodRecord frDB = new foodRecord(getActivity());
                                    frDB.delete((foodRecordId));
                                    String foodDate = getActivity().getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
                                    Intent intent = new Intent();
                                    intent.setClass( getActivity(), MainActivity.class);
                                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
                                    intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();


        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }

    private class navFoodEditButtonOnClickListener implements View.OnClickListener{
        private int foodRecordId;
        private Bitmap foodbitmap;
        private Activity activity;
        public navFoodEditButtonOnClickListener (int foodRecordId, Bitmap foodbitmap, Activity activity){
            this.foodRecordId = foodRecordId;
            this.foodbitmap = foodbitmap;
            this.activity = activity;
        }
        @Override
        public void onClick(View v) {

            navFoodTime timeTool = new navFoodTime();
            String foodDate = timeTool.getDBDateFormal((String) ((TextView) findViewById(R.id.navFoodRecordDate)).getText());
            String foodTime = (String) ((TextView) findViewById(R.id.navFoodRecordTime)).getText();
            String foodType = ((navFoodBiz.DbRecordStringString)(((Spinner)findViewById(R.id.foodType)).getSelectedItem())).getId();
            String foodPlace = ((navFoodBiz.DbRecordStringString)(((Spinner)findViewById(R.id.foodPlace)).getSelectedItem())).getId();

            UserLocalStore userLocalStore = new UserLocalStore(v.getContext());
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);

                String photoPath = "NA"; // NA mean donnot update photoPath

                // handle image change
                if(newFileName!=null && isChangeImage){ // has change the image
                    //System.out.println("Candy can has run  hehe!!!");
                    LinearLayout addPhotoSession = (LinearLayout) findViewById(R.id.addPhotoSession);
                    String oldFileName = (String)addPhotoSession.getContentDescription();

                    //System.out.println("Candy oldFileName=" + oldFileName);

                    // using old file name or new file name
                    if (oldFileName!=null){
                        photoPath = oldFileName;
                    }else{
                        //photoPath = newFileName + userId + systemConstant.IMAGE_TYPE;

                        //Janus
                        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                        String currentTimestamp = s.format(new Date());
                        photoPath = userId+"_"+currentTimestamp;
                    }

                    // pass image to server
                    //Bitmap reducedImage = imageTool.decreaseImageSie(mCurrentPhotoPath);
                    //new navFoodUploadImageAsyncTask(reducedImage,photoPath).execute();

                    //Janus
                    String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+photoPath+".jpg";
                    /*try{
                        copy(new File(mCurrentPhotoPath),new File(strImage));
                    }catch (Exception e){
                        e.printStackTrace();
                    }*/



                    Bitmap image = BitmapFactory.decodeFile(mCurrentPhotoPath);
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    image.compress(Bitmap.CompressFormat.JPEG, systemConstant.IMAGE_COMPRESS, stream);
                    String reducedImage = Base64.encodeToString(stream.toByteArray(), Base64.DEFAULT);

                    imageSyncStack imageSyncStackdb = new imageSyncStack(getBaseContext());
                    imageSyncStackdb.insert(imageSyncStackdb.new imageSyncStack_class(0, photoPath+".jpg", reducedImage));
                    imageSyncStackdb.close();

                    imageStorage imageStorage = new imageStorage(getBaseContext());
                    imageStorage.delete(foodRecord.getPHOTO_PATH()+".jpg");
                    imageStorage.insert(imageStorage.new imageStorage_class(0, photoPath+".jpg", reducedImage));
                    imageStorage.close();

                }

                String inputFoodUnit = ((Spinner)findViewById(R.id.inputFoodUnit)).getSelectedItem().toString();
                //navfoodAsyncTask connect = new navfoodAsyncTask(getActivityContentView(v), navFoodConstant.SAVE_EDIT_FREE_TEXT_FOOD_DETAIL);
                //connect.execute(userId, Integer.toString(foodRecordId), foodDate, foodTime, foodValue, foodType, foodPlace, inputFoodUnit, photoPath);
                //Janus

                foodRecord frdb = new foodRecord(getApplicationContext());
                foodRecord.foodRecord_class orginalRecord = frdb.get(foodRecordId);
                frdb.update(frdb.new foodRecord_class(foodRecordId, Integer.parseInt(userId), foodDate, foodTime, foodType, foodPlace, 0, -1, null, null, null, null, "其他（快速）", "", photoPath));
                frdb.close();

                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), MainActivity.class);
                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_NAVFOOD);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE, foodDate);

                Bundle bundle = new Bundle();
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_FORCE, true);
                bundle.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                ContentResolver.requestSync(null, "com.dmc.myapplication.provider", bundle);



                startActivity(intent);
                finish();

        }
    }

    public void copy(File src, File dst) throws IOException {
        FileInputStream inStream = new FileInputStream(src);
        FileOutputStream outStream = new FileOutputStream(dst);
        FileChannel inChannel = inStream.getChannel();
        FileChannel outChannel = outStream.getChannel();
        inChannel.transferTo(0, inChannel.size(), outChannel);
        inStream.close();
        outStream.close();
    }

    private class imageOnClickListener implements View.OnClickListener{
        private Activity activity;
        private Bitmap foodbitmap;
        public imageOnClickListener(Activity activity, Bitmap foodbitmap){
            this.activity = activity;
            this.foodbitmap = foodbitmap;

        }
        @Override
        public void onClick(View v) {
            CharSequence[] items;
            if (foodbitmap != null){
               items = new CharSequence[]{ "相機", "相簿", "看圖" };
            }else{
               items = new CharSequence[]{ "相機", "相簿" };
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(navFoodEasyEditActivity.this);
            builder.setItems(items, new getImageMethodDialogOnClickListener(this.activity, this.foodbitmap));
            builder.show();
        }
    }

    private class getImageMethodDialogOnClickListener implements DialogInterface.OnClickListener{
        private Activity activity;
        private Bitmap imagebitmap;
        public getImageMethodDialogOnClickListener(Activity activity, Bitmap imagebitmap){
            this.activity = activity;
            this.imagebitmap = imagebitmap;
        }
        public void onClick(DialogInterface dialog, int item) {
            //System.out.println("Candy testing get image=" + item);
            if (item == 0) { // 照相機
                takePhoto();
            }else if (item == 1) { // 相簿
                selectFromGallery();
            }else if(item == 2){
                Intent intent = new Intent(this.activity, NavfoodPhotoViewActivity.class);
                String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
                intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
                navFood_imagebitmapPassing.foodImage = this.imagebitmap;
                overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
                startActivity(intent);
            }
        }
    }

    private void selectFromGallery(){
        Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        intent.setType("image/*");
        startActivityForResult(Intent.createChooser(intent, "Select File"),  navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE);

    }

    private void takePhoto(){
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();
            }
            if (photoFile != null) {
                //Janus
                //UserLocalStore uls = new UserLocalStore(getBaseContext());
                //SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
                //String currentTimestamp = s.format(new Date());

                //String strImage = getBaseContext().getFilesDir().getAbsolutePath()+"/"+uls.getLoggedInUser().userid+"_"+currentTimestamp+".jpg";
                //File myImage = new File(strImage);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(myImage));
                //takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(photoFile));
                startActivityForResult(takePictureIntent, navFoodConstant.GET_ADD_FOOD_IMAGE_CODE);
            }
        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        newFileName =imageTool.getImageName();
        new exportTool().createDirIfNotExists(systemConstant.FILE_PATH_IMAGE);

        File image = new File(systemConstant.FILE_PATH_IMAGE + "/" + newFileName + systemConstant.IMAGE_TYPE);

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.getAbsolutePath();
        return image;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackAction();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK) {
            if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_CODE) { // 照相機
                isChangeImage = true;
                setPic();
            }else if (requestCode == navFoodConstant.GET_ADD_FOOD_IMAGE_FROM_GALLERY_CODE){ //
                isChangeImage = true;
                setGalleryPic(data);
            }
        }
    }

    private void setGalleryPic(Intent data){
        Uri selectedImageUri = data.getData();
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = managedQuery(selectedImageUri, projection, null, null,
                null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();

        mCurrentPhotoPath = cursor.getString(column_index);
        newFileName =imageTool.getImageName();

        //System.out.println("mCurrentPhotoPath="+selectedImagePath);

        Bitmap bm;
        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, options);
        final int REQUIRED_SIZE = 200;
        int scale = 1;
        while (options.outWidth / scale / 2 >= REQUIRED_SIZE
                && options.outHeight / scale / 2 >= REQUIRED_SIZE)
            scale *= 2;
        options.inSampleSize = scale;
        options.inJustDecodeBounds = false;
        bm = imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, options));
        foodImage.setImageBitmap(bm);
    }


    private  void setPic(){
        int targetW = foodImage.getWidth();
        int targetH = foodImage.getHeight();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW/targetW, photoH/targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap rotatedBMP = imageTool.rotateImage90(BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions)) ;

        foodImage.setImageBitmap(rotatedBMP);
        //foodImage.setImageBitmap(bitmap);
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(navFoodConstant.GET_FOOD_DATE, getFoodDate);
        intent.putExtra(systemConstant.REDIRECT_PAGE ,systemConstant.DISPLAY_NAVFOOD);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    class BitmapWorkerTask extends AsyncTask<String, Void, Bitmap> {
        private String imageUrl;

        public BitmapWorkerTask() {
            super();
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onCancelled(Bitmap bitmap) {
            super.onCancelled(bitmap);
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();

        }

        @Override
        protected Bitmap doInBackground(String... strings) {
            imageUrl = strings[0];

                        List<HashMap<String, String>> BRList = new ArrayList<>();
                        HashMap<String, String> temp = new HashMap<>();
                        temp.put("filename", imageUrl);
                        BRList.add(temp);
                        String BRListString = new Gson().toJson(BRList);
                        BRListString = URLEncoder.encode(BRListString);
                        UserLocalStore uld = new UserLocalStore(getBaseContext());

                        String data = "userId="+uld.getLoggedInUser().userid+"&event=food&method=getFoodImage&data="+BRListString;

                        imageStorage imageStorage = new imageStorage(getBaseContext());
                        imageStorage.imageStorage_class localStorageImage = imageStorage.get(imageUrl+".jpg");
                        imageStorage.close();

                        if (localStorageImage!= null) {
                            byte[] decodedString = Base64.decode(localStorageImage.getImageContent(), Base64.DEFAULT);
                            Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                            return decodedByte;
                        }else{
                            String imageString = webHelper.sendRequest("POST", data);

                            if (imageString != null && imageString != ""){
                                byte[] decodedString = Base64.decode(imageString, Base64.DEFAULT);
                                Bitmap decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
                                return decodedByte;

                            }else{
                                return  null;
                            }
                        }


        }
    }
}
