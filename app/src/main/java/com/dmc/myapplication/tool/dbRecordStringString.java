package com.dmc.myapplication.tool;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class dbRecordStringString {
    String value;
    String id;

    public dbRecordStringString(String value, String id) {
        this.value = value;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public String toString() {
        return this.value;
    }
}

