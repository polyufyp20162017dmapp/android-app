package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dmc.myapplication.Models.food;
import com.dmc.myapplication.Models.foodRecord;

/**
 * Created by januslin on 11/10/2016.
 */
public class foodRecordDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "foodRecordDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public foodRecordDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new foodRecordDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //
        db.execSQL(foodRecord.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + foodRecord.TABLE_NAME);
        onCreate(db);
    }
}
