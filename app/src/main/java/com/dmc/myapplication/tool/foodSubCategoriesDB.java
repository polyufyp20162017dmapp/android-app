package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.dmc.myapplication.Models.foodCategories;
import com.dmc.myapplication.Models.foodSubCategories;

/**
 * Created by januslin on 11/10/2016.
 */
public class foodSubCategoriesDB extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "foodSubCategoriesDB.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public foodSubCategoriesDB(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new foodSubCategoriesDB(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //
        db.execSQL(foodSubCategories.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + foodSubCategories.TABLE_NAME);
        onCreate(db);
    }
}
