package com.dmc.myapplication.tool;

import com.dmc.myapplication.systemConstant;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Calendar;

/**
 * Created by KwokSinMan on 11/3/2016.
 */
public class exportTool {

    public void createDirIfNotExists(String filePath){
        File folder = new File(filePath);
        if (!folder.exists()) {
            folder.mkdirs();
        }
    }

    private static String getCurrentTimeFileName(){
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH)+1;
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        int ss = calendar.get(Calendar.SECOND);
        return yy+""+mm+""+dd+"_"+hour+min+ss;
    }

    public static void writeCsvFile(String result, String userId, String file_type) {
        FileOutputStream out =null;
        try {
            String fileName = userId+"_"+file_type+"_"+getCurrentTimeFileName()+".csv";
            new exportTool().createDirIfNotExists(systemConstant.FILE_PATH_EXPORT);
            String filePath = systemConstant.FILE_PATH_EXPORT + File.separator + fileName;
            out =new FileOutputStream(filePath,false);
            //out = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(filePath), "UTF-16LE"));
            //byte[] BOM_UTF8 = { (byte) 0xEF, (byte) 0xBB, (byte) 0xBF }; // uFEFF'
            //out.write(new String(BOM_UTF8));
            //out.write(result);
            out.write(result.getBytes("utf8"));
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            try {
                out.flush();
                out.close();
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}
