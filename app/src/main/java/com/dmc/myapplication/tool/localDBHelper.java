package com.dmc.myapplication.tool;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.*;
import com.dmc.myapplication.reminder.reminderDAO;

/**
 * Created by januslin on 11/10/2016.
 */
public class localDBHelper extends SQLiteOpenHelper {
    public static final String DATABASE_NAME = "data.db";

    public static final int VERSION = 1;
    private static SQLiteDatabase database;

    public localDBHelper(Context context) {
        super(context, DATABASE_NAME, null, VERSION);
    }

    public static SQLiteDatabase getDatabase(Context context) {
        if (database == null || !database.isOpen()) {
            database = new localDBHelper(context).getWritableDatabase();
        }

        return database;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //
        db.execSQL(bodyRecord.CREATE_TABLE);
        //db.execSQL(sync_data_version.CREATE_TABLE);
        db.execSQL(exercise_type.CREATE_TABLE);
        db.execSQL(exercise_type_record.CREATE_TABLE);
        //db.execSQL(food.CREATE_TABLE);
        //db.execSQL(foodCategories.CREATE_TABLE);
        //db.execSQL(foodSubCategories.CREATE_TABLE);
        //db.execSQL(foodRecord.CREATE_TABLE);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        //
        db.execSQL("DROP TABLE IF EXISTS " + bodyRecord.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + exercise_type.TABLE_NAME);
        db.execSQL("DROP TABLE IF EXISTS " + exercise_type_record.TABLE_NAME);
        onCreate(db);
    }
}
