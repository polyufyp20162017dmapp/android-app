package com.dmc.myapplication.tool;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class dbRecordIdString {
    String value;
    int id;

    public dbRecordIdString(String value, int id) {
        this.value = value;
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public String toString() {
        return this.value;
    }

}
