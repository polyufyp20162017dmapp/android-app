package com.dmc.myapplication.tool;

import java.util.Calendar;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class dateTool {
    public static String getDBDateFormal(String uiDateFormal){
        String [] temp = uiDateFormal.split("/");
        uiDateFormal = temp[2]+"-"+temp[1]+"-"+temp[0];  // YYYY-MM-DD
        return uiDateFormal;
    }

    public static String getUIDateFormal(String dbDateFormal){
        String [] temp = dbDateFormal.split("-");
        dbDateFormal = temp[2]+"/"+temp[1]+"/"+temp[0];
        return  dbDateFormal;
    }

    public static String getVurrentDateInDBFormal(){
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        return yy + "-" + mm + "-" +dd;   // UI = DD/MM/YYYY
    }
}
