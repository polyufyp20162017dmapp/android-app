package com.dmc.myapplication.steps;

import android.app.Service;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import com.dmc.myapplication.R;
import com.dmc.myapplication.xiaomi.stepCountObservable;

import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.Observable;
import java.util.Observer;

/**
 * Created by januslin on 15/1/2017.
 */
public class stepObserverService extends Service implements Observer {

    private View v = null;

    @Override
    public void update(Observable observable, Object o) {
        final Button Stepbtn = (Button) v.findViewById(R.id.stepcountbtn);
        // Stepbtn.setText(String.valueOf(stepCount.count));

        try {
            Stepbtn.post(new Runnable() {
                @Override
                public void run() {

                    Stepbtn.setText(String.valueOf(stepCountObservable.count));
                    //stepRecord stepRecordDB = new stepRecord(getContext());
                    //Integer step = (stepRecordDB.getTodayLatestStep());
                    //Stepbtn.setText(String.valueOf(step));
                    //Log.d("Stepbtn.setText", String.valueOf(step));
                    //stepRecordDB.close();
                }
            });
        }catch (Exception e){
            System.out.println(e);
        }

    }
    public stepObserverService() {
        super();
    }

    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    protected void dump(FileDescriptor fd, PrintWriter writer, String[] args) {
        super.dump(fd, writer, args);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
