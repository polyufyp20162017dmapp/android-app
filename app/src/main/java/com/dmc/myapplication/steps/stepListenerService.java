package com.dmc.myapplication.steps;

import android.app.Service;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.IBinder;
import android.util.Log;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.xiaomi.BluetoothState;
import com.dmc.myapplication.xiaomi.stepCountObservable;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.NotifyListener;
import com.zhaoxiaodan.miband.listeners.RealtimeStepsNotifyListener;

import java.io.FileDescriptor;
import java.io.PrintWriter;

/**
 * Created by januslin on 15/1/2017.
 */
public class stepListenerService extends Service {
    public stepListenerService() {
        super();
    }

    @Override
    public void onCreate() {

        super.onCreate();
    }

    @Override
    public void onStart(Intent intent, int startId) {
        super.onStart(intent, startId);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {
                UserLocalStore UserLocalStore = new UserLocalStore(getBaseContext());
                if (UserLocalStore.getUserLoggedIn() && BluetoothState.enabled(getBaseContext()) == true) {
                    System.out.println("BluetoothState.enabled(getContext()) =>" + BluetoothState.enabled(getBaseContext()));
                    //xiaomiDeviceSetting xiaomisetting = new xiaomiDeviceSetting(getContext());
                    //xiaomisetting.update(xiaomisetting.new xiaomiDeviceSetting_class("connectionState", "0"));
                    System.out.println("BluetoothState.connectionState(getContext()) =>" + BluetoothState.connectionState(getBaseContext()));

                    if (true) {
                        BluetoothState.miband = new MiBand(getBaseContext());

                        if (BluetoothState.enabled(getBaseContext())) {
                            BluetoothDevice device = BluetoothState.getDevice(getBaseContext());
                            System.out.println("BluetoothState.getDevice() == null? ==>" + String.valueOf(device));
                            if (device != null) {
                                BluetoothState.miband.connect(device, new ActionCallback() {
                                    @Override
                                    public void onSuccess(Object data) {
                                        Log.d("onSuccess",
                                                "连接成功!!!");


                                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getBaseContext());
                                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_CONNECTED)));
                                        xiaomiDeviceSetting.close();

                                        BluetoothState.miband.setDisconnectedListener(new NotifyListener() {
                                            @Override
                                            public void onNotify(byte[] data) {
                                                Log.d("onSuccess",
                                                        "connection failed!!!");
                                                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getBaseContext());
                                                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                                xiaomiDeviceSetting.close();

                                            }
                                        });

                                        if (BluetoothState.connectionState(getBaseContext()) == BluetoothState.STATE_CONNECTED) {

                                            BluetoothState.miband.getlatestSteps(new ActionCallback() {
                                                @Override
                                                public void onSuccess(Object data) {
                                                    Log.d("RealtimeStepsNotify", data.toString());
                                                    stepCountObservable.getInstance().setData(Integer.parseInt(data.toString()), getBaseContext());


                                                    Log.d("RealtimeStepsNotify", "started!!!");
                                                    BluetoothState.miband.setRealtimeStepsNotifyListener(new RealtimeStepsNotifyListener() {

                                                        @Override
                                                        public void onNotify(int steps) {
                                                            Log.d("", "RealtimeStepsNotifyListener:" + steps);

                                                            stepCountObservable.getInstance().setData(steps, getBaseContext());
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onFail(int errorCode, String msg) {
                                                    System.out.println(msg);
                                                }
                                            });


                                        }
                                    }

                                    @Override
                                    public void onFail(int errorCode, String msg) {
                                        Log.d("onSuccess",
                                                "connection failed!!!");

                                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getBaseContext());
                                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                        xiaomiDeviceSetting.close();
                                    }
                                });

                            }
                        }
                    } else {
                        if (BluetoothState.connectionState(getBaseContext()) == BluetoothState.STATE_CONNECTED) {
                            BluetoothState.miband.getlatestSteps(new ActionCallback() {
                                @Override
                                public void onSuccess(Object data) {
                                    Log.d("RealtimeStepsNotify", data.toString());
                                    stepCountObservable.getInstance().setData(Integer.parseInt(data.toString()), getBaseContext());
                                }

                                @Override
                                public void onFail(int errorCode, String msg) {
                                    System.out.println(msg);
                                }
                            });

                            Log.d("RealtimeStepsNotify", "started!!!");
                            BluetoothState.miband.setRealtimeStepsNotifyListener(new RealtimeStepsNotifyListener() {

                                @Override
                                public void onNotify(int steps) {
                                    Log.d("", "RealtimeStepsNotifyListener:" + steps);

                                    stepCountObservable.getInstance().setData(steps, getBaseContext());
                                }
                            });
                        }
                    }
                }else{
                    stopSelf();
                }
            }
        });

        t.start();


        return super.onStartCommand(intent, flags, startId);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTrimMemory(int level) {
        super.onTrimMemory(level);
    }

    @Override
    public boolean onUnbind(Intent intent) {
        return super.onUnbind(intent);
    }

    @Override
    public void onRebind(Intent intent) {
        super.onRebind(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        super.onTaskRemoved(rootIntent);
    }

    @Override
    protected void dump(FileDescriptor fd, PrintWriter writer, String[] args) {
        super.dump(fd, writer, args);
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}
