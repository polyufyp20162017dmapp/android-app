package com.dmc.myapplication;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import com.dmc.myapplication.Models.sync_data_version;
import com.dmc.myapplication.assessment.assessmentBiz;
import com.dmc.myapplication.assessment.assessmentConstant;
import com.dmc.myapplication.blood.bloodMain;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.bp.bpMain;
import com.dmc.myapplication.dmInfo.webViewFragment;
import com.dmc.myapplication.drug.drugBiz;
import com.dmc.myapplication.drug.drugConstant;
import com.dmc.myapplication.drug.drugDateTool;
import com.dmc.myapplication.export.exportFragment;
import com.dmc.myapplication.gymNew.gymNewBiz;
import com.dmc.myapplication.gymNew.gymNewConstant;
import com.dmc.myapplication.help.helpFragment;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.login.welcomeActivity;
import com.dmc.myapplication.navFood.navFoodBiz;
import com.dmc.myapplication.navFood.navFoodConstant;
import com.dmc.myapplication.navFood.navFoodFragment;
import com.dmc.myapplication.navFood.navFoodTime;
import com.dmc.myapplication.otherBodyIndex.otherBody_Main;
import com.dmc.myapplication.preference.SettingsActivity;
import com.dmc.myapplication.reminder.reminderFragment;
import com.dmc.myapplication.sync.dataSyncService;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.xiaomi.BluetoothState;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.RealtimeStepsNotifyListener;

import java.util.Calendar;

import static java.security.AccessController.getContext;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    public static Context getContext  = null;
    UserLocalStore userLocalStore;
    Integer mode = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        bodyRecordConstant.getApplicationContext = getApplicationContext();
        getContext = this.getBaseContext();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        DrawerLayout mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);

        System.out.println(">>>>>"+getSupportFragmentManager().getBackStackEntryCount());

        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        // set the return control
        //Intent intent = getIntent();
        final int displayId = getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
        System.out.println("displayId="+displayId);

        FragmentManager manager = getSupportFragmentManager();

        if (manager.getBackStackEntryCount() > 0)
        {
            manager.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        }

        getSupportFragmentManager().addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            @Override
            public void onBackStackChanged() {
                System.out.println("getSupportFragmentManager().getBackStackEntryCount()" + getSupportFragmentManager().getBackStackEntryCount());
                System.out.println("getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE)" + getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE));

                if ((getSupportFragmentManager().getBackStackEntryCount() > 0)) {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(true);
                } else {
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                }
            }
        });

        //System.out.println("Candy get display Id="+displayId);



        if(displayId == systemConstant.DISPLAY_HOMEPAGE){
            this.setTitle(R.string.app_name);

            ft.replace(R.id.content_frame, new homePageFragment()).commit();




        }
        if (displayId == systemConstant.DISPLAY_NAVFOOD){
            // 飲食日記
            String getFoodDate = getIntent().getExtras().getString(navFoodConstant.GET_FOOD_DATE);
            setTitle(R.string.nav_food);
            Bundle bundle = new Bundle();
            bundle.putString(navFoodConstant.GET_FOOD_DATE, getFoodDate);
            navFoodFragment navFoodFrag = new navFoodFragment();
            navFoodFrag.setArguments(bundle);
            ft.replace(R.id.content_frame, navFoodFrag).addToBackStack(null).commit();
        }
        if(displayId == systemConstant.DISPLAY_GYM){
            // 運動日記
            //String getGYMDate = getIntent().getExtras().getString(gymConstant.GET_GYM_DATE);
            //gymBiz.startGym(this, getGYMDate, ft);
            String getGYMDate = getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);
            gymNewBiz.startNewGym(this, getGYMDate, ft);
        }
        if(displayId == systemConstant.DISPLAY_DRUG){
            //用藥記錄
            String getDrugDate = getIntent().getExtras().getString(drugConstant.GET_DRUG_DATE);
            drugBiz.startDrug(this, getDrugDate, ft);
        }
        if(displayId == systemConstant.DISPLAY_BLOOD){
            this.setTitle(R.string.title_activity_blood);
            //ft.add(R.id.content_frame, new bloodMain()).hide(new homePageFragment()).commit();
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ft.replace(R.id.content_frame, new bloodMain()).commit();
        }
        if(displayId == systemConstant.DISPLAY_BP){
            this.setTitle(R.string.title_activity_bp);
            ft.replace(R.id.content_frame, new bpMain()).addToBackStack(null).commit();
        }
        if(displayId == systemConstant.DISPLAY_REMINDER){
            this.setTitle(R.string.medic_reminder);
            ft.replace(R.id.content_frame, new reminderFragment()).addToBackStack(null).commit();
        }
        if(displayId == systemConstant.DISPLAY_OTHER_INDEX) {
            this.setTitle(R.string.title_activity_om);
            ft.replace(R.id.content_frame, new otherBody_Main()).addToBackStack(null).commit();
        }
        if(displayId == systemConstant.DISPLAY_ASSESSMENT){
            String year = getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_YEAR);
            String weekNo = getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_WEEK);
            assessmentBiz.startAssessment(this,year, weekNo, ft);
        }


        //Set Menu Drawer
        //Janus
        /*DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();*/

        //NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        //navigationView.setNavigationItemSelectedListener(this);
        userLocalStore = new UserLocalStore(this);


    }

    /*@Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
            this.finish();
            System.exit(0);
        }
    }*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        menu.clear();
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        //if (id == R.id.action_settings) {
        //    return true;
        //}
        switch (item.getItemId()) {
            case android.R.id.home:
                FragmentManager fm = getSupportFragmentManager();
              //  if (fm.getBackStackEntryCount() > 0) {
               //     getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
               //     fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
               //     getSupportActionBar().setDisplayHomeAsUpEnabled(false);
               // }else{
                    getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
                    getSupportFragmentManager().beginTransaction().replace(R.id.content_frame, new homePageFragment())
                            .setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_right).commit();
                    getSupportActionBar().setDisplayHomeAsUpEnabled(false);
                //}
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        int id = item.getItemId();

        if (id == R.id.home_page){
            this.setTitle(R.string.home_page);
            ft.replace(R.id.content_frame, new homePageFragment()).commit();

        }else if (id == R.id.nav_blood) {
            this.setTitle(R.string.nav_blood);
            System.out.println("getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);");
            getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
            bloodMain fragment1 = new bloodMain();
            mode = R.id.nav_blood;
            ft.replace(R.id.content_frame, fragment1).addToBackStack("tag").commit();
            getSupportFragmentManager().executePendingTransactions();

        } else if (id == R.id.nav_food) {
            // 飲食日記
            navFoodBiz.startNavFood(this, navFoodTime.getVurrentDateInDBFormal(), ft);
        } else if (id == R.id.nav_bp) {
            //血壓
            this.setTitle(R.string.nav_bp);
            bpMain fragment1 = new bpMain();
            ft.replace(R.id.content_frame, fragment1).commit();

        } else if (id == R.id.nav_medicine) {
            //用藥記錄
            drugBiz.startDrug(this, drugDateTool.getVurrentDateInDBFormal(), ft);
        } else if (id == R.id.nav_dminfo) {
            //糖尿資訊
            this.setTitle(R.string.title_activity_dminfo);
            webViewFragment fragment1 = new webViewFragment();
            ft.replace(R.id.content_frame, fragment1).commit();

        } else if (id == R.id.nav_gym) {
            // 運動日記
            //gymBiz.startGym(this, gymDateTimeTool.getVurrentDateInDBFormal(), ft);
            gymNewBiz.startNewGym(this, dateTool.getVurrentDateInDBFormal(), ft);

        } else if (id == R.id.other_Index){
            this.setTitle(R.string.other_Index);
            otherBody_Main fragment1 = new otherBody_Main();
            ft.replace(R.id.content_frame, fragment1).commit();

        } else if (id == R.id.medic_reminder) {
            //服藥提醒
            this.setTitle(R.string.medic_reminder);
            ft.replace(R.id.content_frame, new reminderFragment()).commit();

        } else if (id == R.id.nav_forum) {
            //NEW
            Calendar now = Calendar.getInstance();
            String year = Integer.toString(now.get(Calendar.YEAR));
            String weekNo = Integer.toString(now.get(Calendar.WEEK_OF_YEAR));
            assessmentBiz.startAssessment(this, year, weekNo, ft);
        } else if (id == R.id.nav_setting) {
            // 用戶設定
            this.setTitle(R.string.nav_usersetting);
            Intent intent = new Intent();
            intent.setClass(MainActivity.this,SettingsActivity.class);
            startActivity(intent);

        } else if (id == R.id.file_export) {
            this.setTitle(R.string.file_export);
            ft.replace(R.id.content_frame, new exportFragment()).commit();

        } else if (id == R.id.nav_help) {
            this.setTitle(R.string.nav_help);
            ft.replace(R.id.content_frame, new helpFragment()).commit();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        mode = getIntent().getIntExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
        System.out.println("dddd");
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onResume() {
        getIntent().putExtra(systemConstant.REDIRECT_PAGE, mode);
        super.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();

        if (authentication() == true){

        }else {
            startActivity(new Intent(MainActivity.this, welcomeActivity.class));
        }

    }

    private boolean authentication(){

        return userLocalStore.getUserLoggedIn();
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onDestroy() {
        if (BluetoothState.miband != null) {
            try{
                BluetoothState.miband.disconnect();
            }catch (Exception e){
                System.out.print(e);
            }
        }
        super.onDestroy();
    }


}
