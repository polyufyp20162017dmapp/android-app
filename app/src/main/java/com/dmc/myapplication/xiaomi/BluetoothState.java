package com.dmc.myapplication.xiaomi;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothClass;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.BluetoothLeScanner;
import android.content.Context;
import android.content.pm.LauncherApps;
import android.util.Log;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.NotifyListener;

import java.io.*;
import java.util.Set;

/**
 * Created by januslin on 6/1/2017.
 */
public class BluetoothState implements Serializable {

    public static final int STATE_NOT_CONNECTED = 0;
    public static final int STATE_CONNECTED = 1;

    public static final String filename = "btState.pref";
    public static MiBand miband = null;
    public static String deviceAddress = "00:00:00:00:00:00";
    public static String deviceName = "";

    public static int connectionState(Context context){
        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
        xiaomiDeviceSetting.xiaomiDeviceSetting_class obj = xiaomiDeviceSetting.get("connectionState");
        return Integer.parseInt(obj.getoValue());
    }

    public static boolean enabled(Context context){
        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
        xiaomiDeviceSetting.xiaomiDeviceSetting_class obj = xiaomiDeviceSetting.get("enabled");
        if (Integer.parseInt(obj.getoValue()) == 1){
            return true;
        }
        return false;
    }

    public static void setConnectionState(boolean connected,BluetoothDevice device, Context context) {
        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);

        if(connected){
            xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(STATE_CONNECTED)));
        }
        else{
            xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(STATE_NOT_CONNECTED)));

        }

        xiaomiDeviceSetting.close();

        if(device != null) {
            deviceAddress = device.getAddress();
            deviceName = device.getName();
        }

    }

    public static void connect(final Context context){
        System.out.println("Bt Connecting");
        miband = new MiBand(context);
        miband.connect(getDevice(context), new ActionCallback() {

            @Override
            public void onSuccess(Object data) {
                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_CONNECTED)));
                xiaomiDeviceSetting.close();

                miband.setDisconnectedListener(new NotifyListener() {
                    @Override
                    public void onNotify(byte[] data) {
                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                        xiaomiDeviceSetting.close();

                    }
                });

            }

            @Override
            public void onFail(int errorCode, String msg) {
                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                xiaomiDeviceSetting.close();            }
        });
    }

    public static void connect2(final Context context){
        System.out.println("Bt Connecting");
        miband = new MiBand(context);
        final Context thiscontext = context;
        miband.connect(getDevice(context), new ActionCallback() {

            @Override
            public void onSuccess(Object data) {
                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_CONNECTED)));
                xiaomiDeviceSetting.close();

                miband.setDisconnectedListener(new NotifyListener() {
                    @Override
                    public void onNotify(byte[] data) {
                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                        xiaomiDeviceSetting.close();

                    }
                });

            }

            @Override
            public void onFail(int errorCode, String msg) {
                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                xiaomiDeviceSetting.close();
            }
        });
    }



    public static void saveConnectionState(Context cxt) throws IOException {
        //FileOutputStream fos = cxt.openFileOutput(filename, Context.MODE_PRIVATE);
        //ObjectOutputStream oos = new ObjectOutputStream(fos);
        //oos.writeInt(connectionState(cxt));
        //oos.writeUTF(deviceAddress);
        //oos.writeUTF(deviceName);

        Setting setting = new Setting(cxt);
        setting.setSetting(setting.new setting(deviceAddress, deviceName));
    }

    public static void loadConnectionState(Context cxt) throws IOException {
        //FileInputStream fis = cxt.openFileInput(filename);
        //ObjectInputStream ois = new ObjectInputStream(fis);
        //xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(cxt);
        //xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(ois.readInt())));
        //xiaomiDeviceSetting.close();
        Setting setting = new Setting(cxt);
        Setting.setting settingobj = setting.getSetting();
        deviceAddress = settingobj.mac_address;
        deviceName = settingobj.device_name;
    }

    public static BluetoothDevice getDevice(Context cxt) {
        try {
            loadConnectionState(cxt);
        }catch (Exception e){

        }
        BluetoothAdapter btAdapter = BluetoothAdapter.getDefaultAdapter();
        if(!btAdapter.isEnabled())
            btAdapter.enable();

        /*Set<BluetoothDevice> pairedDevices = btAdapter.getBondedDevices();
        System.out.println(pairedDevices);
        for(BluetoothDevice d : pairedDevices){
            System.out.println(d.getName()+" "+d.getAddress());
            if(d.getAddress().equalsIgnoreCase(deviceAddress))
                return d;

            if (d.getName().equals("MI1S")){
                return d;
            }
        }*/
        try{
            BluetoothDevice device = btAdapter.getRemoteDevice(deviceAddress);
            if (device != null){
                return device;
            }

        }catch (Exception e){
            e.printStackTrace();
        }



        return null;
    }
}
