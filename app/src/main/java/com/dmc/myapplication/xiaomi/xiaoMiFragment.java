package com.dmc.myapplication.xiaomi;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;
import com.dmc.myapplication.R;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.NotifyListener;
import com.zhaoxiaodan.miband.listeners.RealtimeStepsNotifyListener;

import java.util.ArrayList;
import java.util.HashMap;

import static android.content.ContentValues.TAG;

/**
 * Created by KwokSinMan on 18/3/2016.
 */
public class xiaoMiFragment extends Fragment {
    private MiBand miband;

    HashMap<String, BluetoothDevice> devices = new HashMap<String, BluetoothDevice>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        getActivity().setTitle("手環設定");

        final View v = inflater.inflate(R.layout.xiaomiband_setting_edit, container, false);

        miband = new MiBand(getContext());

        final ArrayAdapter adapter = new ArrayAdapter<String>(getActivity(), R.layout.xiaomi_deviceitem, new ArrayList<String>());

        final ScanCallback scanCallback = new ScanCallback() {
            @Override
            public void onScanResult(int callbackType, ScanResult result) {
                BluetoothDevice device = result.getDevice();
                Log.d("XiaoMiSearch",
                        "Found Device: name:" + device.getName() + ",uuid:"
                                + device.getUuids() + ",add:"
                                + device.getAddress() + ",type:"
                                + device.getType() + ",bondState:"
                                + device.getBondState() + ",rssi:" + result.getRssi());

                String item = device.getName() + "|" + device.getAddress();
                if (
                        //(device.getName().toUpperCase().contains("MS1A") ||
                         //       device.getName().toUpperCase().contains("MS1S")) &&
                                !devices.containsKey(item)) {
                    devices.put(item, device);
                    adapter.add(item);
                }

            }
        };

        final CompoundButton XiaoMiBandScan = (CompoundButton) v.findViewById(R.id.switch1);

        if (BluetoothState.enabled(getContext())){
            XiaoMiBandScan.setChecked(true);
        }

        XiaoMiBandScan.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                // do something, the isChecked will be
                // true if the switch is in the On position

                if (isChecked){
                    BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
                    if (!mBluetoothAdapter.isEnabled()) {
                        Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                        startActivityForResult(enableBtIntent, 1);
                    }

                    MiBand.startScan(scanCallback);
                }else{
                    MiBand.stopScan(scanCallback);
                    xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getContext());
                    xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("enabled", String.valueOf(0)));
                    xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                    xiaomiDeviceSetting.close();

                    if (BluetoothState.miband != null){
                        try {
                            BluetoothState.miband.disconnect();
                            BluetoothState.miband = null;
                        }catch (Exception e){
                            System.out.println(e);
                        }
                    }
                }
            }
        });

        ListView lv = (ListView) v.findViewById(R.id.listView);
        //final Button stepbtn = (Button) v.findViewById(R.id.stepcountbtn);
        lv.setAdapter(adapter);
        final Fragment that = this;
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String item = ((TextView) view).getText().toString();
                if (devices.containsKey(item)) {

                    MiBand.stopScan(scanCallback);
                    final BluetoothDevice device = devices.get(item);

                    miband.connect(device, new ActionCallback() {

                        @Override
                        public void onSuccess(Object data) {
                            Log.d("onSuccess",
                                    "连接成功!!!");

                            //BluetoothState.enabled = true;
                            xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getContext());
                            xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("enabled", String.valueOf(1)));
                            xiaomiDeviceSetting.close();
                            BluetoothState.setConnectionState(true, device, getContext());
                            try {
                                BluetoothState.saveConnectionState(getContext());
                            }catch (Exception e){
                                e.printStackTrace();
                            }
                            BluetoothState.miband = miband;




                            BluetoothState.miband.getlatestSteps(new ActionCallback() {
                                @Override
                                public void onSuccess(Object data) {
                                    Log.d("RealtimeStepsNotify", data.toString());
                                    stepCountObservable.getInstance().setData(Integer.parseInt(data.toString()), MainActivity.getContext);

                                    Log.d("RealtimeStepsNotify", "started!!!");
                                    BluetoothState.miband.setRealtimeStepsNotifyListener(new RealtimeStepsNotifyListener() {

                                        @Override
                                        public void onNotify(int steps) {
                                            Log.d("", "RealtimeStepsNotifyListener2:" + steps);

                                            stepCountObservable.getInstance().setData(steps, MainActivity.getContext);
                                        }
                                    });
                                }

                                @Override
                                public void onFail(int errorCode, String msg) {
                                    System.out.println(msg);
                                }
                            });



                            miband.setDisconnectedListener(new NotifyListener() {
                                @Override
                                public void onNotify(byte[] data) {
                                    Log.d("onSuccess",
                                            "connection failed!!!");
                                    BluetoothState.setConnectionState(false, device, getContext());

                                }
                            });

                            //Go to Menu
                            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                            ft.remove(that);
                            ft.commit();
                            getActivity().getSupportFragmentManager().popBackStack();

                        }

                        @Override
                        public void onFail(int errorCode, String msg) {
                            Log.d("onFail", "connect fail, code:" + errorCode + ",mgs:" + msg);
                            //BluetoothState.enabled = false;
                            //BluetoothState.connectionState = BluetoothState.STATE_NOT_CONNECTED;
                            xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getContext());
                            xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("enabled", String.valueOf(0)));
                            xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                            xiaomiDeviceSetting.close();
                            AlertDialog.Builder builder1 = new AlertDialog.Builder(getActivity());
                            builder1.setMessage("無法連接此設備。");
                            builder1.setCancelable(true);

                            builder1.setPositiveButton(
                                    "明白！",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    });

                            AlertDialog alert11 = builder1.create();
                            alert11.show();
                        }
                    });
                }
            }
        });


        return v;
    }
}
