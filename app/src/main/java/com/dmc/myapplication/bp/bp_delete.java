package com.dmc.myapplication.bp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.TypedValue;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.blood.blood_edit;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.systemConstant;

import java.util.List;

/**
 * Created by Po on 9/3/2016.
 */
public class bp_delete extends AppCompatActivity {

    private Context ctx;
    private TextView editTextBpHValue, editTextBpLValue, editTextHrValue ;
    private String recordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bp_del);
        ctx = getApplicationContext();


        Intent intent =getIntent();
        recordId = intent.getStringExtra("recordId");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");
        final int getPeriodType = Integer.parseInt(intent.getStringExtra("typePeriod"));
        final String getBpHValue = intent.getStringExtra("bpHValue");
        final String getBpLValue = intent.getStringExtra("bpLValue");
        final String getHeartRateValue = intent.getStringExtra("heartRateValue");

        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date
        TextView editTextDate = (TextView) findViewById(R.id.bp_del_date);
        editTextDate.setEnabled(false);
        editTextDate.setText(getDate);



        //handle the time
        TextView editTextTime = (TextView) findViewById(R.id.bp_del_time);
        editTextTime.setEnabled(false);
        editTextTime.setText(getTime);


        //handle the blood value field
        editTextBpHValue = (TextView) findViewById(R.id.bp_del_bp_up_value);
        editTextBpHValue.setText(getBpHValue);
        editTextBpHValue.setEnabled(false);

        editTextBpLValue = (TextView) findViewById(R.id.bp_del_bp_low_value);
        editTextBpLValue.setText(getBpLValue);
        editTextBpLValue.setEnabled(false);

        editTextHrValue = (TextView) findViewById(R.id.bp_del_hr_value);
        editTextHrValue.setText(getHeartRateValue);
        editTextHrValue.setEnabled(false);

        Button buttonBpDel = (Button) findViewById(R.id.bp_del_button);
        buttonBpDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new deleteDialogFragment(recordId);
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });
        Button buttonBpEdit = (Button) findViewById(R.id.bp_edit_button);
        buttonBpEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getBpEditRecord( recordId, getDate, getTime,  Integer.toString(getPeriodType),  getBpHValue,  getBpLValue,  getHeartRateValue);
            }
        });

    }


   private class deleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private String recordId;
        public deleteDialogFragment (String recordId){
            this.recordId = recordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //bodyAsyncTask delRecord = new bodyAsyncTask(getActivity(), bodyRecordConstant.DELETE_BODY_RECORD,bodyRecordConstant.GET_BP_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                    //String record_type = bodyRecordConstant.GET_BP_TYPE_CODE;
                                    //delRecord.execute(recordId, record_type);
                                    bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                                    bodyRecord.delete(Integer.parseInt(recordId));
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), MainActivity.class);
                                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BP);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();

                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }


    public void getBpEditRecord( String recordId, String date, String time, String typePeriod, String bpHValue , String bpLValue, String heartRateValue){

        Intent intent = new Intent();
        intent.setClass(this, bp_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("bpHValue", bpHValue);
        intent.putExtra("bpLValue", bpLValue);
        intent.putExtra("heartRateValue", heartRateValue);
        startActivity(intent);
        finish();
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        //finish();
    }

}
