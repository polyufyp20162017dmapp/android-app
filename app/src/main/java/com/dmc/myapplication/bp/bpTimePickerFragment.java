package com.dmc.myapplication.bp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.TimePicker;

import com.dmc.myapplication.R;

/**
 * Created by Po on 6/3/2016.
 */
public class bpTimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    public bpTimePickerFragment() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        //get current time

        String displayTime = ((EditText) getActivity().findViewById(R.id.bp_add_time)).getText().toString();
        String [] displayDateArray = displayTime.split(":");
        int hour = Integer.parseInt(displayDateArray[0]);
        int minute = Integer.parseInt(displayDateArray[1]);

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, hour, minute, true);
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (hourOfDay<10 && minute<10){
            String displayAsTime ="0"+hourOfDay + ":0" + minute + ":00";
            ((EditText) getActivity().findViewById(R.id.bp_add_time)).setText(displayAsTime);
        } else
        if (hourOfDay<10 && minute>=10){
            String displayAsTime ="0"+hourOfDay + ":" + minute + ":00";
            ((EditText) getActivity().findViewById(R.id.bp_add_time)).setText(displayAsTime);
        } else
        if (hourOfDay>=10 && minute<10){
            String displayAsTime =hourOfDay + ":0" + minute + ":00";
            ((EditText) getActivity().findViewById(R.id.bp_add_time)).setText(displayAsTime);
        } else
        {
            String displayAsTime =hourOfDay + ":" + minute + ":00";
            ((EditText) getActivity().findViewById(R.id.bp_add_time)).setText(displayAsTime);
        }

    }

}
