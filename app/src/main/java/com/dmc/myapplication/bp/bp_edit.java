package com.dmc.myapplication.bp;

import android.support.v7.app.AppCompatActivity;
import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;


import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

/**
 * Created by Po on 9/3/2016.
 */
public class bp_edit extends AppCompatActivity {

    private Toolbar toolbar;
    private Context ctx;
    private EditText editTextDate, editTextTime, editTextBpHValue, editTextBpLValue, editTextHrValue;
    private Button buttonBpAdd;
    private UserLocalStore userLocalStore;
    private User user;
    private String recordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bp_edit);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();

        Intent intent = getIntent();
        recordId = intent.getStringExtra("recordId");
        String getDate = intent.getStringExtra("date");
        String getTime = intent.getStringExtra("time");
        final String getBpHValue = intent.getStringExtra("bpHValue");
        final String getBpLValue = intent.getStringExtra("bpLValue");
        final String getHeartRateValue = intent.getStringExtra("heartRateValue");


        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.bp_add_date);
        editTextDate.setText(getDate);

        ImageView setBloodDate = (ImageView) findViewById(R.id.setBp_add_date);
        setBloodDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new bpDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.bp_add_time);
        editTextTime.setText(getTime);
        ImageView setTime = (ImageView) findViewById(R.id.setBp_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3 = new bpTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });


        //handle the blood pressure & heart rate values field
        editTextBpHValue = (EditText) findViewById(R.id.bp_add_bp_up_value);
        editTextBpHValue.setText(getBpHValue);
        editTextBpLValue = (EditText) findViewById(R.id.bp_add_bp_low_value);
        editTextBpLValue.setText(getBpLValue);
        editTextHrValue = (EditText) findViewById(R.id.bp_add_hr_value);
        editTextHrValue.setText(getHeartRateValue);

        buttonBpAdd = (Button) findViewById(R.id.bp_add_button);
        buttonBpAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validBpHr();
            }
        });


    }

    private void validBpHr() {

        // Reset errors.
        editTextBpHValue.setError(null);
        editTextBpLValue.setError(null);
        editTextHrValue.setError(null);

        // Store values of input.
        String bpUpper = editTextBpHValue.getText().toString();
        String bpLower = editTextBpLValue.getText().toString();
        String heartRate = editTextHrValue.getText().toString();


        boolean cancel = false;
        View focusView = null;

        // Check for a valid input, if the user entered one.
        if (TextUtils.isEmpty(bpUpper)||!isValidBp(bpUpper)) {
            editTextBpHValue.setError(getString(R.string.bp_bp_error));
            focusView =  editTextBpHValue;
            cancel = true;
        }
        if (TextUtils.isEmpty(bpLower) || !isValidBp(bpLower)) {
            editTextBpLValue.setError(getString(R.string.bp_bp_error));
            focusView =  editTextBpLValue;
            cancel = true;
        }

        if (TextUtils.isEmpty(heartRate) || !isValidHr(heartRate)) {
            editTextHrValue.setError(getString(R.string.bp_hr_error));
            focusView =  editTextHrValue;
            cancel = true;
        }
        if (!TextUtils.isEmpty(bpUpper) && !TextUtils.isEmpty(bpLower)) {
            if (Integer.parseInt(bpLower) >= Integer.parseInt(bpUpper)) {
                editTextBpLValue.setError(getString(R.string.bp_bp_error2));
                focusView =  editTextBpLValue;
                cancel = true;
            }

            if ((Integer.parseInt(bpUpper) > bodyRecordConstant.GET_BP_SYS_UPPER_LIMIT) || (Integer.parseInt(bpUpper) < bodyRecordConstant.GET_BP_LOWER_LIMIT) ) {
                editTextBpHValue.setError(getString(R.string.bp_bp_warning));
                focusView =  editTextBpHValue;
                cancel = true;
            }

            if  ((Integer.parseInt(bpLower) > bodyRecordConstant.GET_BP_DIA_UPPER_LIMIT) || (Integer.parseInt(bpLower) < bodyRecordConstant.GET_BP_LOWER_LIMIT) ) {
                editTextBpLValue.setError(getString(R.string.bp_bp_warning));
                focusView =  editTextBpLValue;
                cancel = true;
            }
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
           // bodyAsyncTask editRecord = new bodyAsyncTask(this, bodyRecordConstant.EDIT_BODY_RECORD, bodyRecordConstant.GET_BP_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
            //String user_id = Integer.toString(user.userid);
            //String record_type = bodyRecordConstant.GET_BP_TYPE_CODE;
            //String date = editTextDate.getText().toString();
            //String time = editTextTime.getText().toString();
            //editRecord.execute(user_id, record_type, date, time, bpUpper, bpLower, heartRate,recordId);
            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();
            bodyRecord.update(a.new bodyRecord(Integer.parseInt(recordId), Integer.parseInt(bodyRecordConstant.GET_BP_TYPE_CODE), (user.userid), editTextDate.getText().toString(), editTextTime.getText().toString(), 0, 0, 0, 0, Integer.parseInt(bpUpper), Integer.parseInt(bpLower), Integer.parseInt(heartRate), 0, 0, 0, 0, 0, 0, 0, 0, ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BP);
            this.startActivity(intent);
            this.finish();
        }
    }


    private boolean isValidBp(String bp){

        // int bpValue = Integer.parseInt(bp);

        try{
            if(bp.contains(".")){
                return false;
            }
        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }


    private boolean isValidHr(String bp) {

        Double bpValue = Double.parseDouble(bp);

        if (bpValue > bodyRecordConstant.GET_HR_UPPER_LIMIT || bpValue <= bodyRecordConstant.GET_HR_LOWER_LIMIT) {
            return false;
        }
        try {
            if (bp.contains(".")) {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BP);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        //finish();
    }
}