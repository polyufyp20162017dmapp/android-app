package com.dmc.myapplication.bp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;


/**
 * Created by Po on 6/3/2016.
 */
public class bpMain extends Fragment {

    //private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Context ctx;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v =  inflater.inflate(R.layout.bp_slide, container, false);
        ctx = getActivity().getApplicationContext();

        setHasOptionsMenu(true);

        //set Table View
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) v.findViewById(R.id.sliding_tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(10);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewPager.removeView((View) getView());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getActivity().getMenuInflater();
        inflater.inflate(R.menu.blood_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent();
                intent.setClass(this.getActivity(), bp_add.class);
                getActivity().startActivity(intent);
                //getActivity().finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        BpPagerAdapter adapter = new BpPagerAdapter(getChildFragmentManager(),ctx);
        adapter.addFragment(new bpFragment(), "圖表");
        adapter.addFragment(new bpFragmentList(), "詳細資料");
        viewPager.setAdapter(adapter);
    }


}
