package com.dmc.myapplication.gym;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

public class gymEditDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gym_add_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        final String gymRecordId = intent.getExtras().getString("gym_id");
        String isFreeText = intent.getExtras().getString("is_free_text");

        //System.out.println("Candy gymRecordId ="+gymRecordId);
        //System.out.println("Candy isFreeText ="+isFreeText);

        //hidden add record session
        LinearLayout addRecordButtonSession = (LinearLayout) findViewById(R.id.addRecordButtonSession);
        addRecordButtonSession.setVisibility(View.GONE);

        // set data picker
        ImageView setGymRecordDate = (ImageView) findViewById(R.id.setGymViewDate);
        setGymRecordDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymRecordDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setGymRecordTime = (ImageView) findViewById(R.id.setNavGymRecordTime);
        setGymRecordTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymRecordTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set value picker
        ImageView setGymRecordValue = (ImageView) findViewById(R.id.changeGymValue);
        setGymRecordValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new gymRecordNumberPickerFragment();
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        //set update Edited Data
        Button gymEditButton = (Button) findViewById(R.id.gymEdit);
        gymEditButton.setOnClickListener(new gymEditButtonOnClickListener(gymRecordId)) ;

        //delete Gym  record
        Button gymDeleteButton = (Button) findViewById(R.id.gymDelete);
        gymDeleteButton.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new gymRecordDeleteDialogFragment(gymRecordId);
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });



        //get Data from DB
        UserLocalStore userLocalStore = new UserLocalStore(this);
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        gymAsyncTask connect = new gymAsyncTask(this, gymConstant.GET_GYM_DETAIL);
        connect.execute(userId, gymRecordId, isFreeText);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private class gymEditButtonOnClickListener implements View.OnClickListener{
        private String gymRecordId;
        public gymEditButtonOnClickListener (String gymRecordId){
            this.gymRecordId = gymRecordId;
        }
        @Override
        public void onClick(View v) {

            UserLocalStore userLocalStore = new UserLocalStore(v.getContext());
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
            gymDateTimeTool tool = new gymDateTimeTool();
            String gymDate = tool.getDBDateFormal((String) ((TextView) findViewById(R.id.gymViewDate)).getText());
            String gymTime = (String) ((TextView) findViewById(R.id.gymRecordTime)).getText();
            String gymValue = (String) ((TextView) findViewById(R.id.gymValue)).getText();

            //System.out.println("Candy edit gymRecordId =" + gymRecordId);
            //System.out.println("Candy edit gymDate =" + gymDate);
            //System.out.println("Candy edit gymTime =" + gymTime);
            //System.out.println("Candy edit gymValue =" + gymValue);

            gymAsyncTask connect = new gymAsyncTask((Activity) v.getContext(), gymConstant.SAVE_EDIT_GYM_RECORD);
            connect.execute(userId, gymRecordId, gymDate, gymTime, gymValue);

        }
    }

    private class gymRecordDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private String gymRecordId;
        public gymRecordDeleteDialogFragment (String gymRecordId){
            this.gymRecordId = gymRecordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    UserLocalStore userLocalStore = new UserLocalStore(getActivity());
                                    String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                                    gymAsyncTask connect = new gymAsyncTask(getActivity(), gymConstant.DELETE_GYM_RECORD);
                                    connect.execute(userId, gymRecordId);
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String gymDate = getIntent().getExtras().getString(gymConstant.GET_GYM_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
        intent.putExtra(gymConstant.GET_GYM_DATE,gymDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }


}
