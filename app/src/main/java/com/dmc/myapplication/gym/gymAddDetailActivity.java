package com.dmc.myapplication.gym;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.Calendar;

public class gymAddDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gym_add_detail);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        Intent intent = getIntent();
        final int gymId = intent.getIntExtra("gym_id", gymConstant.GET_NOT_FOUND_CODE);
        final String isFreeText = intent.getExtras().getString("is_free_text");
        String gymName = intent.getExtras().getString("gym_name");

        //System.out.println("Candy gymId = "+gymId);
        //System.out.println("Candy isFreeText = " + isFreeText);
        //System.out.println("Candy gymName = "+gymName);

        //hidden edit record session
        LinearLayout editRecordButtonSession = (LinearLayout) findViewById(R.id.editRecordButtonSession);
        editRecordButtonSession.setVisibility(View.GONE);

        //set gym name
        TextView gymNameTextView = (TextView)findViewById(R.id.gymName);
        gymNameTextView.setText(gymName);

        // set gym  Date
        String getGymDate = getIntent().getExtras().getString(gymConstant.GET_GYM_DATE);
        TextView gymDate = (TextView)findViewById(R.id.gymViewDate);
        gymDate.setText(new gymDateTimeTool().getUIDateFormal(getGymDate));

        //set gym Time
        TextView gymTimeTextView = (TextView)findViewById(R.id.gymRecordTime);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        gymDateTimeTool gymTime = new gymDateTimeTool();
        gymTimeTextView.setText(gymTime.getTimeString(hour) + ":" + gymTime.getTimeString(min));

        // set data picker
        ImageView setGymRecordDate = (ImageView) findViewById(R.id.setGymViewDate);
        setGymRecordDate.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymRecordDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setGymRecordTime = (ImageView) findViewById(R.id.setNavGymRecordTime);
        setGymRecordTime.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymRecordTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        //set gym value (default)
        TextView gymValue = (TextView) findViewById(R.id.gymValue);
        gymValue.setText("30");


        // set value picker
        ImageView setGymRecordValue = (ImageView) findViewById(R.id.changeGymValue);
        setGymRecordValue.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new gymRecordNumberPickerFragment();
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        //set "Finish" Button Action
        Button finishButton = (Button) findViewById(R.id.gymAdd);
        finishButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View arg0) {
                UserLocalStore userLocalStore = new UserLocalStore(arg0.getContext());
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                gymDateTimeTool tool = new gymDateTimeTool();
                String gymDate = tool.getDBDateFormal((String) ((TextView) findViewById(R.id.gymViewDate)).getText());
                String gymTime = (String) ((TextView) findViewById(R.id.gymRecordTime)).getText();
                String gymValue = (String) ((TextView) findViewById(R.id.gymValue)).getText();

                if (isFreeText.equals(gymConstant.GET_IS_FREE_TEXT_CODE)){
                    String gymName = (String) ((TextView) findViewById(R.id.gymName)).getText();
                    gymAsyncTask connect = new gymAsyncTask((Activity) arg0.getContext(), gymConstant.SAVE_FREE_TEXT_GYM_RECORD);
                    connect.execute(userId, gymDate, gymTime, gymValue, gymName);
                }else if(isFreeText.equals(gymConstant.GET_IS_NOT_FREE_TEXT_CODE)){
                    gymAsyncTask connect = new gymAsyncTask((Activity) arg0.getContext(), gymConstant.SAVE_GYM_RECORD);
                    connect.execute(userId, gymDate, gymTime, gymValue, Integer.toString(gymId));
                }

            }
        });

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        String getGymDate = getIntent().getExtras().getString(gymConstant.GET_GYM_DATE);
        Intent intent = new Intent();
        intent.setClass(this, gymAddActivity.class);
        intent.putExtra(gymConstant.GET_GYM_DATE, getGymDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
