package com.dmc.myapplication.gym;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

/**
 * Created by KwokSinMan on 2/2/2016.
 */
public class gymAddCommonFrag extends Fragment {
    public gymAddCommonFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.gym_add_common, container, false);

        ListView commonListView = (ListView) v.findViewById(R.id.commonListView);
        commonListView.setOnItemClickListener(new gymCommonListOnItemClickListener (this.getActivity()));

        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        gymAsyncTask connect = new gymAsyncTask(this.getActivity(), gymConstant.GET_GYM_COMMON_LIST);
        connect.execute(userId);

        return v;
    }


    private class gymCommonListOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public gymCommonListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int gymId= ((gymBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId();
            String gymName= ((gymBiz.DbRecordStringId) adapter.getItemAtPosition(position)).toString();
            Intent intent = new Intent();
            intent.setClass(activity, gymAddDetailActivity.class);
            intent.putExtra("gym_id", gymId);
            intent.putExtra("gym_name", gymName);
            intent.putExtra("is_free_text",gymConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(gymConstant.GET_GYM_DATE, activity.getIntent().getExtras().getString(gymConstant.GET_GYM_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

}

//String[] mobileArray = {"叉燒包","菜芯","白飯","上素蒸粉果","咖喱蒸魷魚","大包"};
//ArrayAdapter adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.gym_add_common_list, R.id.itemName, mobileArray);
//commonListView.setAdapter(adapter);
