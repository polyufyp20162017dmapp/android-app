package com.dmc.myapplication.gym;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.Calendar;
import java.util.HashMap;


/**
 * Created by KwokSinMan on 1/2/2016.
 */
public class gymFragment extends Fragment {
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.gym_main, container, false);
        String getGymDate = this.getArguments().getString(gymConstant.GET_GYM_DATE);

        // set gym Date
        TextView gymViewDate = (TextView) v.findViewById(R.id.gymViewDate);
        gymViewDate.setText(new gymDateTimeTool().getUIDateFormal(getGymDate));

        // set data picker
        ImageView setGymDate = (ImageView) v.findViewById(R.id.setGymViewDate);
        setGymDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymViewDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        /*
        //set Add record event
        Button gymAdd = (Button) v.findViewById(R.id.gymAdd);
        gymAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), gymAddActivity.class);
                intent.putExtra(gymConstant.GET_GYM_DATE, new gymDateTimeTool().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.gymViewDate)).getText()));
                startActivity(intent);
            }
        });
*/

        // set  gymList onItemClickListener
        ListView gymList = (ListView) v.findViewById(R.id.gymList);
        gymList.setOnItemClickListener(new gymListOnItemClickListener(this.getActivity()));


        // get Data from DB to display total gym min and gym result
        gymAsyncTask connect = new gymAsyncTask(this.getActivity(), gymConstant.GET_GYM_RECORD);
        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        connect.execute(userId, getGymDate);
        return v;
    }

    private class gymViewDatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            String displayDate = (String)((TextView) getActivity().findViewById(R.id.gymViewDate)).getText();
            String [] displayDateArray = displayDate.split("/");
            int yy = Integer.parseInt(displayDateArray[2]);
            int mm = Integer.parseInt(displayDateArray[1]) - 1; // because Jan =0
            int dd = Integer.parseInt(displayDateArray[0]);
            DatePickerDialog dialog =  new DatePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this, yy,mm,dd);
            dialog.getDatePicker().setMaxDate( Calendar.getInstance().getTimeInMillis());
            return dialog;
        }

        // On the date picker , to confirm the following action
        public void onDateSet(DatePicker view, int yy, int mm, int dd) {
            // set display date
            mm = mm + 1; // because Jan =0
            String displayAsDate = dd + "/" + mm + "/" + yy;
            ((TextView) getActivity().findViewById(R.id.gymViewDate)).setText(displayAsDate);

            //get gym record from DB
            gymAsyncTask connect = new gymAsyncTask(this.getActivity(), gymConstant.GET_GYM_RECORD);
            UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
            String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
            gymDateTimeTool tool = new gymDateTimeTool();
            connect.execute(userId, tool.getDBDateFormal(displayAsDate));
        }
    }

    private class gymListOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public gymListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            ListView gymList = (ListView) activity.findViewById(R.id.gymList);
            String gymRecordId=((HashMap<String, String>)(gymList.getItemAtPosition(position))).get("gymRecordId");
            String is_free_text=((HashMap<String, String>)(gymList.getItemAtPosition(position))).get("isFreeText");

            //System.out.println("Candy gymRecordId="+gymRecordId);
            Intent intent = new Intent();
            intent.setClass(activity, gymEditDetailActivity.class);
            intent.putExtra("gym_id", gymRecordId);
            intent.putExtra("is_free_text",is_free_text);
            intent.putExtra(gymConstant.GET_GYM_DATE,  new gymDateTimeTool().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.gymViewDate)).getText()));
            activity.startActivity(intent);
            activity.finish();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.gym_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  R.id.gym_menuItem_add){
            Intent intent = new Intent();
            intent.setClass(getActivity(), gymAddActivity.class);
            intent.putExtra(gymConstant.GET_GYM_DATE, new gymDateTimeTool().getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.gymViewDate)).getText()));
            getActivity().startActivity(intent);
            getActivity().finish();
        }
        return true;
    }

}
