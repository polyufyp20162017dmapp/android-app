package com.dmc.myapplication.gym;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.dmc.myapplication.R;

import java.util.ArrayList;

/**
 * Created by KwokSinMan on 2/2/2016.
 */
public class gymAddSearchFrag extends Fragment {

    SearchView search;

    public gymAddSearchFrag() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.gym_add_search, container, false);

        // input box for searching gym
        search= (SearchView) v.findViewById(R.id.searchGym);
        search.setQueryHint("輸入食物名稱");
        search.setOnQueryTextListener(new gymSearchOnQueryTextListener(this.getActivity()));

        // display searched gym list
        ListView listView = (ListView) v.findViewById(R.id.searchedGymList);
        listView.setOnItemClickListener(new gymSearchOnItemClickListener(this.getActivity()));
        listView.setAdapter(null);

        TextView gymNotFoundTextView = (TextView) v.findViewById(R.id.gymNotFound);
        gymNotFoundTextView.setVisibility(View.GONE);

        // free input gym item
        Button gymFreeAdd = (Button) v.findViewById(R.id.gymFreeAdd);
        gymFreeAdd.setOnClickListener(new gymFreeAddOnClickListener(this.getActivity()));
        gymFreeAdd.setVisibility(View.GONE);



        // speakButton
        ImageButton btnSpeak = (ImageButton) v.findViewById(R.id.speakButton);
        btnSpeak.setOnClickListener(new speakToTextOnClickListener(this.getActivity()));

        return v;
    }

    private class gymSearchOnQueryTextListener implements SearchView.OnQueryTextListener{
        Activity activity;
        public gymSearchOnQueryTextListener(Activity activity){ this.activity = activity;}

        @Override
        public boolean onQueryTextSubmit(String query) {
            return false;
        }

        @Override
        public boolean onQueryTextChange(String newText) {
            // remove all space
            newText = newText.replaceAll(" ", "");

            if (newText.isEmpty()) {
                ListView listView = (ListView) activity.findViewById(R.id.searchedGymList);
                listView.setAdapter(null);

                TextView gymNotFoundTextView = (TextView) activity.findViewById(R.id.gymNotFound);
                gymNotFoundTextView.setVisibility(View.GONE);

                Button gymFreeAdd = (Button) activity.findViewById(R.id.gymFreeAdd);
                gymFreeAdd.setVisibility(View.GONE);
            }
            else{
                gymAsyncTask connect = new gymAsyncTask(activity, gymConstant.GET_GYM_SEARCH_RESULT);
                connect.execute(newText);
            }
            return false;
        }
    }

    private class gymSearchOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public gymSearchOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            int gymId= ((gymBiz.DbRecordStringId) adapter.getItemAtPosition(position)).getId();
            String gymName= ((gymBiz.DbRecordStringId) adapter.getItemAtPosition(position)).toString();
            Intent intent = new Intent();
            intent.setClass(activity, gymAddDetailActivity.class);
            intent.putExtra("gym_id", gymId);
            intent.putExtra("gym_name", gymName);
            intent.putExtra("is_free_text",gymConstant.GET_IS_NOT_FREE_TEXT_CODE);
            intent.putExtra(gymConstant.GET_GYM_DATE, activity.getIntent().getExtras().getString(gymConstant.GET_GYM_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }

    private class gymFreeAddOnClickListener implements View.OnClickListener{
        Activity activity;
        public gymFreeAddOnClickListener(Activity activity){ this.activity=activity; }
        @Override
        public void onClick(View arg0) {
            String gymName = search.getQuery().toString();
            //System.out.println("Candy Sear/ch List gymName=" + gymName);
            Intent intent = new Intent();
            intent.setClass(activity, gymAddDetailActivity.class);
            intent.putExtra("gym_name", gymName);
            intent.putExtra("is_free_text",gymConstant.GET_IS_FREE_TEXT_CODE);
            intent.putExtra(gymConstant.GET_GYM_DATE, activity.getIntent().getExtras().getString(gymConstant.GET_GYM_DATE));
            activity.startActivity(intent);
            activity.finish();
        }
    }



    private class speakToTextOnClickListener implements View.OnClickListener{
        Activity activity;
        public speakToTextOnClickListener(Activity activity){ this.activity=activity; }
        @Override
        public void onClick(View arg0) {
            Intent intent = new Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
            intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, "zh-TW");
            try {
                // start 2 way interface ( A pass data to B, B return data to A)
                startActivityForResult(intent, gymConstant.GET_SPEECH_RESULT_CODE);
            } catch (ActivityNotFoundException aException) {
                Toast t = Toast.makeText(activity,"你的手機不支援語音辦別", Toast.LENGTH_LONG);
                t.show();
            }
        }
    }

    // get the result from the google speech to text (Call Back)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case gymConstant.GET_SPEECH_RESULT_CODE: {
                if (resultCode == gymConstant.GET_SPEECH_RESULT_OK && null != data) {
                    ArrayList<String> text = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                    search.setQuery(text.get(0),true);
                    search.setFocusable(true);
                    search.setIconified(false);
                    search.requestFocusFromTouch();
                }
                break;
            }

        }
    }

}
