package com.dmc.myapplication;


import android.Manifest;
import android.accounts.Account;
import android.accounts.AccountManager;
import android.bluetooth.BluetoothDevice;
import android.content.ContentResolver;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.*;
import android.widget.Button;
import android.widget.LinearLayout;

import com.dmc.myapplication.Models.stepRecord;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;
import com.dmc.myapplication.assessment.assessmentBiz;
import com.dmc.myapplication.blood.bloodMain;
import com.dmc.myapplication.bp.bpMain;
import com.dmc.myapplication.dmInfo.webViewFragment;
import com.dmc.myapplication.drug.drugBiz;
import com.dmc.myapplication.drug.drugDateTool;
import com.dmc.myapplication.export.exportFragment;
import com.dmc.myapplication.gymNew.gymNewBiz;
import com.dmc.myapplication.help.helpFragment;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.navFood.navFoodBiz;
import com.dmc.myapplication.navFood.navFoodTime;
import com.dmc.myapplication.otherBodyIndex.otherBody_Main;
import com.dmc.myapplication.preference.SettingsActivity;
import com.dmc.myapplication.reminder.reminderFragment;
import com.dmc.myapplication.report.reportActivity;
import com.dmc.myapplication.steps.stepFragment;
import com.dmc.myapplication.steps.stepListenerService;
import com.dmc.myapplication.steps.stepObserverService;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.widgets.HomePageAdvisor;
import com.dmc.myapplication.widgets.HomePageAdvisorInfo;
import com.dmc.myapplication.widgets.HomePageRecordGlucoseRemind;
import com.dmc.myapplication.xiaomi.BluetoothState;
import com.dmc.myapplication.xiaomi.stepCountObservable;
import com.dmc.myapplication.xiaomi.xiaoMiFragment;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.NotifyListener;
import com.zhaoxiaodan.miband.listeners.RealtimeStepsNotifyListener;
import com.zhaoxiaodan.miband.model.VibrationMode;

import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import static android.content.Context.ACCOUNT_SERVICE;
import static com.dmc.myapplication.xiaomi.BluetoothState.miband;

/**
 * Created by KwokSinMan on 30/11/2015.
 */
public class homePageFragment extends Fragment implements Observer {

    private View v = null;
    private Thread observerT;

    @Override
    public void update(Observable observable, Object o) {
        final Button Stepbtn = (Button) v.findViewById(R.id.stepcountbtn);
        // Stepbtn.setText(String.valueOf(stepCount.count));

        try {
            Stepbtn.post(new Runnable() {
                @Override
                public void run() {

                    Stepbtn.setText(String.valueOf(stepCountObservable.count));
                    //stepRecord stepRecordDB = new stepRecord(getContext());
                    //Integer step = (stepRecordDB.getTodayLatestStep());
                    //Stepbtn.setText(String.valueOf(step));
                    //Log.d("Stepbtn.setText", String.valueOf(step));
                    //stepRecordDB.close();
                }
            });
        } catch (Exception e) {
            System.out.println(e);
        }

    }


    @Override
    public void onDetach() {
        super.onDetach();
        stepCountObservable.getInstance().deleteObserver(this);

        getActivity().stopService(new Intent(getActivity(), stepListenerService.class));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //Intent serviceIntent = new Intent(stepObserverService.class.getName())
        //serviceIntent.putExtra("homePageThis", this);
        //context.startService(serviceIntent);

        observerT = new Thread(new Runnable() {
            @Override
            public void run() {
                stepCountObservable.getInstance().addObserver(homePageFragment.this);
            }
        });

        observerT.start();


        Runnable r = new Runnable() {
            @Override
            public void run() {
                UserLocalStore UserLocalStore = new UserLocalStore(getContext());

                if (UserLocalStore.getUserLoggedIn()) {
                    Account newAccount = new Account(String.valueOf("更新程式"), "com.dmc.myapplication.myacc");
                    AccountManager accountManager = (AccountManager) getActivity().getSystemService(ACCOUNT_SERVICE);
                    accountManager.addAccountExplicitly(newAccount, null, null);
                    //ContentResolver.addPeriodicSync(newAccount, "com.dmc.myapplication.provider", params, 5);
                    //ContentResolver.setSyncAutomatically(newAccount, "com.dmc.myapplication.provider", true);
                    // ContentResolver.requestSync(newAccount,"com.dmc.myapplication.provider",params);
                    ContentResolver.setIsSyncable(newAccount, "com.dmc.myapplication.provider", 1);
                    //ContentResolver.setIsSyncable(newAccount, "com.dmc.myapplication.provider2", 1);
                    ContentResolver.setSyncAutomatically(newAccount, "com.dmc.myapplication.provider", true);
                    //ContentResolver.setSyncAutomatically(newAccount, "com.dmc.myapplication.provider2", true);

                    ContentResolver.addPeriodicSync(
                            newAccount,
                            "com.dmc.myapplication.provider",
                            Bundle.EMPTY,
                            (60 * 60 * 24));
                }

                if (UserLocalStore.getUserLoggedIn() && BluetoothState.enabled(getContext()) == true) {
                    System.out.println("BluetoothState.enabled(getContext()) =>" + BluetoothState.enabled(getContext()));
                    //xiaomiDeviceSetting xiaomisetting = new xiaomiDeviceSetting(getContext());
                    //xiaomisetting.update(xiaomisetting.new xiaomiDeviceSetting_class("connectionState", "0"));
                    System.out.println("BluetoothState.connectionState(getContext()) =>" + BluetoothState.connectionState(getContext()));

                    if (BluetoothState.miband == null) {
                        BluetoothState.miband = new MiBand(getContext());

                        if (BluetoothState.enabled(getContext())) {
                            BluetoothDevice device = BluetoothState.getDevice(getContext());
                            System.out.println("BluetoothState.getDevice() == null? ==>" + String.valueOf(device));
                            if (device != null) {
                                BluetoothState.miband.connect(device, new ActionCallback() {
                                    @Override
                                    public void onSuccess(Object data) {
                                        Log.d("onSuccess",
                                                "连接成功!!!");


                                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getContext());
                                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_CONNECTED)));
                                        xiaomiDeviceSetting.close();

                                        BluetoothState.miband.setDisconnectedListener(new NotifyListener() {
                                            @Override
                                            public void onNotify(byte[] data) {
                                                Log.d("onSuccess",
                                                        "connection failed!!!");
                                                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getContext());
                                                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                                xiaomiDeviceSetting.close();

                                            }
                                        });

                                        if (BluetoothState.connectionState(getContext()) == BluetoothState.STATE_CONNECTED) {

                                            BluetoothState.miband.getlatestSteps(new ActionCallback() {
                                                @Override
                                                public void onSuccess(Object data) {
                                                    Log.d("RealtimeStepsNotify", data.toString());
                                                    stepCountObservable.getInstance().setData(Integer.parseInt(data.toString()), getContext());


                                                    Log.d("RealtimeStepsNotify", "started!!!");
                                                    BluetoothState.miband.setRealtimeStepsNotifyListener(new RealtimeStepsNotifyListener() {

                                                        @Override
                                                        public void onNotify(int steps) {
                                                            Log.d("", "RealtimeStepsNotifyListener:" + steps);

                                                            stepCountObservable.getInstance().setData(steps, getContext());
                                                        }
                                                    });
                                                }

                                                @Override
                                                public void onFail(int errorCode, String msg) {
                                                    System.out.println(msg);
                                                }
                                            });


                                        }
                                    }

                                    @Override
                                    public void onFail(int errorCode, String msg) {
                                        Log.d("onSuccess",
                                                "connection failed!!!");

                                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(getContext());
                                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                        xiaomiDeviceSetting.close();
                                    }
                                });

                            }
                        }
                    } else {
                        if (BluetoothState.connectionState(getContext()) == BluetoothState.STATE_CONNECTED) {
                            /*BluetoothState.miband.getlatestSteps(new ActionCallback() {
                                @Override
                                public void onSuccess(Object data) {
                                    Log.d("RealtimeStepsNotify", data.toString());
                                    stepCountObservable.getInstance().setData(Integer.parseInt(data.toString()), getContext());
                                }

                                @Override
                                public void onFail(int errorCode, String msg) {
                                    System.out.println(msg);
                                }
                            });*/

                            Log.d("RealtimeStepsNotify", "started!!!");
                            BluetoothState.miband.setRealtimeStepsNotifyListener(new RealtimeStepsNotifyListener() {

                                @Override
                                public void onNotify(int steps) {
                                    Log.d("", "RealtimeStepsNotifyListener:" + steps);

                                    stepCountObservable.getInstance().setData(steps, getContext());
                                }
                            });
                        }
                    }


                }
            }
        };


        View v = inflater.inflate(R.layout.home_page, container, false);
        this.v = v;
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.home_page);
        //FragmentTransaction ft = getFragmentManager().beginTransaction();

        FragmentManager fm = getFragmentManager();
        getActivity().getIntent().putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);

        checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION);
        //ActionBar actionBar = getActivity().getActionBar();
        //actionBar.setDisplayHomeAsUpEnabled(false);
        //actionBar.setDisplayShowHomeEnabled(false);
        /*UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        User user = userLocalStore.getLoggedInUser();
        TextView nameView, birthdayView, sexView, smokerView;

        nameView = (TextView) v.findViewById(R.id.display_name);
        nameView.setText(user.name);

        birthdayView = (TextView) v.findViewById(R.id.birthday);
        birthdayView.setText(user.birthday);

        sexView = (TextView) v.findViewById(R.id.sex);
        if (user.sex == 0)
            sexView.setText("女");
        else
            sexView.setText("男");

         smokerView = (TextView) v.findViewById(R.id.smoker);
        if (user.smoker == 0)
            smokerView.setText("否");
        else
            smokerView.setText("是");*/

        System.out.println("hello----------");

        //Advisor
        HomePageAdvisor.process(v, getActivity());

        Button advisor_help_button = (Button) v.findViewById(R.id.advisor_help_button);
        advisor_help_button.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), HomePageAdvisorInfo.class);
                startActivity(intent);
            }
        });

        Button stepbtn = (Button) v.findViewById(R.id.stepcountbtn);
        stepbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println("hello----------2");

                getActivity().setTitle("步數記錄");
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                stepFragment fragment00 = new stepFragment();
                ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, fragment00).addToBackStack(null).commit();
            }

        });

        Button btnBlood = (Button) v.findViewById(R.id.booldSurgerBtn);
        btnBlood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setTitle(R.string.title_activity_blood);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                bloodMain fragment1 = new bloodMain();
                ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, fragment1).addToBackStack(null).commit();
            }

        });


        Button btnFood = (Button) v.findViewById(R.id.foodBtn);
        btnFood.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                navFoodBiz.startNavFood(getActivity(), navFoodTime.getVurrentDateInDBFormal(), ft);
            }
        });


        Button btnBp = (Button) v.findViewById(R.id.booldPresureBtn);
        btnBp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().setTitle(R.string.title_activity_bp);
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                bpMain fragment1 = new bpMain();
                ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, fragment1).addToBackStack(null).commit();
            }

        });


        Button btnGym = (Button) v.findViewById(R.id.activityBtn);
        btnGym.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                //gymBiz.startGym(getActivity(), gymDateTimeTool.getVurrentDateInDBFormal(), ft);
                gymNewBiz.startNewGym(getActivity(), dateTool.getVurrentDateInDBFormal(), ft);
            }
        });

        LinearLayout btnDrug = (LinearLayout) v.findViewById(R.id.pillBtn);
        btnDrug.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                drugBiz.startDrug(getActivity(), drugDateTool.getVurrentDateInDBFormal(), ft);
            }
        });

        LinearLayout btnDminfo = (LinearLayout) v.findViewById(R.id.diabetesInfoBtn);
        btnDminfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                getActivity().setTitle(R.string.nav_dminfo);
                Bundle bundle = new Bundle();
                webViewFragment webViewFragment = new webViewFragment();
                webViewFragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, webViewFragment).addToBackStack(null).commit();
            }

        });


        LinearLayout alarmBtn = (LinearLayout) v.findViewById(R.id.alarmBtn);
        alarmBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                getActivity().setTitle(R.string.medic_reminder);
                Bundle bundle = new Bundle();
                reminderFragment reminderFragment = new reminderFragment();
                reminderFragment.setArguments(bundle);
                ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, reminderFragment).addToBackStack(null).commit();

            }

        });

        LinearLayout assessmentBtn = (LinearLayout) v.findViewById(R.id.assessmentBtn);
        assessmentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                Calendar now = Calendar.getInstance();
                String year = Integer.toString(now.get(Calendar.YEAR));
                String weekNo = Integer.toString(now.get(Calendar.WEEK_OF_YEAR));
                assessmentBiz.startAssessment(getActivity(), year, weekNo, ft);
            }

        });

        LinearLayout bodyIndicatorBtn = (LinearLayout) v.findViewById(R.id.bodyIndicatorBtn);
        bodyIndicatorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, new otherBody_Main()).addToBackStack(null).commit();

            }

        });


        //Todo: Reported crash
        //UserLocalStore userLocalStore = new UserLocalStore(getContext());
        //if (userLocalStore.getUserLoggedIn()) {
        //    HomePageRecordGlucoseRemind.process(v, getActivity());
        //}

        stepRecord stepRecordDB = new stepRecord(getContext());
        stepCountObservable.getInstance().setDBDatatoPresent(stepRecordDB.getTodayLatestStep());
        System.out.println("Previous step record = " + stepCountObservable.count);
        stepRecordDB.close();
        //r.run();
        getActivity().startService(new Intent(getActivity(), stepListenerService.class));


        return v;
    }

    /*@Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getActivity().getMenuInflater();
        inflater.inflate(R.menu.home_menu, menu);
        super.onPrepareOptionsMenu(menu);
    }*/

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.home_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        switch (item.getItemId()) {
            case R.id.main_usersetting_btn:
                Intent intent = new Intent();
                intent.setClass(getActivity(), SettingsActivity.class);
                startActivity(intent);
                break;

            case R.id.main_file_export_btn:
                ft.replace(R.id.content_frame, new exportFragment()).addToBackStack(null).commit();
                break;

            case R.id.fitnessBand_setting:
                ft.replace(R.id.content_frame, new xiaoMiFragment()).addToBackStack(null).commit();
                break;

            case R.id.main_nav_help_btn:
                ft.replace(R.id.content_frame, new helpFragment()).addToBackStack(null).commit();
                break;

            case R.id.reportBtn:
                //ft.replace(R.id.content_frame, new helpFragment()).addToBackStack(null).commit();
                intent = new Intent();
                intent.setClass(getActivity(), reportActivity.class);
                startActivity(intent);
                break;

            default:
                break;
        }
        return false;
    }

    public int checkPermission(String permission) {
        // Here, thisActivity is the current activity
        //Manifest.permission.READ_CONTACTS
        int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
        if (ContextCompat.checkSelfPermission(getActivity(),
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    permission)) {

            } else {

                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET,
                                Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.VIBRATE, Manifest.permission.RECEIVE_BOOT_COMPLETED,
                                Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.GET_ACCOUNTS, Manifest.permission.ACCOUNT_MANAGER, Manifest.permission.GET_ACCOUNTS_PRIVILEGED,
                                Manifest.permission.READ_SYNC_SETTINGS, Manifest.permission.READ_SYNC_STATS, Manifest.permission.WRITE_SYNC_SETTINGS,
                                Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED, Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            }
        }
        return MY_PERMISSIONS_REQUEST_READ_CONTACTS;
    }
}
