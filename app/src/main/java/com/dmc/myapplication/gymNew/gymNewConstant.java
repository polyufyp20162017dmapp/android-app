package com.dmc.myapplication.gymNew;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewConstant {
    public final static String GET_GYM_DATE = "redirect_gym_new_date"; //DB formal
    public final static String GET_GYM_RECORD ="gymNew/getGymTypeRecord.php";
    public final static String GET_GYM_TYPE_DDL = "gymNew/getGymTypeList.php";
    public final static String GET_ADD_RECORD ="gymNew/saveGymTypeRecord.php";
    public final static String GET_GYM_RECORD_DETAIL = "gymNew/getGymRecordDetail.php";
    public final static String DELETE_GYM_RECORD = "gymNew/deleteGymRecord.php";
    public final static String EDIT_GYP_RECORD="gymNew/editGymRecord.php";

    public final static int GET_NUM_GYM_TYPE = 3;


    //Share Data Temp Storage
    public static String getGymDate = null;




}
