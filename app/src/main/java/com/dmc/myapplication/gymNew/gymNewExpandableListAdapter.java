package com.dmc.myapplication.gymNew;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewExpandableListAdapter extends BaseExpandableListAdapter {
    private Context _context;
    private List<String> _listDataHeader =  new ArrayList<String>(); //低強度, 中等強度, 劇烈活動
    private HashMap<String, List<gymNewRecord>> _listDataChild = new HashMap<String, List<gymNewRecord>>(); // 運動記錄

    public gymNewExpandableListAdapter(Context context, List<String> header, HashMap<String, List<gymNewRecord>> child) {
        this._context = context;
        this._listDataHeader = header;
        this._listDataChild = child;
    }

    @Override
    public Object getChild(int groupPosition, int childPosititon) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).get(childPosititon);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {

        gymNewRecord childText = (gymNewRecord) getChild(groupPosition, childPosition);


        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.gym_new_child_item, null);
        }

        TextView txtListChild = (TextView) convertView.findViewById(R.id.gymNewTime);
        TextView txtListChildValue = (TextView) convertView.findViewById(R.id.gymNewValue);

        txtListChild.setText(childText.getGymNewTime());
        txtListChildValue.setText(childText.getGymNewValue()+"分鐘");

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition)).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition);
        //cANDY
        String[] value = headerTitle.split(",");

        if (convertView == null) {
            LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(R.layout.gym_new_group_item, null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.gymNewTypeName);
        TextView lblListHeaderValue = (TextView) convertView.findViewById(R.id.gymNewTypeTotalTime);

        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(value[0]);
        if (value!=null && value.length>1){
            lblListHeaderValue.setText(value[1]+"分鐘");
        }

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
