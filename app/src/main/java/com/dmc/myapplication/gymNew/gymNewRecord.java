package com.dmc.myapplication.gymNew;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewRecord {
    private String gymNewValue; // for display 總運動時間
    private String gymNewTime; // 開始運動的時間
    private int gymNewRecordId;

    public gymNewRecord(int gymNewRecordId, String gymNewTime, String gymNewValue){
        this.gymNewRecordId = gymNewRecordId;
        this.gymNewTime = gymNewTime;
        this.gymNewValue = gymNewValue;

    }

    public String getGymNewValue() {
        return gymNewValue;
    }
    public String getGymNewTime() {
        return gymNewTime;
    }
    public int getGymNewRecordId() {
        return gymNewRecordId;
    }

}
