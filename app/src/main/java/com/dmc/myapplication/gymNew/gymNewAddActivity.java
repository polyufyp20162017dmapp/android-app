package com.dmc.myapplication.gymNew;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.exercise_type_record;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.dateTool;
import com.dmc.myapplication.tool.dbRecordIdString;
import com.dmc.myapplication.tool.timeObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by KwokSinMan on 13/3/2016.
 */
public class gymNewAddActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.gym_new_add);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //hidden edit record session
        LinearLayout editRecordButtonSession = (LinearLayout) findViewById(R.id.editRecordButtonSession);
        editRecordButtonSession.setVisibility(View.GONE);

        // set gym  Date
        String getGymDate = getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);
        TextView gymDate = (TextView)findViewById(R.id.gymNewViewDate);
        gymDate.setText(dateTool.getUIDateFormal(getGymDate));

        //set gym Time
        TextView gymTimeTextView = (TextView)findViewById(R.id.gymNewRecordTime);
        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        int min = calendar.get(Calendar.MINUTE);
        timeObject gymTime = new timeObject();
        gymTimeTextView.setText(gymTime.getTimeString(hour) + ":" + gymTime.getTimeString(min));

        // set data picker
        ImageView setGymRecordDate = (ImageView) findViewById(R.id.setGymNewViewDate);
        setGymRecordDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymNewDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // set time picker
        ImageView setGymRecordTime = (ImageView) findViewById(R.id.setGymNewRecordTime);
        setGymRecordTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new gymNewTimePickerFragment();
                newFragment.show(getFragmentManager(), "TimePicker");
            }
        });

        // set Help Pop up
        ImageView gymTypeHelp = (ImageView) findViewById(R.id.gymTypeHelp);
        gymTypeHelp.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View arg0) {
                gymNewBiz biz = new gymNewBiz();
                biz.popUpWindow(gymNewAddActivity.this);
            }
        });

        //set gym value (default)
        TextView gymValue = (TextView) findViewById(R.id.gymNewValue);
        gymValue.setText("30");

        // set value picker
        ImageView setGymRecordValue = (ImageView) findViewById(R.id.changeGymNewValue);
        setGymRecordValue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new gymNewNumberPickerFragment();
                newFragment.show(getFragmentManager(), "NumberPicker");
            }
        });

        // finish button
        Button finishButton = (Button) findViewById(R.id.gymNewAdd);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                UserLocalStore userLocalStore = new UserLocalStore(arg0.getContext());
                String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
                String gymDate = dateTool.getDBDateFormal((String) ((TextView) findViewById(R.id.gymNewViewDate)).getText());
                String gymTime = (String) ((TextView) findViewById(R.id.gymNewRecordTime)).getText();
                String gymValue = (String) ((TextView) findViewById(R.id.gymNewValue)).getText();
                String gymType = Integer.toString(((dbRecordIdString) (((Spinner) findViewById(R.id.gymNewType)).getSelectedItem())).getId());

                //System.out.println("Candy userId="+userId);
                //System.out.println("Candy gymDate="+gymDate);
                //System.out.println("Candy gymTime="+gymTime);
                //System.out.println("Candy gymValue="+gymValue);
                //System.out.println("Candy gymType="+gymType);

                //Janus
                //gymNewAsyncTask connect = new gymNewAsyncTask(getActivityContentView(arg0), gymNewConstant.GET_ADD_RECORD);
                //connect.execute(userId, gymDate, gymTime, gymType, gymValue);

                exercise_type_record etrdb = new exercise_type_record(getApplicationContext());
                etrdb.insert(etrdb.new exercise_type_record_class(0, Integer.parseInt(userId), gymDate, gymTime, Integer.parseInt(gymType), Integer.parseInt(gymValue)));

                String gymDate2 = dateTool.getDBDateFormal((String) ((TextView) findViewById(R.id.gymNewViewDate)).getText());

                // System.out.println("Candy gymDate="+gymDate);
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), MainActivity.class);
                intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
                intent.putExtra(gymNewConstant.GET_GYM_DATE, gymDate2);
                startActivity(intent);
                finish();

            }
        });

        gymNewAsyncTask connect = new gymNewAsyncTask(this, gymNewConstant.GET_GYM_TYPE_DDL);
        connect.execute();

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
}

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            handleBackAction();
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction() {
        String getGymDate = getIntent().getExtras().getString(gymNewConstant.GET_GYM_DATE);
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_GYM);
        intent.putExtra(gymNewConstant.GET_GYM_DATE, getGymDate);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
