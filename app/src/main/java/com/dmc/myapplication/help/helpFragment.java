package com.dmc.myapplication.help;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;

/**
 * Created by KwokSinMan on 18/3/2016.
 */
public class helpFragment  extends Fragment {
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.help_main, container, false);
        return v;
    }
}
