package com.dmc.myapplication.login;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import android.widget.Button;

/**
 * Created by januslin on 21/10/2016.
 */

public class welcomeActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        super.onCreate(savedInstanceState);
        // enable transitions
        setContentView(R.layout.welcomescreen);

        checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION);

        Button nextPageBtn = (Button) findViewById(R.id.welcome_nextBtn);
        nextPageBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(welcomeActivity.this, terms.class));
                finish();
            }
        });

    }

    public int checkPermission(String permission) {
        // Here, thisActivity is the current activity
        //Manifest.permission.READ_CONTACTS
        int  MY_PERMISSIONS_REQUEST_READ_CONTACTS =1;
        if (ContextCompat.checkSelfPermission(this,
                permission)
                != PackageManager.PERMISSION_GRANTED) {

            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    permission)) {

            } else {

                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.INTERNET,
                                Manifest.permission.ACCESS_NETWORK_STATE, Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.VIBRATE, Manifest.permission.RECEIVE_BOOT_COMPLETED,
                                Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CONTACTS, Manifest.permission.WRITE_CONTACTS,
                                Manifest.permission.GET_ACCOUNTS, Manifest.permission.ACCOUNT_MANAGER, Manifest.permission.GET_ACCOUNTS_PRIVILEGED,
                                Manifest.permission.READ_SYNC_SETTINGS, Manifest.permission.READ_SYNC_STATS, Manifest.permission.WRITE_SYNC_SETTINGS,
                                Manifest.permission.BLUETOOTH, Manifest.permission.BLUETOOTH_ADMIN, Manifest.permission.BLUETOOTH_PRIVILEGED, Manifest.permission.CAMERA},
                        MY_PERMISSIONS_REQUEST_READ_CONTACTS);

            }
        }
        return MY_PERMISSIONS_REQUEST_READ_CONTACTS;
    }

    @Override
    public void onBackPressed() {
    }
}
