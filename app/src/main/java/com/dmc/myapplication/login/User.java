package com.dmc.myapplication.login;

//import org.joda.time.DateTime;
//import org.joda.time.LocalDate;
//import org.joda.time.LocalTime;

/**
 * Created by Po on 24/1/2016.
 */
public class User {



    public String name;
    public String birthday;
    public String morning_start;
    public String morning_end;
    public String breakfast_start;
    public String breakfast_end;
    public String lunch_start;
    public String lunch_end;
    public String dinner_start;
    public String dinner_end;
    public String bed_start;
    public String bed_end;
    public String injection;
    public int userid, sex, smoker,height_unit, weight_unit, waist_unit; //only 0 and 1

    //Janus
    public int pre_meal_low, pre_meal_med, pre_meal_high;
    public int post_meal_low, post_meal_med, post_meal_high;


    public User(int userid, String name, String birthday, int sex, int smoker, String injection,
                String morning_start,  String morning_end,  String breakfast_start,
                String breakfast_end, String lunch_start, String lunch_end, String dinner_start,
                String dinner_end, String bed_start, String bed_end,int height_unit, int weight_unit, int waist_unit, int pre_meal_low,
                int pre_meal_med, int pre_meal_high, int post_meal_low, int post_meal_med, int post_meal_high) {

        this.userid = userid;
        this.name = name;
        this.birthday = birthday;
        this.sex = sex;
        this.smoker = smoker;
        this.injection = injection;
        this.morning_start = morning_start;
        this.morning_end = morning_end;
        this.breakfast_start = breakfast_start;
        this.breakfast_end = breakfast_end;
        this.lunch_start = lunch_start;
        this.lunch_end = lunch_end;
        this.dinner_start = dinner_start;
        this.dinner_end = dinner_end;
        this.bed_start = bed_start;
        this.bed_end = bed_end;
        this.height_unit = height_unit;
        this.weight_unit = weight_unit;
        this.waist_unit = waist_unit;
        this.pre_meal_low = pre_meal_low;
        this.pre_meal_med = pre_meal_med;
        this.pre_meal_high = pre_meal_high;
        this.post_meal_low = post_meal_low;
        this.post_meal_med = post_meal_med;
        this.post_meal_high = post_meal_high;

    }

    //set default values
    public User(int userid) {
        this(userid, "", "1975-01-01", 0, 0, "N", "07:00:00", "07:45:00", "08:00:00", "10:00:00", "11:00:00", "14:00:00", "18:00:00", "21:00:00", "22:00:00", "23:00:00", 0, 0 ,0, 4, 6, 8, 4, 8, 10);
    }

}
