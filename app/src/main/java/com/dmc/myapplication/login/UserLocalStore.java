package com.dmc.myapplication.login;


import android.content.Context;
import android.content.SharedPreferences;

import com.dmc.myapplication.preference.Settings;


/**
 * Created by Po on 25/1/2016.
 */
public class UserLocalStore {

    //set the SharedPreferences database name
    //public static final String SP_NAME = "userData";

    SharedPreferences userLocalDatabase;

    public UserLocalStore(Context context) {
        userLocalDatabase = context.getSharedPreferences(Settings.SP_NAME,
                Context.MODE_PRIVATE);
    }

    //save user data to the local SharePreferences
    public void storeUserData(User user) {
        SharedPreferences.Editor userLocalDatabaseEditor = userLocalDatabase.edit();
        userLocalDatabaseEditor.putString("userid", Integer.toString(user.userid));
        userLocalDatabaseEditor.putString("name", user.name);
        userLocalDatabaseEditor.putString("birthday", user.birthday);
        userLocalDatabaseEditor.putString("sex", Integer.toString(user.sex));
        userLocalDatabaseEditor.putString("smoker", Integer.toString(user.smoker));
        userLocalDatabaseEditor.putString("injection", user.injection);
        userLocalDatabaseEditor.putString("morning_start", user.morning_start);
        userLocalDatabaseEditor.putString("morning_end", user.morning_end);
        userLocalDatabaseEditor.putString("breakfast_start", user.breakfast_start);
        userLocalDatabaseEditor.putString("breakfast_end", user.breakfast_end);
        userLocalDatabaseEditor.putString("lunch_start", user.lunch_start);
        userLocalDatabaseEditor.putString("lunch_end", user.lunch_end);
        userLocalDatabaseEditor.putString("dinner_start", user.dinner_start);
        userLocalDatabaseEditor.putString("dinner_end", user.dinner_end);
        userLocalDatabaseEditor.putString("bed_start", user.bed_start);
        userLocalDatabaseEditor.putString("bed_end", user.bed_end);
        userLocalDatabaseEditor.putString("height_unit", Integer.toString(user.height_unit));
        userLocalDatabaseEditor.putString("weight_unit",Integer.toString(user.weight_unit));
        userLocalDatabaseEditor.putString("waist_unit", Integer.toString(user.waist_unit));

        //Janus
        userLocalDatabaseEditor.putString("pre_meal_low", Integer.toString(user.pre_meal_low));
        userLocalDatabaseEditor.putString("pre_meal_med", Integer.toString(user.pre_meal_med));
        userLocalDatabaseEditor.putString("pre_meal_high", Integer.toString(user.pre_meal_high));
        userLocalDatabaseEditor.putString("post_meal_low", Integer.toString(user.post_meal_low));
        userLocalDatabaseEditor.putString("post_meal_med", Integer.toString(user.post_meal_med));
        userLocalDatabaseEditor.putString("post_meal_high", Integer.toString(user.post_meal_high));

        userLocalDatabaseEditor.commit();
    }

    public void setUserLoggedIn(boolean loggedIn) {
        SharedPreferences.Editor userLocalDatabaseEditor = userLocalDatabase.edit();
        userLocalDatabaseEditor.putBoolean("loggedIn", loggedIn);
        userLocalDatabaseEditor.commit();
    }

    public void clearUserData() {
        SharedPreferences.Editor userLocalDatabaseEditor = userLocalDatabase.edit();
        userLocalDatabaseEditor.clear();
        userLocalDatabaseEditor.commit();
    }

    public User getLoggedInUser() {

        //read user data from local sharePreferences
        int userid = Integer.parseInt(userLocalDatabase.getString("userid", "-1"));
        String name = userLocalDatabase.getString("name", "");
        String birthday = userLocalDatabase.getString("birthday", "1975-01-01");
        int sex = Integer.parseInt(userLocalDatabase.getString("sex", "0"));
        int smoker = Integer.parseInt(userLocalDatabase.getString("smoker", "0"));
        String injection = userLocalDatabase.getString("injection", "N");
        String morning_start = userLocalDatabase.getString("morning_start", "07:00:00");
        String morning_end = userLocalDatabase.getString("morning_end", "07:30:00");
        String breakfast_start = userLocalDatabase.getString("breakfast_start", "08:00:00");
        String breakfast_end = userLocalDatabase.getString("breakfast_end", "09:00:00");
        String lunch_start = userLocalDatabase.getString("lunch_start", "12:00:00");
        String lunch_end = userLocalDatabase.getString("lunch_end", "13:00:00");
        String dinner_start = userLocalDatabase.getString("dinner_start", "19:00:00");
        String dinner_end = userLocalDatabase.getString("dinner_end", "20:00:00");
        String bed_start = userLocalDatabase.getString("bed_start", "22:00:00");
        String bed_end = userLocalDatabase.getString("bed_end", "23:00:00");
        int height_unit = Integer.parseInt(userLocalDatabase.getString("height_unit", "0"));
        int weight_unit = Integer.parseInt(userLocalDatabase.getString("weight_unit", "0"));
        int waist_unit = Integer.parseInt(userLocalDatabase.getString("waist_unit", "0"));

        //Janus
        int pre_meal_low = Integer.parseInt(userLocalDatabase.getString("pre_meal_low", "4"));
        int pre_meal_med = Integer.parseInt(userLocalDatabase.getString("pre_meal_med", "6"));
        int pre_meal_high = Integer.parseInt(userLocalDatabase.getString("pre_meal_high", "8"));
        int post_meal_low = Integer.parseInt(userLocalDatabase.getString("post_meal_low", "4"));
        int post_meal_med = Integer.parseInt(userLocalDatabase.getString("post_meal_med", "8"));
        int post_meal_high = Integer.parseInt(userLocalDatabase.getString("post_meal_high", "10"));


        User storedUser = new User(userid, name, birthday, sex,
                smoker, injection, morning_start, morning_end, breakfast_start,
                breakfast_end, lunch_start, lunch_end, dinner_start,
                dinner_end, bed_start, bed_end, height_unit, weight_unit, waist_unit, pre_meal_low, pre_meal_med, pre_meal_high, post_meal_low, post_meal_med, post_meal_high);
        return storedUser;
    }

    public boolean getUserLoggedIn(){

        //if the user logged in return true
        if (userLocalDatabase.getBoolean("loggedIn", false) == true){
            return true;
        } else{
            return false;
        }

    }



}
