package com.dmc.myapplication.login;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.SystemClock;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;

import com.dmc.myapplication.R;

/**
 * Created by Po on 18/3/2016.
 */
public class terms extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        this.setTitle(R.string.title_activity_terms);
        setContentView(R.layout.terms);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*getActionBar().setNavigationIcon(R.drawable.ic_add);
        getActionBar().setNavigationOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        System.out.println("clicking the toolbar!");
                    }
                }

        );*/

        Button btnAccept = (Button) findViewById(R.id.terms_button);
        btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(terms.this, LoginActivity.class));
                finish();
            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case android.R.id.home:
                startActivity(new Intent(this, welcomeActivity.class));
                finish();
        }
        return (super.onOptionsItemSelected(menuItem));
    }

    @Override
    public void onBackPressed() {
    }


}
