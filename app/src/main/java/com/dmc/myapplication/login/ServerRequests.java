package com.dmc.myapplication.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;

import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.systemConstant;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.net.Socket;
import java.net.URLEncoder;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.ArrayList;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

/**
 * Created by PoWu on 26/1/16.
 */
public class ServerRequests {
    ProgressDialog progressDialog;

    public ServerRequests(Context context){
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.setTitle("驗證中");
        progressDialog.setMessage("請稍候⋯⋯");

    }


    public void storeUserDataInBackground(User user, GetUserCallback userCallback){
        progressDialog.show();
        new StoreUserDataAsyncTask(user, userCallback).execute();

    }

    public void fetchUserDataInBackground(User user, GetUserCallback callback){
        progressDialog.show();
        new fetchUserDataAsyncTask(user, callback).execute();

    }

    public void editUserDataInBackground(User user, GetUserCallback userCallback){
        progressDialog.show();
        new EditUserDataAsyncTask(user, userCallback).execute();

    }

    public class EditUserDataAsyncTask extends AsyncTask<Void, Void, Void>{

        User user;
        GetUserCallback userCallback;

        public EditUserDataAsyncTask(User user, GetUserCallback userCallback){
            this.user = user;
            this.userCallback = userCallback;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String result= null;
            String data="";

            String user_id = Integer.toString(user.userid);
            String name = (String) user.name;
            String birthday = (String) user.birthday;
            String sex = Integer.toString(user.sex);
            String smoker =  Integer.toString(user.smoker);
            String injection = user.injection;
            String morning_start = user.morning_start;
            String morning_end = user.morning_end;
            String breakfast_start = user.breakfast_start;
            String breakfast_end = user.breakfast_end;
            String lunch_start = user.lunch_start;
            String lunch_end = user.lunch_end;
            String dinner_start = user.dinner_start;
            String dinner_end = user.dinner_end;
            String bed_start = user.bed_start;
            String bed_end = user.bed_end;
            String height_unit =Integer.toString(user.height_unit);
            String weight_unit = Integer.toString(user.weight_unit);
            String waist_unit =Integer.toString(user.waist_unit);


            try {
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                data += "&" + URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8");
                data += "&" + URLEncoder.encode("birthday", "UTF-8") + "=" + URLEncoder.encode(birthday, "UTF-8");
                data += "&" + URLEncoder.encode("sex", "UTF-8") + "=" + URLEncoder.encode(sex, "UTF-8");
                data += "&" + URLEncoder.encode("smoker", "UTF-8") + "=" + URLEncoder.encode(smoker, "UTF-8");
                data += "&" + URLEncoder.encode("injection", "UTF-8") + "=" + URLEncoder.encode(injection, "UTF-8");
                data += "&" + URLEncoder.encode("morning_start", "UTF-8") + "=" + URLEncoder.encode(morning_start, "UTF-8");
                data += "&" + URLEncoder.encode("morning_end", "UTF-8") + "=" + URLEncoder.encode(morning_end, "UTF-8");
                data += "&" + URLEncoder.encode("breakfast_start", "UTF-8") + "=" + URLEncoder.encode(breakfast_start, "UTF-8");
                data += "&" + URLEncoder.encode("breakfast_end", "UTF-8") + "=" + URLEncoder.encode(breakfast_end, "UTF-8");
                data += "&" + URLEncoder.encode("lunch_start", "UTF-8") + "=" + URLEncoder.encode(lunch_start, "UTF-8");
                data += "&" + URLEncoder.encode("lunch_end", "UTF-8") + "=" + URLEncoder.encode(lunch_end, "UTF-8");
                data += "&" + URLEncoder.encode("dinner_start", "UTF-8") + "=" + URLEncoder.encode(dinner_start, "UTF-8");
                data += "&" + URLEncoder.encode("dinner_end", "UTF-8") + "=" + URLEncoder.encode(dinner_end, "UTF-8");
                data += "&" + URLEncoder.encode("bed_start", "UTF-8") + "=" + URLEncoder.encode(bed_start, "UTF-8");
                data += "&" + URLEncoder.encode("bed_end", "UTF-8") + "=" + URLEncoder.encode(bed_end, "UTF-8");
                data += "&" + URLEncoder.encode("height_unit", "UTF-8") + "=" + URLEncoder.encode(height_unit, "UTF-8");
                data += "&" + URLEncoder.encode("weight_unit", "UTF-8") + "=" + URLEncoder.encode(weight_unit, "UTF-8");
                data += "&" + URLEncoder.encode("waist_unit", "UTF-8") + "=" + URLEncoder.encode(waist_unit, "UTF-8");
                //System.out.println(data);

                //get result  from server
                dmAppAsyncTask dmasyncTask = new dmAppAsyncTask("UpdateUserPref.php");
                result = dmasyncTask.getDataFromServer(data);

            }catch (Exception e){
                e.printStackTrace();
            }
            //System.out.println("[EditUserDataAsyncTask - result] "+result);

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            userCallback.done(null);
            super.onPostExecute(aVoid);
        }
    }


    public class StoreUserDataAsyncTask extends AsyncTask<Void, Void, Void>{

        User user;
        GetUserCallback userCallback;

        public StoreUserDataAsyncTask(User user, GetUserCallback userCallback){
            this.user = user;
            this.userCallback = userCallback;
        }

        @Override
        protected Void doInBackground(Void... params) {
            String result= null;
            String data="";

            String user_id = Integer.toString(user.userid);
            String name = (String) user.name;
            String birthday = (String) user.birthday;
            String sex = Integer.toString(user.sex);
            String smoker =  Integer.toString(user.smoker);
            String injection = user.injection;
            String morning_start = user.morning_start;
            String morning_end = user.morning_end;
            String breakfast_start = user.breakfast_start;
            String breakfast_end = user.breakfast_end;
            String lunch_start = user.lunch_start;
            String lunch_end = user.lunch_end;
            String dinner_start = user.dinner_start;
            String dinner_end = user.dinner_end;
            String bed_start = user.bed_start;
            String bed_end = user.bed_end;
            String height_unit =Integer.toString(user.height_unit);
            String weight_unit = Integer.toString(user.weight_unit);
            String waist_unit =Integer.toString(user.waist_unit);


            try {
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                data += "&" + URLEncoder.encode("name", "UTF-8") + "=" + URLEncoder.encode(name, "UTF-8");
                data += "&" + URLEncoder.encode("birthday", "UTF-8") + "=" + URLEncoder.encode(birthday, "UTF-8");
                data += "&" + URLEncoder.encode("sex", "UTF-8") + "=" + URLEncoder.encode(sex, "UTF-8");
                data += "&" + URLEncoder.encode("smoker", "UTF-8") + "=" + URLEncoder.encode(smoker, "UTF-8");
                data += "&" + URLEncoder.encode("injection", "UTF-8") + "=" + URLEncoder.encode(injection, "UTF-8");
                data += "&" + URLEncoder.encode("morning_start", "UTF-8") + "=" + URLEncoder.encode(morning_start, "UTF-8");
                data += "&" + URLEncoder.encode("morning_end", "UTF-8") + "=" + URLEncoder.encode(morning_end, "UTF-8");
                data += "&" + URLEncoder.encode("breakfast_start", "UTF-8") + "=" + URLEncoder.encode(breakfast_start, "UTF-8");
                data += "&" + URLEncoder.encode("breakfast_end", "UTF-8") + "=" + URLEncoder.encode(breakfast_end, "UTF-8");
                data += "&" + URLEncoder.encode("lunch_start", "UTF-8") + "=" + URLEncoder.encode(lunch_start, "UTF-8");
                data += "&" + URLEncoder.encode("lunch_end", "UTF-8") + "=" + URLEncoder.encode(lunch_end, "UTF-8");
                data += "&" + URLEncoder.encode("dinner_start", "UTF-8") + "=" + URLEncoder.encode(dinner_start, "UTF-8");
                data += "&" + URLEncoder.encode("dinner_end", "UTF-8") + "=" + URLEncoder.encode(dinner_end, "UTF-8");
                data += "&" + URLEncoder.encode("bed_start", "UTF-8") + "=" + URLEncoder.encode(bed_start, "UTF-8");
                data += "&" + URLEncoder.encode("bed_end", "UTF-8") + "=" + URLEncoder.encode(bed_end, "UTF-8");
                data += "&" + URLEncoder.encode("height_unit", "UTF-8") + "=" + URLEncoder.encode(height_unit, "UTF-8");
                data += "&" + URLEncoder.encode("weight_unit", "UTF-8") + "=" + URLEncoder.encode(weight_unit, "UTF-8");
                data += "&" + URLEncoder.encode("waist_unit", "UTF-8") + "=" + URLEncoder.encode(waist_unit, "UTF-8");
                //System.out.println(data);

                //get result  from server
                dmAppAsyncTask dmasyncTask = new dmAppAsyncTask("Register.php");
                result = dmasyncTask.getDataFromServer(data);

            }catch (Exception e){
                e.printStackTrace();
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            userCallback.done(null);
            super.onPostExecute(aVoid);
        }
    }


    //method for login process, connect to server to get back the user data
    public class fetchUserDataAsyncTask extends AsyncTask<Void, Void, User> {

        User user;
        GetUserCallback userCallback;

        public fetchUserDataAsyncTask(User user, GetUserCallback userCallback) {
            this.user = user;
            this.userCallback = userCallback;
        }

        @Override
        protected User doInBackground(Void... params) {
            ArrayList<NameValuePair> dataToSend = new ArrayList<>();
            dataToSend.add(new BasicNameValuePair("user_id",user.userid + ""));

            HttpClient client;
            if(!systemConstant.SERVER_ADDRESS.contains("https")){ // run http
                HttpParams httpRequestParams = new BasicHttpParams();
                HttpConnectionParams.setConnectionTimeout(httpRequestParams, systemConstant.CONNECTION_TIMEOUT);
                HttpConnectionParams.setSoTimeout(httpRequestParams, systemConstant.CONNECTION_TIMEOUT);
                client = new DefaultHttpClient(httpRequestParams);
            }else{ // run https
                client = trustAllHosts();
            }

            HttpPost post = new HttpPost(systemConstant.SERVER_ADDRESS + "FetchUserData.php");

            User returnedUser = null;

            try {
                //get the response from server
                post.setEntity(new UrlEncodedFormEntity(dataToSend));
                HttpResponse httpResponse = client.execute(post);

                HttpEntity entity = httpResponse.getEntity();
                String result = EntityUtils.toString(entity);
                //System.out.println("JSON: "+ result);

                if (!result.equals("[]") ) {

                    JSONObject jObject = new JSONObject(result);

                    if (jObject.length() == 0 ){
                        returnedUser = null;
                    }   else{
                        String name = jObject.getString("name");
                        String birthday = jObject.getString("birthday");
                        int sex = jObject.getInt("sex");
                        int smoker = jObject.getInt("smoker");
                        String injection = jObject.getString("injection");
                        String morning_start = jObject.getString("morning_start");
                        String morning_end = jObject.getString("morning_end");
                        String breakfast_start = jObject.getString("breakfast_start");
                        String breakfast_end = jObject.getString("breakfast_end");
                        String lunch_start = jObject.getString("lunch_start");
                        String lunch_end = jObject.getString("lunch_end");
                        String dinner_start = jObject.getString("dinner_start");
                        String dinner_end = jObject.getString("dinner_end");
                        String bed_start = jObject.getString("bed_start");
                        String bed_end = jObject.getString("bed_end");
                        int height_unit = jObject.getInt("height_unit");
                        int weight_unit = jObject.getInt("weight_unit");
                        int waist_unit = jObject.getInt("waist_unit");

                        returnedUser = new User(user.userid, name, birthday, sex, smoker, injection, morning_start, morning_end, breakfast_start, breakfast_end, lunch_start, lunch_end, dinner_start, dinner_end, bed_start, bed_end, height_unit, weight_unit, waist_unit, 4, 6, 8, 4, 8, 10);

                    }
                }else{
                    returnedUser = null;
                }

            }catch (Exception e){
                e.printStackTrace();
            }

            return returnedUser;
        }

        @Override
        protected void onPostExecute(User returnedUser) {
            progressDialog.dismiss();
            userCallback.done(returnedUser);
            super.onPostExecute(returnedUser);
        }
    }

    private HttpClient trustAllHosts() {
        // Create a trust manager that does not validate certificate chains
        try {

            KeyStore trustStore = KeyStore.getInstance(KeyStore.getDefaultType());
            trustStore.load(null, null);
            SSLSocketFactory sf = new MySSLSocketFactory(trustStore);
            sf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);

            HttpParams params = new BasicHttpParams();
            HttpProtocolParams.setVersion(params, HttpVersion.HTTP_1_1);
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpConnectionParams.setConnectionTimeout(params, systemConstant.CONNECTION_TIMEOUT);
            HttpConnectionParams.setSoTimeout(params, systemConstant.CONNECTION_TIMEOUT);

            SchemeRegistry registry = new SchemeRegistry();
            registry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            registry.register(new Scheme("https", sf, 443));

            ClientConnectionManager ccm = new ThreadSafeClientConnManager(params, registry);
            return new DefaultHttpClient(ccm, params);

        }catch (Exception e){
            e.printStackTrace();
            return new DefaultHttpClient();
        }
    }

    private class MySSLSocketFactory extends SSLSocketFactory {
        SSLContext sslContext = SSLContext.getInstance("TLS");

        public MySSLSocketFactory(KeyStore truststore) throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException, UnrecoverableKeyException {
            super(truststore);

            TrustManager tm = new X509TrustManager() {
                public void checkClientTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] chain, String authType) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return null;
                }
            };

            sslContext.init(null, new TrustManager[] { tm }, null);
        }

        @Override
        public Socket createSocket(Socket socket, String host, int port, boolean autoClose) throws IOException, UnknownHostException {
            return sslContext.getSocketFactory().createSocket(socket, host, port, autoClose);
        }

        @Override
        public Socket createSocket() throws IOException {
            return sslContext.getSocketFactory().createSocket();
        }
    }

}
