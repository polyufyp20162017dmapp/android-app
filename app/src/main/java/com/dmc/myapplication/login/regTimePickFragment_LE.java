package com.dmc.myapplication.login;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dmc.myapplication.R;


/**
 * Created by Po on 5/2/2016.
 */
public class regTimePickFragment_LE extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    public regTimePickFragment_LE() {
        // Required empty public constructor
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        //default Lunch start time
        int hour = 14;
        int minute = 0;

        // Create a new instance of TimePickerDialog and return it
        return new TimePickerDialog(getActivity(),AlertDialog.THEME_HOLO_LIGHT, this, hour, minute, true);
    }

    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        if (hourOfDay<10 && minute<10){
            String displayAsTime ="0"+hourOfDay + ":0" + minute + ":00";
            ((TextView) getActivity().findViewById(R.id.reg_lunch_end)).setText(displayAsTime);
        } else
        if (hourOfDay<10 && minute>=10){
            String displayAsTime ="0"+hourOfDay + ":" + minute + ":00";
            ((TextView) getActivity().findViewById(R.id.reg_lunch_end)).setText(displayAsTime);
        } else
        if (hourOfDay>=10 && minute<10){
            String displayAsTime =hourOfDay + ":0" + minute + ":00";
            ((TextView) getActivity().findViewById(R.id.reg_lunch_end)).setText(displayAsTime);
        } else
        {
            String displayAsTime =hourOfDay + ":" + minute + ":00";
            ((TextView) getActivity().findViewById(R.id.reg_lunch_end)).setText(displayAsTime);
        }

    }


}
