package com.dmc.myapplication.bodyRecord;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Paint;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupMenu;
import android.widget.TextView;


import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.INJECTION_RECORD;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.blood.bloodListAdapter;
import com.dmc.myapplication.blood.blood_delete;
import com.dmc.myapplication.bp.bpListAdapter;
import com.dmc.myapplication.bp.bp_delete;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.otherBodyIndex.CustomSeekBar;
import com.dmc.myapplication.otherBodyIndex.ProgressItem;
import com.dmc.myapplication.otherBodyIndex.otherBodyMeasureListAdapter;
import com.dmc.myapplication.otherBodyIndex.otherBodyMeasure_delete;
import com.dmc.myapplication.otherBodyIndex.otherBodyReportListAdapter;
import com.dmc.myapplication.otherBodyIndex.otherBodyReport_delete;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.timeCheck;
import com.dmc.myapplication.tool.unitConvert;
import com.github.mikephil.charting.charts.Chart;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.github.mikephil.charting.utils.ViewPortHandler;

import org.joda.time.DateTime;
import org.joda.time.DateTimeConstants;
import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Type;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Po on 12/2/2016.
 */
public class BodyRecord {

    private LinearLayout testChart, testChart2;
    private PieChart mpPieChart;
    private LineChart lineChart;
    private List<bodyRecord> glu7days, bp7days;
    private  Map<String, Integer> inject7days;
    private static HashMap<String, Object> injectMap  = new HashMap<String, Object>();
    private String resultCache = null;
    UserLocalStore userLocalStore;
    User user;

    //Pie chart data
    private int[] yData = new int[4];
    private String[] xData  = {"欠佳","普通","理想","過低"};
    private String[] xBPData  = {"血壓超標","血壓稍高","理想血壓","血壓偏低"};

    //graphical indicators for other body index
    private CustomSeekBar bmibar, waistbar, hba1cbar, totalcbar, hdlcbar, ldlcbar, tribar;

    //Janus
    private int timeMode = bodyRecordConstant.CHART_WEEKLY;


    public void setBodyRecord(final Activity activity, final String result, final String recordType, final int mode, final Context ctx){

        //System.out.println("Run in setBodyRecord " + result + " Recordtype" + recordType + " mode" + mode);
        System.out.println("Run in setBodyRecord " + " Recordtype" + recordType + " mode" + mode);
        userLocalStore = new UserLocalStore(activity);
        user = userLocalStore.getLoggedInUser();
        resultCache = result;
        switch (recordType) {
            case "1": //glucose (blood sugar) section
            {if (result != null && !result.equals("")) {
                //output the processed data to a hashmap
                HashMap<String, Object> hashmap = prepareBloodListDataNew(result, mode, 1 , bodyRecordConstant.GET_ALL);
                //System.out.println("HashMap: hashmap " + hashmap);

                //handle graph case - only 7 days
                if (mode == 0){
                    //Prepare upper part graph (pie Chart)
                    List<Integer> listFast7daysCount =(List<Integer>) hashmap.get("gluCount7days");
                    ///System.out.println("List: listFast7daysCount " + listFast7daysCount);
                    //System.out.println("Size of listFast7daysCount" + listFast7daysCount.size());


                    yData = new int[listFast7daysCount.size()];
                    for(int i = 0; i < listFast7daysCount.size(); i++) yData[i] = listFast7daysCount.get(i);

                    testChart = (LinearLayout) activity.findViewById(R.id.testChart);
                    mpPieChart = (PieChart) activity.findViewById(R.id.chart1);
                    setupPieChart(Integer.parseInt(recordType));


                    //Prepare lower part graph (line Chart)
                    glu7days =(List<bodyRecord>) hashmap.get("FAST7DAYS");
                    //System.out.println("List: glu7days " + glu7days);
                    //System.out.println("Size of glu7days " + glu7days.size());

                    if(user.injection.equals("Y")){

                        inject7days =(Map<String, Integer>) injectMap.get("LAST7DAYS");
                        //System.out.println("List: inject7days " + inject7days);
                        //System.out.println("Size of inject7days" + inject7days.size());


                    }

                    testChart2 = (LinearLayout) activity.findViewById(R.id.testLineChart);
                    lineChart = (LineChart) activity.findViewById(R.id.chart2);
                    setupLineChart(Integer.parseInt(recordType));


                    class chartSetting{
                        public String btnShowTypeSetting = bodyRecordConstant.GET_ALL;
                        public Integer btnShowTimeSetting = bodyRecordConstant.CHART_WEEKLY;
                    }


                    final chartSetting chs = new chartSetting();


                    final Button btnShowType = (Button) activity.findViewById(R.id.buttonShowType);

                    btnShowType.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PopupMenu popup = new PopupMenu(ctx, v);
                            MenuInflater inflater = popup.getMenuInflater();
                            inflater.inflate(R.menu.blood_menu_click, popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.item_all:{
                                            btnShowType.setText("綜合");
                                            //System.out.println("Show Type: All");
                                            /*bodyAsyncTask getRecord = new bodyAsyncTask(activity, bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                            String user_id = Integer.toString(user.userid);
                                            String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                            final String show_type = bodyRecordConstant.GET_ALL;
                                            getRecord.execute(user_id, record_type, show_type);
                                            mpPieChart.notifyDataSetChanged();
                                            lineChart.notifyDataSetChanged();
                                            mpPieChart.invalidate();
                                            lineChart.invalidate();*/
                                            chs.btnShowTypeSetting = bodyRecordConstant.GET_ALL;
                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);


                                        }
                                        break;
                                        case R.id.item_before:{
                                            btnShowType.setText("餐前");
                                           /* mpPieChart.clear();
                                            lineChart.clear();
                                            //System.out.println("Show Type: before meal");
                                            bodyAsyncTask getRecord = new bodyAsyncTask(activity, bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                            String user_id = Integer.toString(user.userid);
                                            String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                            final String show_type = bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL;
                                            getRecord.execute(user_id, record_type, show_type);
                                            mpPieChart.notifyDataSetChanged();
                                            lineChart.notifyDataSetChanged();
                                            mpPieChart.invalidate();
                                            lineChart.invalidate();*/

                                            chs.btnShowTypeSetting = bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL;
                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);

                                        }
                                        break;
                                        case R.id.item_after:{
                                            btnShowType.setText("餐後");
                                            /*mpPieChart.clear();
                                            lineChart.clear();
                                            //System.out.println("Show Type: after meal");
                                            bodyAsyncTask getRecord = new bodyAsyncTask(activity, bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                            String user_id = Integer.toString(user.userid);
                                            String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                            final String show_type = bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL;
                                            getRecord.execute(user_id, record_type, show_type);
                                            mpPieChart.notifyDataSetChanged();
                                            lineChart.notifyDataSetChanged();
                                            mpPieChart.invalidate();
                                            lineChart.invalidate();*/

                                            chs.btnShowTypeSetting = bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL;
                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);

                                        }
                                        break;
                                        default:
                                            break;
                                    }

                                    return false;
                                }
                            });

                        }
                    });

                    final Button btnShowTime = (Button) activity.findViewById(R.id.buttonShowTime);
                    btnShowTime.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PopupMenu popup = new PopupMenu(ctx, v);
                            MenuInflater inflater = popup.getMenuInflater();
                            inflater.inflate(R.menu.menu_time, popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.menu_weekly2:{
                                            btnShowTime.setText("每星期");
                                            //timeMode = bodyRecordConstant.CHART_DAILY;
                                            chs.btnShowTimeSetting = bodyRecordConstant.CHART_WEEKLY;

                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);
                                        }
                                        break;

                                        case R.id.menu_monthly2:{
                                            btnShowTime.setText("每月");
                                            //timeMode = bodyRecordConstant.CHART_MONTHLY;
                                            System.out.println("Month clicked!");
                                            chs.btnShowTimeSetting = bodyRecordConstant.CHART_MONTHLY;
                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);

                                        }
                                        break;

                                        case R.id.menu_daily2:{
                                            btnShowTime.setText("每日");
                                            //timeMode = bodyRecordConstant.CHART_MONTHLY;
                                            chs.btnShowTimeSetting = bodyRecordConstant.CHART_DAILY;

                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);

                                        }
                                        break;

                                        case R.id.menu_yearly2:{
                                            btnShowTime.setText("每年");
                                            //timeMode = bodyRecordConstant.CHART_MONTHLY;
                                            chs.btnShowTimeSetting = bodyRecordConstant.CHART_YEARLY;

                                            bloodTimeSwitch(result, mode, chs.btnShowTimeSetting, chs.btnShowTypeSetting, activity, recordType);

                                        }
                                        break;

                                        default:
                                            break;
                                    }

                                    return false;
                                }
                            });

                        }
                    });


                    lineChart.notifyDataSetChanged();
                    //System.out.println("[setBodyGraph - case 1 have result: handled graph case ");
                } else

                //handle list case - no date range
                {
                    if (mode == 1) {
                        final ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                        final List<bodyRecord> checkRecord = (List<bodyRecord>) hashmap.get("ALL");

                        //System.out.println("List: checkRecord" + checkRecord);
                        final bloodListAdapter listAdapter = new bloodListAdapter(activity, checkRecord);

                        //System.out.println("bloodListAdapter: listAdapter item:" + listAdapter.getCount());
                        for (bodyRecord record : checkRecord) {
                            System.out.println("checkRecord:itemID: " + record.getRecordId() + " Date: " + record.getDate());
                        }
                        lv1.setAdapter(listAdapter);
                        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                TextView recordIdView = (TextView) view.findViewById(R.id.glurecordId);
                                TextView dateView = (TextView) view.findViewById(R.id.date);
                                TextView timeView = (TextView) view.findViewById(R.id.time);
                                TextView typePeriodView = (TextView) view.findViewById(R.id.gluPeriodType);
                                TextView typeGluView = (TextView) view.findViewById(R.id.gluType);
                                TextView gluValueView = (TextView) view.findViewById(R.id.value);

                                final String recordId = recordIdView.getText().toString();
                                final String date = dateView.getText().toString();
                                final String time = timeView.getText().toString();
                                final String typePeriod = typePeriodView.getText().toString();
                                final String typeGlu = typeGluView.getText().toString();
                                final String gluValue = gluValueView.getText().toString();

                                deleteBloodRecord(activity, recordId, date, time, typePeriod, typeGlu, gluValue);

                                //System.out.println(recordId);
                            }
                        });

                        class bloodViewListEvent{
                            private Integer ListShowType = 0;
                            private Integer ListShowSession = 0;

                            public Integer getListShowType() {
                                return ListShowType;
                            }

                            public void setListShowType(Integer listShowType) {
                                ListShowType = listShowType;
                            }

                            public Integer getListShowSession() {
                                return ListShowSession;
                            }

                            public void setListShowSession(Integer listShowSession) {
                                ListShowSession = listShowSession;
                            }

                            private void refreshListData(){
                                List<bodyRecord> checkRecord2 = new ArrayList<>();

                                for(int o=0; o<checkRecord.size(); o++){
                                    Integer temp = 0;
                                    if (ListShowType == 0){
                                        temp = temp + 1;
                                    }else {
                                        if (ListShowType == checkRecord.get(o).getType_glucose()){
                                            temp = temp + 1;
                                        }
                                    }

                                    if (ListShowSession == 0) {
                                        temp = temp + 1;
                                    }else{
                                        if (ListShowSession == checkRecord.get(o).getPeriod()){
                                            temp = temp + 1;
                                        }
                                    }

                                    if (temp >= 2){
                                        checkRecord2.add(checkRecord.get(o));
                                    }

                                    temp = null;
                                }

                                final bloodListAdapter listAdapter2 = new bloodListAdapter(activity, checkRecord2);
                                lv1.setAdapter(listAdapter2);
                            }
                        }

                        final bloodViewListEvent bloodViewListEvent1 = new bloodViewListEvent();


                        final Button btnListShowType = (Button) activity.findViewById(R.id.btnListShowType);

                        View.OnClickListener showtypeCallback = new View.OnClickListener() {





                            @Override
                            public void onClick(View v) {
                                PopupMenu popup = new PopupMenu(ctx, v);
                                MenuInflater inflater = popup.getMenuInflater();
                                inflater.inflate(R.menu.blood_menu_click, popup.getMenu());
                                popup.show();

                                popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {
                                        switch (item.getItemId()) {
                                            case R.id.item_all: {

                                                btnListShowType.setText("綜合");
                                                //System.out.println("Show Type: All");
                                                /*bodyAsyncTask getRecord = new bodyAsyncTask(activity,bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_LIST, ctx);
                                                String user_id = Integer.toString(user.userid);
                                                String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                                String show_type = bodyRecordConstant.GET_ALL;
                                                getRecord.execute(user_id, record_type, show_type);
                                                listAdapter.notifyDataSetChanged();*/

                                                bloodViewListEvent1.setListShowType(0);
                                                bloodViewListEvent1.refreshListData();


                                            }
                                            break;
                                            case R.id.item_before: {
                                                btnListShowType.setText("餐前");
                                                //System.out.println("Show Type: before meal");
                                                //bodyAsyncTask getRecord = new bodyAsyncTask(activity, bodyRecordConstant.GET_BODY_RECORD, bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_LIST, ctx);
                                                //String user_id = Integer.toString(user.userid);
                                                //String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                                //final String show_type = bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL;
                                                //getRecord.execute(user_id, record_type, show_type);
                                                //listAdapter.notifyDataSetChanged();

                                                //ListShowType = 1;
                                                //refreshListData();
                                                bloodViewListEvent1.setListShowType(1);
                                                bloodViewListEvent1.refreshListData();
                                            }
                                            break;
                                            case R.id.item_after: {
                                                btnListShowType.setText("餐後");
                                                //System.out.println("Show Type: after meal");

                                                /*List<bodyRecord> checkRecord2 = new ArrayList<>();

                                                for(int o=0; o<checkRecord.size(); o++){
                                                    if (checkRecord.get(o).getType_glucose() == 2){
                                                        checkRecord2.add(checkRecord.get(o));
                                                    }
                                                }

                                                final bloodListAdapter listAdapter2 = new bloodListAdapter(activity, checkRecord2);
                                                lv1.setAdapter(listAdapter2);*/

                                                //ListShowType = 2;
                                                //refreshListData();
                                                bloodViewListEvent1.setListShowType(2);
                                                bloodViewListEvent1.refreshListData();
                                                // bodyAsyncTask getRecord = new bodyAsyncTask(activity, bodyRecordConstant.GET_BODY_RECORD, bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_LIST, ctx);
                                                //String user_id = Integer.toString(user.userid);
                                                //String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
                                                //final String show_type = bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL;
                                                //getRecord.execute(user_id, record_type, show_type);
                                                //listAdapter.notifyDataSetChanged();
                                            }
                                            break;
                                            default:
                                                break;
                                        }
                                        return false;
                                    }
                                });

                            }
                        };

                        btnListShowType.setOnClickListener(showtypeCallback);


                        final Button btnListShowSession = (Button) activity.findViewById(R.id.btnListShowSession);
                        btnListShowSession.setOnClickListener(new View.OnClickListener(){

                            @Override
                            public void onClick(View view) {
                                PopupMenu popup2 = new PopupMenu(ctx, view);
                                MenuInflater inflater = popup2.getMenuInflater();
                                inflater.inflate(R.menu.blood_menu_session_click, popup2.getMenu());
                                popup2.show();
                                popup2.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                    @Override
                                    public boolean onMenuItemClick(MenuItem item) {
                                        switch (item.getItemId()) {
                                            case R.id.blood_session_default: {
                                                btnListShowSession.setText("綜合時間");

                                                //showtypeCallback = 0;
                                                //refreshListData();
                                                bloodViewListEvent1.setListShowSession(0);
                                                bloodViewListEvent1.refreshListData();
                                            }
                                            break;

                                            case R.id.breakfast: {
                                                btnListShowSession.setText("早餐");
                                                bloodViewListEvent1.setListShowSession(1);
                                                bloodViewListEvent1.refreshListData();
                                            }
                                            break;
                                            case R.id.lunch: {
                                                btnListShowSession.setText("午餐");
                                                bloodViewListEvent1.setListShowSession(2);
                                                bloodViewListEvent1.refreshListData();

                                            }
                                            break;
                                            case R.id.dinner: {
                                                btnListShowSession.setText("晚餐");
                                                bloodViewListEvent1.setListShowSession(3);
                                                bloodViewListEvent1.refreshListData();

                                            }
                                            break;
                                            default:
                                                break;
                                        }
                                        return false;
                                    }
                                });
                            }
                        });

                        //System.out.println("[setBodyRecord - case 1 have result: handled list case");
                    }
                }

            } else {
                ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                lv1.setAdapter((ListAdapter) null);
                TextView emptyText = (TextView) activity.findViewById(R.id.empty_msg);
                emptyText.setText("沒有任何記錄");

                //System.out.println("[setBodyRecord - case 1 no result");
            }



                break;
            }

            case "2": //BMI & waist section
            {
                //System.out.println("Run in case 2");
                if (result != null && !result.equals("")) {

                    //output the processed data to a hashmap
                    HashMap<String, Object> hashmap = prepareBmiListData(result);
                    //System.out.println("HashMap: hashmap " + hashmap);

                    //handle graph case
                    if (mode == 0){

                        double bmi_value =(double) hashmap.get("lastBMI");
                        //System.out.println("Value: bmi_value " + bmi_value);
                        double height_value =(double) hashmap.get("lastHeight");
                        //System.out.println("Value: height_value " + height_value);
                        double weight_value =(double) hashmap.get("lastWeight");
                        //System.out.println("Value: height_value " + weight_value);
                        double waist_value =(double) hashmap.get("lastWaist");
                        //System.out.println("Value: height_value " + waist_value);

                        String date_value =(String) hashmap.get("lastDate");
                        //System.out.println("Value: height_value " + waist_value);

                        TextView dateView = (TextView) activity.findViewById(R.id.om_bmi_date);
                        TextView dateView2 = (TextView) activity.findViewById(R.id.om_waist_date);
                        dateView.setText(date_value);
                        dateView2.setText(date_value);

                        TextView bmiView = (TextView) activity.findViewById(R.id.bmi);
                        bmiView.setText(Double.toString(bmi_value));
                        TextView heightView = (TextView) activity.findViewById(R.id.height);
                        if (user.height_unit == 1) {
                            double inch = unitConvert.CmToInches(height_value);
                            int feet = (int) Math.floor(inch / 12);
                            int leftover = (int) inch % 12;
                            String display =  Integer.toString(feet)+"\' "+Integer.toString(leftover)+"\"" ;
                            heightView.setText(display);
                        } else {
                            int cm = (int) Math.round(height_value);
                            heightView.setText(Double.toString(cm) +"厘米");
                        }
                        TextView weightView = (TextView) activity.findViewById(R.id.weight);
                        if (user.weight_unit == 1) {
                            int pound = (int) unitConvert.KgToLb(weight_value);
                            weightView.setText(Integer.toString(pound)+"磅");
                        } else {
                            double kg =  (double) Math.round(weight_value *10);
                            weightView.setText(Double.toString(kg/10)+"公斤");
                        }
                        TextView waistView = (TextView) activity.findViewById(R.id.waist);
                        if (user.waist_unit == 1) {
                            double inch = Math.round( unitConvert.CmToInches(waist_value) * 10)  ;
                            waistView.setText(Double.toString(inch/10)+"吋");
                        }else {
                            waistView.setText(Double.toString(waist_value)+"厘米");
                        }

                        TextView bmiViewComment = (TextView) activity.findViewById(R.id.bmi_comment);
                        TextView waistViewComment = (TextView) activity.findViewById(R.id.waist_comment);

                        if (bmi_value < bodyRecordConstant.GET_BMI_LOW) {
                            bmiView.setTextColor(Color.parseColor("#673ab7"));
                            bmiViewComment.setText("過輕");
                        }else if(bmi_value >= bodyRecordConstant.GET_BMI_HIGHFAT){
                            bmiView.setTextColor(Color.parseColor("#FF5722"));
                            bmiViewComment.setText("嚴重肥胖");
                        }else if(bmi_value >= bodyRecordConstant.GET_BMI_MIDFAT && bmi_value < bodyRecordConstant.GET_BMI_HIGHFAT){
                            bmiView.setTextColor(Color.parseColor("#F99611"));
                            bmiViewComment.setText("中度肥胖");
                        }else if(bmi_value >= bodyRecordConstant.GET_BMI_OVERWEIGHT  && bmi_value < bodyRecordConstant.GET_BMI_MIDFAT){
                            bmiView.setTextColor(Color.parseColor("#f7c600"));
                            bmiViewComment.setText("過重");
                        }else{
                            bmiView.setTextColor(Color.parseColor("#4caf50"));
                            bmiViewComment.setText("理想");
                        }

                        if (user.sex == 0) {
                            // fat
                            if (waist_value >= bodyRecordConstant.GET_WAIST_FEMALE_FAT) {
                                waistView.setTextColor(Color.parseColor("#FF5722"));
                                waistViewComment.setText("中央肥胖");
                            } else {//nice
                                waistView.setTextColor(Color.parseColor("#4caf50"));
                                waistViewComment.setText("理想");
                            }
                        }else{
                            if (waist_value >= bodyRecordConstant.GET_WAIST_MALE_FAT) {
                                waistView.setTextColor(Color.parseColor("#FF5722"));
                                waistViewComment.setText("中央肥胖");
                            } else {//nice
                                waistView.setTextColor(Color.parseColor("#4caf50"));
                                waistViewComment.setText("理想");
                            }
                        }
                        int percent = 0;
                        if (bmi_value >= 33){
                            percent = 100;
                        }else if(bmi_value <= 0 ){
                            percent = 0;
                        } else{
                            percent = (int)( (bmi_value - 15) / (33 - 15) * 100 );
                        }

                        bmibar = ((CustomSeekBar) activity.findViewById(R.id.bmiBar0));
                        initDataToBmibar();
                        bmibar.setProgress(percent);
                        bmibar.setClickable(false);
                        bmibar.setFocusable(false);
                        bmibar.setEnabled(false);


                        int percent2 = 0;
                        if (waist_value >= 140){
                            percent2 = 100;
                        }else if(waist_value <= 40 ){
                            percent2 = 0;
                        } else{
                            percent2 = (int)( (waist_value - 40) / (140 - 40) * 100 );
                        }

                        waistbar =  ((CustomSeekBar) activity.findViewById(R.id.waistBar));
                        initDataToWaistBar();
                        waistbar.setProgress(percent2);
                        waistbar.setClickable(false);
                        waistbar.setFocusable(false);
                        waistbar.setEnabled(false);


                        //System.out.println("[setBodyGraph -Case 2 have result:  handled graph case");

                    }

                    //handle list case
                    if (mode == 1) {
                        final ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                        List<bodyRecord> listAllBMIRecord = (List<bodyRecord>) hashmap.get("OM_RECORDS");

                        //System.out.println("HashMap: listAllBMIRecord " + listAllBMIRecord);
                        otherBodyMeasureListAdapter listAdapter = new otherBodyMeasureListAdapter(activity, listAllBMIRecord);

                        //System.out.println("otherBodyMeasureListAdapter: listAdapter item:" + listAdapter.getCount());
                        for (bodyRecord record : listAllBMIRecord) {
                            //System.out.println("listAllBMIRecord:itemID: " + record.getRecordId() + " Date: " + record.getDate());
                        }
                        lv1.setAdapter(listAdapter);
                        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                               TextView recordIdView = (TextView)view.findViewById(R.id.omRecordId);
                                TextView dateView = (TextView)view.findViewById(R.id.date);
                                TextView timeView = (TextView)view.findViewById(R.id.time);
                                TextView omBmiView = (TextView)view.findViewById(R.id.bmi);
                                TextView omHeightView = (TextView)view.findViewById(R.id.height);
                                TextView omWeightView = (TextView)view.findViewById(R.id.weight);
                                TextView omWaistView = (TextView)view.findViewById(R.id.waist);

                                final String recordId = recordIdView.getText().toString();
                                final String date = dateView.getText().toString();
                                final String time = timeView.getText().toString();
                                final String bmiValue = omBmiView.getText().toString();
                                final String heightValue = omHeightView.getText().toString();
                                final String weightValue = omWeightView.getText().toString();
                                final String waistValue = omWaistView.getText().toString();
                                 deleteBmiRecord( activity,  recordId,  date,  time,  bmiValue,  heightValue,  weightValue,  waistValue);

                                //System.out.println(recordId);
                            }
                        });

                        //System.out.println("[setBodyRecord - case 2 have result:  handled list case" + listAdapter);
                    }

                } else {
                    ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                    lv1.setAdapter((ListAdapter) null);
                    TextView emptyText = (TextView) activity.findViewById(R.id.empty_msg);
                    emptyText.setText("沒有任何記錄");

                    //System.out.println("[setBodyRecord - case 2 no result");
                }
                break;
            }

            case "4": //blood pressure & heart rate section
            {
                //System.out.println("Run in case 4");
                //if (result != null && !result.equals("")) {
                System.out.println("4 size = "+new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext).getAllBp().size());
                if (new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext).getAllBp().size() > 0) {

                    //output the processed data to a hashmap
                    //HashMap<String, Object> hashmap = prepareBpListData(result, bodyRecordConstant.CHART_WEEKLY);
                    HashMap<String, Object> hashmap = prepareBpListDataNew(bodyRecordConstant.CHART_WEEKLY);
                    //HashMap<String, Object> hashmap = prepareBpListData(result);

                    //System.out.println("HashMap: hashmap " + hashmap);

                    System.out.println("BpListHashMap=>"+hashmap.size());
                    final Button btnShowTime = (Button) activity.findViewById(R.id.buttonShowTime2);
                    btnShowTime.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            PopupMenu popup = new PopupMenu(ctx, v);
                            MenuInflater inflater = popup.getMenuInflater();
                            inflater.inflate(R.menu.menu_time_bp, popup.getMenu());
                            popup.show();
                            popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                                @Override
                                public boolean onMenuItemClick(MenuItem item) {
                                    switch (item.getItemId()) {
                                        case R.id.bp_menu_weekly:
                                            bpTimeSwitch(resultCache, bodyRecordConstant.CHART_WEEKLY, activity, recordType);
                                            btnShowTime.setText("每星期");
                                            break;

                                        case R.id.bp_menu_monthly:
                                            bpTimeSwitch(resultCache, bodyRecordConstant.CHART_MONTHLY, activity, recordType);
                                            btnShowTime.setText("每月");
                                            break;

                                        case R.id.bp_menu_yearly:
                                            bpTimeSwitch(resultCache, bodyRecordConstant.CHART_YEARLY, activity, recordType);
                                            btnShowTime.setText("每年");
                                            break;

                                        case R.id.bp_menu_daily:
                                            bpTimeSwitch(resultCache, bodyRecordConstant.CHART_DAILY, activity, recordType);
                                            btnShowTime.setText("每日");
                                            break;

                                        default:
                                            break;
                                    }

                                    return false;
                                }
                            });

                        }
                    });


                    //handle graph case
                    if (mode == 0){

                        List<Integer> list7daysCount =(List<Integer>) hashmap.get("bp7daysCount");
                        //System.out.println("List: list7daysCount " + list7daysCount);


                        //TODO control what data to show - for now is show 7 days  for graph
                        yData = new int[list7daysCount.size()];
                        for(int i = 0; i < list7daysCount.size(); i++) yData[i] = list7daysCount.get(i);

                        testChart = (LinearLayout) activity.findViewById(R.id.testChart);
                        mpPieChart = (PieChart) activity.findViewById(R.id.chart1);
                        setupPieChart(Integer.parseInt(recordType), "7天");



                        HashMap<String, List<bodyRecord>> listAllBloodRecord = (HashMap<String, List<bodyRecord>>) hashmap.get("BP_RECORDS");
                        bp7days = listAllBloodRecord.get("LAST7DAYS");

                        testChart2 = (LinearLayout) activity.findViewById(R.id.testLineChart);
                        lineChart = (LineChart) activity.findViewById(R.id.chart2);
                        setupLineChart(Integer.parseInt(recordType));


                        //System.out.println("[setBodyGraph -Case 4 have result:  handled graph case");

                    }

                    //handle list case
                    if (mode == 1) {
                        final ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                        HashMap<String, List<bodyRecord>> listAllBpRecord = (HashMap<String, List<bodyRecord>>) hashmap.get("BP_RECORDS");

                        //TODO control what glucose data to get - for now is show all for list , choice: ALL, FASTING, POST
                        List<bodyRecord> checkRecord = listAllBpRecord.get("ALL");
                        //System.out.println("HashMap: listAllBpRecord " + listAllBpRecord);
                        bpListAdapter listAdapter = new bpListAdapter(activity, checkRecord);

                        //System.out.println("bpListAdapter: listAdapter item:" + listAdapter.getCount());
                        for (bodyRecord record : checkRecord) {
                            //System.out.println("checkRecord:itemID: " + record.getRecordId() + " Date: " + record.getDate());
                        }
                        lv1.setAdapter(listAdapter);
                        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                TextView recordIdView = (TextView)view.findViewById(R.id.bpRecordId);
                                TextView dateView = (TextView)view.findViewById(R.id.date);
                                TextView timeView = (TextView)view.findViewById(R.id.time);
                                TextView periodView = (TextView)view.findViewById(R.id.bpPeriodType);
                                TextView bpHView = (TextView)view.findViewById(R.id.bp_h);
                                TextView bpLView = (TextView)view.findViewById(R.id.bp_l);
                                TextView heartRateView = (TextView)view.findViewById(R.id.heart_rate);

                                final String recordId = recordIdView.getText().toString();
                                final String date = dateView.getText().toString();
                                final String time = timeView.getText().toString();
                                final String typePeriod = periodView.getText().toString();
                                final String bpHValue = bpHView.getText().toString();
                                final String bpLValue = bpLView.getText().toString();
                                final String heartRateValue = heartRateView.getText().toString();
                                deleteBpRecord( activity,  recordId,  date,  time,  typePeriod,  bpHValue,  bpLValue,  heartRateValue);


                                //System.out.println(recordId);
                            }
                        });

                        //System.out.println("[setBodyRecord - case 4 have result:  handled list case" + listAdapter);
                    }

                } else {
                    ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                    lv1.setAdapter((ListAdapter) null);
                    TextView emptyText = (TextView) activity.findViewById(R.id.empty_msg);
                    emptyText.setText("沒有任何記錄");

                    //System.out.println("[setBodyRecord - case 4 no result");
                }
                break;

            }

            case "7": //other body index - report section (mix type, record type = 7)
            {
                //System.out.println("Run in case 7");
                if (result != null && !result.equals("")) {

                    //output the processed data to a hashmap
                    HashMap<String, Object> hashmap = prepareReportListData(result);
                    //System.out.println("HashMap: hashmap " + hashmap);

                    //handle graph case
                    if (mode == 0){

                        double hba1c_value =(double) hashmap.get("lastHbA1C");
                        //System.out.println("Value: hba1c_value " + hba1c_value);
                        String hba1c_date_value =(String) hashmap.get("lastHbA1cDate");

                        double totalC_value =(double) hashmap.get("lastTotalC");
                        //System.out.println("Value: totalC_value " + totalC_value);
                        double hdlC_value =(double) hashmap.get("lastHdlC");
                        //System.out.println("Value: hdlC_value " + hdlC_value);
                        double ldlC_value =(double) hashmap.get("lastLdlC");
                        //System.out.println("Value: ldlC_value " + ldlC_value);
                        double tri_value =(double) hashmap.get("lastTri");
                        //System.out.println("Value: tri_value " + tri_value);
                        String c_date_value =(String) hashmap.get("lastCDate");

                        TextView dateView = (TextView) activity.findViewById(R.id.or_hba1c_date);
                        TextView dateView2 = (TextView) activity.findViewById(R.id.or_totalC_date);
                        dateView.setText(hba1c_date_value);
                        dateView2.setText(c_date_value);

                        TextView hba1cView = (TextView) activity.findViewById(R.id.HbA1cValue);
                        hba1cView.setText(Double.toString(hba1c_value));
                        TextView totalCView = (TextView) activity.findViewById(R.id.TotalCValue);
                        totalCView.setText(Double.toString(totalC_value));
                        TextView hdlCView = (TextView) activity.findViewById(R.id.HdlCValue);
                        hdlCView.setText(Double.toString(hdlC_value));
                        TextView ldlCView = (TextView) activity.findViewById(R.id.LdlCValue);
                        ldlCView.setText(Double.toString(ldlC_value));
                        TextView triView = (TextView) activity.findViewById(R.id.TriValue);
                        triView.setText(Double.toString(tri_value));


                        TextView hba1cViewComment = (TextView) activity.findViewById(R.id.hba1c_comment);
                        TextView totalCViewComment = (TextView) activity.findViewById(R.id.totalc_comment);
                        TextView hdlCViewComment = (TextView) activity.findViewById(R.id.hdlc_comment);
                        TextView ldlCViewComment = (TextView) activity.findViewById(R.id.ldlc_comment);
                        TextView triViewComment = (TextView) activity.findViewById(R.id.tri_comment);

                        if (hba1c_value < bodyRecordConstant.GET_HBA1C_LOWER_LIMIT) {
                            hba1cView.setTextColor(Color.parseColor("#673ab7"));
                            hba1cViewComment.setText("過低");
                        }else if(hba1c_value >= bodyRecordConstant.GET_HBA1C_HIGH){
                            hba1cView.setTextColor(Color.parseColor("#FF5722"));
                            hba1cViewComment.setText("欠佳");
                        }else if(hba1c_value >= bodyRecordConstant.GET_HBA1C_MIDHIGH  && hba1c_value < bodyRecordConstant.GET_HBA1C_HIGH){
                            hba1cView.setTextColor(Color.parseColor("#f7c600"));
                            hba1cViewComment.setText("普通");
                        }else{
                            hba1cView.setTextColor(Color.parseColor("#4caf50"));
                            hba1cViewComment.setText("理想");
                        }

                        if (totalC_value < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                            totalCView.setTextColor(Color.parseColor("#673ab7"));
                            totalCViewComment.setText("過低");
                        }else if(totalC_value >= bodyRecordConstant.GET_TOTALC_HIGH){
                            totalCView.setTextColor(Color.parseColor("#FF5722"));
                            totalCViewComment.setText("欠佳");
                        }else if(totalC_value >= bodyRecordConstant.GET_TOTALC_MIDHIGH  && totalC_value < bodyRecordConstant.GET_TOTALC_HIGH){
                            totalCView.setTextColor(Color.parseColor("#f7c600"));
                            totalCViewComment.setText("普通");
                        }else{
                            totalCView.setTextColor(Color.parseColor("#4caf50"));
                            totalCViewComment.setText("理想");
                        }

                        if (user.sex==0) {
                            if (hdlC_value < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                                hdlCView.setTextColor(Color.parseColor("#673ab7"));
                                hdlCViewComment.setText("過低");
                            } else if (hdlC_value > bodyRecordConstant.GET_HDLC_FEMALE) {
                                hdlCView.setTextColor(Color.parseColor("#4caf50"));
                                hdlCViewComment.setText("理想");
                            } else {
                                hdlCView.setTextColor(Color.parseColor("#FF5722"));
                                hdlCViewComment.setText("欠佳");
                            }
                        }else{
                            if (hdlC_value < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                                hdlCView.setTextColor(Color.parseColor("#673ab7"));
                                hdlCViewComment.setText("過低");
                            } else if (hdlC_value > bodyRecordConstant.GET_HDLC_MALE) {
                                hdlCView.setTextColor(Color.parseColor("#4caf50"));
                                hdlCViewComment.setText("理想");
                            } else {
                                hdlCView.setTextColor(Color.parseColor("#FF5722"));
                                hdlCViewComment.setText("欠佳");
                            }
                        }


                        if (ldlC_value < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                            ldlCView.setTextColor(Color.parseColor("#673ab7"));
                            ldlCViewComment.setText("過低");
                        }else if(ldlC_value >= bodyRecordConstant.GET_LDLC_HIGH){
                            ldlCView.setTextColor(Color.parseColor("#FF5722"));
                            ldlCViewComment.setText("欠佳");
                        }else if(ldlC_value >= bodyRecordConstant.GET_LDLC_MIDHIGH  && ldlC_value < bodyRecordConstant.GET_LDLC_HIGH){
                            ldlCView.setTextColor(Color.parseColor("#f7c600"));
                            ldlCViewComment.setText("普通");
                        }else{
                            ldlCView.setTextColor(Color.parseColor("#4caf50"));
                            ldlCViewComment.setText("理想");
                        }


                        if (tri_value < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                            triView.setTextColor(Color.parseColor("#673ab7"));
                            triViewComment.setText("過低");
                        }else if(tri_value >= bodyRecordConstant.GET_TRIGLYCERIDERS_HIGH){
                            triView.setTextColor(Color.parseColor("#FF5722"));
                            triViewComment.setText("欠佳");
                        }else if(tri_value >= bodyRecordConstant.GET_TRIGLYCERIDERS_MIDHIGH  && tri_value < bodyRecordConstant.GET_TRIGLYCERIDERS_HIGH){
                            triView.setTextColor(Color.parseColor("#f7c600"));
                            triViewComment.setText("普通");
                        }else{
                            triView.setTextColor(Color.parseColor("#4caf50"));
                            triViewComment.setText("理想");
                        }

                        //setup indicator for HbA1c
                        int percent1 = 0;
                        if (hba1c_value >= 12){
                            percent1 = 100;
                        }else if(hba1c_value <= 4.9 ){
                            percent1 = 0;
                        } else{
                            percent1 = (int)( (hba1c_value - 4.9) / (12 - 4.9) * 100 );
                        }

                        hba1cbar =  ((CustomSeekBar) activity.findViewById(R.id.hba1cBar));
                        initDataToHba1cBar();
                        hba1cbar.setProgress(percent1);
                        hba1cbar.setClickable(false);
                        hba1cbar.setFocusable(false);
                        hba1cbar.setEnabled(false);

                        //setup indicator for Total C
                        int percent2 = 0;
                        if (totalC_value >= 15){
                            percent2 = 100;
                        }else if(totalC_value <= 0 ){
                            percent2 = 0;
                        } else{
                            percent2 = (int)( totalC_value / 15 * 100 );
                        }

                        totalcbar =  ((CustomSeekBar) activity.findViewById(R.id.totalcBar));
                        initDataToTotalCBar();
                        totalcbar.setProgress(percent2);
                        totalcbar.setClickable(false);
                        totalcbar.setFocusable(false);
                        totalcbar.setEnabled(false);

                        //setup indicator for HDL-C
                        int percent3 = 0;
                        if (hdlC_value >= 2){
                            percent3 = 100;
                        }else if(hdlC_value <= 0 ){
                            percent3 = 0;
                        } else{
                            percent3 = (int)( hdlC_value / 2 * 100 );
                        }

                        hdlcbar =  ((CustomSeekBar) activity.findViewById(R.id.hdlcBar));
                        initDataToHdlCBar();
                        hdlcbar.setProgress(percent3);
                        hdlcbar.setClickable(false);
                        hdlcbar.setFocusable(false);
                        hdlcbar.setEnabled(false);


                        //setup indicator for LDL-C
                        int percent4 = 0;
                        if (ldlC_value >= 6){
                            percent4 = 100;
                        }else if(ldlC_value <= 0 ){
                            percent4 = 0;
                        } else{
                            percent4 = (int)( ldlC_value / 6 * 100 );
                        }

                        ldlcbar =  ((CustomSeekBar) activity.findViewById(R.id.ldlcBar));
                        initDataToLdlCBar();
                        ldlcbar.setProgress(percent4);
                        ldlcbar.setClickable(false);
                        ldlcbar.setFocusable(false);
                        ldlcbar.setEnabled(false);

                        //setup indicator for Tri
                        int percent5 = 0;
                        if (tri_value >= 6){
                            percent5 = 100;
                        }else if(tri_value <= 0 ){
                            percent5 = 0;
                        } else{
                            percent5 = (int)( tri_value / 6 * 100 );
                        }

                        tribar =  ((CustomSeekBar) activity.findViewById(R.id.triBar));
                        initDataToTriBar();
                        tribar.setProgress(percent5);
                        tribar.setClickable(false);
                        tribar.setFocusable(false);
                        tribar.setEnabled(false);




                        //System.out.println("[setBodyGraph -Case 7 have result:  handled graph case");

                    }

                    //handle list case
                    if (mode == 1) {
                        final ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                        List<bodyRecord> listAllReportRecord = (List<bodyRecord>) hashmap.get("OR_RECORDS");

                        //System.out.println("HashMap: listAllReportRecord " + listAllReportRecord);
                        otherBodyReportListAdapter listAdapter = new otherBodyReportListAdapter(activity, listAllReportRecord);
                        //System.out.println("otherBodyMeasureListAdapter: listAdapter item:" + listAdapter.getCount());

                        for (bodyRecord record : listAllReportRecord) {
                            //System.out.println("listAllReportRecord:itemID: " + record.getRecordId() + " Date: " + record.getDate());
                        }
                        lv1.setAdapter(listAdapter);
                        lv1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                            @Override
                            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                                TextView recordIdView = (TextView)view.findViewById(R.id.orRecordId);
                                TextView recordTypeView = (TextView)view.findViewById(R.id.orRecordType);
                                TextView dateView = (TextView)view.findViewById(R.id.date);
                                TextView timeView = (TextView)view.findViewById(R.id.time);
                                TextView orHbA1cView = (TextView)view.findViewById(R.id.HbA1c);
                                TextView orTotalCView = (TextView)view.findViewById(R.id.totalC);
                                TextView orHdlCView = (TextView)view.findViewById(R.id.hdlC);
                                TextView orLdlCView = (TextView)view.findViewById(R.id.ldlC);
                                TextView orTriView = (TextView)view.findViewById(R.id.tri);
                                TextView orRemarkView = (TextView)view.findViewById(R.id.remark);

                                final String recordId = recordIdView.getText().toString();
                                final String recordType = recordTypeView.getText().toString();
                                final String date = dateView.getText().toString();
                                final String time = timeView.getText().toString();
                                if (recordType.equals("3")) {
                                    final String hba1cValue = orHbA1cView.getText().toString();
                                    deleteHbA1CRecord( activity,  recordId, recordType,  date,  time, hba1cValue );

                                }
                                if (recordType.equals("5")) {
                                    final String totalCValue = orTotalCView.getText().toString();
                                    final String hdlCValue = orHdlCView.getText().toString();
                                    final String ldlCValue = orLdlCView.getText().toString();
                                    final String orTriValue = orTriView.getText().toString();
                                    deleteCRecord(activity, recordId, recordType, date, time, totalCValue, hdlCValue, ldlCValue, orTriValue);
                                }

                                if (recordType.equals("6")) {
                                    final String remarkValue = orRemarkView.getText().toString();
                                    deleteRemarkRecord(activity, recordId, recordType, date, time, remarkValue );
                                }

                                //System.out.println(recordId);
                            }
                        });

                        //System.out.println("[setBodyRecord - case 2 have result:  handled list case" + listAdapter);
                    }

                } else {
                    ListView lv1 = (ListView) activity.findViewById(R.id.custom_list);
                    lv1.setAdapter((ListAdapter) null);
                    TextView emptyText = (TextView) activity.findViewById(R.id.empty_msg);
                    emptyText.setText("沒有任何記錄");

                    //System.out.println("[setBodyRecord - case 2 no result");
                }
                break;
            }


        }
    }

    public static void getInjectionRecord(String result){
        injectMap = prepareInjectData(result);
        //System.out.println("HashMap: injectMap " + injectMap);


    }

    public static void getInjectionRecordNew(Context context){
        INJECTION_RECORD injectionDB = new INJECTION_RECORD(context);
        List<INJECTION_RECORD.INJECTION_RECORD_class> result = injectionDB.getAll();
        injectMap = prepareInjectDataNew(result);
        //System.out.println("HashMap: injectMap " + injectMap);


    }


    public void deleteBloodRecord(Activity activity, String recordId, String date, String time, String typePeriod, String typeGlu, String gluValue){

                Intent intent = new Intent();
                intent.setClass(activity, blood_delete.class);
                intent.putExtra("recordId", recordId);
                intent.putExtra("date", date);
                intent.putExtra("time", time);
                intent.putExtra("typePeriod", typePeriod);
                intent.putExtra("typeGlu", typeGlu);
                intent.putExtra("gluValue", gluValue);
                activity.startActivity(intent);
                activity.finish();
    }

    public void deleteBpRecord(Activity activity, String recordId, String date, String time, String typePeriod, String bpHValue, String bpLValue, String heartRateValue ){

        Intent intent = new Intent();
        intent.setClass(activity, bp_delete.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("typePeriod", typePeriod);
        intent.putExtra("bpHValue", bpHValue);
        intent.putExtra("bpLValue", bpLValue);
        intent.putExtra("heartRateValue", heartRateValue);
        activity.startActivity(intent);
        //activity.finish();
    }



    public void deleteBmiRecord(Activity activity, String recordId, String date, String time, String bmiValue, String heightValue, String weightValue, String waistValue ){

        Intent intent = new Intent();
        intent.setClass(activity, otherBodyMeasure_delete.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("bmiValue", bmiValue);
        intent.putExtra("heightValue", heightValue);
        intent.putExtra("weightValue", weightValue);
        intent.putExtra("waistValue", waistValue);
        activity.startActivity(intent);
        activity.finish();
    }


    public void deleteHbA1CRecord(Activity activity, String recordId, String recordType, String date, String time, String HbA1cValue){

        Intent intent = new Intent();
        intent.setClass(activity, otherBodyReport_delete.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("recordType", recordType);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("HbA1cValue", HbA1cValue);
        activity.startActivity(intent);
        activity.finish();
    }


    public void deleteCRecord(Activity activity, String recordId, String recordType, String date, String time, String totalCValue, String hdlCValue, String ldlCValue, String orTriValue ){

        Intent intent = new Intent();
        intent.setClass(activity, otherBodyReport_delete.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("recordType", recordType);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("totalCValue", totalCValue);
        intent.putExtra("hdlCValue", hdlCValue);
        intent.putExtra("ldlCValue", ldlCValue);
        intent.putExtra("orTriValue", orTriValue);
        activity.startActivity(intent);
        activity.finish();
    }


    public void deleteRemarkRecord(Activity activity, String recordId, String recordType, String date, String time, String remark ){

        Intent intent = new Intent();
        intent.setClass(activity, otherBodyReport_delete.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("recordType", recordType);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("remark", remark);
        activity.startActivity(intent);
        activity.finish();
    }


    public void afterSaveRecord(Activity activity,String result, String recordType, int mode){
        switch (recordType) {
            case "1":{ //glucose (blood sugar) section
                if (result != null && !result.equals("")) {
                        Intent intent = new Intent();
                        intent.setClass(activity, MainActivity.class);
                        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
                        activity.startActivity(intent);
                        activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

            case "2":{ //BMI section
                if (result != null && !result.equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(activity, MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                    activity.startActivity(intent);
                    activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

            case "3":{ //HbA1c section
                if (result != null && !result.equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(activity, MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                    activity.startActivity(intent);
                    activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

            case "4":{ //Blood pressure section
                if (result != null && !result.equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(activity, MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BP);
                    activity.startActivity(intent);
                    activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

            case "5":{ //total cholesterol, LDL-C, HDL-C, TRIGLYCERIDERS section
                if (result != null && !result.equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(activity, MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                    activity.startActivity(intent);
                    activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

            case "6":{ //Remark section
                if (result != null && !result.equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(activity, MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                    activity.startActivity(intent);
                    activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

            case "7":{ //Mix report section
                if (result != null && !result.equals("")) {
                    Intent intent = new Intent();
                    intent.setClass(activity, MainActivity.class);
                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                    activity.startActivity(intent);
                    activity.finish();
                } else {
                    //System.out.println("[afterSaveRecord - no result");
                }
            }break;

        }
    }


    // Injection Data - Process the raw data from JSON raw
    public static HashMap<String, Object> prepareInjectData(String result) {
        //System.out.println("Run in prepareInjectData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            // INJECT_RECORDS data
           // List<Map<String, Integer>> all = new ArrayList<Map<String, Integer>>();
            Map<String, Integer> all = new HashMap<String, Integer>();
            Map<String, Integer> last7daysRecord = new HashMap<String, Integer>();

            //Calculate dates of 7 days
            DateTime last7Days = new DateTime().minusDays(8);
            DateTime now = new DateTime();

            // adding data

            JSONArray JA = new JSONArray(result);
            for (int i = 0; i < JA.length(); i++) {
                String recordDate = JA.getJSONObject(i).getString("INJECTION_DATE");
                try {

                    all.put(recordDate, JA.getJSONObject(i).getInt("INJECTION_VALUE"));
                    //System.out.println("each record injection: "+all);

                    if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                        last7daysRecord.put(recordDate, JA.getJSONObject(i).getInt("INJECTION_VALUE"));
                        //System.out.println("each record 7 days injection: " + last7daysRecord);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }

            }

            //add different types of records to a Hashmap "records"
            //temp.put("ALL", all);
            temp.put("LAST7DAYS", last7daysRecord);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public static HashMap<String, Object> prepareInjectDataNew(List<INJECTION_RECORD.INJECTION_RECORD_class> result) {
        //System.out.println("Run in prepareInjectData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            // INJECT_RECORDS data
            // List<Map<String, Integer>> all = new ArrayList<Map<String, Integer>>();
            Map<String, Integer> all = new HashMap<String, Integer>();
            Map<String, Integer> last7daysRecord = new HashMap<String, Integer>();

            //Calculate dates of 7 days
            DateTime last7Days = new DateTime().minusDays(8);
            DateTime now = new DateTime();

            // adding data

            for (int i = 0; i < result.size(); i++) {
                String recordDate = result.get(i).getINJECTION_DATE();
                try {

                    all.put(recordDate, result.get(i).getINJECTION_VALUE());
                    //System.out.println("each record injection: "+all);

                    if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                        last7daysRecord.put(recordDate, result.get(i).getINJECTION_VALUE());
                        //System.out.println("each record 7 days injection: " + last7daysRecord);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }

            }

            //add different types of records to a Hashmap "records"
            //temp.put("ALL", all);
            temp.put("LAST7DAYS", last7daysRecord);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }


    // BP & heart rate List Data - Process the raw data from JSON raw
    /*public HashMap<String, Object> prepareBpListData(String result) {
        //System.out.println("Run in prepareBpListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {
            HashMap<String, List<bodyRecord>> records = new HashMap<String, List<bodyRecord>>();

            // Statistic bp data
            int bpHighTotal = 0;
            int bpMidhighTotal = 0;
            int bpMidTotal = 0;
            int bpLowTotal = 0;

            int bpHigh7dayTotal = 0;
            int bpMidhigh7dayTotal = 0;
            int bpMid7dayTotal = 0;
            int bpLow7dayTotal = 0;

            List<Integer> bpCount = new ArrayList<Integer>();
            List<Integer> bp7daysCount = new ArrayList<Integer>();


            // BP_RECORDS data
            List<bodyRecord> all = new ArrayList<bodyRecord>();
            List<bodyRecord> last7daysRecord = new ArrayList<bodyRecord>();

            //Calculate dates of 7 days
            DateTime last7Days = new DateTime().minusDays(8);
            DateTime now = new DateTime();

            // adding Statistic bp data
            JSONArray JA = new JSONArray(result);
            for (int i = 0; i < JA.length(); i++) {
                bodyRecord eachRecord;
                String recordDate = JA.getJSONObject(i).getString("DATE");
                try {
                    int bp_up_value = JA.getJSONObject(i).getInt("BP_H");
                    int bp_low_value = JA.getJSONObject(i).getInt("BP_L");

                    if (bp_up_value < bodyRecordConstant.GET_BP_SYS_LOW || bp_low_value < bodyRecordConstant.GET_BP_DIA_LOW) {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            bpLow7dayTotal++;
                        }
                        bpLowTotal++;
                    } else if ((bp_up_value >= bodyRecordConstant.GET_BP_SYS_LOW &&  bp_up_value< bodyRecordConstant.GET_BP_SYS_MIDHIGH)
                            && (bp_low_value >= bodyRecordConstant.GET_BP_DIA_LOW && bp_low_value < bodyRecordConstant.GET_BP_DIA_MIDHIGH)) {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            bpMid7dayTotal++;
                        }
                        bpMidTotal++;
                    }else if ((bp_up_value > bodyRecordConstant.GET_BP_SYS_HIGH )
                            || (bp_low_value > bodyRecordConstant.GET_BP_DIA_HIGH)) {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            bpHigh7dayTotal++;
                        }
                        bpHighTotal++;
                    }
                    else if ((bp_up_value >= bodyRecordConstant.GET_BP_SYS_MIDHIGH && bp_up_value <= bodyRecordConstant.GET_BP_SYS_HIGH)
                            || (bp_low_value >= bodyRecordConstant.GET_BP_DIA_MIDHIGH && bp_low_value <= bodyRecordConstant.GET_BP_DIA_HIGH)) {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            bpMidhigh7dayTotal++;
                        }
                        bpMidhighTotal++;
                    }else{
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            bpHigh7dayTotal++;
                        }
                        bpHighTotal++;
                    }


                    eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                            JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                            0, 0, 0, 0,
                            JA.getJSONObject(i).getInt("BP_H"), JA.getJSONObject(i).getInt("BP_L"), JA.getJSONObject(i).getInt("HEART_RATE"),
                            0, 0, 0, 0,
                            0, 0, 0, 0, "");

                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                // Adding BP_RECORDS data
                all.add(eachRecord);

                if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                    last7daysRecord.add(eachRecord);
                }
            }

            //add different types of records to a Hashmap "records"
            records.put("ALL", all);
            records.put("LAST7DAYS", last7daysRecord);

            //add different types of counting to array lists in High-Mid-Low order
            bpCount.add(bpHighTotal);
            bpCount.add(bpMidhighTotal);
            bpCount.add(bpMidTotal);
            bpCount.add(bpLowTotal);

            bp7daysCount.add(bpHigh7dayTotal);
            bp7daysCount.add(bpMidhigh7dayTotal);
            bp7daysCount.add(bpMid7dayTotal);
            bp7daysCount.add(bpLow7dayTotal);

            temp.put("BP_RECORDS", records);
            temp.put("bpCount", bpCount);
            temp.put("bp7daysCount", bp7daysCount);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }*/

    //Janus
    public HashMap<String, Object> prepareBpListData(String result, Integer timeRange) {
        //System.out.println("Run in prepareBpListData");

        System.out.println("prepareBpListData Mode="+timeRange);
        HashMap<String, Object> temp= new HashMap<String, Object>();

        try {
            HashMap<String, List<bodyRecord>> records = new HashMap<String, List<bodyRecord>>();

            // Statistic bp data
            int bpHighTotal = 0;
            int bpMidhighTotal = 0;
            int bpMidTotal = 0;
            int bpLowTotal = 0;

            int bpHigh7dayTotal = 0;
            int bpMidhigh7dayTotal = 0;
            int bpMid7dayTotal = 0;
            int bpLow7dayTotal = 0;

            List<Integer> bpCount = new ArrayList<Integer>();
            List<Integer> bp7daysCount = new ArrayList<Integer>();


            // BP_RECORDS data
            List<bodyRecord> all = new ArrayList<bodyRecord>();
            List<bodyRecord> last7daysRecord = new ArrayList<bodyRecord>();

            //Calculate dates of 7 days
            DateTime last7Days = null;
            switch (timeRange){
                case bodyRecordConstant.CHART_DAILY:
                    last7Days = new DateTime().minusDays(1).hourOfDay().setCopy(23).minuteOfHour().setCopy(59).secondOfMinute().setCopy(59);
                    break;

                case bodyRecordConstant.CHART_MONTHLY:
                    //last7Days = new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0);
                    last7Days = new DateTime().minusWeeks(4*3);
                    System.out.println("Start Date="+last7Days.toString());
                    //System.out.println("Start Date="+new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0).toString());
                    break;

                case bodyRecordConstant.CHART_WEEKLY:
                    last7Days = new DateTime().minusDays(7);
                    break;

                case bodyRecordConstant.CHART_YEARLY:
                    last7Days = new DateTime().minusMonths(6);
                    break;

            }
            DateTime now = new DateTime();

            // adding Statistic bp data
            JSONArray JA = new JSONArray(result);
            //com.dmc.myapplication.Models.bodyRecord BRDB = new com.dmc.myapplication.Models.bodyRecord;
            //BRDB.getAllBp().get(1).

           // switch (timeRange){
          //      default:
                    for (int i = 0; i < JA.length(); i++) {
                        bodyRecord eachRecord;
                        String recordDate = JA.getJSONObject(i).getString("DATE");
                        int bp_up_value = JA.getJSONObject(i).getInt("BP_H");
                        int bp_low_value = JA.getJSONObject(i).getInt("BP_L");
                        try {

                            if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                                if (bp_up_value < bodyRecordConstant.GET_BP_SYS_LOW || bp_low_value < bodyRecordConstant.GET_BP_DIA_LOW) {
                                    bpLow7dayTotal++;
                                    bpLowTotal++;
                                } else if ((bp_up_value >= bodyRecordConstant.GET_BP_SYS_LOW &&  bp_up_value< bodyRecordConstant.GET_BP_SYS_MIDHIGH)
                                        && (bp_low_value >= bodyRecordConstant.GET_BP_DIA_LOW && bp_low_value < bodyRecordConstant.GET_BP_DIA_MIDHIGH)) {
                                    bpMid7dayTotal++;
                                    bpMidTotal++;
                                }else if ((bp_up_value > bodyRecordConstant.GET_BP_SYS_HIGH )
                                        || (bp_low_value > bodyRecordConstant.GET_BP_DIA_HIGH)) {
                                    bpHigh7dayTotal++;
                                    bpHighTotal++;
                                }
                                else if ((bp_up_value >= bodyRecordConstant.GET_BP_SYS_MIDHIGH && bp_up_value <= bodyRecordConstant.GET_BP_SYS_HIGH)
                                        || (bp_low_value >= bodyRecordConstant.GET_BP_DIA_MIDHIGH && bp_low_value <= bodyRecordConstant.GET_BP_DIA_HIGH)) {
                                    bpMidhigh7dayTotal++;
                                    bpMidhighTotal++;
                                }else{
                                    bpHigh7dayTotal++;
                                    bpHighTotal++;
                                }
                            }


                            System.out.println("Date="+JA.getJSONObject(i).getString("DATE")+" Time="+JA.getJSONObject(i).getString("TIME")+ " HeartRate="+JA.getJSONObject(i).getInt("HEART_RATE"));
                            eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                    JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                    0, 0, 0, 0,
                                    JA.getJSONObject(i).getInt("BP_H"), JA.getJSONObject(i).getInt("BP_L"), JA.getJSONObject(i).getInt("HEART_RATE"),
                                    0, 0, 0, 0,
                                    0, 0, 0, 0, "");

                        } catch (Exception e) {
                            e.printStackTrace();
                            throw new RuntimeException(e);
                        }
                        // Adding BP_RECORDS data
                        all.add(eachRecord);

                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            last7daysRecord.add(eachRecord);
                        }
                    }
              //      break;
            //}

            //add different types of records to a Hashmap "records"
            records.put("ALL", all);

            List<bodyRecord> newlast7daysRecord = new ArrayList<bodyRecord>();

            Map<String, Integer> BP_UP_LIST = new HashMap<String, Integer>();
            Map<String, Integer> BP_LOW_LIST = new HashMap<String, Integer>();
            Map<String, Integer> BP_HB_LIST = new HashMap<String, Integer>();

            switch (timeRange) {
                case bodyRecordConstant.CHART_WEEKLY:
                    for (bodyRecord item: last7daysRecord) {
                        if (BP_UP_LIST.containsKey(item.getDate())){
                            if (BP_UP_LIST.get(item.getDate()) < item.getBp_h()){
                                BP_UP_LIST.put(item.getDate(), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(item.getDate()) > item.getBp_l()){
                                BP_LOW_LIST.put(item.getDate(), item.getBp_l());
                            }

                            BP_HB_LIST.put(item.getDate(), (BP_HB_LIST.get(item.getDate()) + item.getHeart_rate())/2);
                        }else{
                            BP_UP_LIST.put(item.getDate(), item.getBp_h());
                            BP_LOW_LIST.put(item.getDate(), item.getBp_l());
                            BP_HB_LIST.put(item.getDate(), item.getHeart_rate());
                        }
                    }
                    break;

                case bodyRecordConstant.CHART_MONTHLY:
                    System.out.println("No of Record:"+last7daysRecord.size());
                    for (bodyRecord item: last7daysRecord) {
                        //Calendar cal = Calendar.getInstance();
                        DateTime dt = new DateTime();
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                        dt = fmt.parseDateTime(item.getDate());
                        System.out.println(item.getDate());
                        System.out.println("M="+String.valueOf(dt.getMonthOfYear())+" D="+dt.getDayOfMonth());

                        String WeekKey = DateTimeFormat.forPattern("yyyy-MM-dd").print(dt.withDayOfWeek(DateTimeConstants.MONDAY));
                        if (BP_UP_LIST.containsKey(String.valueOf(WeekKey))){
                            if (BP_UP_LIST.get(String.valueOf(WeekKey)) < item.getBp_h()){
                                BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(String.valueOf(WeekKey)) > item.getBp_l()){
                                BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            }

                            System.out.println("->WOM"+String.valueOf(WeekKey));
                            System.out.println("->HB"+BP_HB_LIST.get(String.valueOf(WeekKey)));

                            BP_HB_LIST.put(String.valueOf(WeekKey), (BP_HB_LIST.get(String.valueOf(WeekKey)) + item.getHeart_rate())/2);

                        }else{
                            BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            BP_HB_LIST.put(String.valueOf(WeekKey), item.getHeart_rate());
                        }
                    }
                    break;

                case bodyRecordConstant.CHART_YEARLY:
                    System.out.println("No of Record:"+last7daysRecord.size());
                    for (bodyRecord item: last7daysRecord) {
                        //Calendar cal = Calendar.getInstance();
                        DateTime dt = new DateTime();
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                        dt = fmt.parseDateTime(item.getDate());
                        System.out.println(item.getDate());
                        System.out.println("M="+String.valueOf(dt.getMonthOfYear())+" D="+dt.getDayOfMonth());

                        String WeekKey = DateTimeFormat.forPattern("yyyy-MM").print(dt);
                        if (BP_UP_LIST.containsKey(String.valueOf(WeekKey))){
                            if (BP_UP_LIST.get(String.valueOf(WeekKey)) < item.getBp_h()){
                                BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(String.valueOf(WeekKey)) > item.getBp_l()){
                                BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            }

                            System.out.println("->WOM"+String.valueOf(WeekKey));
                            System.out.println("->HB"+BP_HB_LIST.get(String.valueOf(WeekKey)));

                            BP_HB_LIST.put(String.valueOf(WeekKey), (BP_HB_LIST.get(String.valueOf(WeekKey)) + item.getHeart_rate())/2);

                        }else{
                            BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            BP_HB_LIST.put(String.valueOf(WeekKey), item.getHeart_rate());
                        }
                    }
                    break;

                case bodyRecordConstant.CHART_DAILY:
                    System.out.println("No of Record:"+last7daysRecord.size());
                    for (bodyRecord item: last7daysRecord) {
                        //Calendar cal = Calendar.getInstance();
                        DateTime dt = new DateTime();
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
                        dt = fmt.parseDateTime(item.getTime());
                        System.out.println(item.getDate());
                        System.out.println("M="+String.valueOf(dt.getMonthOfYear())+" D="+dt.getDayOfMonth());

                        String WeekKey = DateTimeFormat.forPattern("HH").print(dt)+"時";
                        if (BP_UP_LIST.containsKey(String.valueOf(WeekKey))){
                            if (BP_UP_LIST.get(String.valueOf(WeekKey)) < item.getBp_h()){
                                BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(String.valueOf(WeekKey)) > item.getBp_l()){
                                BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            }

                            System.out.println("->WOM"+String.valueOf(WeekKey));
                            System.out.println("->HB"+BP_HB_LIST.get(String.valueOf(WeekKey)));

                            BP_HB_LIST.put(String.valueOf(WeekKey), (BP_HB_LIST.get(String.valueOf(WeekKey)) + item.getHeart_rate())/2);

                        }else{
                            BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            BP_HB_LIST.put(String.valueOf(WeekKey), item.getHeart_rate());
                        }
                    }
                    break;

            }


            for (int x = (BP_HB_LIST.size() - 1); x >= 0; x--){
                String label = BP_HB_LIST.keySet().toArray()[x].toString();

                System.out.println("UP="+ BP_UP_LIST.get(label));
                System.out.println("LO="+ BP_LOW_LIST.get(label));
                System.out.println("HB="+ BP_HB_LIST.get(label));

                bodyRecord weekyRecord = new bodyRecord(0, 0, 0,
                        BP_HB_LIST.keySet().toArray()[x].toString(), "",
                        0, 0, 0, 0,
                        BP_UP_LIST.get(label), BP_LOW_LIST.get(label), BP_HB_LIST.get(label),
                        0, 0, 0, 0,
                        0, 0, 0, 0, "");
                newlast7daysRecord.add(weekyRecord);
            }
            last7daysRecord = null;
            records.put("LAST7DAYS", newlast7daysRecord);

            //add different types of counting to array lists in High-Mid-Low order
            bpCount.add(bpHighTotal);
            bpCount.add(bpMidhighTotal);
            bpCount.add(bpMidTotal);
            bpCount.add(bpLowTotal);

            bp7daysCount.add(bpHigh7dayTotal);
            bp7daysCount.add(bpMidhigh7dayTotal);
            bp7daysCount.add(bpMid7dayTotal);
            bp7daysCount.add(bpLow7dayTotal);

            temp.put("BP_RECORDS", records);
            temp.put("bpCount", bpCount);
            temp.put("bp7daysCount", bp7daysCount);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Reset timeMode to Weekly
        //timeMode = bodyRecordConstant.CHART_WEEKLY;
        return temp;
    }


    public HashMap<String, Object> prepareBpListDataNew(Integer timeRange) {
        //System.out.println("Run in prepareBpListData");

        System.out.println("prepareBpListData Mode="+timeRange);
        HashMap<String, Object> temp= new HashMap<String, Object>();

        try {
            HashMap<String, List<bodyRecord>> records = new HashMap<String, List<bodyRecord>>();

            // Statistic bp data
            int bpHighTotal = 0;
            int bpMidhighTotal = 0;
            int bpMidTotal = 0;
            int bpLowTotal = 0;

            int bpHigh7dayTotal = 0;
            int bpMidhigh7dayTotal = 0;
            int bpMid7dayTotal = 0;
            int bpLow7dayTotal = 0;

            List<Integer> bpCount = new ArrayList<Integer>();
            List<Integer> bp7daysCount = new ArrayList<Integer>();


            // BP_RECORDS data
            List<bodyRecord> all = new ArrayList<bodyRecord>();
            List<bodyRecord> last7daysRecord = new ArrayList<bodyRecord>();

            //Calculate dates of 7 days
            DateTime last7Days = null;
            switch (timeRange){
                case bodyRecordConstant.CHART_DAILY:
                    last7Days = new DateTime().minusDays(1).hourOfDay().setCopy(23).minuteOfHour().setCopy(59).secondOfMinute().setCopy(59);
                    break;

                case bodyRecordConstant.CHART_MONTHLY:
                    //last7Days = new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0);
                    last7Days = new DateTime().minusWeeks(4*3);
                    System.out.println("Start Date="+last7Days.toString());
                    //System.out.println("Start Date="+new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0).toString());
                    break;

                case bodyRecordConstant.CHART_WEEKLY:
                    last7Days = new DateTime().minusDays(7);
                    break;

                case bodyRecordConstant.CHART_YEARLY:
                    last7Days = new DateTime().minusMonths(6);
                    break;

            }
            DateTime now = new DateTime();

            // adding Statistic bp data
            //JSONArray JA = new JSONArray(result);
            com.dmc.myapplication.Models.bodyRecord BRDB = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
            List<bodyRecord> JA =  BRDB.getAllBp();

            // switch (timeRange){
            //      default:
            for (int i = 0; i < JA.size(); i++) {
                bodyRecord eachRecord;
                String recordDate = JA.get(i).getDate();
                int bp_up_value = JA.get(i).getBp_h();
                int bp_low_value = JA.get(i).getBp_l();
                try {

                    if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                        if (bp_up_value < bodyRecordConstant.GET_BP_SYS_LOW || bp_low_value < bodyRecordConstant.GET_BP_DIA_LOW) {
                            bpLow7dayTotal++;
                            bpLowTotal++;
                        } else if ((bp_up_value >= bodyRecordConstant.GET_BP_SYS_LOW &&  bp_up_value< bodyRecordConstant.GET_BP_SYS_MIDHIGH)
                                && (bp_low_value >= bodyRecordConstant.GET_BP_DIA_LOW && bp_low_value < bodyRecordConstant.GET_BP_DIA_MIDHIGH)) {
                            bpMid7dayTotal++;
                            bpMidTotal++;
                        }else if ((bp_up_value > bodyRecordConstant.GET_BP_SYS_HIGH )
                                || (bp_low_value > bodyRecordConstant.GET_BP_DIA_HIGH)) {
                            bpHigh7dayTotal++;
                            bpHighTotal++;
                        }
                        else if ((bp_up_value >= bodyRecordConstant.GET_BP_SYS_MIDHIGH && bp_up_value <= bodyRecordConstant.GET_BP_SYS_HIGH)
                                || (bp_low_value >= bodyRecordConstant.GET_BP_DIA_MIDHIGH && bp_low_value <= bodyRecordConstant.GET_BP_DIA_HIGH)) {
                            bpMidhigh7dayTotal++;
                            bpMidhighTotal++;
                        }else{
                            bpHigh7dayTotal++;
                            bpHighTotal++;
                        }
                    }


                    System.out.println("Date="+JA.get(i).getDate()+" Time="+JA.get(i).getTime()+ " HeartRate="+JA.get(i).getHeart_rate());
                    eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                            JA.get(i).getDate(), JA.get(i).getTime(),
                            0, 0, 0, 0,
                            JA.get(i).getBp_h(), JA.get(i).getBp_l(), JA.get(i).getHeart_rate(),
                            0, 0, 0, 0,
                            0, 0, 0, 0, "");

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                // Adding BP_RECORDS data
                all.add(eachRecord);

                if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                    last7daysRecord.add(eachRecord);
                }
            }
            //      break;
            //}

            //add different types of records to a Hashmap "records"
            records.put("ALL", all);

            List<bodyRecord> newlast7daysRecord = new ArrayList<bodyRecord>();

            Map<String, Integer> BP_UP_LIST = new HashMap<String, Integer>();
            Map<String, Integer> BP_LOW_LIST = new HashMap<String, Integer>();
            Map<String, Integer> BP_HB_LIST = new HashMap<String, Integer>();

            switch (timeRange) {
                case bodyRecordConstant.CHART_WEEKLY:
                    for (bodyRecord item: last7daysRecord) {
                        if (BP_UP_LIST.containsKey(item.getDate())){
                            /*if (BP_UP_LIST.get(item.getDate()) < item.getBp_h()){
                                BP_UP_LIST.put(item.getDate(), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(item.getDate()) > item.getBp_l()){
                                BP_LOW_LIST.put(item.getDate(), item.getBp_l());
                            }*/

                            BP_UP_LIST.put(item.getDate(), (BP_UP_LIST.get(item.getDate()) + item.getHeart_rate())/2);
                            BP_LOW_LIST.put(item.getDate(), (BP_LOW_LIST.get(item.getDate()) + item.getHeart_rate())/2);

                            BP_HB_LIST.put(item.getDate(), (BP_HB_LIST.get(item.getDate()) + item.getHeart_rate())/2);
                        }else{
                            BP_UP_LIST.put(item.getDate(), item.getBp_h());
                            BP_LOW_LIST.put(item.getDate(), item.getBp_l());
                            BP_HB_LIST.put(item.getDate(), item.getHeart_rate());
                        }
                    }
                    break;

                case bodyRecordConstant.CHART_MONTHLY:
                    System.out.println("No of Record:"+last7daysRecord.size());
                    for (bodyRecord item: last7daysRecord) {
                        //Calendar cal = Calendar.getInstance();
                        DateTime dt = new DateTime();
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                        dt = fmt.parseDateTime(item.getDate());
                        System.out.println(item.getDate());
                        System.out.println("M="+String.valueOf(dt.getMonthOfYear())+" D="+dt.getDayOfMonth());

                        String WeekKey = DateTimeFormat.forPattern("yyyy-MM-dd").print(dt.withDayOfWeek(DateTimeConstants.MONDAY));
                        if (BP_UP_LIST.containsKey(String.valueOf(WeekKey))){
                            if (BP_UP_LIST.get(String.valueOf(WeekKey)) < item.getBp_h()){
                                BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(String.valueOf(WeekKey)) > item.getBp_l()){
                                BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            }

                            System.out.println("->WOM"+String.valueOf(WeekKey));
                            System.out.println("->HB"+BP_HB_LIST.get(String.valueOf(WeekKey)));

                            BP_HB_LIST.put(String.valueOf(WeekKey), (BP_HB_LIST.get(String.valueOf(WeekKey)) + item.getHeart_rate())/2);

                        }else{
                            BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            BP_HB_LIST.put(String.valueOf(WeekKey), item.getHeart_rate());
                        }
                    }
                    break;

                case bodyRecordConstant.CHART_YEARLY:
                    System.out.println("No of Record:"+last7daysRecord.size());
                    for (bodyRecord item: last7daysRecord) {
                        //Calendar cal = Calendar.getInstance();
                        DateTime dt = new DateTime();
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                        dt = fmt.parseDateTime(item.getDate());
                        System.out.println(item.getDate());
                        System.out.println("M="+String.valueOf(dt.getMonthOfYear())+" D="+dt.getDayOfMonth());

                        String WeekKey = DateTimeFormat.forPattern("yyyy-MM").print(dt);
                        if (BP_UP_LIST.containsKey(String.valueOf(WeekKey))){
                            if (BP_UP_LIST.get(String.valueOf(WeekKey)) < item.getBp_h()){
                                BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(String.valueOf(WeekKey)) > item.getBp_l()){
                                BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            }

                            System.out.println("->WOM"+String.valueOf(WeekKey));
                            System.out.println("->HB"+BP_HB_LIST.get(String.valueOf(WeekKey)));

                            BP_HB_LIST.put(String.valueOf(WeekKey), (BP_HB_LIST.get(String.valueOf(WeekKey)) + item.getHeart_rate())/2);

                        }else{
                            BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            BP_HB_LIST.put(String.valueOf(WeekKey), item.getHeart_rate());
                        }
                    }
                    break;

                case bodyRecordConstant.CHART_DAILY:
                    System.out.println("No of Record:"+last7daysRecord.size());
                    for (bodyRecord item: last7daysRecord) {
                        //Calendar cal = Calendar.getInstance();
                        DateTime dt = new DateTime();
                        DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
                        dt = fmt.parseDateTime(item.getTime());
                        System.out.println(item.getDate());
                        System.out.println("M="+String.valueOf(dt.getMonthOfYear())+" D="+dt.getDayOfMonth());

                        String WeekKey = DateTimeFormat.forPattern("HH").print(dt)+"時";
                        if (BP_UP_LIST.containsKey(String.valueOf(WeekKey))){
                            if (BP_UP_LIST.get(String.valueOf(WeekKey)) < item.getBp_h()){
                                BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            }

                            if (BP_LOW_LIST.get(String.valueOf(WeekKey)) > item.getBp_l()){
                                BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            }

                            System.out.println("->WOM"+String.valueOf(WeekKey));
                            System.out.println("->HB"+BP_HB_LIST.get(String.valueOf(WeekKey)));

                            BP_HB_LIST.put(String.valueOf(WeekKey), (BP_HB_LIST.get(String.valueOf(WeekKey)) + item.getHeart_rate())/2);

                        }else{
                            BP_UP_LIST.put(String.valueOf(WeekKey), item.getBp_h());
                            BP_LOW_LIST.put(String.valueOf(WeekKey), item.getBp_l());
                            BP_HB_LIST.put(String.valueOf(WeekKey), item.getHeart_rate());
                        }
                    }
                    break;

            }


            for (int x = (BP_HB_LIST.size() - 1); x >= 0; x--){
                String label = BP_HB_LIST.keySet().toArray()[x].toString();

                System.out.println("UP="+ BP_UP_LIST.get(label));
                System.out.println("LO="+ BP_LOW_LIST.get(label));
                System.out.println("HB="+ BP_HB_LIST.get(label));

                bodyRecord weekyRecord = new bodyRecord(0, 0, 0,
                        BP_HB_LIST.keySet().toArray()[x].toString(), "",
                        0, 0, 0, 0,
                        BP_UP_LIST.get(label), BP_LOW_LIST.get(label), BP_HB_LIST.get(label),
                        0, 0, 0, 0,
                        0, 0, 0, 0, "");
                newlast7daysRecord.add(weekyRecord);
            }
            last7daysRecord = null;
            records.put("LAST7DAYS", newlast7daysRecord);

            //add different types of counting to array lists in High-Mid-Low order
            bpCount.add(bpHighTotal);
            bpCount.add(bpMidhighTotal);
            bpCount.add(bpMidTotal);
            bpCount.add(bpLowTotal);

            bp7daysCount.add(bpHigh7dayTotal);
            bp7daysCount.add(bpMidhigh7dayTotal);
            bp7daysCount.add(bpMid7dayTotal);
            bp7daysCount.add(bpLow7dayTotal);

            temp.put("BP_RECORDS", records);
            temp.put("bpCount", bpCount);
            temp.put("bp7daysCount", bp7daysCount);

        } catch (Exception e) {
            e.printStackTrace();
        }

        //Reset timeMode to Weekly
        //timeMode = bodyRecordConstant.CHART_WEEKLY;
        return temp;
    }


    // BMI Data - Process the raw data from JSON raw
    /*public HashMap<String, Object> prepareBmiListData(String result) {
        //System.out.println("Run in prepareBmiListData="+result);
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            // Statistic bp data
            double lastBMI = 0;
            double lastHeight = 0;
            double lastWeight = 0;
            double lastWaist = 0;
            String lastDate="";

            List<bodyRecord> all = new ArrayList<bodyRecord>();

             // DateTime now = new DateTime();
            //  int lastRecordDay=0;


            // get lastest record
            JSONArray JA = new JSONArray(result);
            for (int i = 0; i < JA.length(); i++) {
                bodyRecord eachRecord;
                //String recordDate = JA.getJSONObject(i).getString("DATE");
                try {
                    if(i == 0) {
                        lastBMI = JA.getJSONObject(i).getDouble("BMI");
                        lastHeight =  JA.getJSONObject(i).getDouble("HEIGHT");
                        lastWeight =  JA.getJSONObject(i).getDouble("WEIGHT");
                        lastWaist =  JA.getJSONObject(i).getDouble("WAIST");
                        lastDate = JA.getJSONObject(i).getString("DATE");
                        //lastRecordDay = timeCheck.calDaysUptoNow(recordDate, now);
                    }

                    eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                            JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                            JA.getJSONObject(i).getDouble("HEIGHT"), JA.getJSONObject(i).getDouble("WEIGHT"), JA.getJSONObject(i).getDouble("WAIST"), JA.getJSONObject(i).getDouble("BMI"),
                            0, 0, 0,
                            0, 0, 0, 0,
                            0, 0, 0, 0, "");

                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                // Adding OM_RECORDS data
                all.add(eachRecord);
            }
            //System.out.println("Prepare records: "+lastBMI +" "+lastHeight+" "+lastWeight+" "+lastWaist);

            temp.put("OM_RECORDS", all);
            temp.put("lastBMI", lastBMI);
            temp.put("lastHeight", lastHeight);
            temp.put("lastWeight", lastWeight);
            temp.put("lastWaist", lastWaist);
            temp.put("lastDate", lastDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }*/
    public HashMap<String, Object> prepareBmiListData(String result) {
        //System.out.println("Run in prepareBmiListData="+result);
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            // Statistic bp data
            double lastBMI = 0;
            double lastHeight = 0;
            double lastWeight = 0;
            double lastWaist = 0;
            String lastDate="";

            List<bodyRecord> all = new ArrayList<bodyRecord>();

            // DateTime now = new DateTime();
            //  int lastRecordDay=0;


            // get lastest record
            //JSONArray JA = new JSONArray(result);
            com.dmc.myapplication.Models.bodyRecord bodyRecord = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
            List<BodyRecord.bodyRecord> JA = bodyRecord.getAllBMI();

            for (int i = 0; i < JA.size(); i++) {
                bodyRecord eachRecord;
                //String recordDate = JA.getJSONObject(i).getString("DATE");
                try {
                    if(i == 0) {
                        lastBMI = JA.get(i).getBmi();
                        lastHeight =  JA.get(i).getHeight();
                        lastWeight =  JA.get(i).getWeight();
                        lastWaist =  JA.get(i).getWaist();
                        lastDate = JA.get(i).getDate();
                        //lastRecordDay = timeCheck.calDaysUptoNow(recordDate, now);
                    }

                    eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                            JA.get(i).getDate(), JA.get(i).getTime(),
                            JA.get(i).getHeight(), JA.get(i).getWeight(), JA.get(i).getWaist(), JA.get(i).getBmi(),
                            0, 0, 0,
                            0, 0, 0, 0,
                            0, 0, 0, 0, "");

                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                // Adding OM_RECORDS data
                all.add(eachRecord);
            }
            //System.out.println("Prepare records: "+lastBMI +" "+lastHeight+" "+lastWeight+" "+lastWaist);

            temp.put("OM_RECORDS", all);
            temp.put("lastBMI", lastBMI);
            temp.put("lastHeight", lastHeight);
            temp.put("lastWeight", lastWeight);
            temp.put("lastWaist", lastWaist);
            temp.put("lastDate", lastDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    // Reports Data - Process the raw data from JSON raw
    /*public HashMap<String, Object> prepareReportListData(String result) {
        //System.out.println("Run in prepareReportListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            // Statistic bp data
            double lastHbA1C = 0;
            double lastTotalC = 0;
            double lastHdlC = 0;
            double lastLdlC = 0;
            double lastTri = 0;
            String lastHbA1cDate="";
            String lastCDate="";

            List<bodyRecord> all = new ArrayList<bodyRecord>();

            // get lastest record
            JSONArray JA = new JSONArray(result);
            for (int i = 0; i < JA.length(); i++) {
                bodyRecord eachRecord;

                try {
                    int recordType = JA.getJSONObject(i).getInt("RECORD_TYPE");
                    if(recordType == 3) {
                        if (lastHbA1C==0){
                        lastHbA1C = JA.getJSONObject(i).getDouble("HbA1c");
                        lastHbA1cDate = JA.getJSONObject(i).getString("DATE");
                        }
                        eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                0, 0, 0, 0,
                                0, 0, 0,
                                JA.getJSONObject(i).getDouble("HbA1c"), 0, 0, 0,
                                0, 0, 0, 0, "");

                    }else if(recordType == 5) {
                        if (lastTotalC==0) {
                            lastTotalC = JA.getJSONObject(i).getDouble("TOTAL_C");
                            lastHdlC = JA.getJSONObject(i).getDouble("LDL_C");
                            lastLdlC = JA.getJSONObject(i).getDouble("HDL_C");
                            lastTri = JA.getJSONObject(i).getDouble("TRIGLYCERIDES");
                            lastCDate = JA.getJSONObject(i).getString("DATE");
                        }

                        eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, 0, 0, 0,
                                JA.getJSONObject(i).getDouble("TOTAL_C"), JA.getJSONObject(i).getDouble("LDL_C"), JA.getJSONObject(i).getDouble("HDL_C"), JA.getJSONObject(i).getDouble("TRIGLYCERIDES"), "");
                    }else {

                        eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, 0, 0, 0,
                                0, 0, 0, 0, JA.getJSONObject(i).getString("REMARKS"));
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                // Adding OM_RECORDS data
                all.add(eachRecord);
            }
            //System.out.println("Prepare records:");

            temp.put("OR_RECORDS", all);
            temp.put("lastHbA1C", lastHbA1C);
            temp.put("lastHbA1cDate", lastHbA1cDate);
            temp.put("lastTotalC", lastTotalC);
            temp.put("lastHdlC", lastHdlC);
            temp.put("lastLdlC", lastLdlC);
            temp.put("lastTri", lastTri);
            temp.put("lastCDate", lastCDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }*/
    public HashMap<String, Object> prepareReportListData(String result) {
        //System.out.println("Run in prepareReportListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            // Statistic bp data
            double lastHbA1C = 0;
            double lastTotalC = 0;
            double lastHdlC = 0;
            double lastLdlC = 0;
            double lastTri = 0;
            String lastHbA1cDate="";
            String lastCDate="";

            List<bodyRecord> all = new ArrayList<bodyRecord>();

            // get lastest record
            //JSONArray JA = new JSONArray(result);
            com.dmc.myapplication.Models.bodyRecord bodyRecord = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
            List<BodyRecord.bodyRecord> JA = bodyRecord.getAllREPORT();

            for (int i = 0; i < JA.size(); i++) {
                bodyRecord eachRecord;

                try {
                    int recordType = JA.get(i).getRecordType();
                    if(recordType == 3) {
                        if (lastHbA1C==0){
                            lastHbA1C = JA.get(i).getHba1c();
                            lastHbA1cDate = JA.get(i).getDate();
                        }
                        eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                JA.get(i).getDate(), JA.get(i).getTime(),
                                0, 0, 0, 0,
                                0, 0, 0,
                                JA.get(i).getHba1c(), 0, 0, 0,
                                0, 0, 0, 0, "");

                    }else if(recordType == 5) {
                        if (lastTotalC==0) {
                            lastTotalC = JA.get(i).getTotal_c();
                            lastHdlC = JA.get(i).getLdl_c();
                            lastLdlC = JA.get(i).getHdl_c();
                            lastTri = JA.get(i).getTriglyceriders();
                            lastCDate = JA.get(i).getDate();
                        }

                        eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                JA.get(i).getDate(), JA.get(i).getTime(),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, 0, 0, 0,
                                JA.get(i).getTotal_c(), JA.get(i).getLdl_c(), JA.get(i).getHdl_c(), JA.get(i).getTriglyceriders(), "");
                    }else {

                        eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                JA.get(i).getDate(), JA.get(i).getTime(),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, 0, 0, 0,
                                0, 0, 0, 0, JA.get(i).getRemarks());
                    }


                } catch (Exception e) {
                    e.printStackTrace();
                    throw new RuntimeException(e);
                }
                // Adding OM_RECORDS data
                all.add(eachRecord);
            }
            //System.out.println("Prepare records:");

            temp.put("OR_RECORDS", all);
            temp.put("lastHbA1C", lastHbA1C);
            temp.put("lastHbA1cDate", lastHbA1cDate);
            temp.put("lastTotalC", lastTotalC);
            temp.put("lastHdlC", lastHdlC);
            temp.put("lastLdlC", lastLdlC);
            temp.put("lastTri", lastTri);
            temp.put("lastCDate", lastCDate);

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }


    // Glucose List Data - Process the raw data from JSON raw
    public HashMap<String, Object> prepareBloodListData(String result, int viewMode) {
        ////System.out.println("Run in prepareBloodListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            if (viewMode == 1) {
                // Statistic blood data - all
                int gluHighTotal = 0;
                int gluMidHighTotal = 0;
                int gluMidTotal = 0;
                int gluLowTotal = 0;

                List<Integer> gluAllCount = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> all = new ArrayList<bodyRecord>();

                // adding Statistic blood data
                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    bodyRecord eachRecord;
                    double value = JA.getJSONObject(i).getDouble("GLUCOSE");
                    try { //Case : Glucose Before meal
                        if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                            if (value < user.pre_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.pre_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        //Case : Glucose After meal
                        else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                            if (value < user.post_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.post_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }

                        eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, JA.getJSONObject(i).getInt("PERIOD"), JA.getJSONObject(i).getInt("TYPE_GLUCOSE"), JA.getJSONObject(i).getDouble("GLUCOSE"),
                                0, 0, 0, 0, "");

                        all.add(eachRecord);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluAllCount.add(gluHighTotal);
                gluAllCount.add(gluMidHighTotal);
                gluAllCount.add(gluMidTotal);
                gluAllCount.add(gluLowTotal);

                //add 7 days records to a Hashmap
                temp.put("ALL", all);
                temp.put("gluAllCount",gluAllCount);

            } else{
                // Statistic blood data within 7 days
                int gluHigh7days = 0;
                int gluMidHigh7days = 0;
                int gluMid7days = 0;
                int gluLow7days = 0;

                List<Integer> gluCount7days = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> fast7daysList = new ArrayList<bodyRecord>();


                //Calculate dates of 7 days
                DateTime last7Days = new DateTime().minusDays(8);
                DateTime now = new DateTime();

                // adding Statistic blood data
                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    bodyRecord eachRecord;
                    String recordDate = JA.getJSONObject(i).getString("DATE");
                    double value = JA.getJSONObject(i).getDouble("GLUCOSE");
                    try {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            //Case : Glucose Before meal within 7 days
                            if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {

                                if (value < user.pre_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.pre_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            //Case : Glucose After meal within 7 days
                            else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                                if (value < user.post_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.post_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                    JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                    0, 0, 0, 0,
                                    0, 0, 0,
                                    0, JA.getJSONObject(i).getInt("PERIOD"), JA.getJSONObject(i).getInt("TYPE_GLUCOSE"), JA.getJSONObject(i).getDouble("GLUCOSE"),
                                    0, 0, 0, 0, "");

                            // Adding BLOOD_RECORDS data
                            fast7daysList.add(eachRecord);

                        }//end time check

                    }catch (JSONException e){
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluCount7days.add(gluHigh7days);
                gluCount7days.add(gluMidHigh7days);
                gluCount7days.add(gluMid7days);
                gluCount7days.add(gluLow7days);

                //add 7 days records to a Hashmap
                temp.put("FAST7DAYS", fast7daysList);
                temp.put("gluCount7days",gluCount7days);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public HashMap<String, Object> prepareBloodListDataNew(String result, int viewMode) {
        ////System.out.println("Run in prepareBloodListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            if (viewMode == 1) {
                // Statistic blood data - all
                int gluHighTotal = 0;
                int gluMidHighTotal = 0;
                int gluMidTotal = 0;
                int gluLowTotal = 0;

                List<Integer> gluAllCount = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> all = new ArrayList<bodyRecord>();

                // adding Statistic blood data
                //JSONArray JA = new JSONArray(result);
                com.dmc.myapplication.Models.bodyRecord bodyRecord = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
                List<bodyRecord> JA = bodyRecord.getAllGLUCOSE();

                for (int i = 0; i < JA.size(); i++) {
                    bodyRecord eachRecord;
                    double value = JA.get(i).getglucose();
                    try { //Case : Glucose Before meal
                        if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.get(i).getType_glucose()) {
                            if (value < user.pre_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.pre_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        //Case : Glucose After meal
                        else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.get(i).getType_glucose()) {
                            if (value < user.post_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.post_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        System.out.println("getDate="+JA.get(i).getDate());
                        eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                JA.get(i).getDate(), JA.get(i).getTime(),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, JA.get(i).getPeriod(), JA.get(i).getType_glucose(), JA.get(i).getglucose(),
                                0, 0, 0, 0, "");

                        all.add(eachRecord);
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluAllCount.add(gluHighTotal);
                gluAllCount.add(gluMidHighTotal);
                gluAllCount.add(gluMidTotal);
                gluAllCount.add(gluLowTotal);
                System.out.println("gluMidHighTotal="+gluMidHighTotal);
                System.out.println("gluMidTotal="+gluMidTotal);

                //add 7 days records to a Hashmap
                temp.put("ALL", all);
                temp.put("gluAllCount",gluAllCount);

            } else{
                // Statistic blood data within 7 days
                int gluHigh7days = 0;
                int gluMidHigh7days = 0;
                int gluMid7days = 0;
                int gluLow7days = 0;

                List<Integer> gluCount7days = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> fast7daysList = new ArrayList<bodyRecord>();


                //Calculate dates of 7 days
                DateTime last7Days = new DateTime().minusDays(8);
                DateTime now = new DateTime();

                // adding Statistic blood data
                //JSONArray JA = new JSONArray(result);
                com.dmc.myapplication.Models.bodyRecord bodyRecord = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
                List<bodyRecord> JA = bodyRecord.getAllGLUCOSE();

                for (int i = 0; i < JA.size(); i++) {
                    bodyRecord eachRecord;
                    String recordDate = JA.get(i).getDate();
                    double value = JA.get(i).getglucose();
                    try {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            //Case : Glucose Before meal within 7 days
                            if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.get(i).getType_glucose()) {

                                if (value < user.pre_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.pre_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            //Case : Glucose After meal within 7 days
                            else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.get(i).getType_glucose()) {
                                if (value < user.post_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.post_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                    JA.get(i).getDate(), JA.get(i).getTime(),
                                    0, 0, 0, 0,
                                    0, 0, 0,
                                    0, JA.get(i).getPeriod(), JA.get(i).getType_glucose(), JA.get(i).getglucose(),
                                    0, 0, 0, 0, "");

                            // Adding BLOOD_RECORDS data
                            fast7daysList.add(eachRecord);

                        }//end time check

                    }catch (Exception e){
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluCount7days.add(gluHigh7days);
                gluCount7days.add(gluMidHigh7days);
                gluCount7days.add(gluMid7days);
                gluCount7days.add(gluLow7days);

                //add 7 days records to a Hashmap
                temp.put("FAST7DAYS", fast7daysList);
                temp.put("gluCount7days",gluCount7days);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public HashMap<String, Object> prepareBloodListData(String result, int viewMode, int timeMode) {
        ////System.out.println("Run in prepareBloodListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            if (viewMode == 1) {
                // Statistic blood data - all
                int gluHighTotal = 0;
                int gluMidHighTotal = 0;
                int gluMidTotal = 0;
                int gluLowTotal = 0;

                List<Integer> gluAllCount = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> all = new ArrayList<bodyRecord>();

                // adding Statistic blood data
                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    bodyRecord eachRecord;
                    double value = JA.getJSONObject(i).getDouble("GLUCOSE");
                    try { //Case : Glucose Before meal
                        if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                            if (value < user.pre_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.pre_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        //Case : Glucose After meal
                        else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                            if (value < user.post_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.post_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }

                        eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, JA.getJSONObject(i).getInt("PERIOD"), JA.getJSONObject(i).getInt("TYPE_GLUCOSE"), JA.getJSONObject(i).getDouble("GLUCOSE"),
                                0, 0, 0, 0, "");

                        all.add(eachRecord);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluAllCount.add(gluHighTotal);
                gluAllCount.add(gluMidHighTotal);
                gluAllCount.add(gluMidTotal);
                gluAllCount.add(gluLowTotal);

                //add 7 days records to a Hashmap
                temp.put("ALL", all);
                temp.put("gluAllCount",gluAllCount);

            } else{
                // Statistic blood data within 7 days
                int gluHigh7days = 0;
                int gluMidHigh7days = 0;
                int gluMid7days = 0;
                int gluLow7days = 0;

                List<Integer> gluCount7days = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> fast7daysList = new ArrayList<bodyRecord>();


                //Calculate dates of 7 days
                //DateTime last7Days = new DateTime().minusDays(8);

                //Calculate dates of 7 days
                DateTime last7Days = null;
                switch (timeMode){
                    case bodyRecordConstant.CHART_DAILY:
                        last7Days = new DateTime().minusDays(1).hourOfDay().setCopy(23).minuteOfHour().setCopy(59).secondOfMinute().setCopy(59);
                        break;

                    case bodyRecordConstant.CHART_MONTHLY:
                        //last7Days = new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0);
                        last7Days = new DateTime().minusWeeks(4*3);
                        System.out.println("Start Date="+last7Days.toString());
                        //System.out.println("Start Date="+new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0).toString());
                        break;

                    case bodyRecordConstant.CHART_WEEKLY:
                        last7Days = new DateTime().minusDays(7);
                        break;

                    case bodyRecordConstant.CHART_YEARLY:
                        last7Days = new DateTime().minusMonths(6);
                        break;

                }
                DateTime now = new DateTime();

                // adding Statistic blood data
                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    bodyRecord eachRecord;
                    String recordDate = JA.getJSONObject(i).getString("DATE");
                    double value = JA.getJSONObject(i).getDouble("GLUCOSE");
                    try {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            //Case : Glucose Before meal within 7 days
                            if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {

                                if (value < user.pre_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.pre_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            //Case : Glucose After meal within 7 days
                            else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                                if (value < user.post_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.post_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                    JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                    0, 0, 0, 0,
                                    0, 0, 0,
                                    0, JA.getJSONObject(i).getInt("PERIOD"), JA.getJSONObject(i).getInt("TYPE_GLUCOSE"), JA.getJSONObject(i).getDouble("GLUCOSE"),
                                    0, 0, 0, 0, "");

                            // Adding BLOOD_RECORDS data
                            fast7daysList.add(eachRecord);

                        }//end time check

                    }catch (JSONException e){
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                List<bodyRecord> newfast7daysList = new ArrayList<>();
                Map<String, Double> glucoseList = new HashMap<String, Double>();
                int PERIOD = 1;
                int TYPE_GLUCOSE = 1;

                switch (timeMode){
                    case bodyRecordConstant.CHART_DAILY:
                        for (bodyRecord item: fast7daysList){
                            DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
                            DateTime dt = new DateTime();
                            dt = fmt.parseDateTime(item.getTime());
                            String Rkey = DateTimeFormat.forPattern("HH").print(dt)+"時";
                            if (glucoseList.containsKey(Rkey)){
                                glucoseList.put(Rkey, (glucoseList.get(Rkey)+item.getglucose())/2);
                            }else{
                                glucoseList.put(Rkey, item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;

                    case bodyRecordConstant.CHART_WEEKLY:
                        for (bodyRecord item: fast7daysList){
                            if (glucoseList.containsKey(item.getDate())){
                                glucoseList.put(item.getDate(), (glucoseList.get(item.getDate())+item.getglucose())/2);
                            }else{
                                glucoseList.put(item.getDate(), item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;

                    case bodyRecordConstant.CHART_MONTHLY:
                        for (bodyRecord item: fast7daysList){
                            DateTime dt = new DateTime();
                            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                            dt = fmt.parseDateTime(item.getDate());
                            String WeekKey = DateTimeFormat.forPattern("yyyy-MM-dd").print(dt.withDayOfWeek(DateTimeConstants.MONDAY));
                            if (glucoseList.containsKey(WeekKey)){
                                glucoseList.put(WeekKey, (glucoseList.get(WeekKey)+item.getglucose())/2);
                            }else{
                                glucoseList.put(WeekKey, item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;

                    case bodyRecordConstant.CHART_YEARLY:
                        for (bodyRecord item: fast7daysList){
                            DateTime dt = new DateTime();
                            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                            dt = fmt.parseDateTime(item.getDate());
                            String WeekKey = DateTimeFormat.forPattern("yyyy-MM").print(dt);
                            if (glucoseList.containsKey(WeekKey)){
                                glucoseList.put(WeekKey, (glucoseList.get(WeekKey)+item.getglucose())/2);
                            }else{
                                glucoseList.put(WeekKey, item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;
                }

                for (int x = (glucoseList.size() - 1); x >= 0; x--){
                    String label = glucoseList.keySet().toArray()[x].toString();

                    bodyRecord eachRecord = new bodyRecord(0, 0, 0,
                            label, "",
                            0, 0, 0, 0,
                            0, 0, 0,
                            0, PERIOD, TYPE_GLUCOSE, glucoseList.get(label),
                            0, 0, 0, 0, "");

                    newfast7daysList.add(eachRecord);
                }
                    //add the categories to list
                    gluCount7days.add(gluHigh7days);
                    gluCount7days.add(gluMidHigh7days);
                    gluCount7days.add(gluMid7days);
                    gluCount7days.add(gluLow7days);

                    //add 7 days records to a Hashmap
                    temp.put("FAST7DAYS", newfast7daysList);
                    temp.put("gluCount7days",gluCount7days);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    public HashMap<String, Object> prepareBloodListDataNew(String result, int viewMode, int timeMode, String typeMode) {
        ////System.out.println("Run in prepareBloodListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            if (viewMode == 1) {
                // Statistic blood data - all
                int gluHighTotal = 0;
                int gluMidHighTotal = 0;
                int gluMidTotal = 0;
                int gluLowTotal = 0;

                List<Integer> gluAllCount = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> all = new ArrayList<bodyRecord>();

                // adding Statistic blood data
                //JSONArray JA = new JSONArray(result);

                com.dmc.myapplication.Models.bodyRecord bodyRecord = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
                List<bodyRecord> JA = bodyRecord.getAllGLUCOSE();

                for (int i = 0; i < JA.size(); i++) {
                    bodyRecord eachRecord;
                    double value = JA.get(i).getglucose();
                    try { //Case : Glucose Before meal
                        if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.get(i).getType_glucose()) {
                            if (value < user.pre_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.pre_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        //Case : Glucose After meal
                        else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.get(i).getType_glucose()) {
                            if (value < user.post_meal_low) {
                                gluLowTotal++;
                            } else if (value >= user.post_meal_high) {
                                gluHighTotal++;
                            } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        //System.out.println("getDate="+JA.get(i).getDate());
                        eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                JA.get(i).getDate(), JA.get(i).getTime(),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, JA.get(i).getPeriod(), JA.get(i).getType_glucose(), JA.get(i).getglucose(),
                                0, 0, 0, 0, "");

                        all.add(eachRecord);
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluAllCount.add(gluHighTotal);
                gluAllCount.add(gluMidHighTotal);
                gluAllCount.add(gluMidTotal);
                gluAllCount.add(gluLowTotal);

                //add 7 days records to a Hashmap
                temp.put("ALL", all);
                temp.put("gluAllCount",gluAllCount);

            } else{
                // Statistic blood data within 7 days
                int gluHigh7days = 0;
                int gluMidHigh7days = 0;
                int gluMid7days = 0;
                int gluLow7days = 0;

                List<Integer> gluCount7days = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> fast7daysList = new ArrayList<bodyRecord>();


                //Calculate dates of 7 days
                //DateTime last7Days = new DateTime().minusDays(8);

                //Calculate dates of 7 days
                DateTime last7Days = null;
                switch (timeMode){
                    case bodyRecordConstant.CHART_DAILY:
                        last7Days = new DateTime().minusDays(1).hourOfDay().setCopy(23).minuteOfHour().setCopy(59).secondOfMinute().setCopy(59);
                        break;

                    case bodyRecordConstant.CHART_MONTHLY:
                        //last7Days = new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0);
                        last7Days = new DateTime().minusWeeks(4*3);
                        System.out.println("Start Date="+last7Days.toString());
                        //System.out.println("Start Date="+new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 1, 0, 0).toString());
                        break;

                    case bodyRecordConstant.CHART_WEEKLY:
                        last7Days = new DateTime().minusDays(7);
                        break;

                    case bodyRecordConstant.CHART_YEARLY:
                        last7Days = new DateTime().minusMonths(6);
                        break;

                }
                DateTime now = new DateTime();

                // adding Statistic blood data
                //JSONArray JA = new JSONArray(result);

                com.dmc.myapplication.Models.bodyRecord bodyRecord = new com.dmc.myapplication.Models.bodyRecord(bodyRecordConstant.getApplicationContext);
                List<bodyRecord> JA = bodyRecord.getAllGLUCOSE();

                for (int i = 0; i < JA.size(); i++) {
                    bodyRecord eachRecord;
                    String recordDate = JA.get(i).getDate();
                    double value = JA.get(i).getglucose();
                    try {
                        if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                            if (typeMode!=bodyRecordConstant.GET_ALL && JA.get(i).getPeriod() == Integer.parseInt(typeMode)){
                                continue;
                            }
                            System.out.println("7daysCount- Date="+JA.get(i).getDate());

                            //Case : Glucose Before meal within 7 days
                            if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.get(i).getType_glucose()) {
                                System.out.println("7daysCount- getType_glucose=GET_GLUCOSE_BEFORE_MEAL("+bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL+")");
                                System.out.println("7daysCount- getType_glucose="+JA.get(i).getType_glucose());
                                System.out.println("7daysCount- value="+value);
                                System.out.println("7daysCount- getglucose="+JA.get(i).getglucose());

                                if (value < user.pre_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.pre_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.pre_meal_low && value < user.pre_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            //Case : Glucose After meal within 7 days
                            else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.get(i).getType_glucose()) {
                                //System.out.println("7daysCount- getType_glucose=GET_GLUCOSE_AFTER_MEAL("+bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL+")");
                                //System.out.println("7daysCount- getType_glucose="+JA.get(i).getType_glucose());
                                //System.out.println("7daysCount- getType_glucose=GET_GLUCOSE_AFTER_MEAL");
                                //System.out.println("7daysCount- value="+value);
                                //System.out.println("7daysCount- getglucose="+JA.get(i).getglucose());

                                if (value < user.post_meal_low) {
                                    gluLow7days++;
                                } else if (value >= user.post_meal_high) {
                                    gluHigh7days++;
                                } else if (value >= user.post_meal_low && value < user.post_meal_med) {
                                    gluMid7days++;
                                } else {
                                    gluMidHigh7days++;
                                }
                            }
                            System.out.println("JA.get(i).getDate()="+JA.get(i).getDate());
                            eachRecord = new bodyRecord(JA.get(i).getRecordId(), JA.get(i).getRecordType(), JA.get(i).getUserId(),
                                    JA.get(i).getDate(), JA.get(i).getTime(),
                                    0, 0, 0, 0,
                                    0, 0, 0,
                                    0, JA.get(i).getPeriod(), JA.get(i).getType_glucose(), JA.get(i).getglucose(),
                                    0, 0, 0, 0, "");

                            // Adding BLOOD_RECORDS data
                            fast7daysList.add(eachRecord);

                        }//end time check

                    }catch (Exception e){
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                List<bodyRecord> newfast7daysList = new ArrayList<>();
                Map<String, Double> glucoseList = new HashMap<String, Double>();
                int PERIOD = 1;
                int TYPE_GLUCOSE = 1;

                switch (timeMode){
                    case bodyRecordConstant.CHART_DAILY:
                        for (bodyRecord item: fast7daysList){
                            DateTimeFormatter fmt = DateTimeFormat.forPattern("HH:mm:ss");
                            DateTime dt = new DateTime();
                            dt = fmt.parseDateTime(item.getTime());
                            String Rkey = DateTimeFormat.forPattern("HH").print(dt)+"時";
                            if (glucoseList.containsKey(Rkey)){

                                glucoseList.put(Rkey, (glucoseList.get(Rkey)+item.getglucose())/2);
                            }else{
                                glucoseList.put(Rkey, item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;

                    case bodyRecordConstant.CHART_WEEKLY:
                        for (bodyRecord item: fast7daysList){
                            if (glucoseList.containsKey(item.getDate())){
                                glucoseList.put(item.getDate(), (glucoseList.get(item.getDate())+item.getglucose())/2);
                            }else{
                                glucoseList.put(item.getDate(), item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;

                    case bodyRecordConstant.CHART_MONTHLY:
                        for (bodyRecord item: fast7daysList){
                            DateTime dt = new DateTime();
                            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                            dt = fmt.parseDateTime(item.getDate());
                            String WeekKey = DateTimeFormat.forPattern("yyyy-MM-dd").print(dt.withDayOfWeek(DateTimeConstants.MONDAY));
                            if (glucoseList.containsKey(WeekKey)){
                                glucoseList.put(WeekKey, (glucoseList.get(WeekKey)+item.getglucose())/2);
                            }else{
                                glucoseList.put(WeekKey, item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;

                    case bodyRecordConstant.CHART_YEARLY:
                        for (bodyRecord item: fast7daysList){
                            DateTime dt = new DateTime();
                            DateTimeFormatter fmt = DateTimeFormat.forPattern("yyyy-MM-dd");
                            dt = fmt.parseDateTime(item.getDate());
                            String WeekKey = DateTimeFormat.forPattern("yyyy-MM").print(dt);
                            if (glucoseList.containsKey(WeekKey)){
                                glucoseList.put(WeekKey, (glucoseList.get(WeekKey)+item.getglucose())/2);
                            }else{
                                glucoseList.put(WeekKey, item.getglucose());
                            }
                            PERIOD = item.period;
                            TYPE_GLUCOSE = item.type_glucose;
                        }
                        break;
                }

                for (int x = (glucoseList.size() - 1); x >= 0; x--){
                    String label = glucoseList.keySet().toArray()[x].toString();

                    bodyRecord eachRecord = new bodyRecord(0, 0, 0,
                            label, "",
                            0, 0, 0, 0,
                            0, 0, 0,
                            0, PERIOD, TYPE_GLUCOSE, glucoseList.get(label),
                            0, 0, 0, 0, "");

                    newfast7daysList.add(eachRecord);
                }
                //add the categories to list
                gluCount7days.add(gluHigh7days);
                gluCount7days.add(gluMidHigh7days);
                gluCount7days.add(gluMid7days);
                gluCount7days.add(gluLow7days);

                System.out.println("gluHigh7days="+gluHigh7days);
                System.out.println("gluMidHigh7days="+gluMidHigh7days);
                System.out.println("gluMid7days="+gluMid7days);
                System.out.println("gluLow7days="+gluLow7days);

                //add 7 days records to a Hashmap
                temp.put("FAST7DAYS", newfast7daysList);
                temp.put("gluCount7days",gluCount7days);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }

    //Janus
    /*public HashMap<String, Object> prepareBloodListData(String result, int viewMode, Integer timerange) {
        ////System.out.println("Run in prepareBloodListData");
        HashMap<String, Object> temp= new HashMap<String, Object>();
        try {

            if (viewMode == 1) {
                // Statistic blood data - all
                int gluHighTotal = 0;
                int gluMidHighTotal = 0;
                int gluMidTotal = 0;
                int gluLowTotal = 0;

                List<Integer> gluAllCount = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> all = new ArrayList<bodyRecord>();




                // adding Statistic blood data
                JSONArray JA = new JSONArray(result);
                for (int i = 0; i < JA.length(); i++) {
                    bodyRecord eachRecord;
                    double value = JA.getJSONObject(i).getDouble("GLUCOSE");
                    try { //Case : Glucose Before meal
                        if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                            if (value < bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_LOW) {
                                gluLowTotal++;
                            } else if (value >= bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_HIGH) {
                                gluHighTotal++;
                            } else if (value >= bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_LOW && value < bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_MIDHIGH) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }
                        //Case : Glucose After meal
                        else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                            if (value < bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_LOW) {
                                gluLowTotal++;
                            } else if (value >= bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_HIGH) {
                                gluHighTotal++;
                            } else if (value >= bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_LOW && value < bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_MIDHIGH) {
                                gluMidTotal++;
                            } else {
                                gluMidHighTotal++;
                            }
                        }

                        eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                0, 0, 0, 0,
                                0, 0, 0,
                                0, JA.getJSONObject(i).getInt("PERIOD"), JA.getJSONObject(i).getInt("TYPE_GLUCOSE"), JA.getJSONObject(i).getDouble("GLUCOSE"),
                                0, 0, 0, 0, "");

                        all.add(eachRecord);
                    } catch (JSONException e) {
                        e.printStackTrace();
                        throw new RuntimeException(e);
                    }

                } //end for loop

                //add the categories to list
                gluAllCount.add(gluHighTotal);
                gluAllCount.add(gluMidHighTotal);
                gluAllCount.add(gluMidTotal);
                gluAllCount.add(gluLowTotal);

                //add 7 days records to a Hashmap
                temp.put("ALL", all);
                temp.put("gluAllCount",gluAllCount);

            } else{
                // Statistic blood data within 7 days
                int gluHigh7days = 0;
                int gluMidHigh7days = 0;
                int gluMid7days = 0;
                int gluLow7days = 0;

                List<Integer> gluCount7days = new ArrayList<Integer>();

                // BLOOD_RECORDS data
                List<bodyRecord> fast7daysList = new ArrayList<bodyRecord>();


                //Calculate dates of 7 days
                DateTime last7Days = null;

                switch (timerange){
                    case bodyRecordConstant.CHART_DAILY:
                        last7Days = new DateTime().minusHours(12);
                        break;

                    case bodyRecordConstant.CHART_MONTHLY:
                        last7Days = new DateTime(new DateTime().getYear(), new DateTime().getMonthOfYear(), 0, 0, 0);
                        break;

                    case bodyRecordConstant.CHART_WEEKLY:
                        last7Days = new DateTime().minusDays(7);
                        break;

                    case bodyRecordConstant.CHART_YEARLY:
                        last7Days = new DateTime().minusMonths(3);
                        break;
                }

                DateTime now = new DateTime();

                // adding Statistic blood data
                JSONArray JA = new JSONArray(result);

                switch (timerange){
                    case bodyRecordConstant.CHART_WEEKLY:
                        for (int i = 0; i < JA.length(); i++) {
                            bodyRecord eachRecord;
                            String recordDate = JA.getJSONObject(i).getString("DATE");
                            double value = JA.getJSONObject(i).getDouble("GLUCOSE");
                            try {
                                if (timeCheck.compareDateD(last7Days, now, recordDate)) {
                                    //Case : Glucose Before meal within 7 days
                                    if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {

                                        if (value < bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_LOW) {
                                            gluLow7days++;
                                        } else if (value >= bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_HIGH) {
                                            gluHigh7days++;
                                        } else if (value >= bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_LOW && value < bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL_MIDHIGH) {
                                            gluMid7days++;
                                        } else {
                                            gluMidHigh7days++;
                                        }
                                    }
                                    //Case : Glucose After meal within 7 days
                                    else if (Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL) == JA.getJSONObject(i).getInt("TYPE_GLUCOSE")) {
                                        if (value < bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_LOW) {
                                            gluLow7days++;
                                        } else if (value >= bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_HIGH) {
                                            gluHigh7days++;
                                        } else if (value >= bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_LOW && value < bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL_MIDHIGH) {
                                            gluMid7days++;
                                        } else {
                                            gluMidHigh7days++;
                                        }
                                    }
                                    eachRecord = new bodyRecord(JA.getJSONObject(i).getInt("ID"), JA.getJSONObject(i).getInt("RECORD_TYPE"), JA.getJSONObject(i).getInt("USER_ID"),
                                            JA.getJSONObject(i).getString("DATE"), JA.getJSONObject(i).getString("TIME"),
                                            0, 0, 0, 0,
                                            0, 0, 0,
                                            0, JA.getJSONObject(i).getInt("PERIOD"), JA.getJSONObject(i).getInt("TYPE_GLUCOSE"), JA.getJSONObject(i).getDouble("GLUCOSE"),
                                            0, 0, 0, 0, "");

                                    // Adding BLOOD_RECORDS data
                                    fast7daysList.add(eachRecord);

                                }//end time check

                            }catch (JSONException e){
                                e.printStackTrace();
                                throw new RuntimeException(e);
                            }

                        } //end for loop

                        List<bodyRecord> newfast7daysList = new ArrayList<>();

                        Map<String, Double> WEEKLY_GLUCOSE = new HashMap<>();

                        for (bodyRecord item: fast7daysList) {
                            if (WEEKLY_GLUCOSE.containsKey(item.getDate())){
                                WEEKLY_GLUCOSE.put(item.getDate(), (WEEKLY_GLUCOSE.get(item.getDate()) + item.getglucose()) /2);
                            }else{
                                WEEKLY_GLUCOSE.put(item.getDate(), item.getglucose());
                            }
                        }

                        for (int x = (WEEKLY_GLUCOSE.size() - 1); x >= 0; x--){
                            String label = WEEKLY_GLUCOSE.keySet().toArray()[x].toString();
                            bodyRecord weeklyRecord = new bodyRecord(0, 0, 0,
                                    label, "",
                                    0, 0, 0, 0,
                                    0, 0, 0,
                                    0, 0, 0, WEEKLY_GLUCOSE.get(label),
                                    0, 0, 0, 0, "");
                            newfast7daysList.add(weeklyRecord);
                        }

                        fast7daysList = newfast7daysList;
                        newfast7daysList = null;

                        break;
                }


                //add the categories to list
                gluCount7days.add(gluHigh7days);
                gluCount7days.add(gluMidHigh7days);
                gluCount7days.add(gluMid7days);
                gluCount7days.add(gluLow7days);

                //add 7 days records to a Hashmap
                temp.put("FAST7DAYS", fast7daysList);
                temp.put("gluCount7days",gluCount7days);

            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return temp;
    }*/


    private void setupPieChart(int mode, String timeDes){
        System.out.println("setupPieChart - start");
        if(mpPieChart.getParent()!=null)
            ((ViewGroup)mpPieChart.getParent()).removeView(mpPieChart);
        testChart.addView(mpPieChart);
        testChart.setBackgroundColor(Color.WHITE);

        mpPieChart.invalidate();
        mpPieChart.setDescription(null);
        mpPieChart.setNoDataText("沒有任何記錄");
        Paint p = mpPieChart.getPaint(Chart.PAINT_INFO);
        p.setTextSize(40);
        p.setColor(Color.BLACK);

        mpPieChart.setUsePercentValues(true);
        mpPieChart.setDescriptionTextSize(24);

        mpPieChart.setDrawHoleEnabled(false);
        mpPieChart.setRotationEnabled(false);
        mpPieChart.setRotationAngle(0);

        mpPieChart.getLegend().setWordWrapEnabled(true);
        if (mode == 1){
            mpPieChart.setDescription("過去"+timeDes+"血糖");
            addDataGluPieChart();
        }
        if (mode == 4){
           mpPieChart.setDescription("過去"+timeDes+"血壓");
            addDataBpPieChart();
        }

        Legend l = mpPieChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        l.setTextSize(18);
        l.setXEntrySpace(7);
        l.setYEntrySpace(5);
        mpPieChart.invalidate();
        mpPieChart.animateXY(1400, 1400);

    }

    private void setupPieChart(int mode){

        if(mpPieChart.getParent()!=null)
            ((ViewGroup)mpPieChart.getParent()).removeView(mpPieChart);
        testChart.addView(mpPieChart);
        testChart.setBackgroundColor(Color.WHITE);

        mpPieChart.invalidate();
        mpPieChart.setDescription(null);
        mpPieChart.setNoDataText("沒有任何記錄");
        Paint p = mpPieChart.getPaint(Chart.PAINT_INFO);
        p.setTextSize(40);
        p.setColor(Color.BLACK);

        mpPieChart.setUsePercentValues(true);
        mpPieChart.setDescriptionTextSize(24);

        mpPieChart.setDrawHoleEnabled(false);
        mpPieChart.setRotationEnabled(false);
        mpPieChart.setRotationAngle(0);

        mpPieChart.getLegend().setWordWrapEnabled(true);

        if (mode == 1){
            mpPieChart.setDescription("過去7天血糖");
            addDataGluPieChart();
        }
        if (mode == 4){
            mpPieChart.setDescription("過去7天血壓");
            addDataBpPieChart();
        }

        Legend l = mpPieChart.getLegend();
        l.setPosition(Legend.LegendPosition.RIGHT_OF_CHART_CENTER);
        l.setTextSize(18);
        l.setXEntrySpace(7);
        l.setYEntrySpace(5);
        mpPieChart.invalidate();
        mpPieChart.animateXY(1400, 1400);

    }

    private void addDataGluPieChart(){

        ArrayList<Integer> colors = new ArrayList<>();
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();
        for ( int i= 0; i < yData.length; i++)
        { if (yData[i]!=0){
            System.out.println("yData[i]="+yData[i]);
            yVals1.add(new Entry(yData[i], i));
        }
        }

        ArrayList<String> xVals1 = new ArrayList<String>();
        for ( int i= 0; i < xData.length; i++)
            if (yData[i]!=0)
                xVals1.add(xData[i]);

        PieDataSet dataSet = new PieDataSet(yVals1, "血糖指標分佈");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);

        if (yData[0]!=0)
             colors.add(Color.rgb(255, 87, 34));  //HIGH
        if (yData[1]!=0)
             colors.add(Color.rgb(247, 198, 0));  //HI-MID
        if (yData[2]!=0)
            colors.add(Color.rgb(76, 175, 80));  //MID
        if (yData[3]!=0)
            colors.add(Color.rgb(103, 58, 183)); //LOW

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);

        PieData data = new PieData(xVals1, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16);
        data.setValueTextColor(Color.WHITE);

        mpPieChart.setData(data);
        mpPieChart.highlightValues(null);

    }

    private void addDataBpPieChart(){

        ArrayList<Integer> colors = new ArrayList<>();
        ArrayList<Entry> yVals1 = new ArrayList<Entry>();

        for ( int i= 0; i < yData.length; i++)
        { if (yData[i]!=0)
            yVals1.add(new Entry(yData[i], i));
        }

        ArrayList<String> xVals1 = new ArrayList<String>();
        for ( int i= 0; i < xBPData.length; i++)
            if (yData[i]!=0)
                xVals1.add(xBPData[i]);

        PieDataSet dataSet = new PieDataSet(yVals1, "血壓指標分佈");
        dataSet.setSliceSpace(3);
        dataSet.setSelectionShift(5);

        if (yData[0]!=0)
            colors.add(Color.rgb(255, 87, 34));  //HIGH
        if (yData[1]!=0)
            colors.add(Color.rgb(247, 198, 0));  //HI-MID
        if (yData[2]!=0)
            colors.add(Color.rgb(76, 175, 80));  //MID
        if (yData[3]!=0)
            colors.add(Color.rgb(103, 58, 183)); //LOW

        colors.add(ColorTemplate.getHoloBlue());

        dataSet.setColors(colors);


        PieData data = new PieData(xVals1, dataSet);
        data.setValueFormatter(new PercentFormatter());
        data.setValueTextSize(16);
        data.setValueTextColor(Color.WHITE);

        mpPieChart.setData(data);
        mpPieChart.highlightValues(null);
    }


    //Line Chart at lower part

    private void setupLineChart(int mode){
        //System.out.println("[setupLineChart - start ");
        if( lineChart.getParent()!=null)
            ((ViewGroup) lineChart.getParent()).removeView(lineChart);
        testChart2.addView(lineChart);

        //customize Line Chart
        lineChart.setDescription("");
        lineChart.setNoDataTextDescription("暫時沒有資料");

        //enable data Highlighting
        lineChart.setHighlightPerTapEnabled(true);

        //disable touch gestures
        //lineChart.setDrawGridBackground(false);
        lineChart.setTouchEnabled(false);
       // lineChart.setBackgroundColor(Color.LTGRAY);

        if (mode == 1){
            addDataGlu();
        }

        if (mode == 4){
            addDataBp();

        }
        //System.out.println("[setupLineChart - end] ");

    }

    //Glucose Dataset for line chart
    private void addDataGlu() {
        //System.out.println("[addDataGlu - start ");
        Stack<bodyRecord> temp = new Stack <bodyRecord>();
        String dataset_label1 = "血糖指數 mmol/L";
        String dataset_label2 = "打針";

        ArrayList<Integer> colors = new ArrayList<Integer>();
        ArrayList<Entry> entries = new ArrayList<>();       //y-axis glucose
        ArrayList<String> labels = new ArrayList<String>(); //x-axis - date
        ArrayList<Entry> injectRecord = new ArrayList<>();  //y-axis injection
        int i=0;

        for (bodyRecord record : glu7days) {
            //System.out.println("fast7days:Date: " + record.getDate() + " Value: " + record.getglucose());
            temp.push(record);
        }

        System.out.println("LineTotalRecords="+temp.size());
        while(!temp.empty()) {
            bodyRecord record = temp.pop();
            //System.out.println("FILO: fast7days:Date: " + record.getDate() + " Value: " + record.getglucose());
            entries.add(new Entry((float) record.getglucose(), i));
            labels.add(record.getDate());
            if(user.injection.equals("Y")){
                if (inject7days == null || inject7days.isEmpty()) {
                    //System.out.println( "No injection");
                }else{

                        if (inject7days.containsKey(record.getDate())) {
                            String date = record.getDate();
                            //System.out.println(date + " has injection");
                            injectRecord.add(new Entry((int) inject7days.get(date), i));
                        }

                }
            }

            //add color to each point according its glucose level
            if(record.getType_glucose() == 1) {
                if (record.getglucose() < user.pre_meal_low)
                    colors.add(Color.rgb(103, 58, 183)); //LOW
                else if (record.getglucose() >= user.pre_meal_high)
                    colors.add(Color.rgb(255, 87, 34));  //HIGH
                else if (record.getglucose() >= user.pre_meal_low && record.getglucose() < user.pre_meal_med)
                    colors.add(Color.rgb(76, 175, 80));  //MID
                else
                    colors.add(Color.rgb(247, 198, 0));  //MIDHIGH
            }
            if(record.getType_glucose() == 2) {
                if (record.getglucose() < user.post_meal_low)
                    colors.add(Color.rgb(103, 58, 183)); //LOW
                else if (record.getglucose() >= user.post_meal_high)
                    colors.add(Color.rgb(255, 87, 34));  //HIGH
                else if (record.getglucose() >= user.post_meal_low && record.getglucose() < user.post_meal_med)
                    colors.add(Color.rgb(76, 175, 80));  //MID
                else
                    colors.add(Color.rgb(247, 198, 0));  //MIDHIGH
            }
            i++;
        }

        LineDataSet set1 = new LineDataSet(entries, dataset_label1);
        set1.setCircleColors(colors);
        set1.setColor(Color.BLACK);
        set1.setDrawCubic(false);
        set1.setCubicIntensity(0.5f);
        set1.setAxisDependency(YAxis.AxisDependency.LEFT);
        set1.setLineWidth(2f);
        set1.setCircleSize(8f);
        set1.setFillAlpha(65);
        set1.setFillColor(ColorTemplate.getHoloBlue());
        set1.setValueTextColor(Color.DKGRAY);
        set1.setValueTextSize(18f);

        List<LineDataSet> setList = new ArrayList<>();
        setList.add(set1);

        if (user.injection.equals("Y")) {
            LineDataSet set2 = new LineDataSet(injectRecord, dataset_label2);
            set2.setCircleColor(Color.MAGENTA);
            set2.setDrawCubic(false);
            set2.setAxisDependency(YAxis.AxisDependency.RIGHT);
            set2.setLineWidth(0f);
            set2.enableDashedLine(0f,100f,0f);
            set2.setColor(Color.MAGENTA);
            set2.setCircleSize(6f);
            set2.setFillAlpha(65);
            set2.setFillColor(Color.MAGENTA);
            set2.setValueTextSize(0f);
            setList.add(set2);
        }

        LineData data = new LineData(labels, setList);
        data.setValueFormatter(new MyValueFormatter());
        lineChart.setData(data);
        YAxis leftAxis  = lineChart.getAxisLeft();
        leftAxis.setSpaceTop(15);
        leftAxis.setDrawAxisLine(false);
        leftAxis.setDrawGridLines(false);
        lineChart.getAxisRight().setDrawLabels(false);

    }




    //Blood pressure Datasets
    private void addDataBp() {
        Stack<bodyRecord> temp = new Stack <bodyRecord>();
        String dataset_label1 = "收縮壓(上壓)";
        String dataset_label2 = "舒張壓(下壓)";
        String dataset_label3 = "心跳";
        ArrayList<String> xVals = new ArrayList<>();
        ArrayList<Entry> yVals1 = new ArrayList<>();
        ArrayList<Entry> yVals2 = new ArrayList<>();
        ArrayList<Entry> yVals3 = new ArrayList<>();
        ArrayList<Integer> colors1 = new ArrayList<Integer>();
        ArrayList<Integer> colors2 = new ArrayList<Integer>();
        int i=0;

        for (bodyRecord record : bp7days) {
            //System.out.println("BP 7days:Date: " + record.getDate() + " Value: " + record.getBp_h() +" "+ record.getBp_l());
            temp.push(record);
        }
        while(!temp.empty()) {
            bodyRecord record = temp.pop();
            //System.out.println("FILO: BP 7days:Date: " + record.getDate() + " Value: " + record.getBp_h() + " " + record.getBp_l());
            System.out.println("X="+record.getDate());
            System.out.println("y= (BP_H="+record.getBp_h()+", BP_l="+record.getBp_l()+", HR="+record.getHeart_rate()+")");

            xVals.add(record.getDate());
            yVals1.add(new Entry(record.getBp_h(), i));
            yVals2.add(new Entry(record.getBp_l(), i));
            yVals3.add(new Entry(record.getHeart_rate(), i));


            //add color to each point according its level
            if ( record.getBp_h() < bodyRecordConstant.GET_BP_SYS_LOW || record.getBp_l() < bodyRecordConstant.GET_BP_DIA_LOW ){
                colors1.add(Color.rgb(103, 58, 183)); //LOW
                colors2.add(Color.rgb(103, 58, 183));
            }
            else if ( (record.getBp_h() >= bodyRecordConstant.GET_BP_SYS_LOW && record.getBp_h() < bodyRecordConstant.GET_BP_SYS_MIDHIGH)
                    &&(record.getBp_l() >= bodyRecordConstant.GET_BP_DIA_LOW && record.getBp_l() < bodyRecordConstant.GET_BP_DIA_MIDHIGH))
                    {
                        colors1.add(Color.rgb(76, 175, 80));  //MID
                        colors2.add(Color.rgb(76, 175, 80));
                    }
            else if ( (record.getBp_h() > bodyRecordConstant.GET_BP_SYS_HIGH )
                    ||(record.getBp_l() > bodyRecordConstant.GET_BP_DIA_HIGH ))
                    {
                    colors2.add(Color.rgb(255, 87, 34));  //HIGH
                    colors1.add(Color.rgb(255, 87, 34));  //HIGH
                    }
            else if ( (record.getBp_h() >= bodyRecordConstant.GET_BP_SYS_MIDHIGH && record.getBp_h() <= bodyRecordConstant.GET_BP_SYS_HIGH)
                    ||(record.getBp_l() >= bodyRecordConstant.GET_BP_DIA_MIDHIGH && record.getBp_l() <= bodyRecordConstant.GET_BP_DIA_HIGH))
                    {
                        colors1.add(Color.rgb(247, 198, 0));  //MIDHIGH
                        colors2.add(Color.rgb(247, 198, 0));
                    }
                else{
                    colors2.add(Color.rgb(255, 87, 34));  //HIGH
                    colors1.add(Color.rgb(255, 87, 34));  //HIGH
                    }

            i++;
        }
        LineDataSet dataSet1 = new LineDataSet(yVals1, dataset_label1);
        LineDataSet dataSet2 = new LineDataSet(yVals2, dataset_label2);
        LineDataSet dataSet3 = new LineDataSet(yVals3, dataset_label3);

        //upper bp
        dataSet1.setCircleColors(colors1);
        dataSet1.setColor(Color.parseColor("#1098B2"));
        dataSet1.setDrawCubic(false);
        dataSet1.setAxisDependency(YAxis.AxisDependency.LEFT);
        dataSet1.setLineWidth(4);
        dataSet1.setCircleSize(8);
        dataSet1.setFillAlpha(65);
        dataSet1.setValueTextColor(Color.DKGRAY);
        dataSet1.setValueTextSize(0);


        //lower bp
        dataSet2.setCircleColors(colors2);
        dataSet2.setColor(Color.parseColor("#B25209"));
        dataSet2.setLineWidth(4);
        dataSet2.setCircleSize(8);
        dataSet2.setFillAlpha(65);
        dataSet2.setValueTextColor(Color.DKGRAY);
        dataSet2.setValueTextSize(0);

        //heart rate
        dataSet3.setColor(Color.parseColor("#FF4B96"));
        dataSet3.setLineWidth(5);
        dataSet3.setCircleSize(2);
        dataSet3.setFillAlpha(50);
        dataSet3.setValueTextColor(Color.BLUE);
        dataSet3.setValueTextSize(0);


        List<LineDataSet> dataSetList = new ArrayList<>();
        dataSetList.add(dataSet1);
        dataSetList.add(dataSet2);
        dataSetList.add(dataSet3);

        LineData data = new LineData(xVals, dataSetList);
        //data.setValueTextColor(Color.DKGRAY);
        //data.setHighlightEnabled(true);

        //add data to line chart
        lineChart.setData(data);
        YAxis leftAxis  = lineChart.getAxisLeft();
        leftAxis.setSpaceTop(15);
        lineChart.getAxisRight().setDrawLabels(false);


    }


// override the number format shown in line chart (blood glucose chart)
    public class MyValueFormatter implements ValueFormatter {

        private DecimalFormat mFormat;

        public MyValueFormatter() {
            mFormat = new DecimalFormat("###,###,##0.0"); // use one decimal
        }

        @Override
        public String getFormattedValue(float value, Entry entry, int dataSetIndex, ViewPortHandler viewPortHandler) {

            return mFormat.format(value);
        }
    }



/* Class BodyRecord - save record attributes in a object */
    public class bodyRecord {
        public int recordId;
        private int recordType;
        private int userId;
        private String date;
        private String time;
        private double height;
        private double weight;
        private double waist;
        private double bmi;
        private int bp_h;
        private int bp_l;
        private int heart_rate;
        private double hba1c;
        private int period;
        private int type_glucose;
        private double glucose;
        private double total_c;
        private double ldl_c;
        private double hdl_c;
        private double triglyceriders;
        private String remarks;
        public String create_datetime = null;
       public String edit_datetime = null;

        public bodyRecord(int recordId, int recordType, int userId,
                          String date, String time,
                          double height, double weight, double waist, double bmi,
                          int bp_h, int bp_l, int heart_rate,
                          double hba1c,  int period, int type_glucose, double glucose,
                          double total_c, double ldl_c, double hdl_c, double triglyceriders, String remarks) {

            this.recordId = recordId;
            this.recordType = recordType;
            this.userId = userId;
            this.date = date;
            this.time = time;
            this.height = height;
            this.weight = weight;
            this.waist = waist;
            this.bmi = bmi;
            this.bp_h = bp_h;
            this.bp_l = bp_l;
            this.heart_rate = heart_rate;
            this.hba1c = hba1c;
            this.period = period;
            this.type_glucose = type_glucose;
            this.glucose = glucose;
            this.total_c = total_c;
            this.ldl_c = ldl_c;
            this.hdl_c = hdl_c;
            this.triglyceriders = triglyceriders;
            this.remarks = remarks;

        }

        public int getRecordId() { return recordId; }
        public int getRecordType() { return recordType; }
        public int getUserId() { return userId; }
        public String getDate() {  return date;  }
        public String getTime() {
            return time;
        }
        public double getWeight() {
            return weight;
        }
        public double getHeight() {
            return height;
        }
        public double getWaist() {
            return waist;
        }
        public double getBmi() {
            return bmi;
        }
        public int getBp_h() { return bp_h; }
        public int getBp_l() { return bp_l; }
        public int getHeart_rate() { return heart_rate; }

        public double getHba1c() {
            return hba1c;
        }
        public int getPeriod() {
        return period;
    }
        public int getType_glucose() {
            return type_glucose;
        }
        public double getglucose() {
            return glucose;
        }
        public double getTotal_c() {
        return total_c;
    }
        public double getLdl_c() {
            return ldl_c;
        }
        public double getHdl_c() {
            return hdl_c;
        }
        public double getTriglyceriders() {
            return triglyceriders;
        }
        public String getRemarks() {
            return remarks;
        }

        public HashMap<String, String> toHashMap(){
            HashMap<String, String> result = new HashMap<>();
            result.put("recordId", String.valueOf(this.recordId));
            result.put("recordType", String.valueOf(this.recordType));
            result.put("userId", String.valueOf(this.userId));
            result.put("date", String.valueOf(this.date));
            result.put("time", String.valueOf(this.time));
            result.put("height", String.valueOf(this.height));
            result.put("weight", String.valueOf(this.weight));
            result.put("waist", String.valueOf(this.waist));
            result.put("bmi", String.valueOf(this.bmi));
            result.put("bp_h", String.valueOf(this.bp_h));
            result.put("bp_l", String.valueOf(this.bp_l));
            result.put("heart_rate", String.valueOf(this.heart_rate));
            result.put("hba1c", String.valueOf(this.hba1c));
            result.put("period", String.valueOf(this.period));
            result.put("type_glucose", String.valueOf(this.type_glucose));
            result.put("glucose", String.valueOf(this.glucose));
            result.put("total_c", String.valueOf(this.total_c));
            result.put("ldl_c", String.valueOf(this.ldl_c));
            result.put("hdl_c", String.valueOf(this.hdl_c));
            result.put("triglyceriders", String.valueOf(this.triglyceriders));
            result.put("remarks", String.valueOf(this.remarks));
            result.put("create_datetime", String.valueOf(this.create_datetime));
            result.put("edit_datetime", String.valueOf(this.edit_datetime));
            return result;
        }

    }


    public class periodType {
        String value;
        String id;

        public periodType(String value, String id) {
            this.value = value;
            this.id = id;
        }
        public String getId() {
            return id;
        }

        public String toString() {
            return this.value;
        }
    }



    public List<periodType> getPeriodTypeList(){
        List<periodType> periodTypeList = new ArrayList<periodType>();
        periodTypeList.add(new periodType("早餐",Integer.toString(bodyRecordConstant.GET_GLUCOSE_BREAKFAST_CODE)));
        periodTypeList.add(new periodType("午餐",Integer.toString(bodyRecordConstant.GET_GLUCOSE_LUNCH_CODE)));
        periodTypeList.add(new periodType("晚餐",Integer.toString(bodyRecordConstant.GET_GLUCOSE_DINNER_CODE)));
        return periodTypeList;
    }

    public class gluType {
        String value;
        String id;

        public gluType(String value, String id) {
            this.value = value;
            this.id = id;
        }
        public String getId() {
            return id;
        }

        public String toString() {
            return this.value;
        }
    }


    public List<gluType> getGluTypeList(){
        List<gluType> gluTypeList = new ArrayList<gluType>();
        gluTypeList.add(new gluType("餐前",bodyRecordConstant.GET_GLUCOSE_BEFORE_MEAL));
        gluTypeList.add(new gluType("餐後",bodyRecordConstant.GET_GLUCOSE_AFTER_MEAL));

        return gluTypeList;
    }


    private void initDataToBmibar() {


        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float lowSpan = 194;
        float normalSpan = 250;
        float overSpan = 111;
        float midHighSpan = 277;
        float highSpan = totalSpan -  lowSpan - normalSpan - overSpan - midHighSpan  ;

        // low level span
        mProgressItem.progressItemPercentage = ((lowSpan / totalSpan) * 100);
        mProgressItem.color = R.color.valueLow;
        progressItemList.add(mProgressItem);

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        // overweight span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (overSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueOver;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (midHighSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueMidhigh;
        progressItemList.add(mProgressItem);

        // high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        bmibar.initData(progressItemList);
        bmibar.invalidate();
    }

    private void initDataToWaistBar() {
        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float normalSpan = 400;
        // check user sex to change standard
        if (user.sex == 1) {
              normalSpan = 500;
        }
        float highSpan = totalSpan - normalSpan;

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        waistbar.initData(progressItemList);
        waistbar.invalidate();
    }


    private void initDataToHba1cBar() {

        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float lowSpan = 14;
        float normalSpan = 281;
        float midHighSpan = 241;
        float highSpan = totalSpan -  lowSpan - normalSpan  - midHighSpan  ;

        // low level span
        mProgressItem.progressItemPercentage = ((lowSpan / totalSpan) * 100);
        mProgressItem.color = R.color.valueLow;
        progressItemList.add(mProgressItem);

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (midHighSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueMidhigh;
        progressItemList.add(mProgressItem);

        // high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        hba1cbar.initData(progressItemList);
        hba1cbar.invalidate();
    }


    private void initDataToTotalCBar() {

        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float lowSpan = 60;
        float normalSpan = 347;
        float midHighSpan = 67;
        float highSpan = totalSpan -  lowSpan - normalSpan  - midHighSpan  ;

        // low level span
        mProgressItem.progressItemPercentage = ((lowSpan / totalSpan) * 100);
        mProgressItem.color = R.color.valueLow;
        progressItemList.add(mProgressItem);

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (midHighSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueMidhigh;
        progressItemList.add(mProgressItem);

        // high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        totalcbar.initData(progressItemList);
        totalcbar.invalidate();
    }


    private void initDataToHdlCBar() {
        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float normalSpan = 650;
        // check user sex to change standard
        if (user.sex == 1) {
            normalSpan = 500;
        }
        float highSpan = totalSpan - normalSpan;

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        hdlcbar.initData(progressItemList);
        hdlcbar.invalidate();
    }



    private void initDataToLdlCBar() {

        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float lowSpan = 16;
        float normalSpan = 416;
        float midHighSpan = 150;
        float highSpan = totalSpan - lowSpan - normalSpan  - midHighSpan  ;

        // low level span
        mProgressItem.progressItemPercentage = ((lowSpan / totalSpan) * 100);
        mProgressItem.color = R.color.valueLow;
        progressItemList.add(mProgressItem);

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (midHighSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueMidhigh;
        progressItemList.add(mProgressItem);

        // high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        ldlcbar.initData(progressItemList);
        ldlcbar.invalidate();
    }


    private void initDataToTriBar() {

        ArrayList<ProgressItem> progressItemList;
        ProgressItem mProgressItem;

        progressItemList = new ArrayList<ProgressItem>();
        mProgressItem = new ProgressItem();

        float totalSpan = 1000;
        float lowSpan = 16;
        float normalSpan = 416;
        float midHighSpan = 150;
        float highSpan = totalSpan - lowSpan - normalSpan  - midHighSpan  ;

        // low level span
        mProgressItem.progressItemPercentage = ((lowSpan / totalSpan) * 100);
        mProgressItem.color = R.color.valueLow;
        progressItemList.add(mProgressItem);

        //normal span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (normalSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueNormal;
        progressItemList.add(mProgressItem);

        // mid high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (midHighSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueMidhigh;
        progressItemList.add(mProgressItem);

        // high span
        mProgressItem = new ProgressItem();
        mProgressItem.progressItemPercentage = (highSpan / totalSpan) * 100;
        mProgressItem.color = R.color.valueHigh;
        progressItemList.add(mProgressItem);

        tribar.initData(progressItemList);
        tribar.invalidate();
    }


    public void bpTimeSwitch(String result, int timeMode, Activity activity, String recordType){

        System.out.println("Mode="+timeMode);
        System.out.println("Result="+result);
        //HashMap<String, Object> hashmap = prepareBpListData(result, timeMode);
        HashMap<String, Object> hashmap = prepareBpListDataNew(timeMode);

        List<Integer> list7daysCount =(List<Integer>) hashmap.get("bp7daysCount");
        //System.out.println("List: list7daysCount " + list7daysCount);


        //TODO control what data to show - for now is show 7 days  for graph
        yData = new int[list7daysCount.size()];
        for(int i = 0; i < list7daysCount.size(); i++) yData[i] = list7daysCount.get(i);

        testChart = (LinearLayout) activity.findViewById(R.id.testChart);
        mpPieChart = (PieChart) activity.findViewById(R.id.chart1);
        String timeDes = "7天";

        if(timeMode == bodyRecordConstant.CHART_DAILY) {
            timeDes = "每個小時";
        }else if (timeMode == bodyRecordConstant.CHART_MONTHLY){
                timeDes = "12個星期";
            }else if(timeMode == bodyRecordConstant.CHART_YEARLY){
                timeDes = "6個月";
            }
        setupPieChart(Integer.parseInt(recordType), timeDes);



        HashMap<String, List<bodyRecord>> listAllBloodRecord = (HashMap<String, List<bodyRecord>>) hashmap.get("BP_RECORDS");
        bp7days = listAllBloodRecord.get("LAST7DAYS");

        testChart2 = (LinearLayout) activity.findViewById(R.id.testLineChart);
        lineChart = (LineChart) activity.findViewById(R.id.chart2);
        setupLineChart(Integer.parseInt(recordType));
    }

    public void bloodTimeSwitch(String result, int mode, int timeMode, String typeMode, Activity activity, String recordType){

        System.out.println("bloodTimeSwitch-Mode="+timeMode);
        System.out.println("Result="+result);

        HashMap<String, Object> hashmap = prepareBloodListDataNew(result, mode, timeMode, typeMode);
        List<Integer> listFast7daysCount =(List<Integer>) hashmap.get("gluCount7days");
        yData = new int[listFast7daysCount.size()];
        for(int i = 0; i < listFast7daysCount.size(); i++) yData[i] = listFast7daysCount.get(i);
        testChart = (LinearLayout) activity.findViewById(R.id.testChart);
        mpPieChart = (PieChart) activity.findViewById(R.id.chart1);


        String timeDes = "7天";

        if(timeMode == bodyRecordConstant.CHART_DAILY) {
            timeDes = "每個小時";
        }else if (timeMode == bodyRecordConstant.CHART_MONTHLY){
            timeDes = "12個星期";
        }else if(timeMode == bodyRecordConstant.CHART_YEARLY){
            timeDes = "6個月";
        }

        testChart = (LinearLayout) activity.findViewById(R.id.testChart);
        mpPieChart = (PieChart) activity.findViewById(R.id.chart1);
        setupPieChart(1, timeDes);


        //Prepare lower part graph (line Chart)
        glu7days =(List<bodyRecord>) hashmap.get("FAST7DAYS");
        System.out.println("List: glu7days " + glu7days);
        System.out.println("Size of glu7days " + glu7days.size());

        if(user.injection.equals("Y")){

            inject7days =(Map<String, Integer>) injectMap.get("LAST7DAYS");
            System.out.println("List: inject7days " + inject7days);
            System.out.println("Size of inject7days" + inject7days.size());


        }


        testChart2 = (LinearLayout) activity.findViewById(R.id.testLineChart);
        lineChart = (LineChart) activity.findViewById(R.id.chart2);
        System.out.println("Go!");
        setupLineChart(Integer.parseInt(recordType));
        //lineChart.notifyDataSetChanged();
    }


    /////

    public void bloodTimeSwitch(String result, int mode, Activity activity, String recordType){

        System.out.println("bloodTimeSwitch-Mode="+timeMode);
        System.out.println("Result="+result);

        HashMap<String, Object> hashmap = prepareBloodListDataNew(result, mode);
        List<Integer> listFast7daysCount =(List<Integer>) hashmap.get("gluCount7days");
        yData = new int[listFast7daysCount.size()];
        for(int i = 0; i < listFast7daysCount.size(); i++) yData[i] = listFast7daysCount.get(i);
        testChart = (LinearLayout) activity.findViewById(R.id.testChart);
        mpPieChart = (PieChart) activity.findViewById(R.id.chart1);


        String timeDes = "7天";

        testChart = (LinearLayout) activity.findViewById(R.id.testChart);
        mpPieChart = (PieChart) activity.findViewById(R.id.chart1);
        setupPieChart(Integer.parseInt(recordType), timeDes);


        //Prepare lower part graph (line Chart)
        glu7days =(List<bodyRecord>) hashmap.get("FAST7DAYS");
        //System.out.println("List: glu7days " + glu7days);
        //System.out.println("Size of glu7days " + glu7days.size());

        if(user.injection.equals("Y")){

            inject7days =(Map<String, Integer>) injectMap.get("LAST7DAYS");
            //System.out.println("List: inject7days " + inject7days);
            //System.out.println("Size of inject7days" + inject7days.size());


        }

        testChart2 = (LinearLayout) activity.findViewById(R.id.testLineChart);
        lineChart = (LineChart) activity.findViewById(R.id.chart2);
        setupLineChart(Integer.parseInt(recordType));
        //lineChart.notifyDataSetChanged();

    }


}
