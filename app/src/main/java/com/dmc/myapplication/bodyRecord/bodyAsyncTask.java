package com.dmc.myapplication.bodyRecord;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Switch;

import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by Po on 11/2/2016.
 */
public class bodyAsyncTask extends AsyncTask<String,Void,String> {
    private ProgressDialog progressBar;
    private String model;
    private Activity activity;
    private String recordType;
    private int viewMode;

    Context ctx;

    public bodyAsyncTask(Activity activity, String model, String recordType, int viewMode, Context ctx) {
        this.activity = activity;
        this.model = model;
        this.recordType = recordType;
        this.viewMode = viewMode;
        this.ctx = ctx;

    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        if (progressBar == null) {
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
        }
    }


    @Override
    protected String doInBackground(String... arg0) {
        //System.out.println("[Run at doInBackground] recordType = "+ recordType+" Viewmode= "+viewMode);
        String result= null;
        String data="";
        try {
              if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                  switch (recordType) {
                      case "1": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          String showType = (String) arg0[2];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          data += "&" + URLEncoder.encode("showType", "UTF-8") + "=" + URLEncoder.encode(showType, "UTF-8");
                          //System.out.println(data);

                      }
                      break;
                      case "2": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          //System.out.println(data);
                      }
                      break;
                      case "3": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          //System.out.println(data);
                      }
                      break;
                      case "4": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          //System.out.println(data);
                      }
                      break;
                      case "5": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          //System.out.println(data);
                      }
                      break;
                      case "6": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          //System.out.println(data);
                      }
                      break;
                      case "7": {
                          String user_id = (String) arg0[0];
                          String recordType = (String) arg0[1];
                          data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                          data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                          //System.out.println(data);
                      }
                      break;
                  }
              }

            if(model.equals(bodyRecordConstant.SAVE_BODY_RECORD)){

                //TODO handle different record type mode with different string
                switch (recordType){
                    case "1": { //for glucose add record
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String period = (String)arg0[4];
                        String typeGlucose = (String)arg0[5];
                        String glucose = (String)arg0[6];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("period", "UTF-8") + "=" + URLEncoder.encode(period, "UTF-8");
                        data += "&" + URLEncoder.encode("typeGlucose", "UTF-8") + "=" + URLEncoder.encode(typeGlucose, "UTF-8");
                        data += "&" + URLEncoder.encode("glucose", "UTF-8") + "=" + URLEncoder.encode(glucose, "UTF-8");
                        //System.out.println(data);
                    } break;

                    case "2": { //for add height,weight,waist,bmi record, record_type = 2
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String height = (String)arg0[4];
                        String weight = (String)arg0[5];
                        String bmi = (String)arg0[6];
                        String waist = (String)arg0[7];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("height", "UTF-8") + "=" + URLEncoder.encode(height, "UTF-8");
                        data += "&" + URLEncoder.encode("weight", "UTF-8") + "=" + URLEncoder.encode(weight, "UTF-8");
                        data += "&" + URLEncoder.encode("bmi", "UTF-8") + "=" + URLEncoder.encode(bmi, "UTF-8");
                        data += "&" + URLEncoder.encode("waist", "UTF-8") + "=" + URLEncoder.encode(waist, "UTF-8");
                        //System.out.println(data);
                    } break;


                    case "3": { //for add HbA1c record, record_type = 3
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String HbA1c = (String)arg0[4];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("HbA1c", "UTF-8") + "=" + URLEncoder.encode(HbA1c, "UTF-8");
                        //System.out.println(data);
                    } break;

                    case "4": { //for bp & heart rate add record, record_type = 4
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String bpUpper = (String)arg0[4];
                        String bpLower = (String)arg0[5];
                        String heartRate = (String)arg0[6];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("bpUpper", "UTF-8") + "=" + URLEncoder.encode(bpUpper, "UTF-8");
                        data += "&" + URLEncoder.encode("bpLower", "UTF-8") + "=" + URLEncoder.encode(bpLower, "UTF-8");
                        data += "&" + URLEncoder.encode("heartRate", "UTF-8") + "=" + URLEncoder.encode(heartRate, "UTF-8");

                        //System.out.println(data);
                    } break;

                    case "5": { //for add total cholesterol, LDL-C, HDL-C, TRIGLYCERIDERS record, record_type = 5
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String total_c = (String)arg0[4];
                        String ldlC = (String)arg0[5];
                        String hdlC = (String)arg0[6];
                        String triglycerides = (String)arg0[7];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("total_c", "UTF-8") + "=" + URLEncoder.encode(total_c, "UTF-8");
                        data += "&" + URLEncoder.encode("ldlC", "UTF-8") + "=" + URLEncoder.encode(ldlC, "UTF-8");
                        data += "&" + URLEncoder.encode("hdlC", "UTF-8") + "=" + URLEncoder.encode(hdlC, "UTF-8");
                        data += "&" + URLEncoder.encode("triglycerides", "UTF-8") + "=" + URLEncoder.encode(triglycerides, "UTF-8");
                       // System.out.println(data);
                    } break;

                    case "6": { //for add remarks, record_type = 6
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String remarks = (String)arg0[4];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("remarks", "UTF-8") + "=" + URLEncoder.encode(remarks, "UTF-8");
                       //System.out.println(data);
                    } break;

                }

            }

            if(model.equals(bodyRecordConstant.EDIT_BODY_RECORD)){
                //TODO handle different record type mode with different string
                switch (recordType){
                    case "1":{ //for glucose add record
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String period = (String)arg0[4];
                        String typeGlucose = (String)arg0[5];
                        String glucose = (String)arg0[6];
                        String recordId = (String)arg0[7];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("period", "UTF-8") + "=" + URLEncoder.encode(period, "UTF-8");
                        data += "&" + URLEncoder.encode("typeGlucose", "UTF-8") + "=" + URLEncoder.encode(typeGlucose, "UTF-8");
                        data += "&" + URLEncoder.encode("glucose", "UTF-8") + "=" + URLEncoder.encode(glucose, "UTF-8");
                        data += "&" + URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                        //System.out.println(data);
                    }
                    break;

                    case "2":{ //for edit height,weight,waist,bmi record, record_type = 2
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String height = (String)arg0[4];
                        String weight = (String)arg0[5];
                        String bmi = (String)arg0[6];
                        String waist = (String)arg0[7];
                        String recordId = (String)arg0[8];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("height", "UTF-8") + "=" + URLEncoder.encode(height, "UTF-8");
                        data += "&" + URLEncoder.encode("weight", "UTF-8") + "=" + URLEncoder.encode(weight, "UTF-8");
                        data += "&" + URLEncoder.encode("bmi", "UTF-8") + "=" + URLEncoder.encode(bmi, "UTF-8");
                        data += "&" + URLEncoder.encode("waist", "UTF-8") + "=" + URLEncoder.encode(waist, "UTF-8");
                        data += "&" + URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                        //System.out.println(data);
                    }
                    break;

                    case "3":{ //for edit HbA1c record, record_type = 3
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String HbA1c = (String)arg0[4];
                        String recordId = (String)arg0[5];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("HbA1c", "UTF-8") + "=" + URLEncoder.encode(HbA1c, "UTF-8");
                        data += "&" + URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                        //System.out.println(data);
                    }
                    break;

                    case "4":{ //for edit height,weight,waist,bmi record, record_type = 2
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String bp_h = (String)arg0[4];
                        String bp_l = (String)arg0[5];
                        String heart_rate = (String)arg0[6];
                        String recordId = (String)arg0[7];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("bp_h", "UTF-8") + "=" + URLEncoder.encode(bp_h, "UTF-8");
                        data += "&" + URLEncoder.encode("bp_l", "UTF-8") + "=" + URLEncoder.encode(bp_l, "UTF-8");
                        data += "&" + URLEncoder.encode("heart_rate", "UTF-8") + "=" + URLEncoder.encode(heart_rate, "UTF-8");
                        data += "&" + URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                        //System.out.println(data);
                    }
                    break;

                    case "5":{ //for edit total cholesterol, LDL-C, HDL-C, TRIGLYCERIDERS record, record_type = 5
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String total_c = (String)arg0[4];
                        String hdlC = (String)arg0[5];
                        String ldlC = (String)arg0[6];
                        String triglycerides = (String)arg0[7];
                        String recordId = (String)arg0[8];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("total_c", "UTF-8") + "=" + URLEncoder.encode(total_c, "UTF-8");
                        data += "&" + URLEncoder.encode("hdlC", "UTF-8") + "=" + URLEncoder.encode(hdlC, "UTF-8");
                        data += "&" + URLEncoder.encode("ldlC", "UTF-8") + "=" + URLEncoder.encode(ldlC, "UTF-8");
                        data += "&" + URLEncoder.encode("triglycerides", "UTF-8") + "=" + URLEncoder.encode(triglycerides, "UTF-8");
                        data += "&" + URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                        //System.out.println(data);
                    }
                    break;

                    case "6":{ //for edit remarks, record_type = 6
                        String user_id = (String)arg0[0];
                        String recordType = (String)arg0[1];
                        String date = (String)arg0[2];
                        String time = (String)arg0[3];
                        String remarks = (String)arg0[4];
                        String recordId = (String)arg0[5];
                        data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(user_id, "UTF-8");
                        data += "&" + URLEncoder.encode("recordType", "UTF-8") + "=" + URLEncoder.encode(recordType, "UTF-8");
                        data += "&" + URLEncoder.encode("date", "UTF-8") + "=" + URLEncoder.encode(date, "UTF-8");
                        data += "&" + URLEncoder.encode("time", "UTF-8") + "=" + URLEncoder.encode(time, "UTF-8");
                        data += "&" + URLEncoder.encode("remarks", "UTF-8") + "=" + URLEncoder.encode(remarks, "UTF-8");
                        data += "&" + URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                        //System.out.println(data);
                    }
                    break;
                }

            }

            if(model.equals(bodyRecordConstant.DELETE_BODY_RECORD)){
                String recordId = (String)arg0[0];
                String record_type = (String)arg0[1];
                data = URLEncoder.encode("recordId", "UTF-8") + "=" + URLEncoder.encode(recordId, "UTF-8");
                data += "&" + URLEncoder.encode("record_type", "UTF-8") + "=" + URLEncoder.encode(record_type, "UTF-8");
            }

            //get result  from server
            if (!(recordType.equals(bodyRecordConstant.GET_BP_TYPE_CODE) || recordType.equals(bodyRecordConstant.GET_GLUCOSE_TYPE_CODE) || recordType.equals(bodyRecordConstant.GET_BMI_TYPE_CODE)|| recordType.equals(bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE))){
                dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
                result = dmasyncTask.getDataFromServer(data);
            }else{
                result = "{}";
            }


        } catch (SocketTimeoutException e){
            e.printStackTrace();
            return dmAppAsyncTask.SERVER_SLEEP;
        } catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
        //System.out.println("[bodyAsyncTask - result] "+result+" Viewmode "+viewMode);
        return result;
    }


    @Override
    protected void onPostExecute(String result){
        //System.out.println("[Run at onPostExecute] recordType = "+ recordType+" Viewmode= "+viewMode);
        if (result!=null) {
            if (result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)) {
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            } else if (result.equals(dmAppAsyncTask.SERVER_SLEEP)) {
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            } else {
                BodyRecord bodyRecord = new BodyRecord();
                progressBar.dismiss();
                switch (recordType) {
                    //Glucose Function
                    case "1": {
                        //System.out.println("[Run at onPostExecute case 1] recordType = " + recordType + " Viewmode= " + viewMode);
                        //SHOW EXISTING glucose record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, viewMode, ctx);
                        }
                        //ADD NEW glucose record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, viewMode);
                        }
                        //EDIT save glucose record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, viewMode);
                        }//DELETE glucose record
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, viewMode);
                        }

                        break;
                    }

                    // height,weight,waist,bmi record, record_type = 2
                    case "2": {
                        //System.out.println("[Run at onPostExecute case 2] recordType = " + recordType + " Viewmode= " + viewMode);
                        //get bp record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_BMI_TYPE_CODE, viewMode, ctx);
                        }
                        //add bmi record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_BMI_TYPE_CODE, viewMode);
                        }
                        //edit save bmi record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit bmi process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_BMI_TYPE_CODE, viewMode);
                        }
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete bmi process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_BMI_TYPE_CODE, viewMode);
                        }
                        break;
                    }

                    //HbA1c record, record_type = 3
                    case "3": {
                        //System.out.println("[Run at onPostExecute case 3] recordType = " + recordType + " Viewmode= " + viewMode);
                        //get bp record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_HBA1C_TYPE_CODE, viewMode, ctx);
                        }
                        //add bp record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_HBA1C_TYPE_CODE, viewMode);
                        }
                        //edit save bp record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit hba1c process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_HBA1C_TYPE_CODE, viewMode);
                        }
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete hba1c process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_HBA1C_TYPE_CODE, viewMode);
                        }
                        break;
                    }

                    //Blood pressure &  heart rate
                    case "4": {
                        //System.out.println("[Run at onPostExecute case 4] recordType = " + recordType + " Viewmode= " + viewMode);
                        //get bp record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_BP_TYPE_CODE, viewMode, ctx);
                        }
                        //add bp record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_BP_TYPE_CODE, viewMode);
                        }
                        //edit save bp record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_BP_TYPE_CODE, viewMode);
                        }
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_BP_TYPE_CODE, viewMode);
                        }
                        break;
                    }

                    //total cholesterol, LDL-C, HDL-C, TRIGLYCERIDERS record, record_type = 5
                    case "5": {
                        //System.out.println("[Run at onPostExecute case 5] recordType = " + recordType + " Viewmode= " + viewMode);
                        //get record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE, viewMode, ctx);
                        }
                        //add record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE, viewMode);
                        }
                        //edit save record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE, viewMode);
                        }
                        //delete record
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_TRIGLYCERIDERS_TYPE_CODE, viewMode);
                        }
                        break;
                    }

                    case "6": {
                        //System.out.println("[Run at onPostExecute case 6] recordType = " + recordType + " Viewmode= " + viewMode);
                        //get record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_REMARK_TYPE_CODE, viewMode, ctx);
                        }
                        //add record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_REMARK_TYPE_CODE, viewMode);
                        }
                        //edit save record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_REMARK_TYPE_CODE, viewMode);
                        }
                        //delete record
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_REMARK_TYPE_CODE, viewMode);
                        }
                        break;
                    }

                    case "7": { //report mix type
                        //System.out.println("[Run at onPostExecute case 7] recordType = " + recordType + " Viewmode= " + viewMode);
                        //get record
                        if (model.equals(bodyRecordConstant.GET_BODY_RECORD)) {
                            bodyRecord.setBodyRecord(this.activity, result, bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE, viewMode, ctx);
                        }
                        //add record
                        if (model.equals(bodyRecordConstant.SAVE_BODY_RECORD)) {
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE, viewMode);
                        }
                        //edit save record
                        if (model.equals(bodyRecordConstant.EDIT_BODY_RECORD)) {
                            //System.out.println("Edit process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE, viewMode);
                        }
                        //delete record
                        if (model.equals(bodyRecordConstant.DELETE_BODY_RECORD)) {
                            //System.out.println("Delete process: " + result);
                            bodyRecord.afterSaveRecord(this.activity, result, bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE, viewMode);
                        }
                        break;
                    }

                }
            }
        }
    }
}
