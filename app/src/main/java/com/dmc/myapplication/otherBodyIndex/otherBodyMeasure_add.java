package com.dmc.myapplication.otherBodyIndex;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.unitConvert;

import java.util.Calendar;

/**
 * Created by Po on 14/3/2016.
 */
public class otherBodyMeasure_add extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    Context ctx;
    private EditText editTextDate, editTextTime, editTextOmHeightValue, editTextOmWeightValue, editTextWaistValue;
    private EditText editTextOmHeightU11Value, editTextOmHeightU12Value, editTextOmWeightU1Value, editTextWaistU1Value;
    UserLocalStore userLocalStore;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherbodymeasure_add);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();
        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.om_add_date);
        Calendar calendar = Calendar.getInstance();
        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH) + 1; // because Jan =0
        int dd = calendar.get(Calendar.DAY_OF_MONTH);
        editTextDate.setText(yy + "-" + mm + "-" + dd);

        ImageView setOmDate = (ImageView) findViewById(R.id.setOm_add_date);
        setOmDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new otherBodyMeasureDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.om_add_time);
        Calendar time = Calendar.getInstance();
        int HH = time.get(Calendar.HOUR_OF_DAY);
        int MM = time.get(Calendar.MINUTE);

        editTextTime.setText(HH + ":" + MM + ":00");
        ImageView setTime = (ImageView) findViewById(R.id.setOm_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new otherBodyMeasureTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
            }
        });



        //handle the value field
        editTextOmHeightValue = (EditText) findViewById(R.id.om_add_height_value);
        editTextOmHeightU11Value = (EditText) findViewById(R.id.om_add_height_value_u1_1);
        editTextOmHeightU12Value = (EditText) findViewById(R.id.om_add_height_value_u1_2);
        TextView editTextOmHeightUnit = (TextView) findViewById(R.id.om_add_height_unit_0);
        TextView editTextOmHeightUnit1_1 = (TextView) findViewById(R.id.om_add_height_unit_1_1);
        TextView editTextOmHeightUnit1_2 = (TextView) findViewById(R.id.om_add_height_unit_1_2);

        if (user.height_unit == 1) {
            editTextOmHeightValue.setVisibility(View.GONE);
            editTextOmHeightUnit.setVisibility(View.GONE);
        } else {
            editTextOmHeightU11Value.setVisibility(View.GONE);
            editTextOmHeightU12Value.setVisibility(View.GONE);
            editTextOmHeightUnit1_1.setVisibility(View.GONE);
            editTextOmHeightUnit1_2.setVisibility(View.GONE);
        }

        //handle the weight value & unit fields
        editTextOmWeightValue = (EditText) findViewById(R.id.om_add_weight_value);
        editTextOmWeightU1Value = (EditText) findViewById(R.id.om_add_weight_value_u1);
        TextView editTextOmWeightUnit = (TextView) findViewById(R.id.om_add_weight_unit_0);
        TextView editTextOmWeightUnit1 = (TextView) findViewById(R.id.om_add_weight_unit_1);

        if (user.weight_unit == 1) {
            editTextOmWeightValue.setVisibility(View.GONE);
            editTextOmWeightUnit.setVisibility(View.GONE);
        } else {
            editTextOmWeightU1Value.setVisibility(View.GONE);
            editTextOmWeightUnit1.setVisibility(View.GONE);
         }

        //handle the waist value & unit fields
        editTextWaistValue = (EditText) findViewById(R.id.om_add_waist_value);
        editTextWaistU1Value = (EditText) findViewById(R.id.om_add_waist_value_u1);
        TextView editTextOmWaistUnit = (TextView) findViewById(R.id.om_add_waist_unit_0);
        TextView editTextOmWaistUnit1 = (TextView) findViewById(R.id.om_add_waist_unit_1);

        if (user.waist_unit == 1) {
            editTextWaistValue.setVisibility(View.GONE);
            editTextOmWaistUnit.setVisibility(View.GONE);
        }else {
            editTextWaistU1Value.setVisibility(View.GONE);
            editTextOmWaistUnit1.setVisibility(View.GONE);
        }


        Button buttonOmAdd = (Button) findViewById(R.id.om_add_button);
        buttonOmAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validOm();
            }
        });

    }

    private void validOm() {

        // Reset errors.
        editTextOmHeightValue.setError(null);
        editTextOmWeightValue.setError(null);
        editTextWaistValue.setError(null);

        editTextOmHeightU11Value.setError(null);
        editTextOmHeightU12Value.setError(null);
        editTextOmWeightU1Value.setError(null);
        editTextWaistU1Value.setError(null);


        // Store values of input.
        String omHeight = editTextOmHeightValue.getText().toString();
        String omWeight = editTextOmWeightValue.getText().toString();
        String omWaist = editTextWaistValue.getText().toString();

        boolean cancel = false;
        View focusView = null;

        //handle other than default units cases
        if(user.height_unit == 1){
            String feet = editTextOmHeightU11Value.getText().toString();
            String inch = editTextOmHeightU12Value.getText().toString();
            if (TextUtils.isEmpty(feet) || TextUtils.isEmpty(inch) ) {
                editTextOmHeightU11Value.setError(getString(R.string.om_empty_error));
                editTextOmHeightU12Value.setError(getString(R.string.om_empty_error));
                focusView =  editTextOmHeightValue;
                cancel = true;
            }else{
                int feetValue = Integer.valueOf(feet);
                int leftOverValue = Integer.valueOf(inch);
                double inches = feetValue * 12 + leftOverValue;
                double cm = unitConvert.InchToCm(inches);
                omHeight = Double.toString(cm);
            }
        }

        if(user.weight_unit == 1){
            String pound = editTextOmWeightU1Value.getText().toString();

            if (TextUtils.isEmpty(pound)) {
                editTextOmWeightU1Value.setError(getString(R.string.om_empty_error));
                focusView =  editTextOmWeightU1Value;
                cancel = true;
            }else{
                int poundValue = Integer.valueOf(pound);
                double kg = (double) unitConvert.LbToKg(poundValue);
                omWeight = Double.toString(kg);
            }
        }


        if(user.waist_unit == 1){
            String inch = editTextWaistU1Value.getText().toString();
            if (TextUtils.isEmpty(inch)) {
                editTextWaistU1Value.setError(getString(R.string.om_empty_error));
                focusView =  editTextWaistU1Value;
                cancel = true;
            }else{
                double inchValue = Double.valueOf(inch);
                int cm = (int) unitConvert.InchToCm(inchValue);
                omWaist = Integer.toString(cm);
            }
        }



        // Check for a valid input, if the user entered one.
        if (TextUtils.isEmpty(omHeight)) {
            editTextOmHeightValue.setError(getString(R.string.om_empty_error));
            focusView =  editTextOmHeightValue;
            cancel = true;
        }

        if (TextUtils.isEmpty(omWeight)) {
            editTextOmWeightValue.setError(getString(R.string.om_empty_error));
            focusView =  editTextOmWeightValue;
            cancel = true;
        }

        if ( TextUtils.isEmpty(omWaist) || !isValidWaist(omWaist)) {
            editTextWaistValue.setError(getString(R.string.om_waist_error));
            focusView =  editTextWaistValue;
            if(user.waist_unit == 1){
                editTextWaistU1Value.setError(getString(R.string.om_waist_error));
                focusView =  editTextWaistU1Value;
            }
            cancel = true;
        }

        if (!TextUtils.isEmpty(omHeight) && !TextUtils.isEmpty(omWeight)) {
            double bmi = Double.parseDouble(omWeight) / (Double.parseDouble(omHeight)*0.01 * (Double.parseDouble(omHeight) * 0.01)) ;
            if(!isValidBmi(bmi)) {
                editTextOmHeightValue.setError(getString(R.string.om_height_error));
                editTextOmWeightValue.setError(getString(R.string.om_weight_error));
                focusView = editTextOmHeightValue;
                if(user.height_unit == 1){
                    editTextOmHeightU11Value.setError(getString(R.string.om_height_error));
                    focusView =  editTextOmHeightU11Value;
                }
                if(user.weight_unit == 1){
                    editTextOmWeightU1Value.setError(getString(R.string.om_weight_error));
                    focusView =  editTextOmWeightU1Value;
                }
                cancel = true;
            }
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            //bodyAsyncTask addRecord = new bodyAsyncTask(this, bodyRecordConstant.SAVE_BODY_RECORD ,bodyRecordConstant.GET_BMI_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
            String user_id = Integer.toString(user.userid);
            String record_type = bodyRecordConstant.GET_BMI_TYPE_CODE;
            String date = editTextDate.getText().toString();
            String time = editTextTime.getText().toString();
            double bmi = Double.parseDouble(omWeight) / (Double.parseDouble(omHeight)*0.01 * (Double.parseDouble(omHeight) * 0.01)) ;
            String sBMI = Double.toString(Math.round(bmi*10)/10);
            //addRecord.execute(user_id, record_type, date, time, omHeight,omWeight,sBMI, omWaist);

            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();
            BodyRecord.bodyRecord bodyrecord = bodyRecord.insert(a.new bodyRecord(0, Integer.parseInt(bodyRecordConstant.GET_BMI_TYPE_CODE), (user.userid), editTextDate.getText().toString(), editTextTime.getText().toString(), Double.parseDouble(omHeight), Double.parseDouble(omWeight), Double.parseDouble(omWaist), Double.parseDouble(sBMI), 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
            this.startActivity(intent);
            this.finish();
        }
    }
    //For spinner control - manual
    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

    }
    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //
    }
    //For spinner control - auto


    private boolean isValidWaist(String om){

        double omValue = Double.parseDouble(om);

        if (omValue < bodyRecordConstant.GET_WAIST_LOWER_LIMIT || omValue >= bodyRecordConstant.GET_WAIST_UPPER_LIMIT){
            return false;
        }
        return true;
    }

    private boolean isValidBmi(double bmi){

        if (bmi < bodyRecordConstant.GET_BMI_LOWER_LIMIT || bmi >= bodyRecordConstant.GET_BMI_UPPER_LIMIT){
            return false;
        }
        return true;
    }



    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
