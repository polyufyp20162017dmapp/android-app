package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;

/**
 * Created by Po on 13/3/2016.
 */
public class otherBodyMeasureFragment extends Fragment {

    UserLocalStore userLocalStore;
    Context ctx;


    public otherBodyMeasureFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v =  inflater.inflate(R.layout.othermeasurement_main, container, false);
        ctx = getContext();
        if(v.getParent() != null)
            ((ViewGroup)v.getParent()).removeView(v);

        userLocalStore = new UserLocalStore(getActivity());
        User user = userLocalStore.getLoggedInUser();

        bodyAsyncTask getRecord = new bodyAsyncTask(this.getActivity(), bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_BMI_TYPE_CODE,bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
        String user_id = Integer.toString(user.userid);
        String record_type = bodyRecordConstant.GET_BMI_TYPE_CODE;
        getRecord.execute(user_id, record_type);

        return v;
    }

}
