package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.dmc.myapplication.R;
import com.dmc.myapplication.blood.blood_add;

/**
 * Created by Po on 25/3/2016.
 */
public class otherBody_Main extends Fragment {


    Context ctx;
    private static int flag=0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v =  inflater.inflate(R.layout.otherbody_container, container, false);
        ctx = getActivity().getApplicationContext();
        setHasOptionsMenu(true);

        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.otherBody_frame, new otherBodyMeasurementMain()).commit();

        final Button btnBodyMeasurement = (Button) v.findViewById(R.id.title_activity_om_switch);
        final Button btnBodyReport = (Button) v.findViewById(R.id.title_activity_or_switch);
        btnBodyMeasurement.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBodyMeasurement.setBackgroundColor(Color.parseColor("#FF7a00")); //active in orange
                btnBodyReport.setBackgroundColor(Color.parseColor("#26C6DA"));
                FragmentTransaction fragmentTransaction =getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.otherBody_frame, new otherBodyMeasurementMain()).commit();
                flag = 0;


            }
        });

        btnBodyReport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnBodyReport.setBackgroundColor(Color.parseColor("#FF7a00")); //active in orange
                btnBodyMeasurement.setBackgroundColor(Color.parseColor("#26C6DA"));
                FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                fragmentTransaction.replace(R.id.otherBody_frame, new otherBodyReportMain()).commit();
                flag = 1;
            }
        });

        return v;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getActivity().getMenuInflater();
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.om_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent();
                if(flag == 0)
                    intent.setClass(this.getActivity() ,otherBodyMeasure_add.class);
                else
                    intent.setClass(this.getActivity(), otherBodyReport_add.class);
                getActivity().startActivity(intent);
                getActivity().finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        flag =0;
        setHasOptionsMenu(true);
    }
}
