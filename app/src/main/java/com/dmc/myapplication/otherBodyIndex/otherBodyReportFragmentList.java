package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;

/**
 * Created by Po on 25/3/2016.
 */
public class otherBodyReportFragmentList extends Fragment {

    UserLocalStore userLocalStore;
    Context ctx;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.otherbodyreport_list_view, container, false);
        ctx = getContext();
        userLocalStore = new UserLocalStore(getActivity());
        User user = userLocalStore.getLoggedInUser();

        bodyAsyncTask getRecord = new bodyAsyncTask(this.getActivity(), bodyRecordConstant.GET_BODY_RECORD, bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE, bodyRecordConstant.VIEW_MODE_LIST, ctx);
        String user_id = Integer.toString(user.userid);
        String record_type = bodyRecordConstant.GET_REPORT_MIX_TYPE_CODE;
        getRecord.execute(user_id, record_type);

        return v;
    }


}