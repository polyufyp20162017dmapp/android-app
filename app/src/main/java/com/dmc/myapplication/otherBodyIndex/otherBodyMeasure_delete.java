package com.dmc.myapplication.otherBodyIndex;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.unitConvert;


/**
 * Created by Po on 20/3/2016.
 */
public class otherBodyMeasure_delete extends AppCompatActivity {

    private Context ctx;
    private String recordId;
    UserLocalStore userLocalStore;
    User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otherbodymeasure_del);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();


        Intent intent =getIntent();
        recordId = intent.getStringExtra("recordId");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");
        final double getBmi = Double.parseDouble(intent.getStringExtra("bmiValue"));
        final double getHeight =  Double.parseDouble(intent.getStringExtra("heightValue"));
        final double getWeight = Double.parseDouble(intent.getStringExtra("weightValue"));
        final double getWaist =  Double.parseDouble(intent.getStringExtra("waistValue"));


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date
        EditText editTextDate = (EditText) findViewById(R.id.om_del_date);
        editTextDate.setText(getDate);
        editTextDate.setEnabled(false);



        //handle the time
        EditText editTextTime = (EditText) findViewById(R.id.om_del_time);
        editTextTime.setText(getTime);
        editTextTime.setEnabled(false);

        //handle the height value & unit fields
        EditText editTextOmHeightValue = (EditText) findViewById(R.id.om_del_height_value);
        EditText editTextOmHeightU11Value = (EditText) findViewById(R.id.om_del_height_value_u1_1);
        EditText editTextOmHeightU12Value = (EditText) findViewById(R.id.om_del_height_value_u1_2);
        TextView editTextOmHeightUnit = (TextView) findViewById(R.id.om_add_height_unit_0);
        TextView editTextOmHeightUnit1_1 = (TextView) findViewById(R.id.om_add_height_unit_1_1);
        TextView editTextOmHeightUnit1_2 = (TextView) findViewById(R.id.om_add_height_unit_1_2);
        editTextOmHeightValue.setText(Double.toString(getHeight));

        if (user.height_unit == 1) {
            double inch = unitConvert.CmToInches(getHeight);
            int feet = (int) Math.floor(inch / 12);
            int leftover = (int) inch % 12;
            editTextOmHeightU11Value.setText(Integer.toString(feet));
            editTextOmHeightU12Value.setText(Integer.toString(leftover));
            editTextOmHeightValue.setVisibility(View.GONE);
            editTextOmHeightUnit.setVisibility(View.GONE);
            editTextOmHeightU11Value.setEnabled(false);
            editTextOmHeightU12Value.setEnabled(false);
        } else {
            editTextOmHeightU11Value.setVisibility(View.GONE);
            editTextOmHeightU12Value.setVisibility(View.GONE);
            editTextOmHeightUnit1_1.setVisibility(View.GONE);
            editTextOmHeightUnit1_2.setVisibility(View.GONE);
            editTextOmHeightValue.setEnabled(false);
        }

        //handle the weight value & unit fields
        EditText editTextOmWeightValue = (EditText) findViewById(R.id.om_del_weight_value);
        EditText editTextOmWeightU1Value = (EditText) findViewById(R.id.om_del_weight_value_u1);
        TextView editTextOmWeightUnit = (TextView) findViewById(R.id.om_add_weight_unit_0);
        TextView editTextOmWeightUnit1 = (TextView) findViewById(R.id.om_add_weight_unit_1);
        editTextOmWeightValue.setText(Double.toString(getWeight));
        if (user.weight_unit == 1) {
            int pound = (int) unitConvert.KgToLb(getWeight);
            editTextOmWeightU1Value.setText(Integer.toString(pound));
            editTextOmWeightValue.setVisibility(View.GONE);
            editTextOmWeightUnit.setVisibility(View.GONE);
            editTextOmWeightU1Value.setEnabled(false);
        } else {
            editTextOmWeightU1Value.setVisibility(View.GONE);
            editTextOmWeightUnit1.setVisibility(View.GONE);
            editTextOmWeightValue.setEnabled(false);
        }

        //handle the waist value & unit fields
        EditText editTextWaistValue = (EditText) findViewById(R.id.om_del_waist_value);
        EditText editTextWaistU1Value = (EditText) findViewById(R.id.om_del_waist_value_u1);
        TextView editTextOmWaistUnit = (TextView) findViewById(R.id.om_add_waist_unit_0);
        TextView editTextOmWaistUnit1 = (TextView) findViewById(R.id.om_add_waist_unit_1);
        editTextWaistValue.setText(Double.toString(getWaist));
        if (user.waist_unit == 1) {
            double inch = unitConvert.CmToInches(getWaist);
            editTextWaistU1Value.setText(Double.toString(inch));
            editTextWaistValue.setVisibility(View.GONE);
            editTextOmWaistUnit.setVisibility(View.GONE);
            editTextWaistU1Value.setEnabled(false);
        }else {
            editTextWaistU1Value.setVisibility(View.GONE);
            editTextOmWaistUnit1.setVisibility(View.GONE);
            editTextWaistValue.setEnabled(false);
        }


        Button buttonOmDel = (Button) findViewById(R.id.om_del_button);
        buttonOmDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                DialogFragment newFragment = new deleteDialogFragment(recordId);
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        Button buttonOmEdit = (Button) findViewById(R.id.om_edit_button);
        buttonOmEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getOmEditRecord(recordId, getDate, getTime, Double.toString(getBmi), Double.toString(getHeight), Double.toString(getWeight), Double.toString(getWaist));
            }
        });

    }



    public void getOmEditRecord( String recordId, String date, String time, String bmiValue, String heightValue, String weightValue, String waistValue ){

        Intent intent = new Intent();
        intent.setClass(this, otherBodyMeasure_edit.class);
        intent.putExtra("recordId", recordId);
        intent.putExtra("date", date);
        intent.putExtra("time", time);
        intent.putExtra("bmiValue", bmiValue);
        intent.putExtra("heightValue", heightValue);
        intent.putExtra("weightValue", weightValue);
        intent.putExtra("waistValue", waistValue);
        this.startActivity(intent);
        this.finish();
    }


    private class deleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        private String recordId;
        public deleteDialogFragment (String recordId){
            this.recordId = recordId;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    //bodyAsyncTask delRecord = new bodyAsyncTask(getActivity(), bodyRecordConstant.DELETE_BODY_RECORD,bodyRecordConstant.GET_BMI_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
                                    //String record_type = bodyRecordConstant.GET_BMI_TYPE_CODE;
                                    //delRecord.execute(recordId, record_type);

                                    bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
                                    BodyRecord a = new BodyRecord();
                                    bodyRecord.delete(Integer.parseInt(recordId));
                                    Intent intent = new Intent();
                                    intent.setClass(getActivity(), MainActivity.class);
                                    intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
                                    getActivity().startActivity(intent);
                                    getActivity().finish();
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }

    @Override
    public void onBackPressed() {
        handleBackAction();
    }


    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_OTHER_INDEX);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}