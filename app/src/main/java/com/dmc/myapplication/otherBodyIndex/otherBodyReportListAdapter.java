package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Po on 23/3/2016.
 */
public class otherBodyReportListAdapter  extends BaseAdapter {
    private final Context context;
    private List<BodyRecord.bodyRecord> mListData =   new ArrayList<BodyRecord.bodyRecord>();

    private LayoutInflater layoutInflater;


    public otherBodyReportListAdapter(Context aContext,  List<BodyRecord.bodyRecord> listData) {
        context = aContext;
        mListData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        BodyRecord.bodyRecord record = (BodyRecord.bodyRecord) getItem(position);
        UserLocalStore userLocalStore = new UserLocalStore(context);
        User user = userLocalStore.getLoggedInUser();

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.otherbodyreport_list_row_layout, null);
            holder = new ViewHolder();
            holder.orHbA1cTitleView = (TextView) convertView.findViewById(R.id.hba1c_title);
            holder.orTotalCTitleView = (TextView) convertView.findViewById(R.id.totalC_title);
            holder.orHdlCTitleView = (TextView) convertView.findViewById(R.id.hdlC_title);
            holder.orLdlCTitleView = (TextView) convertView.findViewById(R.id.ldlC_title);
            holder.orTriTitleView = (TextView) convertView.findViewById(R.id.tri_title);
            holder.orRemarkTitleView = (TextView) convertView.findViewById(R.id.remark_title);
            holder.orHbA1cView = (TextView) convertView.findViewById(R.id.HbA1c);
            holder.orTotalCView = (TextView) convertView.findViewById(R.id.totalC);
            holder.orHdlCView = (TextView) convertView.findViewById(R.id.hdlC);
            holder.orLdlCView = (TextView) convertView.findViewById(R.id.ldlC);
            holder.orTriView = (TextView) convertView.findViewById(R.id.tri);
            holder.orRemarkView = (TextView) convertView.findViewById(R.id.remark);
            holder.recordIdView = (TextView) convertView.findViewById(R.id.orRecordId);
            holder.recordTypeView = (TextView) convertView.findViewById(R.id.orRecordType);
            holder.dateView = (TextView) convertView.findViewById(R.id.date);
            holder.timeView = (TextView) convertView.findViewById(R.id.time);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (record.getRecordType()==3) {
            holder.orTotalCTitleView.setVisibility(View.GONE);
            holder.orHdlCTitleView.setVisibility(View.GONE);
            holder.orLdlCTitleView.setVisibility(View.GONE);
            holder.orTriTitleView.setVisibility(View.GONE);
            holder.orTotalCView.setVisibility(View.GONE);
            holder.orHdlCView.setVisibility(View.GONE);
            holder.orLdlCView.setVisibility(View.GONE);
            holder.orTriView.setVisibility(View.GONE);
            holder.orRemarkTitleView.setVisibility(View.GONE);
            holder.orRemarkView.setVisibility(View.GONE);

            holder.orHbA1cTitleView.setVisibility(View.VISIBLE);
            holder.orHbA1cView.setVisibility(View.VISIBLE);
            holder.orHbA1cView.setText(String.valueOf(record.getHba1c()));
            if (record.getHba1c() < bodyRecordConstant.GET_HBA1C_LOWER_LIMIT) {
                holder.orHbA1cView.setTextColor(Color.parseColor("#673ab7"));
            }  //high HbA1c
            else if (record.getHba1c() >= bodyRecordConstant.GET_HBA1C_HIGH) {
                holder.orHbA1cView.setTextColor(Color.parseColor("#FF5722"));
            } //mid HbA1c (average)
            else if (record.getHba1c() >= bodyRecordConstant.GET_HBA1C_MIDHIGH && record.getHba1c() < bodyRecordConstant.GET_HBA1C_HIGH) {
                holder.orHbA1cView.setTextColor(Color.parseColor("#F99611"));
            } else {//medium HbA1c (nice)
                holder.orHbA1cView.setTextColor(Color.parseColor("#4caf50"));
            }
        }

        else if (record.getRecordType()==5) {
            holder.orTotalCTitleView.setVisibility(View.VISIBLE);
            holder.orHdlCTitleView.setVisibility(View.VISIBLE);
            holder.orLdlCTitleView.setVisibility(View.VISIBLE);
            holder.orTriTitleView.setVisibility(View.VISIBLE);
            holder.orTotalCView.setVisibility(View.VISIBLE);
            holder.orHdlCView.setVisibility(View.VISIBLE);
            holder.orLdlCView.setVisibility(View.VISIBLE);
            holder.orTriView.setVisibility(View.VISIBLE);
            holder.orHbA1cTitleView.setVisibility(View.GONE);
            holder.orHbA1cView.setVisibility(View.GONE);
            holder.orRemarkTitleView.setVisibility(View.GONE);
            holder.orRemarkView.setVisibility(View.GONE);

            holder.orTotalCView.setText(String.valueOf(record.getTotal_c()));
            if (record.getTotal_c() < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                holder.orTotalCView.setTextColor(Color.parseColor("#673ab7"));
            }  //high total cholesterol
            else if (record.getTotal_c() >= bodyRecordConstant.GET_TOTALC_HIGH) {
                holder.orTotalCView.setTextColor(Color.parseColor("#FF5722"));
            } //mid total cholesterol (average)
            else if (record.getTotal_c() >= bodyRecordConstant.GET_TOTALC_MIDHIGH && record.getTotal_c() < bodyRecordConstant.GET_TOTALC_HIGH) {
                holder.orTotalCView.setTextColor(Color.parseColor("#F99611"));
            } else {//medium total cholesterol (nice)
                holder.orTotalCView.setTextColor(Color.parseColor("#4caf50"));
            }

            holder.orHdlCView.setText(String.valueOf(record.getHdl_c()));
            //check user's sex in order to use different standard
            if (user.sex == 0) { //FEMALE
                if (record.getHdl_c() < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                    holder.orHdlCView.setTextColor(Color.parseColor("#673ab7"));
                }  //high HDL-C
                else if (record.getHdl_c() <= bodyRecordConstant.GET_HDLC_FEMALE) {
                    holder.orHdlCView.setTextColor(Color.parseColor("#FF5722"));
                } else {//medium HDL-C (nice)
                    holder.orHdlCView.setTextColor(Color.parseColor("#4caf50"));
                }
            }else { //MALE
                if (record.getHdl_c() < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT) {
                    holder.orHdlCView.setTextColor(Color.parseColor("#673ab7"));
                }  //high HDL-C
                else if (record.getHdl_c() <= bodyRecordConstant.GET_HDLC_MALE) {
                    holder.orHdlCView.setTextColor(Color.parseColor("#FF5722"));
                } else {//medium HDL-C (nice)
                    holder.orHdlCView.setTextColor(Color.parseColor("#4caf50"));
                }
            }

            holder.orLdlCView.setText(String.valueOf(record.getLdl_c()));
            //set level color
            // low
            if (record.getLdl_c() < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT){
                holder.orLdlCView.setTextColor(Color.parseColor("#673ab7"));
            }  //high LDL-C
            else if ( record.getLdl_c() >= bodyRecordConstant.GET_LDLC_HIGH){
                holder.orLdlCView.setTextColor(Color.parseColor("#FF5722"));
            } //mid LDL-C
            else if ( record.getLdl_c() >= bodyRecordConstant.GET_LDLC_MIDHIGH && record.getLdl_c() < bodyRecordConstant.GET_LDLC_HIGH){
                holder.orLdlCView.setTextColor(Color.parseColor("#F99611"));
            }
            else {//medium LDL-C (Nice)
                holder.orLdlCView.setTextColor(Color.parseColor("#4caf50"));
            }

            holder.orTriView.setText(String.valueOf(record.getTriglyceriders()));
            //set level color
            // low
            if (record.getTriglyceriders() < bodyRecordConstant.GET_TOTALC_LOWER_LIMIT){
                holder.orTriView.setTextColor(Color.parseColor("#673ab7"));
            }  //high TRIGLYCERIDERS
            else if ( record.getTriglyceriders() >= bodyRecordConstant.GET_TRIGLYCERIDERS_HIGH){
                holder.orTriView.setTextColor(Color.parseColor("#FF5722"));
            } //mid TRIGLYCERIDERS
            else if ( record.getTriglyceriders() >= bodyRecordConstant.GET_TRIGLYCERIDERS_MIDHIGH && record.getTriglyceriders() < bodyRecordConstant.GET_TRIGLYCERIDERS_HIGH){
                holder.orTriView.setTextColor(Color.parseColor("#F99611"));
            }
            else {//medium TRIGLYCERIDERS (Nice)
                holder.orTriView.setTextColor(Color.parseColor("#4caf50"));
            }
        }else{

            holder.orTotalCTitleView.setVisibility(View.GONE);
            holder.orHdlCTitleView.setVisibility(View.GONE);
            holder.orLdlCTitleView.setVisibility(View.GONE);
            holder.orTriTitleView.setVisibility(View.GONE);
            holder.orTotalCView.setVisibility(View.GONE);
            holder.orHdlCView.setVisibility(View.GONE);
            holder.orLdlCView.setVisibility(View.GONE);
            holder.orTriView.setVisibility(View.GONE);
            holder.orHbA1cTitleView.setVisibility(View.GONE);
            holder.orHbA1cView.setVisibility(View.GONE);
            holder.orRemarkTitleView.setVisibility(View.VISIBLE);
            holder.orRemarkView.setVisibility(View.VISIBLE);

            holder.orRemarkView.setText(String.valueOf(record.getRemarks()));

        }

        holder.recordIdView.setText(Integer.toString(record.getRecordId()));
        holder.recordTypeView.setText(Integer.toString(record.getRecordType()));
        holder.dateView.setText(record.getDate());
        holder.timeView.setText(record.getTime());
        return convertView;
    }

    static class ViewHolder {
        TextView orHbA1cTitleView;
        TextView orTotalCTitleView;
        TextView orHdlCTitleView;
        TextView orLdlCTitleView;
        TextView orTriTitleView;
        TextView orRemarkTitleView;
        TextView orHbA1cView;
        TextView orTotalCView;
        TextView orHdlCView;
        TextView orLdlCView;
        TextView orTriView;
        TextView orRemarkView;
        TextView dateView;
        TextView timeView;
        TextView recordIdView;
        TextView recordTypeView;

    }

}
