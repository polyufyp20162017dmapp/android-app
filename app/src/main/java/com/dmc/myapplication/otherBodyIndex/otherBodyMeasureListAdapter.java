package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.tool.unitConvert;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Po on 13/3/2016.
 */
public class otherBodyMeasureListAdapter  extends BaseAdapter {
    private final Context context;
    private List<BodyRecord.bodyRecord> mListData =   new ArrayList<BodyRecord.bodyRecord>();

    private LayoutInflater layoutInflater;


    public otherBodyMeasureListAdapter(Context aContext,  List<BodyRecord.bodyRecord> listData) {
        context = aContext;
        mListData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        BodyRecord.bodyRecord record = (BodyRecord.bodyRecord) getItem(position);
        UserLocalStore userLocalStore = new UserLocalStore(context);
        User user = userLocalStore.getLoggedInUser();

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.otherbodymeasure_list_row_layout, null);
            holder = new ViewHolder();
            holder.omBmiView = (TextView) convertView.findViewById(R.id.bmi);
            holder.omHeightView = (TextView) convertView.findViewById(R.id.height);
            holder.omWeightView = (TextView) convertView.findViewById(R.id.weight);
            holder.omWaistView = (TextView) convertView.findViewById(R.id.waist);

            holder.omHeightU1View = (TextView) convertView.findViewById(R.id.height_unit1);
            holder.omWeightU1View = (TextView) convertView.findViewById(R.id.weight_unit1);
            holder.omWaistU1View = (TextView) convertView.findViewById(R.id.waist_unit1);

            holder.dateView = (TextView) convertView.findViewById(R.id.date);
            holder.timeView = (TextView) convertView.findViewById(R.id.time);
            holder.recordIdView = (TextView) convertView.findViewById(R.id.omRecordId);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (record.getRecordType()==2) {

            holder.omBmiView.setText(String.valueOf(record.getBmi()));
            holder.omWaistView.setText(Double.toString(record.getWaist()));

            if (user.waist_unit == 1) {
                double inch = Math.round( unitConvert.CmToInches(record.getWaist())*10 )  ;
                holder.omWaistU1View.setText(Double.toString(inch/10)+"吋");
            }else{
                int waistInCm = (int)record.getWaist();
                holder.omWaistU1View.setText(String.valueOf(waistInCm+"厘米"));
            }
            //set level color
           // low
            if (record.getBmi() < bodyRecordConstant.GET_BMI_LOW){
                holder.omBmiView.setTextColor(Color.parseColor("#673ab7"));
            }  //high fat
            else if ( record.getBmi() >= bodyRecordConstant.GET_BMI_HIGHFAT){
                holder.omBmiView.setTextColor(Color.parseColor("#FF5722"));
            } //mid fat
            else if ( record.getBmi() >= bodyRecordConstant.GET_BMI_MIDFAT && record.getBmi() < bodyRecordConstant.GET_BMI_HIGHFAT){
                holder.omBmiView.setTextColor(Color.parseColor("#F99611"));
            }//overweight
            else if ( record.getBmi() >= bodyRecordConstant.GET_BMI_OVERWEIGHT && record.getBmi() < bodyRecordConstant.GET_BMI_MIDFAT){
                holder.omBmiView.setTextColor(Color.parseColor("#f7c600"));
            }
            else {//medium
                holder.omBmiView.setTextColor(Color.parseColor("#4caf50"));
            }

            //check user's sex in order to use different standard
            if (user.sex == 0) {
                // fat
                if (record.getWaist() >= bodyRecordConstant.GET_WAIST_FEMALE_FAT) {
                    holder.omWaistU1View.setTextColor(Color.parseColor("#FF5722"));
                } else {//nice
                    holder.omWaistU1View.setTextColor(Color.parseColor("#4caf50"));
                }
            }else{
                if (record.getWaist() >= bodyRecordConstant.GET_WAIST_MALE_FAT) {
                    holder.omWaistU1View.setTextColor(Color.parseColor("#FF5722"));
                } else {//nice
                    holder.omWaistU1View.setTextColor(Color.parseColor("#4caf50"));
                }
            }

            holder.omHeightView.setText(String.valueOf(record.getHeight()));
            if (user.height_unit == 1) {
                double inch = unitConvert.CmToInches(record.getHeight());
                int feet = (int) Math.floor(inch / 12);
                int leftover = (int) inch % 12;
                String display =  Integer.toString(feet)+"\' "+Integer.toString(leftover)+"\"" ;
                holder.omHeightU1View.setText(display);
            } else {
                int heightInCm = (int) record.getHeight();
                holder.omHeightU1View.setText(String.valueOf(heightInCm+"厘米"));
            }

            holder.omWeightView.setText(String.valueOf(record.getWeight()));
            if (user.weight_unit == 1) {
                int pound = (int) unitConvert.KgToLb(record.getWeight());
                holder.omWeightU1View.setText(Integer.toString(pound)+"磅");
            } else {
                double kg =  (double) Math.round(record.getWeight() *10);
                holder.omWeightU1View.setText(String.valueOf(kg/10+"公斤"));
            }
            holder.recordIdView.setText(Integer.toString(record.getRecordId()));
            holder.dateView.setText(record.getDate());
            holder.timeView.setText(record.getTime());

            //hide the true value views
            holder.omHeightView.setVisibility(View.GONE);
            holder.omWeightView.setVisibility(View.GONE);
            holder.omWaistView.setVisibility(View.GONE);

        }
        return convertView;
    }

    static class ViewHolder {
        TextView omBmiView;
        TextView omHeightView;
        TextView omWeightView;
        TextView omWaistView;
        TextView dateView;
        TextView timeView;
        TextView recordIdView;
        TextView omHeightU1View;
        TextView omWeightU1View;
        TextView omWaistU1View;

    }

}
