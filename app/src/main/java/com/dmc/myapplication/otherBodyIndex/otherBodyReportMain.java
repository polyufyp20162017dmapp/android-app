package com.dmc.myapplication.otherBodyIndex;

import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;

/**
 * Created by Po on 23/3/2016.
 */
public class otherBodyReportMain extends Fragment {

    //private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Context ctx;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){

        View v =  inflater.inflate(R.layout.otherbodyreport_slide, container, false);
        ctx = getActivity().getApplicationContext();

        // setHasOptionsMenu(true);

        //set Table View
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) v.findViewById(R.id.sliding_tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(5);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewPager.removeView((View) getView());
    }



    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    private void setupViewPager(ViewPager viewPager) {
        OtherBodyMeasurePagerAdapter adapter = new OtherBodyMeasurePagerAdapter(getChildFragmentManager(),ctx);
        adapter.addFragment(new otherBodyReportFragment(), "圖表");
        adapter.addFragment(new otherBodyReportFragmentList(), "詳細資料");
        viewPager.setAdapter(adapter);
    }


}
