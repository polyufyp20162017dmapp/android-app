package com.dmc.myapplication.assessment;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.TintContextWrapper;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;

import java.util.Calendar;

/**
 * Created by KwokSinMan on 27/3/2016.
 */
public class assessmentAddActivity extends AppCompatActivity {
    static String year;
    static String weekNo;
    static String userId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.assessment_add);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        LinearLayout addSession = (LinearLayout) findViewById(R.id.addSession);
        addSession.setVisibility(View.GONE);
        LinearLayout editSession = (LinearLayout) findViewById(R.id.editSession);
        editSession.setVisibility(View.GONE);

        Button addRecordButton = (Button) findViewById(R.id.assessmentFoodAdd);
        addRecordButton.setEnabled(false);
        addRecordButton.setOnClickListener(new assessmentFoodAddUpdateRecordListener());

        UserLocalStore userLocalStore = new UserLocalStore(this);
        userId = Integer.toString(userLocalStore.getLoggedInUser().userid);


        year = getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_YEAR);
        weekNo =  getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_WEEK);

        //System.out.println("Candy userId="+userId + " year= "+year + " weekNo=" + weekNo);
        assessmentAsyncTask connect = new assessmentAsyncTask(this,assessmentConstant.GET_FOOD_ASSESSMENT_RECORD);
        connect.execute(userId, weekNo, year);

        ((RadioGroup) findViewById(R.id.question1)).setOnCheckedChangeListener(new radioOnCheckedChangeListener());
        ((RadioGroup) findViewById(R.id.question2)).setOnCheckedChangeListener(new radioOnCheckedChangeListener());
        ((RadioGroup) findViewById(R.id.question3)).setOnCheckedChangeListener(new radioOnCheckedChangeListener());
        ((RadioGroup) findViewById(R.id.question4)).setOnCheckedChangeListener(new radioOnCheckedChangeListener());
        ((RadioGroup) findViewById(R.id.question5)).setOnCheckedChangeListener(new radioOnCheckedChangeListener());
        ((RadioGroup) findViewById(R.id.question6)).setOnCheckedChangeListener(new radioOnCheckedChangeListener());

        Button editRecordButton = (Button) findViewById(R.id.assessmentFoodEdit);
        editRecordButton.setOnClickListener(new assessmentFoodAddUpdateRecordListener());

        Button deleteButton = (Button) findViewById(R.id.assessmentFoodDelete);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogFragment newFragment = new assessmentFoodDeleteDialogFragment();
                newFragment.show(getFragmentManager(), "DeleteDialog");
            }
        });

        // SET return <- into left hand side
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() ==  android.R.id.home){
            handleBackAction();
        }
        return true;
    }

    // handle back button in android
    @Override
    public void onBackPressed() {
        handleBackAction();
    }

    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_ASSESSMENT);
        intent.putExtra(assessmentConstant.GET_ASSESSMENT_WEEK, getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_WEEK));
        intent.putExtra(assessmentConstant.GET_ASSESSMENT_YEAR, getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_YEAR));
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

    private String getAnsForEachQuestion(int radioGroup, int ansA, int ansB, int ansC, int ansD, int ansN ){
        String ans="";
        int checkedId = ((RadioGroup) findViewById(radioGroup)).getCheckedRadioButtonId();
        if (checkedId == ansA) {
            ans = assessmentConstant.ansA;
        }
        if (checkedId == ansB) {
            ans = assessmentConstant.ansB;
        }
        if (checkedId == ansC) {
            ans = assessmentConstant.ansC;
        }
        if (checkedId == ansD) {
            ans = assessmentConstant.ansD;
        }
        if (checkedId == ansN) {
            ans = assessmentConstant.ansN;
        }
        return ans;
    }

    private void checkAllQuestionIsAnswered(){
        boolean result = true;
        if ( ((RadioGroup) findViewById(R.id.question1)).getCheckedRadioButtonId() == assessmentConstant.notSelectedRadio){
            result = false;
        }
        if ( ((RadioGroup) findViewById(R.id.question2)).getCheckedRadioButtonId() == assessmentConstant.notSelectedRadio){
            result = false;
        }
        if ( ((RadioGroup) findViewById(R.id.question3)).getCheckedRadioButtonId() == assessmentConstant.notSelectedRadio){
            result = false;
        }
        if ( ((RadioGroup) findViewById(R.id.question4)).getCheckedRadioButtonId() == assessmentConstant.notSelectedRadio){
            result = false;
        }
        if ( ((RadioGroup) findViewById(R.id.question5)).getCheckedRadioButtonId() == assessmentConstant.notSelectedRadio){
            result = false;
        }
        if ( ((RadioGroup) findViewById(R.id.question6)).getCheckedRadioButtonId() == assessmentConstant.notSelectedRadio){
            result = false;
        }
        Button addRecordButton = (Button) findViewById(R.id.assessmentFoodAdd);
        addRecordButton.setEnabled(result);
    }

    private class radioOnCheckedChangeListener implements RadioGroup.OnCheckedChangeListener {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            checkAllQuestionIsAnswered();
        }
    }


    public static class assessmentFoodDeleteDialogFragment extends DialogFragment implements DialogInterface.OnClickListener {
        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            TextView messageTextView =  new TextView(getActivity());
            messageTextView.setText("確定刪除這條記錄嗎？");
            messageTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20);
            messageTextView.setPadding(0,30,0,30);

            return new AlertDialog.Builder(getActivity())
                    .setTitle("刪除記錄")
                    .setView(messageTextView)
                    .setPositiveButton("確定",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                    Activity activity = (Activity) getActivity();
                                    assessmentAsyncTask connect = new assessmentAsyncTask(activity,assessmentConstant.DELETE_FOOD_ASSESSMENT_RECORD);
                                    connect.execute(userId,weekNo,year);
                                }
                            })
                    .setNegativeButton("取消",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int whichButton) {
                                }
                            })
                    .create();
        }
        @Override
        public void onClick(DialogInterface dialog, int whichButton){}
    }

    private class assessmentFoodAddUpdateRecordListener implements View.OnClickListener{
        @Override
        public void onClick(View arg0) {
            Activity activity = getActivityContentView(arg0);
            String ans1 = getAnsForEachQuestion(R.id.question1,R.id.question1ansA,R.id.question1ansB,R.id.question1ansC,R.id.question1ansD,R.id.question1ansNA);
            String ans2 = getAnsForEachQuestion(R.id.question2,R.id.question2ansA,R.id.question2ansB,R.id.question2ansC,R.id.question2ansD,R.id.question1ansNA);
            String ans3 = getAnsForEachQuestion(R.id.question3,R.id.question3ansA,R.id.question3ansB,R.id.question3ansC,R.id.question3ansD,R.id.question1ansNA);
            String ans4 = getAnsForEachQuestion(R.id.question4,R.id.question4ansA,R.id.question4ansB,R.id.question4ansC,R.id.question4ansD,R.id.question1ansNA);
            String ans5 = getAnsForEachQuestion(R.id.question5,R.id.question5ansA,R.id.question5ansB,R.id.question5ansC,R.id.question5ansD,R.id.question1ansNA);
            String ans6 = getAnsForEachQuestion(R.id.question6,R.id.question6ansA,R.id.question6ansB,R.id.question6ansC,R.id.question6ansD,R.id.question1ansNA);

            assessmentAsyncTask connect = new assessmentAsyncTask(activity,assessmentConstant.SAVE_FOOD_ASSESSMENT_RECORD);
            connect.execute(userId, weekNo, year, ans1, ans2, ans3, ans4, ans5, ans6);
        }
    }

    public Activity getActivityContentView(View arg0){
        try {
            if (arg0.getContext() instanceof TintContextWrapper){
                return ((Activity) ((TintContextWrapper) arg0.getContext()).getBaseContext());
            }else{
                return ((Activity) arg0.getContext());
            }
        }catch (ClassCastException e){
            throw new ClassCastException("Cast Error");
        }
    }

}
