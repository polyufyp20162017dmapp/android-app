package com.dmc.myapplication.assessment;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.Calendar;
import java.util.Scanner;

/**
 * Created by KwokSinMan on 27/3/2016.
 */
public class assessmentBiz {

    public static void startAssessment(Activity activity,String year, String weekNo, FragmentTransaction ft){
        activity.setTitle(R.string.nav_forum);
        Bundle bundle = new Bundle();
        bundle.putString(assessmentConstant.GET_ASSESSMENT_WEEK, weekNo);
        bundle.putString(assessmentConstant.GET_ASSESSMENT_YEAR, year);
        assessmentFragment assFrag = new assessmentFragment();
        assFrag.setArguments(bundle);
        ft.setCustomAnimations(R.anim.slide1, R.anim.slide2).replace(R.id.content_frame, assFrag).addToBackStack(null).commit();
    }

    public void afterSaveFoodAssessmentRecord(Activity activity){
        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_ASSESSMENT);
        intent.putExtra(assessmentConstant.GET_ASSESSMENT_WEEK, activity.getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_WEEK));
        intent.putExtra(assessmentConstant.GET_ASSESSMENT_YEAR, activity.getIntent().getExtras().getString(assessmentConstant.GET_ASSESSMENT_YEAR));
        activity.startActivity(intent);
        activity.finish();
    }

    public void getAssessmentRecord(Activity activity, String result){
        //{"SMOKER":"D","BMI":"D","HbA1c":"D","bp":"C","chol":"A","food":"D","exe":"D"}
        //System.out.println("Candy getAssessmentRecord result=" + result);
        try{
            int countA =0; int countB=0; int countC=0; int countD=0;
            JSONObject jsonResult = new JSONObject(result);
            String[] assessmentResultCode = {jsonResult.getString("SMOKER"),jsonResult.getString("BMI"),jsonResult.getString("HbA1c"),jsonResult.getString("bp"),jsonResult.getString("chol"),jsonResult.getString("food"),jsonResult.getString("exe")};

            getEncourageSentence(activity,assessmentResultCode);

            assessmentResultListAdapter resultListAdapter = new assessmentResultListAdapter(activity,assessmentConstant.assessmentResultItem,getStandardByCode(assessmentResultCode),assessmentConstant.iconPath);
            ListView resultList = (ListView) activity.findViewById(R.id.assessmentResultList);
            resultList.setAdapter(resultListAdapter);

        }catch (Exception e){
            e.printStackTrace();
        }
    }
    private void getEncourageSentence(Activity activity,String[] code){
        int countA=0; int countB=0; int countC=0; int countD=0;
        for (int i=0; i<code.length; i++ ) {
            if (code[i].equals(assessmentConstant.standardCodeA)) {
                countA++;
            }else if (code[i].equals(assessmentConstant.standardCodeB)) {
                countB++;
            }else if (code[i].equals(assessmentConstant.standardCodeC)) {
                countC++;
            }else if (code[i].equals(assessmentConstant.standardCodeD)) {
                countD++;
            }
        }
        //System.out.println("countA="+countA);
        //System.out.println("countB=" + countB);
        //System.out.println("countC="+countC);
        //System.out.println("countD="+countD);
        String sentence="";

        /*
        if(countA!=0){
            sentence += assessmentConstant.standardA+"有"+countA+"項\n";
        }
        if(countB!=0){
            sentence += assessmentConstant.standardB+"有"+countB+"項\n";
        }
        if(countC!=0){
            sentence += assessmentConstant.standardC+"有"+countC+"項\n";
        }
        if(countD!=0){
            sentence += assessmentConstant.standardD+"有"+countD+"項\n";
        }
        */
        if(countA==7){
            sentence = "恭喜!! 所有的項目也"+assessmentConstant.standardA+"\n請繼續加油，不要鬆懈";
        }else if(countD==7){
            sentence = "所有的項目也"+assessmentConstant.standardD+"\n加油，不要放棄!!";
        }else{
            sentence = "還有"+(7-countA)+"個項目未能"+assessmentConstant.standardA+"目標，請加油努力!! ";
        }

        //System.out.println("Candy sentence="+sentence);
        TextView encourageSentence = (TextView) activity.findViewById(R.id.encourageSentence);
        encourageSentence.setText(sentence);
    }

    private String[] getStandardByCode(String[] code){
        for (int i=0; i<code.length; i++ ) {
            //System.out.println("Candy code="+code[i]);
            if (code[i].equals(assessmentConstant.standardCodeA)) {
                code[i] = assessmentConstant.standardA;
            }else if (code[i].equals(assessmentConstant.standardCodeB)) {
                code[i] = assessmentConstant.standardB;
            }else if (code[i].equals(assessmentConstant.standardCodeC)) {
                code[i] = assessmentConstant.standardC;
            }else if (code[i].equals(assessmentConstant.standardCodeD)) {
                code[i] = assessmentConstant.standardD;
            }else if (code[i].equals(assessmentConstant.standardCodeN)) {
                code[i] = assessmentConstant.standardN;
            }
        }
        //System.out.println("Code="+code.toString());
        return code;
    }

    public void getFoodAssessment(Activity activity, String result){
        LinearLayout addSession = (LinearLayout) activity.findViewById(R.id.addSession);
        LinearLayout editSession = (LinearLayout) activity.findViewById(R.id.editSession);

        if(result.isEmpty()){
            addSession.setVisibility(View.VISIBLE);
            editSession.setVisibility(View.GONE);
        }else{
            addSession.setVisibility(View.GONE);
            editSession.setVisibility(View.VISIBLE);
            try{
                JSONArray JA = new JSONArray(result);
                if (JA.length()!=0) {
                    checkSelectedAns(activity,JA.getJSONObject(0).getString("QUESTION_ANS1"),R.id.question1,R.id.question1ansA,R.id.question1ansB,R.id.question1ansC,R.id.question1ansD, true);
                    checkSelectedAns(activity,JA.getJSONObject(0).getString("QUESTION_ANS2"),R.id.question2,R.id.question2ansA,R.id.question2ansB,R.id.question2ansC,R.id.question2ansD, false);
                    checkSelectedAns(activity,JA.getJSONObject(0).getString("QUESTION_ANS3"),R.id.question3,R.id.question3ansA,R.id.question3ansB,R.id.question3ansC,R.id.question3ansD, false);
                    checkSelectedAns(activity,JA.getJSONObject(0).getString("QUESTION_ANS4"),R.id.question4,R.id.question4ansA,R.id.question4ansB,R.id.question4ansC,R.id.question4ansD, false);
                    checkSelectedAns(activity,JA.getJSONObject(0).getString("QUESTION_ANS5"),R.id.question5,R.id.question5ansA,R.id.question5ansB,R.id.question5ansC,R.id.question5ansD, false);
                    checkSelectedAns(activity,JA.getJSONObject(0).getString("QUESTION_ANS6"),R.id.question6,R.id.question6ansA,R.id.question6ansB,R.id.question6ansC,R.id.question6ansD, false);
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    public weekObject getWeekByNumber(int number,String label){
        Calendar now = Calendar.getInstance();
        now.add(Calendar.WEEK_OF_YEAR, number);
        String year = Integer.toString(now.get(Calendar.YEAR));
        String week = Integer.toString(now.get(Calendar.WEEK_OF_YEAR));
        now.set(Calendar.DAY_OF_WEEK, 1); // Sunday
        String startWeekDate = now.get(Calendar.YEAR)+ "-" + (now.get(Calendar.MONTH) + 1) + "-" +now.get(Calendar.DAY_OF_MONTH);
        now.set(Calendar.DAY_OF_WEEK, 7); // Saturday
        String endWeekDate = now.get(Calendar.YEAR)+ "-" + (now.get(Calendar.MONTH) + 1) + "-" +now.get(Calendar.DAY_OF_MONTH);
        return new weekObject(year,week,label,startWeekDate,endWeekDate);
    }

    private void checkSelectedAns(Activity activity,String ans,int RadioGroup, int radioAnsA, int radioAnsB, int radioAnsC, int radioAnsD, boolean isNA){
        RadioGroup questionRadioGroup = (RadioGroup) activity.findViewById(RadioGroup);
        if (ans.equals(assessmentConstant.ansA)) {
            questionRadioGroup.check(radioAnsA);
        }
        if (ans.equals(assessmentConstant.ansB)) {
            questionRadioGroup.check(radioAnsB);
        }
        if (ans.equals(assessmentConstant.ansC)) {
            questionRadioGroup.check(radioAnsC);
        }
        if (ans.equals(assessmentConstant.ansD)) {
            questionRadioGroup.check(radioAnsD);
        }
        if(isNA) {
            if (ans.equals(assessmentConstant.ansN)) {
                questionRadioGroup.check(R.id.question1ansNA);
            }
        }
    }

}
