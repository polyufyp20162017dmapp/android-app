package com.dmc.myapplication.assessment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;

import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import java.net.SocketTimeoutException;
import java.net.URLEncoder;

/**
 * Created by KwokSinMan on 27/3/2016.
 */
public class assessmentAsyncTask extends AsyncTask<String,Void,String> {
    private ProgressDialog progressBar;
    private String model;
    private Activity activity;


    public assessmentAsyncTask(Activity activity,  String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }


    @Override
    protected String doInBackground(String... arg0) {
        String data = "";
        try {
            if(model.equals(assessmentConstant.GET_ASSESSMENT_RECORD)){
                String userId = (String) arg0[0];
                String assessmentWeek = (String) arg0[1];
                String assessmentYear = (String) arg0[2];
                String startWeekDate =  (String) arg0[3];
                String endWeekDate =  (String) arg0[4];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_week", "UTF-8") + "=" + URLEncoder.encode(assessmentWeek, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_year", "UTF-8") + "=" + URLEncoder.encode(assessmentYear, "UTF-8");
                    data += "&" + URLEncoder.encode("start_week_date", "UTF-8") + "=" + URLEncoder.encode(startWeekDate, "UTF-8");
                data += "&" + URLEncoder.encode("end_week_date", "UTF-8") + "=" + URLEncoder.encode(endWeekDate, "UTF-8");
            }

            if (model.equals(assessmentConstant.GET_FOOD_ASSESSMENT_RECORD)) {
                String userId = (String) arg0[0];
                String assessmentWeek = (String) arg0[1];
                String assessmentYear = (String) arg0[2];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_week", "UTF-8") + "=" + URLEncoder.encode(assessmentWeek, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_year", "UTF-8") + "=" + URLEncoder.encode(assessmentYear, "UTF-8");
            }
            if (model.equals(assessmentConstant.DELETE_FOOD_ASSESSMENT_RECORD)) {
                String userId = (String) arg0[0];
                String assessmentWeek = (String) arg0[1];
                String assessmentYear = (String) arg0[2];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_week", "UTF-8") + "=" + URLEncoder.encode(assessmentWeek, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_year", "UTF-8") + "=" + URLEncoder.encode(assessmentYear, "UTF-8");
            }
            if(model.equals(assessmentConstant.SAVE_FOOD_ASSESSMENT_RECORD)){
                String userId = (String) arg0[0];
                String assessmentWeek = (String) arg0[1];
                String assessmentYear = (String) arg0[2];
                String ans1 = (String) arg0[3];
                String ans2 = (String) arg0[4];
                String ans3 = (String) arg0[5];
                String ans4 = (String) arg0[6];
                String ans5 = (String) arg0[7];
                String ans6 = (String) arg0[8];
                data = URLEncoder.encode("user_id", "UTF-8") + "=" + URLEncoder.encode(userId, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_week", "UTF-8") + "=" + URLEncoder.encode(assessmentWeek, "UTF-8");
                data += "&" + URLEncoder.encode("assessment_year", "UTF-8") + "=" + URLEncoder.encode(assessmentYear, "UTF-8");
                data += "&" + URLEncoder.encode("ans1", "UTF-8") + "=" + URLEncoder.encode(ans1, "UTF-8");
                data += "&" + URLEncoder.encode("ans2", "UTF-8") + "=" + URLEncoder.encode(ans2, "UTF-8");
                data += "&" + URLEncoder.encode("ans3", "UTF-8") + "=" + URLEncoder.encode(ans3, "UTF-8");
                data += "&" + URLEncoder.encode("ans4", "UTF-8") + "=" + URLEncoder.encode(ans4, "UTF-8");
                data += "&" + URLEncoder.encode("ans5", "UTF-8") + "=" + URLEncoder.encode(ans5, "UTF-8");
                data += "&" + URLEncoder.encode("ans6", "UTF-8") + "=" + URLEncoder.encode(ans6, "UTF-8");
            }

            dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
            return dmasyncTask.getDataFromServer(data);
        } catch (SocketTimeoutException e) {
            e.printStackTrace();
            return dmAppAsyncTask.SERVER_SLEEP;
        } catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
    }

    @Override
    protected void onPostExecute(String result){
        if (result!=null) {
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else {
                assessmentBiz assessmentBiz = new assessmentBiz();
                if(model.equals(assessmentConstant.GET_ASSESSMENT_RECORD)){
                    assessmentBiz.getAssessmentRecord(this.activity, result);
                }
                if (model.equals(assessmentConstant.GET_FOOD_ASSESSMENT_RECORD)) {
                    assessmentBiz.getFoodAssessment(this.activity, result);
                }

                if (model.equals(assessmentConstant.DELETE_FOOD_ASSESSMENT_RECORD)) {
                    assessmentBiz.afterSaveFoodAssessmentRecord(this.activity);
                }

                if (model.equals(assessmentConstant.SAVE_FOOD_ASSESSMENT_RECORD)) {
                    assessmentBiz.afterSaveFoodAssessmentRecord(this.activity);
                }
                progressBar.dismiss();
            }
        }
    }


}
