package com.dmc.myapplication.reminder;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import com.dmc.myapplication.R;
import com.dmc.myapplication.tool.timeObject;

/**
 * Created by KwokSinMan on 16/3/2016.
 */
public class reminderTime4PickerFragment extends DialogFragment implements TimePickerDialog.OnTimeSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        String displayTime = (String)((TextView) getActivity().findViewById(R.id.time4)).getText();
        timeObject displayDateTimeObject = new timeObject(displayTime);
        int hour = displayDateTimeObject.getHour();
        int min = displayDateTimeObject.getMin();
        return new TimePickerDialog(getActivity(), AlertDialog.THEME_HOLO_LIGHT, this,hour,min,true);
    }

    @Override
    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
        timeObject gymTime = new timeObject();
        String displayTime = gymTime.getTimeString(hourOfDay)+":"+gymTime.getTimeString(minute);
        ((TextView) getActivity().findViewById(R.id.time4)).setText(displayTime);
    }
}
