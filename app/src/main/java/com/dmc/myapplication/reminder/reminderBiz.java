package com.dmc.myapplication.reminder;

import android.app.Activity;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.timeObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class reminderBiz {

    public void afterSaveReminderRecord(Activity activity){
        Intent intent = new Intent();
        intent.setClass(activity, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_REMINDER);
        activity.startActivity(intent);
        activity.finish();
    }

    public String getManyTimeLab (int manyTime){
        if(manyTime == reminderConstant.GET_FREQUENCY_ONE){
            return "每日一次";
        }
        if (manyTime == reminderConstant.GET_FREQUENCY_TWO){
            return "每日二次";
        }
        if (manyTime == reminderConstant.GET_FREQUENCY_THREE){
            return "每日三次";
        }
        if(manyTime == reminderConstant.GET_FREQUENCY_FOUR){
            return "每日四次";
        }
        return "";
    }

    public List<reminderRecordStringId> getFrequencyDDL(){
        List<reminderRecordStringId> frequencyList = new ArrayList<reminderRecordStringId>();
        frequencyList.add(new reminderRecordStringId("一日一次", reminderConstant.GET_FREQUENCY_ONE));
        frequencyList.add(new reminderRecordStringId("一日二次",reminderConstant.GET_FREQUENCY_TWO));
        frequencyList.add(new reminderRecordStringId("一日三次", reminderConstant.GET_FREQUENCY_THREE));
        frequencyList.add(new reminderRecordStringId("一日四次", reminderConstant.GET_FREQUENCY_FOUR));
        return frequencyList;
    }

    public  void setReminder(Context context, String timeString, long drugId,long BroadcastNo){
        timeObject time = new timeObject(timeString);

        Calendar calendar = Calendar.getInstance();
        Calendar now = Calendar.getInstance();
        now.setTimeInMillis(System.currentTimeMillis());
        calendar.setTimeInMillis(System.currentTimeMillis());
        calendar.set(Calendar.HOUR_OF_DAY, time.getHour());
        calendar.set(Calendar.MINUTE, time.getMin());
        calendar.set(Calendar.SECOND, 0);

        if(now.after(calendar)){
            calendar.add(Calendar.DATE, 1);
        }

        //System.out.println("candy System.currentTimeMillis()="+System.currentTimeMillis());
        //System.out.println("candy time.getHour()=" + time.getHour());
        //System.out.println("candy time.getMin()="+time.getMin());
        //System.out.println("candy calendar=" + calendar.toString());
        // System.out.println("Candy add getApplicationContext="+getApplicationContext());

        AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent intent = new Intent(context, reminderReveiver.class);
        intent.putExtra("title", Long.toString(drugId));
        PendingIntent alarmIntent = PendingIntent.getBroadcast(context, (int)BroadcastNo , intent, 0);
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP, calendar.getTimeInMillis(), AlarmManager.INTERVAL_DAY, alarmIntent);
    }

    public void deleteReminder(Context context,String broadcastNo){
        if (broadcastNo!=null && !broadcastNo.isEmpty()){
            Intent intent = new Intent(context, reminderReveiver.class);
            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, Integer.parseInt(broadcastNo) , intent, 0);
            AlarmManager alarmMgr = (AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
            alarmMgr.cancel(alarmIntent);
        }
    }

}
