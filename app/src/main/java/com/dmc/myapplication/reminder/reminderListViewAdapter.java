package com.dmc.myapplication.reminder;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.tool.imageTool;

import java.util.List;

/**
 * Created by KwokSinMan on 9/3/2016.
 */
public class reminderListViewAdapter extends ArrayAdapter<reminder> {
    private final Activity activity;
    //private final List<reminderRecordStringId> drugName;
    //private final List<String> drugValue;
    //private final int[] drugIcon;
    private  List<reminder> reminderList;

    public reminderListViewAdapter(Activity activity, List<reminder> reminderList) {

        super(activity, R.layout.reminder_list, reminderList);
        this.activity=activity;
        this.reminderList = reminderList;
    }

    public View getView(int position, View view, ViewGroup parent) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View rowView=inflater.inflate(R.layout.reminder_list, null, true);

        TextView name = (TextView) rowView.findViewById(R.id.drugName);
        TextView value = (TextView) rowView.findViewById(R.id.drugNumber);
        ImageView icon = (ImageView) rowView.findViewById(R.id.drugIcon);

        name.setText(reminderList.get(position).getDrugName());
        value.setText(new reminderBiz().getManyTimeLab(reminderList.get(position).getManyTime()));
        //name.setText(drugName.get(position).toString());
        //value.setText(drugValue.get(position).toString());
        //icon.setImageResource(drugIcon[position]);
        //icon.setBackgroundResource(drugIcon[position]);
        //icon.setImageBitmap(drugIcon[position]);
        imageTool.setPic(icon,reminderList.get(position).getPhotoPath());
        return rowView;
    }


}
