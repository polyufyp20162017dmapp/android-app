package com.dmc.myapplication.reminder;

/**
 * Created by KwokSinMan on 17/3/2016.
 */
public class broadcastNumber {
    private long broadcastId;
    private long reminderId;

    public broadcastNumber() {
    }
    public broadcastNumber(long reminderId) {
        this.reminderId = reminderId;
    }
    public broadcastNumber(long broadcastId,long reminderId) {
        this.broadcastId = broadcastId;
        this.reminderId = reminderId;
    }

    public long getBroadcastId() {
        return broadcastId;
    }
    public void setBroadcastId(long broadcastId) {
        this.broadcastId = broadcastId;
    }
    public long getReminderId() {
        return reminderId;
    }
    public void setReminderId(long reminderId) {
        this.reminderId = reminderId;
    }

    public String toString(){
        return "broadcastId="+broadcastId+" reminderId="+reminderId;
    }
}
