package com.dmc.myapplication.reminder;

import android.app.Activity;
import android.content.Intent;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.dmc.myapplication.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KwokSinMan on 24/2/2016.
 */
public class reminderFragment extends Fragment {
    private reminderDAO reminderDAO;

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.reminder_main, container, false);


        reminderDAO = new reminderDAO(getActivity().getApplicationContext());
        TextView noReminderRecord = (TextView) v.findViewById(R.id.noReminderRecord);
        if (reminderDAO.getCount() == 0) {
            //reminderDAO.sample();
            noReminderRecord.setVisibility(View.VISIBLE);

        }else{
            noReminderRecord.setVisibility(View.GONE);
            List<reminder> reminderList = reminderDAO.getAll();
            //System.out.println("Candy reminderList.toString()="+reminderList.toString());
            //for(int i=0; i<reminderList.size(); i++){
            //   System.out.println("Candy reminderList="+reminderList.get(i).toString());
            //}
            reminderListViewAdapter adapter = new reminderListViewAdapter(getActivity(), reminderList);
            ListView listView = (ListView) v.findViewById(R.id.reminderList);
            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new reminderListOnItemClickListener(this.getActivity()));
        }
        //broadcastNumberDAO broadcastNumberDAO = new broadcastNumberDAO(getActivity().getApplicationContext());
        //if(broadcastNumberDAO.getCount() != 0 ){
        //    List<broadcastNumber> broadcastNumberList = broadcastNumberDAO.getAll();
        //    System.out.println("Candy broadcastNumberList.toString()="+broadcastNumberList.toString());
        //    for(int i=0; i<broadcastNumberList.size(); i++){
        //        System.out.println("Candy broadcastNumberList="+broadcastNumberList.get(i).toString());
        //    }
        //}

        return v;
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.reminder_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.reminder_menuItem_add) {
            Intent intent = new Intent();
            intent.setClass(getActivity(), reminderAddActivity.class);
            getActivity().startActivity(intent);
            getActivity().finish();
        }
        return true;
    }

    private class reminderListOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public reminderListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            ListView reminderList = (ListView) activity.findViewById(R.id.reminderList);
            reminder reminder = (reminder)reminderList.getItemAtPosition(position);
            //System.out.println("Candy reminderList.toString()=" + reminderList.toString());

            Intent intent = new Intent();
            intent.setClass(activity, reminderEditActivity.class);
            intent.putExtra("id", Long.toString(reminder.getId()));
            intent.putExtra("name", reminder.getDrugName());
            intent.putExtra("manyTime", Integer.toString(reminder.getManyTime()));
            intent.putExtra("time1", reminder.getTime1());
            intent.putExtra("time2", reminder.getTime2());
            intent.putExtra("time3", reminder.getTime3());
            intent.putExtra("time4", reminder.getTime4());
            intent.putExtra("remark", reminder.getRemark());
            intent.putExtra("photoPath", reminder.getPhotoPath());
            activity.startActivity(intent);
            activity.finish();
        }
    }


}

        /*
        int count =2;
        List<reminderRecordStringId> drugName = new ArrayList<reminderRecordStringId>();
        List<String> drugValue = new ArrayList<String>();
        int[] drugIcon = new int[count];

        for (int i=0; i<count; i++){
            drugName.add(new reminderRecordStringId("藥品名稱",i+1));
            drugValue.add("每日三次");
            drugIcon[i] = getActivity().getResources().getIdentifier("ic_local_see_black_48dp", "drawable", getActivity().getPackageName());
        }
        reminderListViewAdapter adapter = new reminderListViewAdapter(getActivity(),drugName,drugValue, drugIcon);
        ListView listView = (ListView) v.findViewById(R.id.reminderList);
        listView.setAdapter(adapter);
*/

        /*
        Button finishButton = (Button) v.findViewById(R.id.btnShowNotification);
        finishButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                reminderAsyncTask connect = new reminderAsyncTask(getActivity(), "test.php");
                connect.execute();
            }
        });
*/
