package com.dmc.myapplication.reminder;

import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Base64;
import android.widget.ImageView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.dmAppAsyncTask;
import com.dmc.myapplication.noInternetConnectionDialog;
import com.dmc.myapplication.serverNoResponseDialog;

import org.json.JSONArray;

import java.net.SocketTimeoutException;

/**
 * Created by KwokSinMan on 2/3/2016.
 */
public class reminderAsyncTask extends AsyncTask<String,Void,String> {

    private ProgressDialog progressBar;
    private String model;
    private Activity activity;

    public reminderAsyncTask(Activity activity, String model ) {
        this.activity = activity;
        this.model = model;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressBar = new ProgressDialog(activity);
        progressBar.setCancelable(false);
        progressBar.setTitle("載入中...");
        progressBar.setMessage("請稍候 !!");
        progressBar.show();
    }

    @Override
    protected String doInBackground(String... arg0) {
        String data="";
        try {
            //System.out.println("Candy has run=" + model);
            //get result  from server
            dmAppAsyncTask dmasyncTask = new dmAppAsyncTask(model);
            return dmasyncTask.getDataFromServer(data);

        }  catch (SocketTimeoutException e){
            e.printStackTrace();
            return dmAppAsyncTask.SERVER_SLEEP;
        }catch (Exception e) {
            e.printStackTrace();
            return dmAppAsyncTask.NO_INTERNET_CONNECTION;
        }
    }


    @Override
    protected void onPostExecute(String result){
        if (result!=null) {
            if(result.equals(dmAppAsyncTask.NO_INTERNET_CONNECTION)){
                progressBar.dismiss();
                noInternetConnectionDialog dialog = new noInternetConnectionDialog();
                dialog.show(this.activity.getFragmentManager(), "NoInternetConnectionDialog");
            }else if(result.equals(dmAppAsyncTask.SERVER_SLEEP)){
                progressBar.dismiss();
                serverNoResponseDialog dialog = new serverNoResponseDialog();
                dialog.show(this.activity.getFragmentManager(), "NoServerResponseDialog");
            }else {
                try{
                    JSONArray JA = new JSONArray(result);
                    //System.out.println("Candy 1 food_date=" + JA.getJSONObject(1).getString("food_date"));
                    //System.out.println("Candy 1 image=" + JA.getJSONObject(1).getString("image"));

                    byte[] image = Base64.decode(JA.getJSONObject(0).getString("image"), Base64.DEFAULT);
                    Bitmap bmp = BitmapFactory.decodeByteArray(image, 0, image.length);
                    ImageView imageView = (ImageView) activity.findViewById(R.id.image);
                    imageView.setImageBitmap(bmp);

                }catch (Exception e){
                    e.printStackTrace();
                }
                progressBar.dismiss();
            }
        }
    }

}
