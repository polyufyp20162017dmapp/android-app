package com.dmc.myapplication.reminder;

/**
 * Created by KwokSinMan on 10/3/2016.
 */
public class reminderConstant {
    public final static int GET_FREQUENCY_ONE = 1;
    public final static int GET_FREQUENCY_TWO = 2;
    public final static int GET_FREQUENCY_THREE = 3;
    public final static int GET_FREQUENCY_FOUR = 4;

    public final static int GET_ADD_REMINDER_IMAGE = 777;
    public final static int GET_ADD_REMINDER_IMAGE_FROM_GALLERY_CODE = 778;
    public final static int GET_EDIT_REMINDER_IMAGE = 779;
    public final static int GET_EDIT_REMINDER_IMAGE_FROM_GALLERY_CODE = 780;


}
