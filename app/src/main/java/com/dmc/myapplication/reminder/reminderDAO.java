package com.dmc.myapplication.reminder;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by KwokSinMan on 15/3/2016.
 */
public class reminderDAO {
    // 表格名稱
    public static final String TABLE_NAME = "MEDICATION_REMINDER_SETTING ";
    // 編號表格欄位名稱，固定不變
    public static final String KEY_ID = "_reminder_id";
    public static final String NAME_COLUMN = "DRUG_NAME";
    public static final String MANY_TIME_COLUMN = "MANY_TIME";
    public static final String TIME1_COLUMN = "TIME_1";
    public static final String TIME2_COLUMN = "TIME_2";
    public static final String TIME3_COLUMN = "TIME_3";
    public static final String TIME4_COLUMN = "TIME_4";
    public static final String BROADCAST1_NO = "BROADCAST_NUMBER_1";
    public static final String BROADCAST2_NO = "BROADCAST_NUMBER_2";
    public static final String BROADCAST3_NO = "BROADCAST_NUMBER_3";
    public static final String BROADCAST4_NO = "BROADCAST_NUMBER_4";
    public static final String REMARK = "REMARK";
    public static final String PHOTO_PATH = "PHOTO_PATH";


    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    NAME_COLUMN + " TEXT NOT NULL, " +
                    MANY_TIME_COLUMN + " INTEGER NOT NULL, " +
                    TIME1_COLUMN + " TEXT NOT NULL, " +
                    TIME2_COLUMN + " TEXT, " +
                    TIME3_COLUMN + " TEXT, " +
                    TIME4_COLUMN + " TEXT, " +
                    BROADCAST1_NO + " TEXT, " +
                    BROADCAST2_NO + " TEXT, " +
                    BROADCAST3_NO + " TEXT, " +
                    BROADCAST4_NO + " TEXT, " +
                    REMARK + " TEXT, " +
                    PHOTO_PATH + " TEXT)";

    private SQLiteDatabase db;

    public reminderDAO(Context context) {
        db = MyDBHelper.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public reminder insert(reminder reminder) {
        ContentValues cv = new ContentValues();
        cv.put(NAME_COLUMN, reminder.getDrugName());
        cv.put(MANY_TIME_COLUMN, reminder.getManyTime());
        cv.put(TIME1_COLUMN, reminder.getTime1());
        cv.put(TIME2_COLUMN, reminder.getTime2());
        cv.put(TIME3_COLUMN, reminder.getTime3());
        cv.put(TIME4_COLUMN, reminder.getTime4());
        cv.put(BROADCAST1_NO, reminder.getBroadcastNumber1());
        cv.put(BROADCAST2_NO, reminder.getBroadcastNumber2());
        cv.put(BROADCAST3_NO, reminder.getBroadcastNumber3());
        cv.put(BROADCAST4_NO, reminder.getBroadcastNumber4());
        cv.put(REMARK, reminder.getRemark());
        cv.put(PHOTO_PATH, reminder.getPhotoPath());
        long id = db.insert(TABLE_NAME, null, cv);
        reminder.setId(id);
        return reminder;
    }

    public boolean update(reminder reminder) {
        ContentValues cv = new ContentValues();
        cv.put(NAME_COLUMN, reminder.getDrugName());
        cv.put(MANY_TIME_COLUMN, reminder.getManyTime());
        cv.put(TIME1_COLUMN, reminder.getTime1());
        cv.put(TIME2_COLUMN, reminder.getTime2());
        cv.put(TIME3_COLUMN, reminder.getTime3());
        cv.put(TIME4_COLUMN, reminder.getTime4());
        cv.put(BROADCAST1_NO, reminder.getBroadcastNumber1());
        cv.put(BROADCAST2_NO, reminder.getBroadcastNumber2());
        cv.put(BROADCAST3_NO, reminder.getBroadcastNumber3());
        cv.put(BROADCAST4_NO, reminder.getBroadcastNumber4());
        cv.put(REMARK, reminder.getRemark());
        cv.put(PHOTO_PATH, reminder.getPhotoPath());

        String where = KEY_ID + "=" + reminder.getId();
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public boolean delete(long id){
        String where = KEY_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean deleteAll(){
        return db.delete(TABLE_NAME, null, null) > 0;
    }


    public List<reminder> getAll() {
        List<reminder> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    // 取得指定編號的資料物件
    public reminder get(long id) {
        reminder reminder = null;
        String where = KEY_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            reminder = getRecord(result);
        }
        result.close();
        return reminder;
    }

    // 把Cursor目前的資料包裝為物件
    public reminder getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        reminder result = new reminder();
        result.setId(cursor.getLong(0));
        result.setDrugName(cursor.getString(1));
        result.setManyTime(cursor.getInt(2));
        result.setTime1(cursor.getString(3));
        result.setTime2(cursor.getString(4));
        result.setTime3(cursor.getString(5));
        result.setTime4(cursor.getString(6));
        result.setBroadcastNumber1(cursor.getString(7));
        result.setBroadcastNumber2(cursor.getString(8));
        result.setBroadcastNumber3(cursor.getString(9));
        result.setBroadcastNumber4(cursor.getString(10));
        result.setRemark(cursor.getString(11));
        result.setPhotoPath(cursor.getString(12));
        // 回傳結果
        return result;
    }

    // 取得資料數量
    public int getCount() {
        int result = 0;
        Cursor cursor = db.rawQuery("SELECT COUNT(*) FROM " + TABLE_NAME, null);

        if (cursor.moveToNext()) {
            result = cursor.getInt(0);
        }
        return result;
    }

    public void sample() {
        reminder reminder1 = new reminder("藥物名稱一",4,"10:00","14:00","16:00","20:00","","","","","沒有","");
        reminder reminder2 = new reminder("藥物名稱二",2,"11:00","15:00","19:00","23:00","","","","","沒有","");
        insert(reminder1);
        insert(reminder2);
    }

}
