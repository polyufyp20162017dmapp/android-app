package com.dmc.myapplication.reminder;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v7.app.NotificationCompat;
import android.util.Log;
import android.widget.Toast;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.xiaomiDeviceSetting;
import com.dmc.myapplication.R;
import com.dmc.myapplication.blood.blood_add;
import com.dmc.myapplication.tool.imageTool;
import com.dmc.myapplication.xiaomi.BluetoothState;
import com.zhaoxiaodan.miband.ActionCallback;
import com.zhaoxiaodan.miband.MiBand;
import com.zhaoxiaodan.miband.listeners.NotifyListener;
import com.zhaoxiaodan.miband.model.VibrationMode;

import java.io.File;

public class reminderReveiver extends BroadcastReceiver {
    public reminderReveiver() {
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO: This method is called when the BroadcastReceiver is receiving
        // an Intent broadcast.
        //String title = intent.getStringExtra("title");
        //Toast.makeText(context, title, Toast.LENGTH_LONG).show();
        //System.out.println("has run");
        sendNotify(intent,context,0);
    }

    private void sendNotify(Intent intent, final Context context, long id) {
        // get Infromation
        long reminderId = Long.parseLong(intent.getStringExtra("title"));
        reminderDAO reminderDAO = new reminderDAO(context.getApplicationContext());
        reminder result = reminderDAO.get(reminderId);
        String photoPath = result.getPhotoPath();

        File file = new File(photoPath);
        boolean isPicture = (photoPath != null && photoPath.length() > 0 &&  file.exists());

        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        if(isPicture){
            //System.out.println("Candy haha 123 is Picture");
            Notification.Builder builder = new Notification.Builder(context);
            builder.setSmallIcon(android.R.drawable.star_big_on)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText("請按時服食 "+result.getDrugName()+" !!");
            // 建立震動效果，陣列中元素依序為停止、震動的時間，單位是毫秒
            //long[] vibrate_effect ={1000, 500, 1000, 400, 1000, 300, 1000, 200, 1000, 100};
            long[] vibrate_effect ={ 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000 };
            builder.setVibrate(vibrate_effect);

            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            builder.setSound(alarmSound); // 某些手機不支援 請加

            // 設定閃燈效果，參數依序為顏色、打開與關閉時間，單位是毫秒
            builder.setLights(Color.GREEN, 1000, 1000);
            Notification.BigPictureStyle bigPictureStyle = new Notification.BigPictureStyle();
            Bitmap bitmap = imageTool.decreaseImageSie(photoPath);
            bigPictureStyle.bigPicture(bitmap).setSummaryText(result.getDrugName());
            builder.setStyle(bigPictureStyle);
            manager.notify((int) reminderId, builder.build());

            if (BluetoothState.enabled(context) == true){


                if(BluetoothState.miband == null){
                    BluetoothDevice device = BluetoothState.getDevice(context);
                    System.out.println("BluetoothState.getDevice() == null? ==>" + String.valueOf(device));
                    if (device != null){
                        BluetoothState.miband = new MiBand(context);
                        BluetoothState.miband.connect(device, new ActionCallback() {
                            @Override
                            public void onSuccess(Object data) {
                                Log.d("onSuccess",
                                        "连接成功!!!");


                                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_CONNECTED)));
                                xiaomiDeviceSetting.close();

                                BluetoothState.miband.setDisconnectedListener(new NotifyListener() {
                                    @Override
                                    public void onNotify(byte[] data) {
                                        Log.d("onSuccess",
                                                "connection failed!!!");
                                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                        xiaomiDeviceSetting.close();

                                    }
                                });

                                if (BluetoothState.connectionState(context) == BluetoothState.STATE_CONNECTED){
                                    BluetoothState.miband.startVibration(VibrationMode.VIBRATION_10_TIMES_WITH_LED);
                                }
                            }

                            @Override
                            public void onFail(int errorCode, String msg) {
                                Log.d("onSuccess",
                                        "connection failed!!!");

                                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                xiaomiDeviceSetting.close();
                            }
                        });

                    }

                    /////
                    //BluetoothState.connect(context);
                }else{
                    if (BluetoothState.connectionState(context) == BluetoothState.STATE_CONNECTED){
                        BluetoothState.miband.startVibration(VibrationMode.VIBRATION_10_TIMES_WITH_LED);
                    }
                }


            }



        }else{
           // System.out.println("Candy haha 123 is not icture");
            NotificationCompat.Builder builder =  new NotificationCompat.Builder(context);


            // set pop message
            builder.setSmallIcon(android.R.drawable.star_big_on)
                    .setWhen(System.currentTimeMillis())
                    .setContentTitle(context.getString(R.string.app_name))
                    .setContentText("請按時服食 "+result.getDrugName()+" !!");


            long[] vibrate_effect ={ 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000 };
            builder.setVibrate(vibrate_effect);

            Uri alarmSound = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

            builder.setSound(alarmSound);

            builder.setLights(Color.GREEN, 1000, 1000);
            manager.notify((int) reminderId, builder.build());

            if (BluetoothState.enabled(context) == true){


                if(BluetoothState.miband == null){
                    BluetoothDevice device = BluetoothState.getDevice(context);
                    System.out.println("BluetoothState.getDevice() == null? ==>" + String.valueOf(device));
                    if (device != null){
                        BluetoothState.miband = new MiBand(context);
                        BluetoothState.miband.connect(device, new ActionCallback() {
                            @Override
                            public void onSuccess(Object data) {
                                Log.d("onSuccess",
                                        "连接成功!!!");


                                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_CONNECTED)));
                                xiaomiDeviceSetting.close();

                                BluetoothState.miband.setDisconnectedListener(new NotifyListener() {
                                    @Override
                                    public void onNotify(byte[] data) {
                                        Log.d("onSuccess",
                                                "connection failed!!!");
                                        xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                                        xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                        xiaomiDeviceSetting.close();

                                    }
                                });

                                if (BluetoothState.connectionState(context) == BluetoothState.STATE_CONNECTED){
                                    BluetoothState.miband.startVibration(VibrationMode.VIBRATION_10_TIMES_WITH_LED);
                                }
                            }

                            @Override
                            public void onFail(int errorCode, String msg) {
                                Log.d("onSuccess",
                                        "connection failed!!!");

                                xiaomiDeviceSetting xiaomiDeviceSetting = new xiaomiDeviceSetting(context);
                                xiaomiDeviceSetting.update(xiaomiDeviceSetting.new xiaomiDeviceSetting_class("connectionState", String.valueOf(BluetoothState.STATE_NOT_CONNECTED)));
                                xiaomiDeviceSetting.close();
                            }
                        });

                    }

                    /////
                    //BluetoothState.connect(context);
                }else{
                    if (BluetoothState.connectionState(context) == BluetoothState.STATE_CONNECTED){
                        BluetoothState.miband.startVibration(VibrationMode.VIBRATION_10_TIMES_WITH_LED);
                    }
                }


            }



        }

    }
}
