package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.tool.foodCategoriesDB;
import com.dmc.myapplication.tool.localDBHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 28/12/2016.
 */
public class foodCategories {
    public static final String TABLE_NAME = "FOOD_CATEGORIES";

    public static final String FOOD_CATE_ID = "FOOD_CATE_ID";
    public static final String FOOD_CATE_NAME = "FOOD_CATE_NAME";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            FOOD_CATE_ID + " INTEGER PRIMARY KEY, " +
            FOOD_CATE_NAME + " TEXT)";

    private static SQLiteDatabase db;

    public foodCategories(Context context) {
        db = foodCategoriesDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public foodCategories_class insert(foodCategories_class BR) {
        ContentValues cv = new ContentValues();
        cv.put(FOOD_CATE_ID, BR.getFOOD_CATE_ID());
        cv.put(FOOD_CATE_NAME, BR.getFOOD_CATE_NAME());
        long id = db.insert(TABLE_NAME, null, cv);
        //db.close();
        return BR;
    }

    public foodCategories_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        foodCategories_class result = new foodCategories_class(
                cursor.getInt(0),
                cursor.getString(1)
        );

        db.close();

        // 回傳結果
        return result;
    }

    public foodCategories_class get(long id) {
        foodCategories_class BR = null;
        String where = FOOD_CATE_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAll() {
        List<HashMap<String, String>> BR = new ArrayList<>();
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (result.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            foodCategories_class br = getRecord(result);
            newL.put("FOOD_CATE_ID", String.valueOf(br.getFOOD_CATE_ID()));
            newL.put("FOOD_CATE_NAME", br.getFOOD_CATE_NAME());
            BR.add(newL);
            br = null;
            newL = null;
        }

        result.close();
        return BR;
    }

    public boolean delete(long id){
        String where = FOOD_CATE_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean deleteAll(){
        return db.delete(TABLE_NAME, null, null) > 0;
    }




    public class foodCategories_class{
        private Integer FOOD_CATE_ID;
        private String FOOD_CATE_NAME;

        public foodCategories_class(Integer FOOD_CATE_ID, String FOOD_CATE_NAME){
            this.FOOD_CATE_ID = FOOD_CATE_ID;
            this.FOOD_CATE_NAME = FOOD_CATE_NAME;
        }

        public Integer getFOOD_CATE_ID(){
            return FOOD_CATE_ID;
        }

        public String getFOOD_CATE_NAME(){
            return FOOD_CATE_NAME;
        }
    }


}
