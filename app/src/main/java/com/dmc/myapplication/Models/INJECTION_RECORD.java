package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.tool.INJECTION_RECORDDB;
import com.dmc.myapplication.tool.localDBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 11/10/2016.
 */
public class INJECTION_RECORD {
    public static final String TABLE_NAME = "INJECTION_RECORD";

    public static final String INJECTION_RECORD_ID = "INJECTION_RECORD_ID";

    // 其它表格欄位名稱
    public static final String USER_ID = "USER_ID";
    public static final String INJECTION_DATE = "INJECTION_DATE";
    public static final String INJECTION_TIME = "INJECTION_TIME";
    public static final String INJECTION_NAME = "INJECTION_NAME";
    public static final String INJECTION_VALUE = "INJECTION_VALUE";
    public static final String create_datetime = "create_datetime";
    public static final String edit_datetime = "edit_datetime";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    INJECTION_RECORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    USER_ID + " INTEGER NOT NULL, " +
                        INJECTION_DATE + " TEXT NULL, " +
                    INJECTION_TIME + " TEXT NULL, " +
                    INJECTION_NAME + " TEXT NULL, " +
                    INJECTION_VALUE + " INTEGER, " +
                    create_datetime + " TEXT NOT NULL, " +
                    edit_datetime + " TEXT NOT NULL)";

    private static SQLiteDatabase db;

    // 建構子，一般的應用都不需要修改
    public INJECTION_RECORD(Context context) {
        db = INJECTION_RECORDDB.getDatabase(context);
    }

    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        db.close();
    }


    public static INJECTION_RECORD_class insert(INJECTION_RECORD_class BR){

        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(INJECTION_DATE, dateToDatabaseDate(BR.getINJECTION_DATE()));
        cv.put(INJECTION_TIME, BR.getINJECTION_TIME());
        cv.put(INJECTION_NAME, BR.getINJECTION_NAME());
        cv.put(INJECTION_VALUE, BR.getINJECTION_VALUE());
        if (BR.create_datetime == null || BR.create_datetime == ""){
            cv.put(create_datetime, currentTimestamp);
            cv.put(edit_datetime, currentTimestamp);
        }else{
            cv.put(create_datetime, BR.create_datetime);
            cv.put(edit_datetime, BR.edit_datetime);
        }

        long id = db.insert(TABLE_NAME, null, cv);

        BR.setINJECTION_RECORD_ID((int) id);
        return BR;
    }

    private static String dateToDatabaseDate(String date){
        String[] dateObjs = date.split("-");
        String year = dateObjs[0];
        String Month = dateObjs[1];
        String Day = dateObjs[2];

        if (Month.length() == 1){
            Month = "0"+Month;
        }

        if (Day.length() == 1){
            Day = "0"+Day;
        }

        return year+"-"+Month+"-"+Day;

    }

    public static boolean update(INJECTION_RECORD_class BR) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(INJECTION_DATE, dateToDatabaseDate(BR.getINJECTION_DATE()));
        cv.put(INJECTION_TIME, BR.getINJECTION_TIME());
        cv.put(INJECTION_NAME, BR.getINJECTION_NAME());
        cv.put(INJECTION_VALUE, BR.getINJECTION_VALUE());
        cv.put(edit_datetime, currentTimestamp);
        String where = INJECTION_RECORD_ID + "=" + BR.getINJECTION_RECORD_ID();
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public boolean delete(long id){
        String where = INJECTION_RECORD_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public INJECTION_RECORD_class get(long id) {
        INJECTION_RECORD_class BR = null;
        String where = INJECTION_RECORD_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public INJECTION_RECORD_class getByCreateDatetime(String createDatetime) {
        INJECTION_RECORD_class BR = null;
        String where = create_datetime + "=" + createDatetime;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime(){

        List<HashMap<String, String>> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            INJECTION_RECORD_class br = getRecord(cursor);
            newL.put(create_datetime, br.create_datetime);
            newL.put(edit_datetime, br.edit_datetime);
            result.add(newL);
            br = null;
            newL = null;
        }

        cursor.close();
        return result;
    }

    public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList){
        List<HashMap<String, String>> result = new ArrayList<>();
        for (int x = 0; x < requestedList.size(); x++){
            INJECTION_RECORD_class objectBR = getByCreateDatetime(requestedList.get(x));
            result.add(objectBR.toHashMap());
        }

        return result;
    }

    public List<INJECTION_RECORD_class> getAllrecordbyrecorddate(String recordDate){

        List<INJECTION_RECORD_class> result = new ArrayList<>();
        String where = INJECTION_DATE + "='" + dateToDatabaseDate(recordDate) +"'";

        Cursor cursor = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    public List<INJECTION_RECORD_class> getAll(){

        List<INJECTION_RECORD_class> result = new ArrayList<>();

        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    public INJECTION_RECORD_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        INJECTION_RECORD_class result = new INJECTION_RECORD_class(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getInt(5)
                );
        result.setCreate_datetime(cursor.getString(6));
        result.setEdit_datetime(cursor.getString(7));

        /*result.setId(cursor.getLong(0));
        result.setDrugName(cursor.getString(1));
        result.setManyTime(cursor.getInt(2));
        result.setTime1(cursor.getString(3));
        result.setTime2(cursor.getString(4));
        result.setTime3(cursor.getString(5));
        result.setTime4(cursor.getString(6));
        result.setBroadcastNumber1(cursor.getString(7));
        result.setBroadcastNumber2(cursor.getString(8));
        result.setBroadcastNumber3(cursor.getString(9));
        result.setBroadcastNumber4(cursor.getString(10));
        result.setRemark(cursor.getString(11));
        result.setPhotoPath(cursor.getString(12));*/

        // 回傳結果
        return result;
    }



    public class INJECTION_RECORD_class{
        Integer INJECTION_RECORD_ID;
        Integer USER_ID;
        String INJECTION_DATE;
        String INJECTION_TIME;
        String INJECTION_NAME;
        Integer INJECTION_VALUE;
        String create_datetime;
        String edit_datetime;

        public INJECTION_RECORD_class(Integer INJECTION_RECORD_ID, Integer USER_ID, String INJECTION_DATE, String INJECTION_TIME, String INJECTION_NAME, Integer INJECTION_VALUE) {
            this.INJECTION_RECORD_ID = INJECTION_RECORD_ID;
            this.USER_ID = USER_ID;
            this.INJECTION_DATE = dateToDatabaseDate(INJECTION_DATE);
            this.INJECTION_TIME = INJECTION_TIME;
            this.INJECTION_NAME = INJECTION_NAME;
            this.INJECTION_VALUE = INJECTION_VALUE;
        }

        public Integer getINJECTION_RECORD_ID() {
            return INJECTION_RECORD_ID;
        }

        public void setINJECTION_RECORD_ID(Integer INJECTION_RECORD_ID) {
            this.INJECTION_RECORD_ID = INJECTION_RECORD_ID;
        }

        public Integer getUSER_ID() {
            return USER_ID;
        }

        public void setUSER_ID(Integer USER_ID) {
            this.USER_ID = USER_ID;
        }

        public String getINJECTION_DATE() {
            return INJECTION_DATE;
        }

        public void setINJECTION_DATE(String INJECTION_DATE) {
            this.INJECTION_DATE = dateToDatabaseDate(INJECTION_DATE);
        }

        public String getINJECTION_TIME() {
            return INJECTION_TIME;
        }

        public void setINJECTION_TIME(String INJECTION_TIME) {
            this.INJECTION_TIME = INJECTION_TIME;
        }

        public String getINJECTION_NAME() {
            return INJECTION_NAME;
        }

        public void setINJECTION_NAME(String INJECTION_NAME) {
            this.INJECTION_NAME = INJECTION_NAME;
        }

        public Integer getINJECTION_VALUE() {
            return INJECTION_VALUE;
        }

        public void setINJECTION_VALUE(Integer INJECTION_VALUE) {
            this.INJECTION_VALUE = INJECTION_VALUE;
        }

        public String getCreate_datetime() {
            return create_datetime;
        }

        public void setCreate_datetime(String create_datetime) {
            this.create_datetime = create_datetime;
        }

        public String getEdit_datetime() {
            return edit_datetime;
        }

        public void setEdit_datetime(String edit_datetime) {
            this.edit_datetime = edit_datetime;
        }

        public HashMap<String, String> toHashMap(){
            HashMap<String, String> output = new HashMap<>();
            output.put("INJECTION_RECORD_ID", String.valueOf(INJECTION_RECORD_ID));
            output.put("USER_ID", String.valueOf(USER_ID));
            output.put("INJECTION_DATE", String.valueOf(INJECTION_DATE));
            output.put("INJECTION_TIME", String.valueOf(INJECTION_TIME));
            output.put("INJECTION_NAME", String.valueOf(INJECTION_NAME));
            output.put("INJECTION_VALUE", String.valueOf(INJECTION_VALUE));
            output.put("create_datetime", String.valueOf(create_datetime));
            output.put("edit_datetime", String.valueOf(edit_datetime));
            return output;
        }
    }


}
