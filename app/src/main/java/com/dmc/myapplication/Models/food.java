package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.renderscript.Double2;
import com.dmc.myapplication.tool.foodDB;
import com.dmc.myapplication.tool.localDBHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 28/12/2016.
 */
public class food {
    public static final String TABLE_NAME = "FOOD";

    public static final String FOOD_ID = "FOOD_ID";
    public static final String FOOD_NAME = "FOOD_NAME";
    public static final String FOOD_UNIT = "FOOD_UNIT";
    public static final String FOOD_GRAM = "FOOD_GRAM";
    public static final String FOOD_CALORIE = "FOOD_CALORIE";
    public static final String FOOD_CARBOHYDRATE = "FOOD_CARBOHYDRATE";
    public static final String FOOD_PROTEIN = "FOOD_PROTEIN";
    public static final String FOOD_FAT = "FOOD_FAT";
    public static final String FOOD_SUGAR = "FOOD_SUGAR";
    public static final String FOOD_FIBER = "FOOD_FIBER";
    public static final String FOOD_CHOLESTEROL = "FOOD_CHOLESTEROL";
    public static final String FOOD_SODIUM = "FOOD_SODIUM";
    public static final String FOOD_SUB_CATE_ID = "FOOD_SUB_CATE_ID";
    public static final String PHOTO_PATH = "PHOTO_PATH";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            FOOD_ID + " INTEGER PRIMARY KEY, " +
            FOOD_NAME + " TEXT NOT NULL, " +
            FOOD_UNIT + " TEXT NOT NULL, " +
            FOOD_GRAM + " REAL NOT NULL, " +
            FOOD_CALORIE + " REAL NOT NULL, " +
            FOOD_CARBOHYDRATE + " REAL NOT NULL, " +
            FOOD_PROTEIN + " REAL NOT NULL, " +
            FOOD_FAT + " REAL NOT NULL, " +
            FOOD_SUGAR + " REAL NOT NULL, " +
            FOOD_FIBER + " REAL NOT NULL, " +
            FOOD_CHOLESTEROL + " REAL NOT NULL, " +
            FOOD_SODIUM + " REAL NOT NULL, " +
            FOOD_SUB_CATE_ID + " INTEGER, " +
            PHOTO_PATH + " TEXT)";

    private static SQLiteDatabase db;

    public food(Context context) {
        db = foodDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public food_class get(long id) {
        food_class BR = null;
        String where = FOOD_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllByFoodSubCateID(long id) {
        //food_class BR = null;
        String where = FOOD_SUB_CATE_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);
        List<HashMap<String, String>> out = new ArrayList<>();

        while (result.moveToNext()){
            out.add(getRecord(result).toHashMap());
        }


        result.close();
        return out;
    }

    public List<HashMap<String, String>> search(String keyword) {
       // food_class BR = null;

        Cursor result = db.rawQuery("select * from "+TABLE_NAME+" where "+FOOD_NAME+" like '%"+keyword+"%'", null);

        List<HashMap<String, String>> out = new ArrayList<>();

        while (result.moveToNext()){
            out.add(getRecord(result).toHashMap());
        }
        result.close();
        return out;
    }

    public food_class insert(food_class BR) {
        ContentValues cv = new ContentValues();
        cv.put(FOOD_ID, BR.getFOOD_ID());
        cv.put(FOOD_NAME, BR.getFOOD_NAME());
        cv.put(FOOD_UNIT, BR.getFOOD_UNIT());
        cv.put(FOOD_GRAM, BR.getFOOD_GRAM());
        cv.put(FOOD_CALORIE, BR.getFOOD_CALORIE());
        cv.put(FOOD_CARBOHYDRATE, BR.getFOOD_CARBOHYDRATE());
        cv.put(FOOD_PROTEIN, BR.getFOOD_PROTEIN());
        cv.put(FOOD_FAT, BR.getFOOD_FAT());
        cv.put(FOOD_SUGAR, BR.getFOOD_SUGAR());
        cv.put(FOOD_FIBER, BR.getFOOD_FIBER());
        cv.put(FOOD_CHOLESTEROL, BR.getFOOD_CHOLESTEROL());
        cv.put(FOOD_SODIUM, BR.getFOOD_SODIUM());
        cv.put(FOOD_SUB_CATE_ID, BR.getFOOD_SUB_CATE_ID());
        cv.put(PHOTO_PATH, BR.getPHOTO_PATH());

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public boolean delete(long id){
        String where = FOOD_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean deleteAll(){
        return db.delete(TABLE_NAME, null, null) > 0;
    }

    public food_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        food_class result = new food_class(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getString(2),
                cursor.getDouble(3),
                cursor.getDouble(4),
                cursor.getDouble(5),
                cursor.getDouble(6),
                cursor.getDouble(7),
                cursor.getDouble(8),
                cursor.getDouble(9),
                cursor.getDouble(10),
                cursor.getDouble(11),
                cursor.getInt(12),
                cursor.getString(13)
        );

        // 回傳結果
        return result;
    }

    public class food_class
    {
        private Integer FOOD_ID;
        private String FOOD_NAME;
        private String FOOD_UNIT;
        private Double FOOD_GRAM;
        private Double FOOD_CALORIE;
        private Double FOOD_CARBOHYDRATE;
        private Double FOOD_PROTEIN;
        private Double FOOD_FAT;
        private Double FOOD_SUGAR;
        private Double FOOD_FIBER;
        private Double FOOD_CHOLESTEROL;
        private Double FOOD_SODIUM;
        private Integer FOOD_SUB_CATE_ID;
        private String PHOTO_PATH;

        public food_class(Integer iFOOD_ID, String iFOOD_NAME, String iFOOD_UNIT, Double iFOOD_GRAM, Double iFOOD_CALORIE, Double iFOOD_CARBOHYDRATE, Double iFOOD_PROTEIN, Double iFOOD_FAT, Double iFOOD_SUGAR, Double iFOOD_FIBER, Double iFOOD_CHOLESTEROL, Double iFOOD_SODIUM, Integer iFOOD_SUB_CATE_ID, String iPHOTO_PATH){
            FOOD_ID = iFOOD_ID;
            FOOD_NAME = iFOOD_NAME;
            FOOD_UNIT = iFOOD_UNIT;
            FOOD_GRAM = iFOOD_GRAM;
            FOOD_CALORIE = iFOOD_CALORIE;
            FOOD_CARBOHYDRATE = iFOOD_CARBOHYDRATE;
            FOOD_PROTEIN = iFOOD_PROTEIN;
            FOOD_FAT = iFOOD_FAT;
            FOOD_SUGAR = iFOOD_SUGAR;
            FOOD_FIBER = iFOOD_FIBER;
            FOOD_CHOLESTEROL = iFOOD_CHOLESTEROL;
            FOOD_SODIUM = iFOOD_SODIUM;
            FOOD_SUB_CATE_ID = iFOOD_SUB_CATE_ID;
            PHOTO_PATH = iPHOTO_PATH;
        }

        public Integer getFOOD_ID(){
            return FOOD_ID;
        }

        public String getFOOD_NAME(){
            return FOOD_NAME;
        }

        public String getFOOD_UNIT(){
            return FOOD_UNIT;
        }

        public Double getFOOD_GRAM(){
            return FOOD_GRAM;
        }

        public Double getFOOD_CALORIE(){
            return FOOD_CALORIE;
        }

        public Double getFOOD_CARBOHYDRATE(){
            return FOOD_CARBOHYDRATE;
        }

        public Double getFOOD_PROTEIN(){
            return FOOD_PROTEIN;
        }

        public Double getFOOD_FAT(){
            return FOOD_FAT;
        }

        public Double getFOOD_SUGAR(){
            return FOOD_SUGAR;
        }

        public Double getFOOD_FIBER(){
            return FOOD_FIBER;
        }

        public Double getFOOD_CHOLESTEROL(){
            return FOOD_CHOLESTEROL;
        }

        public Double getFOOD_SODIUM(){
            return FOOD_SODIUM;
        }

        public String getPHOTO_PATH(){
            return PHOTO_PATH;
        }

        public Integer getFOOD_SUB_CATE_ID(){
            return FOOD_SUB_CATE_ID;
        }

        public HashMap<String, String> toHashMap(){
            HashMap<String, String> out = new HashMap<>();
            out.put("FOOD_ID", String.valueOf(this.FOOD_ID));
            out.put("FOOD_NAME", this.FOOD_NAME);
            out.put("FOOD_UNIT", this.FOOD_UNIT);
            out.put("FOOD_GRAM", String.valueOf(this.FOOD_GRAM));
            out.put("FOOD_CALORIE", String.valueOf(this.FOOD_CALORIE));
            out.put("FOOD_CARBOHYDRATE", String.valueOf(this.FOOD_CARBOHYDRATE));
            out.put("FOOD_PROTEIN", String.valueOf(this.FOOD_PROTEIN));
            out.put("FOOD_FAT", String.valueOf(this.FOOD_FAT));
            out.put("FOOD_SUGAR", String.valueOf(this.FOOD_SUGAR));
            out.put("FOOD_FIBER", String.valueOf(this.FOOD_FIBER));
            out.put("FOOD_CHOLESTEROL", String.valueOf(this.FOOD_CHOLESTEROL));
            out.put("FOOD_SODIUM", String.valueOf(this.FOOD_SODIUM));
            out.put("FOOD_SUB_CATE_ID", String.valueOf(this.FOOD_SUB_CATE_ID));
            out.put("PHOTO_PATH", this.PHOTO_PATH);
            return out;
        }
    }


}
