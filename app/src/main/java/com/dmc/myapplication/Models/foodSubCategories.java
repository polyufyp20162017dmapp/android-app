package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.tool.foodSubCategoriesDB;
import com.dmc.myapplication.tool.localDBHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 28/12/2016.
 */
public class foodSubCategories {
    public static final String TABLE_NAME = "FOOD_SUB_CATEGORIES";

    public static final String FOOD_SUB_CATE_ID = "FOOD_SUB_CATE_ID";
    public static final String FOOD_SUB_CATE_NAME = "FOOD_SUB_CATE_NAME";
    public static final String FOOD_CATE_ID = "FOOD_CATE_ID";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            FOOD_SUB_CATE_ID + " INTEGER PRIMARY KEY, " +
            FOOD_SUB_CATE_NAME + " TEXT, " +
            FOOD_CATE_ID + " INTEGER)";

    private static SQLiteDatabase db;

    public foodSubCategories(Context context) {
        db = foodSubCategoriesDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }


    public foodSubCategories_class insert(foodSubCategories_class BR) {
        ContentValues cv = new ContentValues();
        cv.put(FOOD_CATE_ID, BR.getFOOD_CATE_ID());
        cv.put(FOOD_SUB_CATE_NAME, BR.getFOOD_SUB_CATE_NAME());
        cv.put(FOOD_SUB_CATE_ID, BR.getFOOD_SUB_CATE_ID());
        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public foodSubCategories_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        foodSubCategories_class result = new foodSubCategories_class(
                cursor.getInt(0),
                cursor.getString(1),
                cursor.getInt(2)
        );

        // 回傳結果
        return result;
    }

    public foodSubCategories_class get(long id) {
        foodSubCategories_class BR = null;
        String where = FOOD_SUB_CATE_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllByFoodCateKey(long id) {
        List<HashMap<String, String>> BR = new ArrayList<>();
        String where = FOOD_CATE_ID+ "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        while (result.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            foodSubCategories_class br = getRecord(result);
            newL.put("FOOD_SUB_CATE_ID", String.valueOf(br.getFOOD_SUB_CATE_ID()));
            newL.put("FOOD_SUB_CATE_NAME", br.getFOOD_SUB_CATE_NAME());
            newL.put("FOOD_CATE_ID", String.valueOf(br.getFOOD_CATE_ID()));
            BR.add(newL);
            br = null;
            newL = null;
        }

        result.close();
        return BR;
    }

    public boolean delete(long id){
        String where = FOOD_CATE_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean deleteAll(){
        return db.delete(TABLE_NAME, null, null) > 0;
    }

    public List<HashMap<String, String>> getAll() {
        List<HashMap<String, String>> BR = new ArrayList<>();
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (result.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            foodSubCategories_class br = getRecord(result);
            newL.put("FOOD_SUB_CATE_ID", String.valueOf(br.getFOOD_SUB_CATE_ID()));
            newL.put("FOOD_SUB_CATE_NAME", br.getFOOD_SUB_CATE_NAME());
            newL.put("FOOD_CATE_ID", String.valueOf(br.getFOOD_CATE_ID()));
            BR.add(newL);
            br = null;
            newL = null;
        }

        result.close();
        return BR;
    }

    public class foodSubCategories_class{
        private Integer FOOD_SUB_CATE_ID;
        private String FOOD_SUB_CATE_NAME;
        private Integer FOOD_CATE_ID;

        public foodSubCategories_class(Integer FOOD_SUB_CATE_ID, String FOOD_SUB_CATE_NAME, Integer FOOD_CATE_ID){
            this.FOOD_CATE_ID = FOOD_CATE_ID;
            this.FOOD_SUB_CATE_ID = FOOD_SUB_CATE_ID;
            this.FOOD_SUB_CATE_NAME = FOOD_SUB_CATE_NAME;
        }

        public Integer getFOOD_CATE_ID(){
            return FOOD_CATE_ID;
        }

        public String getFOOD_SUB_CATE_NAME(){
            return FOOD_SUB_CATE_NAME;
        }

        public Integer getFOOD_SUB_CATE_ID(){
            return FOOD_SUB_CATE_ID;
        }
    }

}
