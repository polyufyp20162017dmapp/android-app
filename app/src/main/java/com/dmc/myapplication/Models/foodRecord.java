package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.dmc.myapplication.tool.foodRecordDB;
import com.dmc.myapplication.tool.localDBHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 28/12/2016.
 */
public class foodRecord {
    public static final String TABLE_NAME = "FOOD_RECORD";

    public static final String FOOD_RECORD_ID = "FOOD_RECORD_ID";
    public static final String USER_ID = "USER_ID";
    public static final String FOOD_DATE = "FOOD_DATE";
    public static final String FOOD_TIME = "FOOD_TIME";
    public static final String FOOD_SESSION = "FOOD_SESSION";
    public static final String FOOD_PLACE = "FOOD_PLACE";
    public static final String FOOD_ID = "FOOD_ID";
    public static final String FOOD_QUANTITY = "FOOD_QUANTITY";
    public static final String FOOD_CALORIE = "FOOD_CALORIE";
    public static final String FOOD_CARBOHYDRATE = "FOOD_CARBOHYDRATE";
    public static final String FOOD_PROTEIN = "FOOD_PROTEIN";
    public static final String FOOD_FAT = "FOOD_FAT";
    public static final String FOOD_NAME = "FOOD_NAME";
    public static final String FOOD_UNIT = "FOOD_UNIT";
    public static final String PHOTO_PATH = "PHOTO_PATH";
    public static final String create_datetime = "create_datetime";
    public static final String edit_datetime = "edit_datetime";

    public static final String CREATE_TABLE = "CREATE TABLE " + TABLE_NAME + " (" +
            FOOD_RECORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
            USER_ID + " INTEGER NOT NULL, " +
            FOOD_DATE + " TEXT NOT NULL, " +
            FOOD_TIME + " TEXT NOT NULL, " +
            FOOD_SESSION + " TEXT NULL, " +
            FOOD_PLACE + " TEXT NULL, " +
            FOOD_ID + " INTEGER NULL, " +
            FOOD_QUANTITY + " INTEGER NULL, " +
            FOOD_CALORIE + " REAL NULL, " +
            FOOD_CARBOHYDRATE + " REAL NULL, " +
            FOOD_PROTEIN + " REAL NULL, " +
            FOOD_FAT + " REAL NULL, " +
            FOOD_NAME + " TEXT NULL, " +
            FOOD_UNIT + " TEXT NULL, " +
            PHOTO_PATH + " TEXT NULL, " +
            create_datetime + " TEXT NOT NULL, " +
            edit_datetime + " TEXT NOT NULL)";

    private static SQLiteDatabase db;

    public foodRecord_class get(long id) {
        foodRecord_class BR = null;
        String where = FOOD_RECORD_ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllByFoodDate(String foodDate) {
        List<HashMap<String, String>> out = new ArrayList<>();
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (result.moveToNext()) {
            foodRecord_class obj = getRecord(result);

            if (obj.getFOOD_DATE().equals(foodDate)) {
                out.add(obj.toHashMap());
            }
        }

        result.close();
        return out;
    }

    public List<foodRecord_class> getAll() {
        List<foodRecord_class> out = new ArrayList<>();
        Cursor result = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (result.moveToNext()) {
            out.add(getRecord(result));
        }

        Collections.sort(out,
                new Comparator<foodRecord_class>() {
                    public int compare(foodRecord_class o1, foodRecord_class o2) {
                        SimpleDateFormat format1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        try {

                            Date a = format1.parse(o1.getFOOD_DATE() + " " + o1.getFOOD_TIME());
                            Date b = format1.parse(o2.getFOOD_DATE() + " " + o2.getFOOD_TIME());
                            //System.out.println(o1.getFOOD_DATE() + " " + o1.getFOOD_TIME());
                            //System.out.println(a.toString());

                            //System.out.println(o2.getFOOD_DATE() + " " + o2.getFOOD_TIME());
                            //System.out.println(b.toString());


                            return b.compareTo(a);
                        } catch (Exception e) {
                            return 0;
                        }
                    }
                });

        result.close();

        for (foodRecord_class a :
                out) {
            System.out.println("[" + a.getFOOD_DATE() + " " + a.getFOOD_TIME() + "]");
        }
        return out;
    }

    public foodRecord_class getLastRecord() {
        List<foodRecord_class> list = getAll();
        return list.get(0);
    }

    public foodRecord_class insert(foodRecord_class BR) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(FOOD_DATE, BR.getFOOD_DATE());
        cv.put(FOOD_TIME, BR.getFOOD_TIME());
        cv.put(FOOD_SESSION, BR.getFOOD_SESSION());
        cv.put(FOOD_PLACE, BR.getFOOD_PLACE());
        cv.put(FOOD_ID, BR.getFOOD_ID());
        cv.put(FOOD_QUANTITY, BR.getFOOD_QUANTITY());
        cv.put(FOOD_CALORIE, BR.getFOOD_CALORIE());
        cv.put(FOOD_CARBOHYDRATE, BR.getFOOD_CARBOHYDRATE());
        cv.put(FOOD_PROTEIN, BR.getFOOD_PROTEIN());
        cv.put(FOOD_FAT, BR.getFOOD_FAT());
        cv.put(FOOD_NAME, BR.getFOOD_NAME());
        cv.put(FOOD_UNIT, BR.getFOOD_UNIT());
        cv.put(PHOTO_PATH, BR.getPHOTO_PATH());

        if (BR.create_datetime == null || BR.create_datetime == "") {
            cv.put("create_datetime", currentTimestamp);
            cv.put("edit_datetime", currentTimestamp);
        } else {
            cv.put("create_datetime", BR.create_datetime);
            cv.put("edit_datetime", BR.edit_datetime);
        }

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public Boolean update(foodRecord_class BR) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(FOOD_DATE, BR.getFOOD_DATE());
        cv.put(FOOD_TIME, BR.getFOOD_TIME());
        cv.put(FOOD_SESSION, BR.getFOOD_SESSION());
        cv.put(FOOD_PLACE, BR.getFOOD_PLACE());
        cv.put(FOOD_ID, BR.getFOOD_ID());
        cv.put(FOOD_QUANTITY, BR.getFOOD_QUANTITY());
        cv.put(FOOD_CALORIE, BR.getFOOD_CALORIE());
        cv.put(FOOD_CARBOHYDRATE, BR.getFOOD_CARBOHYDRATE());
        cv.put(FOOD_PROTEIN, BR.getFOOD_PROTEIN());
        cv.put(FOOD_FAT, BR.getFOOD_FAT());
        cv.put(FOOD_NAME, BR.getFOOD_NAME());
        cv.put(FOOD_UNIT, BR.getFOOD_UNIT());
        cv.put(PHOTO_PATH, BR.getPHOTO_PATH());
        cv.put(edit_datetime, currentTimestamp);

        String where = FOOD_RECORD_ID + "=" + BR.getFOOD_RECORD_ID();
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public boolean delete(long id) {
        String where = FOOD_RECORD_ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime() {

        List<HashMap<String, String>> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            foodRecord_class br = getRecord(cursor);
            newL.put("create_datetime", br.create_datetime);
            newL.put("edit_datetime", br.edit_datetime);
            result.add(newL);
            br = null;
            newL = null;
        }

        cursor.close();
        return result;
    }

    public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList) {
        List<HashMap<String, String>> result = new ArrayList<>();
        for (int x = 0; x < requestedList.size(); x++) {
            foodRecord_class objectBR = getByCreateDatetime(requestedList.get(x));
            result.add(objectBR.toHashMap());
        }

        return result;
    }

    public foodRecord_class getByCreateDatetime(String createDatetime) {
        foodRecord_class BR = null;
        String where = create_datetime + "=" + createDatetime;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public foodRecord(Context context) {
        db = foodRecordDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public foodRecord_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        Integer foodId = null;
        if (!cursor.isNull(6)) {
            foodId = cursor.getInt(6);
        }
        foodRecord_class result = new foodRecord_class(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3),
                cursor.getString(4),
                cursor.getString(5),
                foodId,
                cursor.getInt(7),
                cursor.getDouble(8),
                cursor.getDouble(9),
                cursor.getDouble(10),
                cursor.getDouble(11),
                cursor.getString(12),
                cursor.getString(13),
                cursor.getString(14)

        );

        result.create_datetime = cursor.getString(15);
        result.edit_datetime = cursor.getString(16);

        // 回傳結果
        return result;
    }

    public class foodRecord_class {
        private Integer FOOD_RECORD_ID;
        private Integer USER_ID;
        private String FOOD_DATE;
        private String FOOD_TIME;
        private String FOOD_SESSION;
        private String FOOD_PLACE;
        private Integer FOOD_ID;
        private Integer FOOD_QUANTITY;
        private Double FOOD_CALORIE;
        private Double FOOD_CARBOHYDRATE;
        private Double FOOD_PROTEIN;
        private Double FOOD_FAT;
        private String FOOD_NAME;
        private String FOOD_UNIT;
        private String PHOTO_PATH;

        public String create_datetime;
        public String edit_datetime;

        public foodRecord_class(Integer FOOD_RECORD_ID, Integer USER_ID, String FOOD_DATE, String FOOD_TIME, String FOOD_SESSION, String FOOD_PLACE, Integer FOOD_ID, Integer FOOD_QUANTITY, Double FOOD_CALORIE, Double FOOD_CARBOHYDRATE, Double FOOD_PROTEIN, Double FOOD_FAT, String FOOD_NAME, String FOOD_UNIT, String PHOTO_PATH) {
            this.FOOD_CALORIE = FOOD_CALORIE;
            this.FOOD_CARBOHYDRATE = FOOD_CARBOHYDRATE;
            this.FOOD_DATE = FOOD_DATE;
            this.FOOD_FAT = FOOD_FAT;
            this.FOOD_ID = FOOD_ID;
            this.FOOD_NAME = FOOD_NAME;
            this.FOOD_PLACE = FOOD_PLACE;
            this.FOOD_PROTEIN = FOOD_PROTEIN;
            this.FOOD_QUANTITY = FOOD_QUANTITY;
            this.FOOD_RECORD_ID = FOOD_RECORD_ID;
            this.FOOD_SESSION = FOOD_SESSION;
            this.FOOD_TIME = FOOD_TIME;
            this.USER_ID = USER_ID;
            this.FOOD_UNIT = FOOD_UNIT;
            this.PHOTO_PATH = PHOTO_PATH;
        }

        public Integer getFOOD_RECORD_ID() {
            return FOOD_RECORD_ID;
        }

        public Integer getUSER_ID() {
            return USER_ID;
        }

        public String getFOOD_DATE() {
            return FOOD_DATE;
        }

        public String getFOOD_TIME() {
            if (FOOD_TIME.length() == 5) {
                return FOOD_TIME+":00";
            }
            return FOOD_TIME;
        }

        public String getFOOD_SESSION() {
            return FOOD_SESSION;
        }

        public String getFOOD_PLACE() {
            return FOOD_PLACE;
        }

        public Integer getFOOD_ID() {
            return FOOD_ID;
        }

        public Integer getFOOD_QUANTITY() {
            return FOOD_QUANTITY;
        }

        public Double getFOOD_CALORIE() {
            return FOOD_CALORIE;
        }

        public Double getFOOD_CARBOHYDRATE() {
            return FOOD_CARBOHYDRATE;
        }

        public Double getFOOD_PROTEIN() {
            return FOOD_PROTEIN;
        }

        public Double getFOOD_FAT() {
            return FOOD_FAT;
        }

        public String getFOOD_NAME() {
            return FOOD_NAME;
        }

        public String getFOOD_UNIT() {
            return FOOD_UNIT;
        }

        public String getPHOTO_PATH() {
            return PHOTO_PATH;
        }

        public HashMap<String, String> toHashMap() {

            HashMap<String, String> out = new HashMap<>();

            out.put(foodRecord.FOOD_RECORD_ID, String.valueOf(this.FOOD_RECORD_ID));
            out.put(foodRecord.USER_ID, String.valueOf(this.USER_ID));
            out.put(foodRecord.FOOD_DATE, String.valueOf(this.FOOD_DATE));
            out.put(foodRecord.FOOD_TIME, String.valueOf(this.FOOD_TIME));
            out.put(foodRecord.FOOD_SESSION, String.valueOf(this.FOOD_SESSION));
            out.put(foodRecord.FOOD_PLACE, String.valueOf(this.FOOD_PLACE));
            out.put(foodRecord.FOOD_ID, String.valueOf(this.FOOD_ID));
            out.put(foodRecord.FOOD_QUANTITY, String.valueOf(this.FOOD_QUANTITY));
            out.put(foodRecord.FOOD_CALORIE, String.valueOf(this.FOOD_CALORIE));
            out.put(foodRecord.FOOD_CARBOHYDRATE, String.valueOf(this.FOOD_CARBOHYDRATE));
            out.put(foodRecord.FOOD_PROTEIN, String.valueOf(this.FOOD_PROTEIN));
            out.put(foodRecord.FOOD_FAT, String.valueOf(this.FOOD_FAT));
            out.put(foodRecord.FOOD_NAME, String.valueOf(this.FOOD_NAME));
            out.put(foodRecord.FOOD_UNIT, String.valueOf(this.FOOD_UNIT));
            out.put(foodRecord.PHOTO_PATH, String.valueOf(this.PHOTO_PATH));
            out.put(foodRecord.create_datetime, String.valueOf(this.create_datetime));
            out.put(foodRecord.edit_datetime, String.valueOf(this.edit_datetime));

            return out;

        }

        public Boolean isSpeedRecordMode() {
            if (getFOOD_NAME().equals("其他（快速）") && getFOOD_UNIT().equals("") && (getFOOD_ID().equals(0) || getFOOD_ID().equals(null)) && getFOOD_QUANTITY() == -1) {
                return true;
            } else {
                return false;
            }
        }
    }

}
