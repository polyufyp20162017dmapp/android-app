package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.tool.syncDataVersionDB;
import com.dmc.myapplication.tool.xiaomiDeviceSettingDB;

/**
 * Created by januslin on 24/12/2016.
 */
public class xiaomiDeviceSetting {
    public static final String TABLE_NAME = "XIAOMIDEVICE_SETTING";

    public static final String oKey	 = "oKey";
    public static final String oValue = "oValue";

    public static final String CREATE_TABLE =  "CREATE TABLE " + TABLE_NAME + " (" +
            oKey + " TEXT PRIMARY KEY, " +
            oValue + " INTEGER NULL);";

    private static SQLiteDatabase db;

    public xiaomiDeviceSetting(Context context) {
        db = xiaomiDeviceSettingDB.getDatabase(context);
    }

    public void close() {
        db.close();
    }

    public xiaomiDeviceSetting_class insert(xiaomiDeviceSetting_class BR){
        ContentValues cv = new ContentValues();
        cv.put(oKey, BR.getoKey());
        cv.put(oValue, BR.getoValue());

        long id = db.insert(TABLE_NAME, null, cv);
        return BR;
    }

    public boolean update(xiaomiDeviceSetting_class BR) {
        System.out.println("xiaomiDeviceSetting_class Update "+BR.getoKey()+" "+BR.getoValue());
        ContentValues cv = new ContentValues();
        cv.put(oValue, BR.getoValue());

        String where = oKey + "='" + BR.getoKey() + "'";
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public xiaomiDeviceSetting_class get(String id) {
        xiaomiDeviceSetting_class BR = null;
        String where = oKey + "='" + id + "'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        //result.close();
        return BR;
    }

    public xiaomiDeviceSetting_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        xiaomiDeviceSetting_class result = new xiaomiDeviceSetting_class(
                cursor.getString(0),
                cursor.getString(1)
        );
        System.out.println("getRecord 0="+cursor.getString(0) + " 1="+cursor.getString(1));
        // 回傳結果
        return result;
    }

    public class xiaomiDeviceSetting_class{
        private String oKey;
        private String oValue;

        public xiaomiDeviceSetting_class(String oKey, String oValue){
            this.oKey = oKey;
            this.oValue = oValue;
        }

        public String getoKey() {
            return oKey;
        }

        public void setoKey(String oKey) {
            this.oKey = oKey;
        }

        public String getoValue() {
            return oValue;
        }

        public void setoValue(String oValue) {
            this.oValue = oValue;
        }
    }
}
