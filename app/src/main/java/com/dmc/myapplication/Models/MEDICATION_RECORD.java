package com.dmc.myapplication.Models;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.dmc.myapplication.tool.INJECTION_RECORDDB;
import com.dmc.myapplication.tool.MEDICATION_RECORDDB;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Created by januslin on 11/10/2016.
 */
public class MEDICATION_RECORD {
    public static final String TABLE_NAME = "MEDICATION_RECORD";

    public static final String ID = "_ID";

    // 其它表格欄位名稱
    public static final String USER_ID = "USER_ID";
    public static final String MEDICATION_DATE = "MEDICATION_DATE";
    public static final String HAS_TAKE_MEDICATION = "HAS_TAKE_MEDICATION";
    public static final String create_datetime = "create_datetime";
    public static final String edit_datetime = "edit_datetime";

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + " (" +
                    ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    USER_ID + " INTEGER NOT NULL, " +
                    MEDICATION_DATE + " TEXT NULL, " +
                    HAS_TAKE_MEDICATION + " TEXT NULL, " +
                    create_datetime + " TEXT NOT NULL, " +
                    edit_datetime + " TEXT NOT NULL)";

    private static SQLiteDatabase db;

    // 建構子，一般的應用都不需要修改
    public MEDICATION_RECORD(Context context) {
        db = MEDICATION_RECORDDB.getDatabase(context);
    }

    // 關閉資料庫，一般的應用都不需要修改
    public void close() {
        db.close();
    }


    public static MEDICATION_RECORD_class insert(MEDICATION_RECORD_class BR){

        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(MEDICATION_DATE, BR.getMEDICATION_DATE());
        cv.put(HAS_TAKE_MEDICATION, BR.getHAS_TAKE_MEDICATION());
        if (BR.create_datetime == null || BR.create_datetime == ""){
            cv.put(create_datetime, currentTimestamp);
            cv.put(edit_datetime, currentTimestamp);
        }else{
            cv.put(create_datetime, BR.create_datetime);
            cv.put(edit_datetime, BR.edit_datetime);
        }

        long id = db.insert(TABLE_NAME, null, cv);

        BR.setID((int) id);
        return BR;
    }

    public static boolean update(MEDICATION_RECORD_class BR) {
        SimpleDateFormat s = new SimpleDateFormat("yyyyMMddhhmmssSSS");
        String currentTimestamp = s.format(new Date());

        ContentValues cv = new ContentValues();
        cv.put(USER_ID, BR.getUSER_ID());
        cv.put(MEDICATION_DATE, BR.getMEDICATION_DATE());
        cv.put(HAS_TAKE_MEDICATION, BR.getHAS_TAKE_MEDICATION());
        cv.put(edit_datetime, currentTimestamp);
        String where = ID + "=" + BR.getID();
        return db.update(TABLE_NAME, cv, where, null) > 0;
    }

    public boolean delete(long id){
        String where = ID + "=" + id;
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public boolean deleteByDate(String date){
        String where = MEDICATION_DATE + "='" + dateToDatabaseDate(date) + "'";
        return db.delete(TABLE_NAME, where, null) > 0;
    }

    public MEDICATION_RECORD_class get(long id) {
        MEDICATION_RECORD_class BR = null;
        String where = ID + "=" + id;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public MEDICATION_RECORD_class getByDate(String date) {
        MEDICATION_RECORD_class BR = null;
        String where = MEDICATION_DATE + "='" + dateToDatabaseDate(date)+"'";
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public MEDICATION_RECORD_class getByCreateDatetime(String createDatetime) {
        MEDICATION_RECORD_class BR = null;
        String where = create_datetime + "=" + createDatetime;
        Cursor result = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        if (result.moveToFirst()) {
            BR = getRecord(result);
        }
        result.close();
        return BR;
    }

    public List<HashMap<String, String>> getAllCreatedatetimeEditdatetime(){

        List<HashMap<String, String>> result = new ArrayList<>();
        Cursor cursor = db.query(TABLE_NAME, null, null, null, null, null, null, null);

        while (cursor.moveToNext()) {
            HashMap<String, String> newL = new HashMap<>();
            MEDICATION_RECORD_class br = getRecord(cursor);
            newL.put(create_datetime, br.create_datetime);
            newL.put(edit_datetime, br.edit_datetime);
            result.add(newL);
            br = null;
            newL = null;
        }

        cursor.close();
        return result;
    }

    public List<MEDICATION_RECORD_class> getAllrecordbyrecorddate(String recordDate){

        List<MEDICATION_RECORD_class> result = new ArrayList<>();
        String where = MEDICATION_DATE + "='" + dateToDatabaseDate(recordDate) + "'";

        Cursor cursor = db.query(TABLE_NAME, null, where, null, null, null, null, null);

        while (cursor.moveToNext()) {
            result.add(getRecord(cursor));
        }

        cursor.close();
        return result;
    }

    private String dateToDatabaseDate(String date){
        String[] dateObjs = date.split("-");
        String year = dateObjs[0];
        String Month = dateObjs[1];
        String Day = dateObjs[2];

        if (Month.length() == 1){
            Month = "0"+Month;
        }

        if (Day.length() == 1){
            Day = "0"+Day;
        }

        return year+"-"+Month+"-"+Day;

    }

    public List<HashMap<String, String>> getRecordHashMapByCreateDatetime(List<String> requestedList){
        List<HashMap<String, String>> result = new ArrayList<>();
        for (int x = 0; x < requestedList.size(); x++){
            MEDICATION_RECORD_class objectBR = getByCreateDatetime(requestedList.get(x));
            result.add(objectBR.toHashMap());
        }

        return result;
    }

    public MEDICATION_RECORD_class getRecord(Cursor cursor) {
        // 準備回傳結果用的物件
        MEDICATION_RECORD_class result = new MEDICATION_RECORD_class(
                cursor.getInt(0),
                cursor.getInt(1),
                cursor.getString(2),
                cursor.getString(3)
                );
        result.setCreate_datetime(cursor.getString(4));
        result.setEdit_datetime(cursor.getString(5));

        /*result.setId(cursor.getLong(0));
        result.setDrugName(cursor.getString(1));
        result.setManyTime(cursor.getInt(2));
        result.setTime1(cursor.getString(3));
        result.setTime2(cursor.getString(4));
        result.setTime3(cursor.getString(5));
        result.setTime4(cursor.getString(6));
        result.setBroadcastNumber1(cursor.getString(7));
        result.setBroadcastNumber2(cursor.getString(8));
        result.setBroadcastNumber3(cursor.getString(9));
        result.setBroadcastNumber4(cursor.getString(10));
        result.setRemark(cursor.getString(11));
        result.setPhotoPath(cursor.getString(12));*/

        // 回傳結果
        return result;
    }



    public class MEDICATION_RECORD_class{
        Integer ID;
        Integer USER_ID;
        String MEDICATION_DATE;
        String HAS_TAKE_MEDICATION;
        String create_datetime;
        String edit_datetime;

        public MEDICATION_RECORD_class(Integer ID, Integer USER_ID, String MEDICATION_DATE, String HAS_TAKE_MEDICATION) {
            this.ID = ID;
            this.USER_ID = USER_ID;
            this.MEDICATION_DATE = dateToDatabaseDate(MEDICATION_DATE);
            this.HAS_TAKE_MEDICATION = HAS_TAKE_MEDICATION;
        }

        public Integer getID() {
            return ID;
        }

        public void setID(Integer ID) {
            this.ID = ID;
        }

        public Integer getUSER_ID() {
            return USER_ID;
        }

        public void setUSER_ID(Integer USER_ID) {
            this.USER_ID = USER_ID;
        }

        public String getMEDICATION_DATE() {
            return MEDICATION_DATE;
        }

        public void setMEDICATION_DATE(String MEDICATION_DATE) {
            this.MEDICATION_DATE = dateToDatabaseDate(MEDICATION_DATE);
        }

        public String getHAS_TAKE_MEDICATION() {
            return HAS_TAKE_MEDICATION;
        }

        public void setHAS_TAKE_MEDICATION(String HAS_TAKE_MEDICATION) {
            this.HAS_TAKE_MEDICATION = HAS_TAKE_MEDICATION;
        }

        public String getCreate_datetime() {
            return create_datetime;
        }

        public void setCreate_datetime(String create_datetime) {
            this.create_datetime = create_datetime;
        }

        public String getEdit_datetime() {
            return edit_datetime;
        }

        public void setEdit_datetime(String edit_datetime) {
            this.edit_datetime = edit_datetime;
        }

        public HashMap<String, String> toHashMap(){
            HashMap<String, String> output = new HashMap<>();
            output.put("ID", String.valueOf(ID));
            output.put("USER_ID", String.valueOf(USER_ID));
            output.put("MEDICATION_DATE", String.valueOf(dateToDatabaseDate(MEDICATION_DATE)));
            output.put("HAS_TAKE_MEDICATION", String.valueOf(HAS_TAKE_MEDICATION));
            output.put("create_datetime", String.valueOf(create_datetime));
            output.put("edit_datetime", String.valueOf(edit_datetime));
            return output;
        }
    }


}
