package com.dmc.myapplication;

import android.graphics.Bitmap;
import android.os.Environment;

import java.io.File;

/**
 * Created by KwokSinMan on 8/2/2016.
 */
public class systemConstant {
    public static final String DATABASE_NAME = "dmapp.db";
    public static final int  DATABASE_VERSION = 1;

    //public static final String SERVER_ADDRESS = "http://192.168.43.45/dmapp/";
    //public static final String SERVER_ADDRESS = "http://www2.comp.polyu.edu.hk/~13090903d/dmapp/";
    //public static final String SERVER_ADDRESS = "http://192.168.2.11/dmapp/";
    public static final String SERVER_ADDRESS = "https://158.132.8.53/dma/";
    //public static final String SERVER_ADDRESS = "http://192.168.0.11/dma/";
   // public static final String SERVER_ADDRESS = "https://ser1.januslin.me/dma/";
   // public static final String SERVER_ADDRESS = "http://172.16.1.203/dma/";

    //Development Server
    //public static final String SERVER_ADDRESS = "https://158.132.8.53/dma/";


    public static final int CONNECTION_TIMEOUT= 1000 * 15 ;    //15  seconds

    public static final String FILE_PATH = Environment.getExternalStorageDirectory().toString()+ File.separator + "dmapp";
    public static final String
            FILE_PATH_IMAGE = FILE_PATH + File.separator + "picupload";
    public static final String FILE_PATH_REMINDER_IMAGE = FILE_PATH + File.separator + "reminder";
    public static final String FILE_PATH_EXPORT = FILE_PATH + File.separator + "export";

    public static final String IMAGE_TYPE=".jpg";
    public static final int IMAGE_COMPRESS = 50; // 100 = no compress
    public static Bitmap tempbitmap;


    public static final String REDIRECT_PAGE = "REDIRECT_PAGE";
    public static final int DISPLAY_HOMEPAGE = 0;
    public static final int DISPLAY_NAVFOOD = 1;
    public static final int DISPLAY_GYM = 2;
	public static final int DISPLAY_BLOOD = 3;
    public static final int DISPLAY_BP= 4;
    public static final int DISPLAY_DRUG = 5;
    public static final int DISPLAY_REMINDER = 6;
    public static final int DISPLAY_OTHER_INDEX = 7;
    public static final int DISPLAY_ASSESSMENT = 8;
}
