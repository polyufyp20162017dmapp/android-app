package com.dmc.myapplication.report;

import android.accounts.Account;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.UiThread;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.test.UiThreadTest;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.webHelper;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.ProgressCallback;

import java.io.File;
import java.security.GeneralSecurityException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.util.HashMap;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

public class reportActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_report);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        /*FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        final Activity that = this;
        final Button downloadPDFButton = (Button) findViewById(R.id.download_report_button);
        downloadPDFButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog();
            }

            private void showDialog() {
                Bundle b = new Bundle(  );
                b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

                Account newAccount = new Account(String.valueOf("更新程式"), "com.dmc.myapplication.myacc");
                ContentResolver.requestSync(newAccount,"com.dmc.myapplication.provider",b);

                FragmentTransaction ft = getFragmentManager().beginTransaction();
                ft.addToBackStack(null);
                // Create and show the dialog.
                android.app.DialogFragment newFragment = new downloadPDFDialog();
                newFragment.show(ft, "選擇時間");
            }
        });

        final TextView keyValueLabel = (TextView) findViewById(R.id.shareid);
        final TextView lastAccessTimeLabel = (TextView) findViewById(R.id.lastaccesstimelabel);

        runOnUiThread(new Runnable(){
            @Override
            public void run() {
                final UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());
                final String [] answer = new String[1];
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        answer[0] =  webHelper.sendRequest("POST", "userId=" + userLocalStore.getLoggedInUser().userid + "&event=report&method=getOneTimeKey&data=");
                    }
                });

                thread.start();
                try {
                    thread.join();
                }catch (InterruptedException e){
                    e.printStackTrace();
                }
                if (answer[0] != null && !answer[0].equals("")){
                    Gson gson = new Gson();
                    HashMap<String, String> result = gson.fromJson(answer[0], new TypeToken<HashMap<String, String>>(){}.getType());
                    String shareKey = result.get("key");
                    if (shareKey != null || !shareKey.equals("")){
                        keyValueLabel.setText(shareKey);
                        System.out.println("One-time-key: "+shareKey);
                    }else{
                        keyValueLabel.setText("------------");
                    }


                    String lastAccessTime = result.get("LAST_ACCESS_DATETIME");
                    if (lastAccessTime != null || !lastAccessTime.equals("")){
                        lastAccessTimeLabel.setText(lastAccessTime);
                    }else{
                        lastAccessTimeLabel.setText("沒有。");
                    }

                    downloadPDFButton.setEnabled(true);
                }else{
                    keyValueLabel.setText("------------");
                    downloadPDFButton.setEnabled(false);
                    Snackbar.make(keyValueLabel, "未能連接到互聯網。", Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                handleBackAction();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onUserSelectValue(String fromYear, String fromMonth, String toYear, String toMonth) {
        final Button downloadPDFButton = (Button) findViewById(R.id.download_report_button);
        downloadPDFButton.setText("下載中...");
        downloadPDFButton.setEnabled(false);
        enableViews(downloadPDFButton.getRootView(), false);

        File file = new File(getExternalFilesDir(null), "report.pdf");

        if (file.exists()) {
            file.delete();
        }
        //webHelper.sendRequestAndDownloadFileTo("POST", "", file);
        final Intent intent = new Intent();
        final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar2);

        UserLocalStore userLocalStore = new UserLocalStore(getBaseContext());

        ///SSL

        if (systemConstant.SERVER_ADDRESS.contains("https")) {
            /**
             * Dummy trust that will accept anything.
             */
            TrustManager[] trustAllCerts = new TrustManager[] {
                    new X509TrustManager() {
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                            return new X509Certificate[0];
                        }
                        public void checkClientTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                        public void checkServerTrusted(
                                java.security.cert.X509Certificate[] certs, String authType) {
                        }
                    }
            };

            // Install the all-trusting trust manager

            try {
                SSLContext sc = SSLContext.getInstance("SSL");
                sc.init(null, trustAllCerts, new java.security.SecureRandom());
                Ion.getDefault(getBaseContext()).getHttpClient().getSSLSocketMiddleware().setTrustManagers(trustAllCerts);
                Ion.getDefault(getBaseContext()).getHttpClient().getSSLSocketMiddleware().setHostnameVerifier(new HostnameVerifier(){
                    @Override
                    public boolean verify (String var1, SSLSession var2){
                        Log.i("RestUtilImpl", "Approving certificate for " + var1);
                        return true;
                    }
                });
                Ion.getDefault(getBaseContext()).getHttpClient().getSSLSocketMiddleware().setSSLContext(sc);
            } catch (final NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (final KeyManagementException e) {
                e.printStackTrace();
            }
        }
            Ion.with(getBaseContext())
                .load(systemConstant.SERVER_ADDRESS)
                .progressBar(progressBar)
                .progress(new ProgressCallback() {@Override
                public void onProgress(final long downloaded,final long total) {
                    System.out.println("" + downloaded + " / " + total);
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            downloadPDFButton.setText("下載中... ("+String.valueOf(downloaded) +"/ "+String.valueOf(total)+")");
                        }
                    });
                }
                })
                .setBodyParameter("userId", String.valueOf(userLocalStore.getLoggedInUser().userid))
                .setBodyParameter("event", "report")
                .setBodyParameter("method", "fetchReport")
                .setBodyParameter("data", "[{\"from_year\":"+fromYear+",\"to_year\":"+toYear+",\"from_month\":"+fromMonth+",\"to_month\":"+toMonth+"  }]")
                .write(file)
                .setCallback(new FutureCallback<File>() {
                    @Override
                    public void onCompleted(Exception e, File file) {
                        // download done...

                        downloadPDFButton.setText("查看報告");
                        downloadPDFButton.setEnabled(true);
                        enableViews(downloadPDFButton.getRootView(), true);

                        if (e != null){
                            e.printStackTrace();
                        }else{
                            intent.setClass(getBaseContext(),pdfViwerActivity.class);
                            startActivity(intent);
                        }
                    }
                });
    }

    private void enableViews(View v, boolean enabled) {
        if (v instanceof ViewGroup) {
            ViewGroup vg = (ViewGroup) v;
            for (int i = 0;i<vg.getChildCount();i++) {
                enableViews(vg.getChildAt(i), enabled);
            }
        }
        v.setEnabled(enabled);
    }

    private void handleBackAction(){
        Intent intent = new Intent();
        intent.setClass(this, MainActivity.class);
        intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_HOMEPAGE);
        startActivity(intent);
        overridePendingTransition(R.anim.slide_in_left,R.anim.slide_out_right);
        finish();
    }

}
