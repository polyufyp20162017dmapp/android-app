package com.dmc.myapplication.drug;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SimpleAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.tool.dateTool;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

/**
 * Created by KwokSinMan on 4/2/2016.
 */
public class drugFragment extends Fragment {

    private Fragment myFragment = this;

    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v = inflater.inflate(R.layout.drug_main, container, false);
        String getDrugDate = this.getArguments().getString(drugConstant.GET_DRUG_DATE);

        // set gym Date
        TextView drugViewDate = (TextView) v.findViewById(R.id.drugViewDate);
        drugViewDate.setText(drugDateTool.getUIDateFormal(getDrugDate));

        // set data picker
        ImageView setDrugDate = (ImageView) v.findViewById(R.id.setDrugViewDate);
        setDrugDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                drugDatePickerFragment newFragment = new drugDatePickerFragment();
                newFragment.myFragment = myFragment;
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        // clear all record
        LinearLayout drugRecord = (LinearLayout) v.findViewById(R.id.drugDetail);
        drugRecord.setVisibility(View.GONE);
        RadioButton radioButtonY = (RadioButton) v.findViewById(R.id.ansY);
        radioButtonY.setVisibility(View.GONE);
        RadioButton radioButtonN = (RadioButton) v.findViewById(R.id.ansN);
        radioButtonN.setVisibility(View.GONE);
        //LinearLayout drugAddArea = (LinearLayout) v.findViewById(R.id.drugAddArea);
        //drugAddArea.setVisibility(View.GONE);
        String isInjectionSharePre = new UserLocalStore(getActivity()).getLoggedInUser().injection;
        if(drugBiz.hasInjection(isInjectionSharePre)){
            LinearLayout injectionSession = (LinearLayout) v.findViewById(R.id.injectionSession);
            injectionSession.setVisibility(View.VISIBLE);
            setHasOptionsMenu(true);
        }else{
            LinearLayout injectionSession = (LinearLayout) v.findViewById(R.id.injectionSession);
            injectionSession.setVisibility(View.GONE);
            setHasOptionsMenu(false);
        }

        LinearLayout noDrugRecord = (LinearLayout) v.findViewById(R.id.noDrugRecord);
        noDrugRecord.setVisibility(View.GONE);
        LinearLayout noInjectionRecord = (LinearLayout) v.findViewById(R.id.noInjectionRecord);
        noInjectionRecord.setVisibility(View.GONE);


        // set onClick Color
        drugRecord.setBackgroundResource(R.drawable.border_drug_ui);
        //drugRecord.setContentDescription("Candy gaga");

        // set drugDetail onClickListener
        drugRecord.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                // LinearLayout drugRecord = (LinearLayout) getActivity().findViewById(R.id.drugDetail);
                // System.out.println(drugRecord.getContentDescription());
                Intent intent = new Intent();
                intent.setClass(getActivity(), drugEditActivity.class);
                intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.drugViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        });


        // set  gymList onItemClickListener
        ListView injectionList = (ListView) v.findViewById(R.id.injectionList);
        injectionList.setOnItemClickListener(new injectionListOnItemClickListener(this.getActivity()));

        UserLocalStore userLocalStore = new UserLocalStore(this.getActivity());
        String userId = Integer.toString(userLocalStore.getLoggedInUser().userid);
        drugAsyncTask connect = new drugAsyncTask(this.getActivity(),myFragment, drugConstant.GET_DRUG_RECORD);
        connect.execute(userId,getDrugDate);

        return v;
    }



    @Override
    public void onCreateOptionsMenu(Menu menu,MenuInflater inflater){
        inflater.inflate(R.menu.drug_menu, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        String isInjectionSharePre = new UserLocalStore(getActivity()).getLoggedInUser().injection;
        if (item.getItemId() == R.id.drug_menuItem_add) {
            if(drugBiz.hasInjection(isInjectionSharePre)){
                LinearLayout noDrugRecord = (LinearLayout) getActivity().findViewById(R.id.noDrugRecord);
                drugBiz drugBiz = new drugBiz();
                if(noDrugRecord.getContentDescription().equals(drugConstant.NO_RECORD)) {
                    drugBiz.drugAndInjection(getActivity());
                }else{
                    drugBiz.onlyInjection(getActivity());
                }
            } else {
                Intent intent = new Intent();
                intent.setClass(getActivity(), drugAddActivity.class);
                intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.drugViewDate)).getText()));
                getActivity().startActivity(intent);
                getActivity().finish();
            }
        }


        return true;
    }

    private class injectionListOnItemClickListener implements  ListView.OnItemClickListener {
        Activity activity;
        public injectionListOnItemClickListener(Activity activity){ this.activity=activity; }

        @Override
        public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
            ListView injectionList = (ListView) activity.findViewById(R.id.injectionList);
            String injectionRecordId=((HashMap<String, String>)(injectionList.getItemAtPosition(position))).get("id");
            String injectionName=((HashMap<String, String>)(injectionList.getItemAtPosition(position))).get("Name");
            String injectionTime=((HashMap<String, String>)(injectionList.getItemAtPosition(position))).get("Time");
            String injectionValue=((HashMap<String, String>)(injectionList.getItemAtPosition(position))).get("Value");

            Intent intent = new Intent();
            intent.setClass(activity, injectionEditActivity.class);
            intent.putExtra("injection_id", injectionRecordId);
            intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.drugViewDate)).getText()));
            intent.putExtra("injection_time",injectionTime);
            intent.putExtra("injection_name", injectionName);
            intent.putExtra("injection_value", injectionValue);

            activity.startActivity(intent);
            activity.finish();
        }
    }

}

// set disable radio group
//RadioGroup questionRadioGroup = (RadioGroup) v.findViewById(R.id.question);
//for (int i = 0; i < questionRadioGroup.getChildCount(); i++) {
//    questionRadioGroup.getChildAt(i).setEnabled(false);
//}


        /*
        //set Add record event
        Button navfoodAdd = (Button) v.findViewById(R.id.drugAdd);
        navfoodAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                Intent intent = new Intent();
                intent.setClass(getActivity(), drugAddActivity.class);
                intent.putExtra(drugConstant.GET_DRUG_DATE, drugDateTool.getDBDateFormal((String) ((TextView) getActivity().findViewById(R.id.drugViewDate)).getText()));
                startActivity(intent);
            }
        });
        */