package com.dmc.myapplication.drug;

/**
 * Created by KwokSinMan on 7/2/2016.
 */
public class drugConstant {
    public final static String GET_DRUG_RECORD = "drug/getDrugRecord.php";
    public final static String CHANGE_DRUG_RECORD = "drug/changeDrugRecord.php";
    public final static String DELETE_DRUG_RECORD = "drug/deleteDrugRecord.php";

    public final static String ADD_INJECTION_RECORD = "drug/addInjectionRecord.php";
    public final static String CHANGE_INJECTION_RECORD = "drug/changeInjectionRecord.php";
    public final static String DELETE_INJECTION_RECORD = "drug/deleteInjectionRecord.php";

    public final static String ANS_YES = "Y";
    public final static String ANS_NO = "N";
    public final static String NO_RECORD = "NA";


    public final static String GET_DRUG_DATE = "redirect_drug_date";
}
