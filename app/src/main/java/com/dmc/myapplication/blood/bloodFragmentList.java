package com.dmc.myapplication.blood;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.github.mikephil.charting.data.Entry;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Po on 11/2/2016.
 */
public class bloodFragmentList extends Fragment {

    Context ctx;
    UserLocalStore userLocalStore;
    User user;


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.blood_list_view, container, false);
        ctx = getContext();
        userLocalStore = new UserLocalStore(getActivity());
        user = userLocalStore.getLoggedInUser();
        bodyAsyncTask getRecord = new bodyAsyncTask(this.getActivity(),bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_LIST, ctx);
        String user_id = Integer.toString(user.userid);
        String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
        String show_type = bodyRecordConstant.GET_ALL;
        getRecord.execute(user_id, record_type, show_type);


        return v;
    }


}
