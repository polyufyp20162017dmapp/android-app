package com.dmc.myapplication.blood;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.IntegerRes;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;

import com.dmc.myapplication.R;
import com.dmc.myapplication.systemConstant;


/**
 * Created by Po on 24/2/2016.
 */
public class bloodMain extends Fragment {

    //private Toolbar toolbar;
    private TabLayout tabLayout;
    private ViewPager viewPager;
    Context ctx;

    private static final String KEY_ADAPTER_STATE = "com.github.curioustechizen.fragmentstate.KEY_ADAPTER_STATE";


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        Intent intentBundle = getActivity().getIntent();
        Integer displayId = intentBundle.getIntExtra(systemConstant.REDIRECT_PAGE, -1);
        System.out.println("dd="+displayId);


        View v =  inflater.inflate(R.layout.blood_slide, container, false);
        ctx = getActivity().getApplicationContext();

        setHasOptionsMenu(true);

        //set Table View
        viewPager = (ViewPager) v.findViewById(R.id.viewpager);
        tabLayout = (TabLayout) v.findViewById(R.id.sliding_tabs);

        setupViewPager(viewPager);
        tabLayout.setupWithViewPager(viewPager);
        viewPager.setOffscreenPageLimit(10);

        return v;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        viewPager.removeView((View) getView());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        MenuInflater inflater = this.getActivity().getMenuInflater();
        inflater.inflate(R.menu.blood_menu, menu);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();
        setHasOptionsMenu(true);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add:
                Intent intent = new Intent();
                intent.setClass(getActivity(), blood_add.class);
                getActivity().startActivity(intent);
                //getActivity().finish();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void setupViewPager(ViewPager viewPager) {
        BloodPagerAdapter adapter = new BloodPagerAdapter(getChildFragmentManager(),ctx);
        adapter.addFragment(new bloodFragment(), "圖表");
        adapter.addFragment(new bloodFragmentList(), "詳細資料");
        viewPager.setAdapter(adapter);
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        System.out.println("Saaaa");
        savedInstanceState.putInt(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
    }




}
