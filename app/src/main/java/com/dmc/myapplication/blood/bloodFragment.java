package com.dmc.myapplication.blood;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupMenu;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.bodyRecord.injectionAsyncTask;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;


public class bloodFragment extends Fragment {

    UserLocalStore userLocalStore;
    User user;
    Context ctx;
    //Button btnShowType;

    public bloodFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        View v =  inflater.inflate(R.layout.blood_main, container, false);
        ctx = getContext();
        if(v.getParent() != null)
            ((ViewGroup)v.getParent()).removeView(v);


        userLocalStore = new UserLocalStore(getActivity());
        user = userLocalStore.getLoggedInUser();
        String user_id = Integer.toString(user.userid);

        if(user.injection.equals("Y")){
            injectionAsyncTask getInjectRecord = new injectionAsyncTask(this.getActivity(), bodyRecordConstant.GET_INJECTION_RECORD);
            getInjectRecord.execute(user_id);
        }

        bodyAsyncTask getRecord = new bodyAsyncTask(this.getActivity(), bodyRecordConstant.GET_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
        String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
        final String show_type = bodyRecordConstant.GET_ALL;

        getRecord.execute(user_id, record_type, show_type);



        return v;
    }



}
