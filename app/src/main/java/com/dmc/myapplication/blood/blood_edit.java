package com.dmc.myapplication.blood;

import android.app.DialogFragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;

import com.dmc.myapplication.MainActivity;
import com.dmc.myapplication.Models.bodyRecord;
import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyAsyncTask;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.User;
import com.dmc.myapplication.login.UserLocalStore;
import com.dmc.myapplication.systemConstant;
import com.dmc.myapplication.tool.timeCheck;

import java.util.List;

/**
 * Created by Po on 28/2/2016.
 */
public class blood_edit extends AppCompatActivity implements AdapterView.OnItemSelectedListener {

    private Toolbar toolbar;
    private Context ctx;
    private EditText editTextDate, editTextTime, editTextBloodValue ;
    private Spinner spinnerPeriod, spinnerType;
    private int selectedPeriod;
    private int selectedType;
    private Button buttonBloodAdd;
    private UserLocalStore userLocalStore;
    private User user;
    private String recordId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        overridePendingTransition(R.anim.slide1, R.anim.slide2);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blood_fragment_edit);
        ctx = getApplicationContext();
        userLocalStore = new UserLocalStore(this);
        user = userLocalStore.getLoggedInUser();


        Intent intent =getIntent();
        recordId = intent.getStringExtra("recordId");
        final String getDate = intent.getStringExtra("date");
        final String getTime = intent.getStringExtra("time");
        final int getPeriodGlu = Integer.parseInt(intent.getStringExtra("typePeriod"));
        final int getTypeGlu = Integer.parseInt(intent.getStringExtra("typeGlu"));
        final String getGluValue = intent.getStringExtra("gluValue");

        BodyRecord record = new BodyRecord();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // SET return <- into left hand side
        if (getSupportActionBar() != null){
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
        }
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //handle the date picker
        editTextDate = (EditText) findViewById(R.id.blood_add_date);
        editTextDate.setText(getDate);

        ImageView setBloodDate = (ImageView) findViewById(R.id.setBlood_add_date);
        setBloodDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment = new bloodDatePickerFragment();
                newFragment.show(getFragmentManager(), "DatePicker");
            }
        });

        //handle the time picker
        editTextTime = (EditText) findViewById(R.id.blood_add_time);
        editTextTime.setText(getTime);
        ImageView setTime = (ImageView) findViewById(R.id.setBlood_add_time);
        setTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                DialogFragment newFragment3  = new bloodTimePickerFragment();
                newFragment3.show(getFragmentManager(), "Time Dialog");
                selectPeriod();
            }
        });
        editTextTime.addTextChangedListener(new TextWatcher(){
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
            public void afterTextChanged(Editable s) {
                selectPeriod();
            }});

        //handle the period spinner drop down menu
        spinnerPeriod = (Spinner) findViewById(R.id.spinnerPeriod);

        // Spinner Drop down elements
        List<BodyRecord.periodType> period = record.getPeriodTypeList() ;
        ArrayAdapter<BodyRecord.periodType> adp= new ArrayAdapter<BodyRecord.periodType>(this,
                android.R.layout.simple_list_item_1,period);
        adp.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerPeriod.setAdapter(adp);
        spinnerPeriod.setSelection(getPeriodGlu - 1);
        selectedPeriod = getPeriodGlu;
        // Spinner click listener
        spinnerPeriod.setOnItemSelectedListener(this);


        spinnerType = (Spinner) findViewById(R.id.spinnerType);
        // Spinner Drop down elements
        List<BodyRecord.gluType> gluType = record.getGluTypeList() ;
        ArrayAdapter<BodyRecord.gluType> adp1= new ArrayAdapter<BodyRecord.gluType>(this,
                android.R.layout.simple_list_item_1,gluType);
        adp1.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinnerType.setAdapter(adp1);
        spinnerType.setSelection(getTypeGlu - 1);
        selectedType = getTypeGlu;
        spinnerType.setOnItemSelectedListener(this);


        //handle the blood value field
        editTextBloodValue = (EditText) findViewById(R.id.blood_add_value);
        editTextBloodValue.setText(getGluValue);

        buttonBloodAdd = (Button) findViewById(R.id.blood_add_button);
        buttonBloodAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                validGlucose();
            }
        });



    }

    private void validGlucose() {

        // Reset errors.
        editTextBloodValue.setError(null);

        // Store values at the time of the login attempt.
        String glucose = editTextBloodValue.getText().toString();

        boolean cancel = false;
        View focusView = null;

        // Check for a valid password, if the user entered one.
        if (TextUtils.isEmpty(glucose) || !isValid(glucose)) {
            editTextBloodValue.setError(getString(R.string.blood_error));
            focusView =  editTextBloodValue;
            cancel = true;
        }

        if (!TextUtils.isEmpty(glucose)){
            if (Double.parseDouble(glucose)>bodyRecordConstant.GET_GLUCOSE_UPPER_LIMIT){
                editTextBloodValue.setError(getString(R.string.blood_warning));
                focusView =  editTextBloodValue;
                cancel = true;
            }
        }


        if (cancel) {
            focusView.requestFocus();
        } else {

            //bodyAsyncTask editRecord = new bodyAsyncTask(this, bodyRecordConstant.EDIT_BODY_RECORD,bodyRecordConstant.GET_GLUCOSE_TYPE_CODE, bodyRecordConstant.VIEW_MODE_GRAPH, ctx);
            //String user_id = Integer.toString(user.userid);
            //String record_type = bodyRecordConstant.GET_GLUCOSE_TYPE_CODE;
            //String date = editTextDate.getText().toString();
            //String time = editTextTime.getText().toString();
            //String period = Integer.toString(selectedPeriod);
            //String typeGlucose = Integer.toString(selectedType);

            //editRecord.execute(user_id, record_type, date, time, period, typeGlucose, glucose, recordId);

            bodyRecord bodyRecord = new bodyRecord(getApplicationContext());
            BodyRecord a = new BodyRecord();
            bodyRecord.update(a.new bodyRecord(Integer.parseInt(recordId), Integer.parseInt(bodyRecordConstant.GET_GLUCOSE_TYPE_CODE), (user.userid), editTextDate.getText().toString(), editTextTime.getText().toString(), 0, 0, 0, 0, 0, 0,0 , 0, (selectedPeriod), selectedType, Double.parseDouble(glucose), 0, 0, 0, 0, ""));
            Intent intent = new Intent();
            intent.setClass(this, MainActivity.class);
            intent.putExtra(systemConstant.REDIRECT_PAGE, systemConstant.DISPLAY_BLOOD);
            this.startActivity(intent);
            this.finish();

        }
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Spinner spinner = (Spinner) parent;
        switch (spinner.getId()){
            case R.id.spinnerPeriod:
                spinnerPeriod.setSelection(position);
                selectedPeriod = position + 1;
                break;
            case R.id.spinnerType:
                spinnerType.setSelection(position);
                selectedType = position + 1;
                break;
        }

    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
        //for set selected item of spinner programmatically



    }
    private void selectPeriod(){

        //get set time
        //String time = editTextTime.getText().toString();
        String time = editTextTime.getText().toString();

        //get user pref time and compare to current
        if (timeCheck.compareTime(user.morning_start, user.morning_end, time)){
            spinnerPeriod.setSelection(0);
            selectedPeriod = 1;
            if (timeCheck.compareTimePlus2(user.morning_start, user.morning_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            } else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.breakfast_start, user.breakfast_end, time)){
            spinnerPeriod.setSelection(0);
            selectedPeriod = 1;
            if (timeCheck.compareTimePlus2(user.breakfast_start, user.breakfast_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.lunch_start, user.lunch_end, time)){
            spinnerPeriod.setSelection(1);
            selectedPeriod = 2;
            if (timeCheck.compareTimePlus2(user.lunch_start, user.lunch_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.dinner_start, user.dinner_end, time)){
            spinnerPeriod.setSelection(2);
            selectedPeriod = 3;
            if (timeCheck.compareTimePlus2(user.dinner_start, user.dinner_end, time)){
                spinnerType.setSelection(0);
                selectedType = 1;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else if (timeCheck.compareTime(user.bed_start, user.bed_end, time)){
            spinnerPeriod.setSelection(2);
            selectedPeriod = 3;
            if (timeCheck.compareTimePlus2(user.bed_start, user.bed_end, time)){
                spinnerType.setSelection(1);
                selectedType = 2;
            }else {
                spinnerType.setSelection(0);
                selectedType = 1;
            }
        }else {
            spinnerPeriod.setSelection(0);
            selectedPeriod = 1;
            spinnerType.setSelection(0);
            selectedType = 1;
        }

    }



    private boolean isValid(String glucose){

        Double glucoseValue = Double.parseDouble(glucose);

        if ( glucoseValue <= bodyRecordConstant.GET_GLUCOSE_LOWER_LIMIT){
            return false;
        }
        try{

            if(glucose.contains(".")){
                int integerPlaces = glucose.indexOf('.');
                int decimalPlaces = glucose.length() - integerPlaces - 1;
                //System.out.println("Blood add decimalPlaces "+decimalPlaces);
                if (decimalPlaces > 1){
                    return false;
                }
            }else{
                return true;
            }

        } catch (Exception e){
            e.printStackTrace();
            return false;
        }
        return true;
    }




}
