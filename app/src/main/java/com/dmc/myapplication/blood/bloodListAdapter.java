package com.dmc.myapplication.blood;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.dmc.myapplication.R;
import com.dmc.myapplication.bodyRecord.BodyRecord;
import com.dmc.myapplication.bodyRecord.bodyRecordConstant;
import com.dmc.myapplication.login.UserLocalStore;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;


/**
 * Created by Po on 14/2/2016.
 */
public class bloodListAdapter extends BaseAdapter {
    private final Context context;
    private List <BodyRecord.bodyRecord> mListData =   new ArrayList<BodyRecord.bodyRecord> ();

    private LayoutInflater layoutInflater;


    public bloodListAdapter(Context aContext,  List<BodyRecord.bodyRecord> listData) {
        context = aContext;
        mListData = listData;
        layoutInflater = LayoutInflater.from(aContext);
    }

    @Override
    public int getCount() {
        return mListData.size();
    }

    @Override
    public Object getItem(int position) {
        return mListData.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        BodyRecord.bodyRecord record = (BodyRecord.bodyRecord) getItem(position);

        if (convertView == null) {
            convertView = layoutInflater.inflate(R.layout.blood_list_row_layout, null);
            holder = new ViewHolder();
            holder.valueView = (TextView) convertView.findViewById(R.id.value);
            holder.dateView = (TextView) convertView.findViewById(R.id.date);
            holder.timeView = (TextView) convertView.findViewById(R.id.time);
            holder.typeView = (TextView) convertView.findViewById(R.id.glucoseType);
            holder.recordIdView = (TextView) convertView.findViewById(R.id.glurecordId);
            holder.recordPeriodType = (TextView) convertView.findViewById(R.id.gluPeriodType);
            holder.recordGluType = (TextView) convertView.findViewById(R.id.gluType);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if (record.getRecordType()==1) {
            UserLocalStore userLocalStore = new UserLocalStore(context);
            holder.valueView.setText(String.valueOf(record.getglucose()));
            if (record.getType_glucose()==1) {
                //handle before meal glucose record
                if (record.getglucose() < userLocalStore.getLoggedInUser().pre_meal_low ){
                    holder.valueView.setTextColor(Color.parseColor("#673ab7"));} //LOW
                else if ( record.getglucose() >= userLocalStore.getLoggedInUser().pre_meal_high){
                    holder.valueView.setTextColor(Color.parseColor("#FF5722"));}//HIGH
                else if ( record.getglucose() >= userLocalStore.getLoggedInUser().pre_meal_low && record.getglucose() < userLocalStore.getLoggedInUser().pre_meal_med){
                    holder.valueView.setTextColor(Color.parseColor("#4caf50")); //MID
                } else {
                    holder.valueView.setTextColor(Color.parseColor("#F7C600")); //MIDHIGH
                }

                holder.recordIdView.setText(Integer.toString(record.getRecordId()));
                holder.dateView.setText(record.getDate());
                holder.timeView.setText(record.getTime());
                holder.recordPeriodType.setText(Integer.toString(record.getPeriod()));
                holder.recordGluType.setText(Integer.toString(record.getType_glucose()));
                switch (record.getPeriod()){
                    case 1:holder.typeView.setText("早餐（餐前）");
                        break;
                    case 2:holder.typeView.setText("午餐（餐前）");
                        break;
                    case 3:holder.typeView.setText("晚餐（餐前）");
                        break;
                }

            }else {
                //handle after meal  glucose
                holder.valueView.setText(Double.toString(record.getglucose()));
                if (record.getglucose() < userLocalStore.getLoggedInUser().post_meal_low ){
                    holder.valueView.setTextColor(Color.parseColor("#673ab7"));} //LOW
                else if ( record.getglucose() >= userLocalStore.getLoggedInUser().post_meal_high){
                    holder.valueView.setTextColor(Color.parseColor("#FF5722")); //HIGH
                }else if (record.getglucose() >= userLocalStore.getLoggedInUser().post_meal_low && record.getglucose() < userLocalStore.getLoggedInUser().post_meal_med){
                    holder.valueView.setTextColor(Color.parseColor("#4caf50")); //MID
                } else {
                    holder.valueView.setTextColor(Color.parseColor("#F7C600")); //MIDHIGH
                }
                holder.recordIdView.setText(Integer.toString(record.getRecordId()));
                holder.dateView.setText(record.getDate());
                holder.timeView.setText(record.getTime());
                holder.recordPeriodType.setText(Integer.toString(record.getPeriod()));
                holder.recordGluType.setText(Integer.toString(record.getType_glucose()));
                switch (record.getPeriod()){
                    case 1:holder.typeView.setText("早餐（餐後）");
                        break;
                    case 2:holder.typeView.setText("午餐（餐後）");
                        break;
                    case 3:holder.typeView.setText("晚餐（餐後）");
                        break;
                }
            }

        }
        return convertView;
    }

    static class ViewHolder {
        TextView valueView;
        TextView dateView;
        TextView timeView;
        TextView typeView;
        TextView recordIdView;
        TextView recordPeriodType;
        TextView recordGluType;
    }

}
